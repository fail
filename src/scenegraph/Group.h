/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_SCENEGRAPH_GROUP_H_
#define FAIL_SCENEGRAPH_GROUP_H_

#include "core/core.h"
#include "scenegraph/scenegraph_export.h"
#include "Renderable.h"
#include "Frame.h"
#include <algorithm>

namespace fail { namespace scenegraph
{
	class FLSCENEGRAPH_EXPORT Group : public Renderable
	{
		public:
			Group();
			Group( const shared_ptr< Frame >& pFrame );
			Group( const Serialization_tag& );
			
 			virtual void evaluate( const shared_ptr< RenderPass >& pPass );
			
			void add( const shared_ptr< Renderable >& pChild )
			{
				m_Children.push_back( pChild );
			}

			void remove( const shared_ptr< Renderable >& pChildToRemove )
			{
				std::remove( m_Children.begin(), m_Children.end(), pChildToRemove );
			}

			const shared_ptr< Frame >& getpFrame() const { return m_pFrame; }
			void setpFrame( const shared_ptr< Frame >& x ) { m_pFrame = x; }
			
		private:
			template< class C, typename T > friend struct fail::attribute_traits;
			std::list< shared_ptr< Renderable > > m_Children;
			shared_ptr< Frame >	m_pFrame;
	};
}}

#endif
