/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_SCENEGRAPH_MATERIAL_H_
#define FAIL_SCENEGRAPH_MATERIAL_H_

#include "core/core.h"
#include "scenegraph/scenegraph_export.h"
#include "Vector4fInput.h"
#include <GL/gl.h>

namespace fail { namespace scenegraph
{
	class FLSCENEGRAPH_EXPORT Material : public Serializable
	{
		public:
			enum Channel
			{
				c_None = -1,
				c_Emission = GL_EMISSION,
				c_Ambient = GL_AMBIENT,
				c_Diffuse = GL_DIFFUSE,
				c_Specular = GL_SPECULAR,
				c_AmbientAndDiffuse = GL_AMBIENT_AND_DIFFUSE
			};

			enum StencilCondition
			{
				sc_Never = GL_NEVER,
				sc_Less = GL_LESS,
				sc_LessOrEqual = GL_LEQUAL,
				sc_Greater = GL_GREATER,
				sc_GeaterOrEqual = GL_GEQUAL,
				sc_Equal = GL_EQUAL,
				sc_NotEqual = GL_NOTEQUAL,
				sc_Always = GL_ALWAYS
			};
	
			enum StencilOperation
			{
				so_Keep = GL_KEEP,
				so_Zero = GL_ZERO,
				so_Replace = GL_REPLACE,
				so_Increment = GL_INCR,
				so_IncrementWrap = GL_INCR_WRAP,
				so_Decrement = GL_DECR,
				so_DecrementWrap = GL_DECR_WRAP,
				so_Complement = GL_INVERT
			};
			
			Material();
			Material( const Serialization_tag& );
			
			bool operator==( const Material& b ) const;
			bool operator<( const Material& b ) const;
			
			const Vector4fInput& getAmbient() const { return m_Ambient; }
			Vector4fInput& getAmbient() { return m_Ambient; }
			void setAmbient( const Vector4fInput& x ) { m_Ambient = x; }
			
			const Vector4fInput& getDiffuse() const { return m_Diffuse; }
			Vector4fInput& getDiffuse() { return m_Diffuse; }
			void setDiffuse( const Vector4fInput& x ) { m_Diffuse = x; }

			const Vector4fInput& getSpecular() const { return m_Specular; }
			Vector4fInput& getSpecular() { return m_Specular; }
			void setSpecular( const Vector4fInput& x ) { m_Specular = x; }

			const Vector4fInput& getEmission() const { return m_Emission; }
			Vector4fInput& getEmission() { return m_Emission; }
			void setEmission( const Vector4fInput& x ) { m_Emission = x; }

			const float& getShininess() const { return m_Shininess; }
			void setShininess( const float& x ) { m_Shininess = x; }
			
			const Channel& getVertexColorChannel() const { return m_VertexColorChannel; }
			void setVertexColorChannel( const Channel& x ) { m_VertexColorChannel = x; }
			
			const bool& getbLighting() const { return m_bLighting; }
			void setbLighting( const bool& x ) { m_bLighting = x; }

			const bool& getbDepthTest() const { return m_bDepthTest; }
			void setbDepthTest( const bool& x ) { m_bDepthTest = x; }
			
			const bool& getbSmoothShading() const { return m_bSmoothShading; }
			void setbSmoothShading( const bool& x ) { m_bSmoothShading = x; }
			
			const bool& getbBackfaceCulling() const { return m_bBackfaceCulling; }
			void setbBackfaceCulling( const bool& x ) { m_bBackfaceCulling = x; }			
			
			const bool& getbStencil() const { return m_bStencil; }
			void setbStencil( const bool& x ) { m_bStencil = x; }

			const uint32_t& getStencilValue() const { return m_StencilValue; }
			void setStencilValue( const uint32_t& x ) { m_StencilValue = x; }

			const StencilCondition& getStencilCondition() const { return m_StencilCondition; }
			void setStencilCondition( const StencilCondition& x ) { m_StencilCondition = x; }

			const StencilOperation& getStencilOpFail() const { return m_StencilOpFail; }
			void setStencilOpFail( const StencilOperation& x ) { m_StencilOpFail = x; }
			
			const StencilOperation& getStencilOpDTFail() const { return m_StencilOpDTFail; }
			void setStencilOpDTFail( const StencilOperation& x ) { m_StencilOpDTFail = x; }

			const StencilOperation& getStencilOpPass() const { return m_StencilOpPass; }
			void setStencilOpPass( const StencilOperation& x ) { m_StencilOpPass = x; }

			void renderSetup() const;
			
		private:
			template< class C, typename T > friend struct fail::attribute_traits;
			
			Channel			m_VertexColorChannel;

			Vector4fInput	m_Ambient;
			Vector4fInput	m_Diffuse;
			Vector4fInput	m_Specular;
			Vector4fInput	m_Emission;
			float m_Shininess;
			
			bool m_bLighting;
			bool m_bDepthTest;
			bool m_bSmoothShading;
			bool m_bBackfaceCulling;

			bool				m_bStencil;
			uint32_t			m_StencilValue;
			StencilCondition	m_StencilCondition;
			StencilOperation	m_StencilOpFail;
			StencilOperation	m_StencilOpDTFail;
			StencilOperation	m_StencilOpPass;
	};
}}

#endif
