/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_SCENEGRAPH_VERTEXBUFFER_H_
#define FAIL_SCENEGRAPH_VERTEXBUFFER_H_

#include "core/core.h"
#include "scenegraph/scenegraph_export.h"
#include "math/Vector2f.h"
#include "math/Vector3f.h"
#include "math/Vector4f.h"
#include "math/Vector4u8.h"
#include "VertexFormat.h"
#include "IndexBuffer.h"
#include <vector>

namespace fail { namespace scenegraph
{
	class FLSCENEGRAPH_EXPORT VertexBuffer : public Serializable, public enable_shared_from_this< VertexBuffer >
	{
		public:
			VertexBuffer( shared_ptr< VertexFormat > pFormat, uint32_t Size );
			VertexBuffer( const Serialization_tag& );
			
			// Vertex buffer binding and state management
			void bind() const;
			
			void renderSetup() const
			{
				if( ms_pBound.lock().get() == this )
					return;
				
				bind();
			}
			
			static void ClearBound()
			{
				ms_pBound.reset();
			}

			// Vertex layout setup
	/*		void addAttribute( e_AttrType Type, e_Attr Attr );
			
			// Buffer allocation
			void resize( uint32_t Size )
			{
				m_Buffer.resize( Size );
			}*/
			
			// Buffer access.
			void setFloatAttribute( uint32_t VertexIndex, uint8_t AttributeIndex, float Value )
			{
				// TODO: throw if the type doesn't match
				m_Buffer.put( VertexIndex, getColumnIndex( AttributeIndex ), Value );
			}
			float getFloatAttribute( uint32_t VertexIndex, uint8_t AttributeIndex ) const
			{
				return m_Buffer.get< float >( VertexIndex, getColumnIndex( AttributeIndex ) );
			}

			void setDoubleAttribute( uint32_t VertexIndex, uint8_t AttributeIndex, double Value )
			{
				// TODO: throw if the type doesn't match
				m_Buffer.put( VertexIndex, getColumnIndex( AttributeIndex ), Value );
			}
			float getDoubleAttribute( uint32_t VertexIndex, uint8_t AttributeIndex ) const
			{
				return m_Buffer.get< double >( VertexIndex, getColumnIndex( AttributeIndex ) );
			}
			
			void setVector2fAttribute( uint32_t VertexIndex, uint8_t AttributeIndex, const math::Vector2f& Value )
			{
				// TODO: throw if the type doesn't match
				uint8_t attrIndex = getColumnIndex( AttributeIndex );
				m_Buffer.put( VertexIndex, attrIndex, Value.x() );
				m_Buffer.put( VertexIndex, attrIndex + 1, Value.y() );
			}
			math::Vector2f getVector2fAttribute( uint32_t VertexIndex, uint8_t AttributeIndex ) const
			{
				uint8_t attrIndex = getColumnIndex( AttributeIndex );
				return math::Vector2f( m_Buffer.get< float >( VertexIndex, attrIndex ),
									   m_Buffer.get< float >( VertexIndex, attrIndex + 1 ) );
			}
			
			void setVector3fAttribute( uint32_t VertexIndex, uint8_t AttributeIndex, const math::Vector3f& Value )
			{
				// TODO: throw if the type doesn't match
				uint8_t attrIndex = getColumnIndex( AttributeIndex );
				m_Buffer.put( VertexIndex, attrIndex, Value.x() );
				m_Buffer.put( VertexIndex, attrIndex + 1, Value.y() );
				m_Buffer.put( VertexIndex, attrIndex + 2, Value.z() );
			}
			math::Vector3f getVector3fAttribute( uint32_t VertexIndex, uint8_t AttributeIndex ) const
			{
				uint8_t attrIndex = getColumnIndex( AttributeIndex );
				return math::Vector3f( m_Buffer.get< float >( VertexIndex, attrIndex ),
									   m_Buffer.get< float >( VertexIndex, attrIndex + 1 ),
									   m_Buffer.get< float >( VertexIndex, attrIndex + 2 ) );
			}

			void setVector4fAttribute( uint32_t VertexIndex, uint8_t AttributeIndex, const math::Vector4f& Value )
			{
				// TODO: throw if the type doesn't match
				uint8_t attrIndex = getColumnIndex( AttributeIndex );
				m_Buffer.put( VertexIndex, attrIndex, Value.x() );
				m_Buffer.put( VertexIndex, attrIndex + 1, Value.y() );
				m_Buffer.put( VertexIndex, attrIndex + 2, Value.z() );
				m_Buffer.put( VertexIndex, attrIndex + 3, Value.w() );
			}
			math::Vector4f getVector4fAttribute( uint32_t VertexIndex, uint8_t AttributeIndex ) const
			{
				uint8_t attrIndex = getColumnIndex( AttributeIndex );
				return math::Vector4f( m_Buffer.get< float >( VertexIndex, attrIndex ),
									   m_Buffer.get< float >( VertexIndex, attrIndex + 1 ),
									   m_Buffer.get< float >( VertexIndex, attrIndex + 2 ),
									   m_Buffer.get< float >( VertexIndex, attrIndex + 3 ) );
			}
			
			void setVector4u8Attribute( uint32_t VertexIndex, uint8_t AttributeIndex, const math::Vector4u8& Value )
			{
				// TODO: throw if the type doesn't match
				uint8_t attrIndex = getColumnIndex( AttributeIndex );
				m_Buffer.put( VertexIndex, attrIndex, Value.r() );
				m_Buffer.put( VertexIndex, attrIndex + 1, Value.g() );
				m_Buffer.put( VertexIndex, attrIndex + 2, Value.b() );
				m_Buffer.put( VertexIndex, attrIndex + 3, Value.a() );
			}
			math::Vector4u8 getVector4u8Attribute( uint32_t VertexIndex, uint8_t AttributeIndex ) const
			{
				uint8_t attrIndex = getColumnIndex( AttributeIndex );
				return math::Vector4u8( m_Buffer.get< uint8_t >( VertexIndex, attrIndex ),
									    m_Buffer.get< uint8_t >( VertexIndex, attrIndex + 1 ),
									    m_Buffer.get< uint8_t >( VertexIndex, attrIndex + 2 ),
									    m_Buffer.get< uint8_t >( VertexIndex, attrIndex + 3 ) );
			}			
		private:
			uint8_t getColumnIndex( uint8_t AttributeIndex ) const
			{
				return m_pFormat->getColumnIndex( AttributeIndex );
			}
			
			void enableStates() const;
			void disableStates() const;
			
			static weak_ptr< const VertexBuffer > ms_pBound;

			template< class C, typename T > friend struct fail::attribute_traits;
			shared_ptr< VertexFormat >			m_pFormat;
			DynamicBuffer						m_Buffer;
	};
}}

#endif
