/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_SCENEGRAPH_PERSPECTIVEPROJECTION_H_
#define FAIL_SCENEGRAPH_PERSPECTIVEPROJECTION_H_

#include "core/core.h"
#include "scenegraph/scenegraph_export.h"
#include "Projection.h"

namespace fail { namespace scenegraph
{
	class FLSCENEGRAPH_EXPORT PerspectiveProjection : public Projection
	{
		public:
			PerspectiveProjection() {}
			PerspectiveProjection( const Serialization_tag& ) {}
			virtual void renderSetup( const math::Rectu32& viewrect, float FOV ) const;
	};
}}

#endif
