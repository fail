/*
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "scenegraph/VertexFormat.h"
#include <GL/gl.h>

using namespace fail;
using namespace fail::scenegraph;

VertexFormat::VertexFormat()
{
}

VertexFormat::VertexFormat( const Serialization_tag& )
{
}

void VertexFormat::addAttribute( e_AttrType Type, e_Attr Attr )
{
	m_Attributes.push_back( std::make_pair( Type, Attr ) );
	m_ColumnIndices.push_back( m_ColumnTypes.size() );
	
	switch( Type )
	{
		case at_float:
			m_ColumnTypes.push_back( at_float );
			break;
			
		case at_double:
			m_ColumnTypes.push_back( at_double );
			break;
			
		case at_Vector2f:
			m_ColumnTypes.push_back( at_float );
			m_ColumnTypes.push_back( at_float );
			break;
			
		case at_Vector3f:
			m_ColumnTypes.push_back( at_float );
			m_ColumnTypes.push_back( at_float );
			m_ColumnTypes.push_back( at_float );
			break;

		case at_Vector4f:
			m_ColumnTypes.push_back( at_float );
			m_ColumnTypes.push_back( at_float );
			m_ColumnTypes.push_back( at_float );
			m_ColumnTypes.push_back( at_float );
			break;
			
		case at_Vector4u8:
			m_ColumnTypes.push_back( at_uint8 );
			m_ColumnTypes.push_back( at_uint8 );
			m_ColumnTypes.push_back( at_uint8 );
			m_ColumnTypes.push_back( at_uint8 );
			break;
	}
}
