/*
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_SCENEGRAPH_VERTEXFORMAT_H_
#define FAIL_SCENEGRAPH_VERTEXFORMAT_H_

#include "core/core.h"
#include "scenegraph/scenegraph_export.h"
#include "math/Vector2f.h"
#include "math/Vector3f.h"
#include "math/Vector4f.h"
#include "math/Vector4u8.h"
#include "IndexBuffer.h"
#include <vector>

namespace fail { namespace scenegraph
{
	class FLSCENEGRAPH_EXPORT VertexFormat : public Serializable
	{
		public:
			enum e_AttrType
			{
				at_uint8,
				at_float,
				at_double,
				at_Vector2f,
				at_Vector3f,
				at_Vector4f,
				at_Vector4u8
			};
			
			enum e_Attr
			{
				a_Position,
				a_Normal,
				a_Color,
				a_SecondaryColor,
				a_TexCoord0,
				a_TexCoord1,
				a_TexCoord2,
				a_TexCoord3,
				a_TexCoord4,
				a_TexCoord5,
				a_TexCoord6,
				a_TexCoord7,
				a_FogCoord
			};
			
			VertexFormat();
			VertexFormat( const Serialization_tag& );

			// Vertex layout setup
			void addAttribute( e_AttrType Type, e_Attr Attr );
			
			uint8_t getColumnIndex( uint8_t AttrIndex ) const
			{
				return m_ColumnIndices[ AttrIndex ];
			}
			
			typedef std::vector< e_AttrType > ColumnTypes;
			
			const ColumnTypes& getColumnTypes() const
			{
				return m_ColumnTypes;
			}
			
			typedef std::pair< e_AttrType, e_Attr > Attribute;
			typedef std::vector< Attribute > Attributes;
	
			const Attributes& getAttributes() const
			{
				return m_Attributes;
			}
			
		private:
			template< class C, typename T > friend struct fail::attribute_traits;
			Attributes					m_Attributes;
			std::vector< uint8_t >		m_ColumnIndices;
			std::vector< e_AttrType >	m_ColumnTypes;
	};
}}

#endif
