/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_SCENEGRAPH_INDEXBUFFER16_H_
#define FAIL_SCENEGRAPH_INDEXBUFFER16_H_

#include "core/core.h"
#include "scenegraph/scenegraph_export.h"
#include "IndexBuffer.h"
#include <vector>

namespace fail { namespace scenegraph
{
	class FLSCENEGRAPH_EXPORT IndexBuffer16 : public IndexBuffer
	{
		public:
			IndexBuffer16();
			IndexBuffer16( uint32_t Size_ );
			IndexBuffer16( const Serialization_tag& );
			
			void setIndex( uint32_t i, uint16_t Index )
			{
				m_Indices[i] = Index;
			}
			uint16_t getIndex( uint32_t i ) const
			{
				return m_Indices[i];
			}
			
			virtual void bind() const;

			const uint32_t& getSize() const { return m_Size; }
			void setSize( const uint32_t& Size_ ) { m_Size = Size_; m_Indices.resize( Size_ ); }

		private:
			template< class C, typename T > friend struct fail::attribute_traits;
			uint32_t				m_Size;
			std::vector< uint16_t > m_Indices;
	};
}}

#endif
