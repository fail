/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_SCENEGRAPH_GEOMETRY_H_
#define FAIL_SCENEGRAPH_GEOMETRY_H_

#include "core/core.h"
#include "scenegraph/scenegraph_export.h"
#include "Primitive.h"
#include "IndexBuffer.h"
#include "VertexBuffer.h"
#include <vector>

namespace fail { namespace scenegraph
{
	class FLSCENEGRAPH_EXPORT Geometry : public Serializable
	{
		public:
			Geometry( shared_ptr< VertexBuffer > pVertices, shared_ptr< IndexBuffer > pIndices );
			Geometry( const Serialization_tag& );
						
			const shared_ptr< VertexBuffer >& getpVertexBuffer() const { return m_pVertexBuffer; }
			void setpVertexBuffer( const shared_ptr< VertexBuffer >& x ) { m_pVertexBuffer = x; }
			
			const shared_ptr< IndexBuffer >& getpIndexBuffer() const { return m_pIndexBuffer; }
			void setpIndexBuffer( const shared_ptr< IndexBuffer >& x ) { m_pIndexBuffer = x; }
			
			void addPrimitive( const Primitive& Prim )
			{
				m_Primitives.push_back( Prim );
			}
			
			void addPrimitive( enum Primitive::e_Type Type_, uint32_t Start_, uint32_t Count_ )
			{
				addPrimitive( Primitive( Type_, Start_, Count_ ) );
			}
			
			void render() const
			{
				m_pIndexBuffer->bind();
				m_pVertexBuffer->renderSetup();
				std::vector< Primitive >::const_iterator it;
				for( it = m_Primitives.begin(); it != m_Primitives.end(); ++it )
					it->render();
			}
			
		private:
			template< class C, typename T > friend struct fail::attribute_traits;
			shared_ptr< VertexBuffer >		m_pVertexBuffer;
			shared_ptr< IndexBuffer >		m_pIndexBuffer;
			std::vector< Primitive >		m_Primitives;
	};
}}

#endif
