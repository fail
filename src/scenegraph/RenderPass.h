/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_SCENEGRAPH_RENDERPASS_H_
#define FAIL_SCENEGRAPH_RENDERPASS_H_

#include "core/core.h"
#include "scenegraph/scenegraph_export.h"
#include "Material.h"
#include "Drawable.h"
//#include "Text.h"
#include "ViewPort.h"
#include "Scissor.h"
#include <list>
#include <map>

namespace fail { namespace scenegraph
{
	class Drawable;
	//class Text;
	
	class FLSCENEGRAPH_EXPORT RenderPass : public Serializable, public enable_shared_from_this< RenderPass >
	{
		public:
			RenderPass( const shared_ptr< ViewPort >& pViewPort );
			RenderPass( const shared_ptr< ViewPort >& pViewPort, const shared_ptr< Scissor >& pScissor );
			RenderPass( const Serialization_tag& );

			void addDrawable( const shared_ptr< Drawable >& pDrawable_ )
			{
				MaterialKey key( pDrawable_->getpMaterial() );
				m_RenderMap[ key ].push_back( pDrawable_ );
			}
			
		/*	void addText( const Pointer< Text >& pText_, float z )
			{
				m_TextMap.insert( std::make_pair( z, pText_ ) );
			}*/
			
			void appendToRenderList()
			{
				ms_RenderList.push_back( shared_from_this() );
			}

			void prependToRenderList()
			{
				ms_RenderList.push_front( shared_from_this() );
			}

			void addPreRenderChildPass( shared_ptr< RenderPass > pChildPass )
			{
				m_PreRenderList.push_front( pChildPass );
			}

			void addPostRenderChildPass( shared_ptr< RenderPass > pChildPass )
			{
				m_PostRenderList.push_back( pChildPass );
			}
			
			static void RenderAll( bool bPreserveGLState );
			static void ClearAll();
			void clear();
			
			const shared_ptr< ViewPort >& getpViewPort() const { return m_pViewPort; }
			void setpViewPort( const shared_ptr< ViewPort >& x ) { m_pViewPort = x; }

			const shared_ptr< Scissor >& getpScissor() const { return m_pScissor; }
			void setpScissor( const shared_ptr< Scissor >& x ) { m_pScissor = x; }

			const bool& getbClearColorBuffer() const { return m_bClearColorBuffer; }
			void setbClearColorBuffer( const bool& x ) { m_bClearColorBuffer = x; }
			
			const bool& getbClearDepthBuffer() const { return m_bClearDepthBuffer; }
			void setbClearDepthBuffer( const bool& x ) { m_bClearDepthBuffer = x; }

			const bool& getbClearStencil() const { return m_bClearStencil; }
			void setbClearStencil( const bool& x ) { m_bClearStencil = x; }

			const math::Vector4f& getColorClearValue() const { return m_ColorClearValue; }
			math::Vector4f& getColorClearValue() { return m_ColorClearValue; }
			void setColorClearValue( const math::Vector4f& x ) { m_ColorClearValue = x; }

			const uint32_t& getStencilClearValue() const { return m_StencilClearValue; }
			void setStencilClearValue( const uint32_t& x ) { m_StencilClearValue = x; }

		private:
			template< class C, typename T > friend struct fail::attribute_traits;
			
			void render() const;

			typedef std::list< shared_ptr< RenderPass > > PassList;
			static PassList	ms_RenderList;
			
			struct MaterialKey
			{
				MaterialKey( shared_ptr< const Material > pMaterial_ ) : pMaterial( pMaterial_ ) {}
				
				shared_ptr< const Material > pMaterial;
				
				bool operator==( const MaterialKey& b ) const
				{
					return *pMaterial == *b.pMaterial;
				}
				
				bool operator<( const MaterialKey& b ) const
				{
					return *pMaterial < *b.pMaterial;
				}

			};
			
			shared_ptr< ViewPort >	m_pViewPort;
			shared_ptr< Scissor >	m_pScissor;

			bool				m_bClearColorBuffer;
			bool				m_bClearDepthBuffer;
			bool				m_bClearStencil;

			math::Vector4f			m_ColorClearValue;
			uint32_t			m_StencilClearValue;
			
			typedef std::list< shared_ptr< const Drawable > > Bucket;
			typedef std::map< MaterialKey, Bucket > RenderMap;
			RenderMap	m_RenderMap;
			
			//typedef std::multimap< float, ConstPointer< Text > > TextMap;
			//TextMap		m_TextMap;

			PassList	m_PreRenderList;
			PassList	m_PostRenderList;
	};
}}

#endif
