/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "scenegraph/VertexBuffer.h"
#include <GL/gl.h>

using namespace fail;
using namespace fail::scenegraph;

weak_ptr< const VertexBuffer > VertexBuffer::ms_pBound;

namespace
{
	struct AttrType
	{
		int Size;
		int Type;
	};
	
	AttrType Types[] =
	{
		{ 1, GL_BYTE },
		{ 1, GL_FLOAT },
		{ 1, GL_DOUBLE },
		{ 2, GL_FLOAT },
		{ 3, GL_FLOAT },
		{ 4, GL_FLOAT },
  		{ 4, GL_UNSIGNED_BYTE }
	};
}

VertexBuffer::VertexBuffer( shared_ptr< VertexFormat > pFormat, uint32_t Size ) :
	m_pFormat( pFormat )
{
	const VertexFormat::ColumnTypes& types = pFormat->getColumnTypes();
	VertexFormat::ColumnTypes::const_iterator it;
	for( it = types.begin(); it != types.end(); ++it )
	{
		switch( *it )
		{
			case VertexFormat::at_uint8:
				m_Buffer.addColumn< uint8_t >();
				break;
				
			case VertexFormat::at_float:
				m_Buffer.addColumn< float >();
				break;
			
			case VertexFormat::at_double:
				m_Buffer.addColumn< double >();
				break;
		}
	}

	m_Buffer.resize( Size );
}

VertexBuffer::VertexBuffer( const Serialization_tag& )
{
}

void VertexBuffer::bind() const
{
	{
		shared_ptr< const VertexBuffer > pBound = ms_pBound.lock();
		if( pBound )
			pBound->disableStates();
	}
	
// 	ms_pBound = shared_from_this();
	
	int Stride = m_Buffer.getStride();
	
	int AttrIndex = 0;
	const VertexFormat::Attributes& attrs = m_pFormat->getAttributes();
	VertexFormat::Attributes::const_iterator it;
	for( it = attrs.begin(); it != attrs.end(); ++it, ++AttrIndex )
	{
		int Size = Types[ it->first ].Size;
		int Type = Types[ it->first ].Type;
		const void* pBufferStart = m_Buffer.getColumnAddress( getColumnIndex( AttrIndex ) );
		
		switch( it->second )
		{
			case VertexFormat::a_Position:
				glVertexPointer( Size, Type, Stride, pBufferStart );
				break;

			case VertexFormat::a_Normal:
				glNormalPointer( Type, Stride, pBufferStart );
				break;

			case VertexFormat::a_Color:
				glColorPointer( Size, Type, Stride, pBufferStart );
				break;
				
		//	case a_SecondaryColor:
		//		glSecondaryColorPointer( Size, Type, Stride, pBufferStart );
		//		break;
			
			case VertexFormat::a_TexCoord0:
				//glClientActiveTexture( GL_TEXTURE0 );
				glTexCoordPointer( Size, Type, Stride, pBufferStart );
				break;

			// TODO: No point in handling other texture coordinate attributes until
			// I implement shaders, so let's put this on the "after long peaks" list for now,
			// aswell as fogcoord.
		}
	}

	enableStates();
}

void VertexBuffer::enableStates() const
{
	const VertexFormat::Attributes& attrs = m_pFormat->getAttributes();
	VertexFormat::Attributes::const_iterator it;
	for( it = attrs.begin(); it != attrs.end(); ++it )
	{		
		switch( it->second )
		{
			case VertexFormat::a_Position:
				glEnableClientState( GL_VERTEX_ARRAY );
				break;

			case VertexFormat::a_Normal:
				glEnableClientState( GL_NORMAL_ARRAY );
				break;

			case VertexFormat::a_Color:
				glEnableClientState( GL_COLOR_ARRAY );
				break;
				
		//	case VertexFormat::a_SecondaryColor:
		//		glEnableClientState( GL_SECONDARY_COLOR_ARRAY );
		//		break;
			
			case VertexFormat::a_TexCoord0:
				glEnableClientState( GL_TEXTURE_COORD_ARRAY );
				break;
		}
	}
}

void VertexBuffer::disableStates() const
{
	const VertexFormat::Attributes& attrs = m_pFormat->getAttributes();
	VertexFormat::Attributes::const_iterator it;
	for( it = attrs.begin(); it != attrs.end(); ++it )
	{		
		switch( it->second )
		{
			case VertexFormat::a_Position:
				glDisableClientState( GL_VERTEX_ARRAY );
				break;

			case VertexFormat::a_Normal:
				glDisableClientState( GL_NORMAL_ARRAY );
				break;

			case VertexFormat::a_Color:
				glDisableClientState( GL_COLOR_ARRAY );
				break;
				
		//	case VertexFormat::a_SecondaryColor:
		//		glDisableClientState( GL_SECONDARY_COLOR_ARRAY );
		//		break;
			
			case VertexFormat::a_TexCoord0:
				glDisableClientState( GL_TEXTURE_COORD_ARRAY );
				break;
		}
	}
}
/*
void VertexBuffer::addAttribute( e_AttrType Type, e_Attr Attr )
{
	m_Attributes.push_back( std::make_pair( Type, Attr ) );
	m_ColumnIndices.push_back( m_Buffer.numColumns() );
	
	switch( Type )
	{
		case at_float:
			m_Buffer.addColumn< float >();
			break;
			
		case at_double:
			m_Buffer.addColumn< double >();
			break;
			
		case at_Vector2f:
			m_Buffer.addColumn< float >();
			m_Buffer.addColumn< float >();
			break;
			
		case at_Vector3f:
			m_Buffer.addColumn< float >();
			m_Buffer.addColumn< float >();
			m_Buffer.addColumn< float >();
			break;

		case at_Vector4f:
			m_Buffer.addColumn< float >();
			m_Buffer.addColumn< float >();
			m_Buffer.addColumn< float >();
			m_Buffer.addColumn< float >();
			break;
			
		case at_Vector4u8:
			m_Buffer.addColumn< uint8_t >();
			m_Buffer.addColumn< uint8_t >();
			m_Buffer.addColumn< uint8_t >();
			m_Buffer.addColumn< uint8_t >();
			break;
	}
}
*/
