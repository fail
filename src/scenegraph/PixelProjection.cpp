/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "scenegraph/PixelProjection.h"
#include <GL/gl.h>
#include <GL/glu.h>

using namespace fail;
using namespace fail::scenegraph;

void PixelProjection::renderSetup( const math::Rectu32& viewrect, float FOV ) const
{
	float ratio = static_cast< float >( viewrect.width() ) /
					static_cast< float >( viewrect.height() );
		
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	float size = viewrect.width() / 2.f;
	glOrtho( -size, size, 
			 -size, size,
			 -1.f, 1.f ); //m_ZNear, m_ZFar );

	glScalef( 1.f, ratio, 1.f );
	glTranslatef( -( viewrect.width() / 2.f ), viewrect.height() / 2.f, 0.f );
}
