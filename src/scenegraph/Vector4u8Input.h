/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_SCENEGRAPH_VECTOR4U8INPUT_H_
#define FAIL_SCENEGRAPH_VECTOR4U8INPUT_H_

#include "core/core.h"
#include "scenegraph/scenegraph_export.h"
#include "math/Vector4u8.h"

namespace fail { namespace scenegraph
{
	struct FLSCENEGRAPH_EXPORT Vector4u8Input
	{
		Vector4u8Input( uint8_t r = 255, uint8_t g = 255, uint8_t b = 255, uint8_t a = 255 ) :
			m_Value( r, g, b, a )
		{
		}
		
		math::Vector4u8&		value() { return m_Value; }
		const math::Vector4u8&	value() const { return m_Value; }
		
		bool operator==( const Vector4u8Input& b ) const
		{
			return m_Value == b.m_Value;
		}
		
		bool operator!=( const Vector4u8Input& b ) const
		{
			return m_Value != b.m_Value;
		}
		
		bool operator<( const Vector4u8Input& b ) const
		{
			return m_Value < b.m_Value;
		}

		private:
			math::Vector4u8	m_Value;
	};
}}

#endif
