/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "scenegraph/Frame.h"

using namespace fail;
using namespace fail::scenegraph;

weak_ptr< const Frame > Frame::ms_pBound;

Frame::Frame() : m_bDirty( true )
{
}

Frame::Frame( const Serialization_tag& ) : m_bDirty( true )
{
}

void Frame::postLoad()
{
	if( m_pParent )
	{
		m_pParent->m_Children.push_front( static_pointer_cast< Frame >( shared_from_this() ) );
		m_itParentList = m_pParent->m_Children.begin();
	}
}

void Frame::setpParent( const shared_ptr< Frame >& pParent )
{
	if( m_pParent == pParent )
		return;
	
	detach();
	
	m_pParent = pParent;
	
	if( m_pParent )
	{
		m_pParent->m_Children.push_front( static_pointer_cast< Frame >( shared_from_this() ) );
		m_itParentList = m_pParent->m_Children.begin();
	}
	
	dirty();
}

void Frame::markAsDirty() const
{
	m_bDirty = true;
	
	sig_Dirty();
	
	ChildrenList::const_iterator it;
	for( it = m_Children.begin(); it != m_Children.end(); ++it )
	{
		shared_ptr< Frame > pChild = ( *it ).lock();
		if( pChild )
			pChild->dirty();
	}
}

const math::Matrix44f& Frame::getLocalToWorld() const
{
	if( !m_pParent )
	{
		m_bDirty = false;
		return m_LocalToParent;
	}
	
	if( m_bDirty )
	{
		m_bDirty = false;
				
		m_LocalToWorld = m_pParent->getLocalToWorld() * m_LocalToParent;
				
		/*std::cout << "frame: local mtx is\n"
			<< m_LocalToParent.v1().x() << " " << m_LocalToParent.v1().y() << " " << m_LocalToParent.v1().z() << " " << m_LocalToParent.v1().w() << std::endl
			<< m_LocalToParent.v2().x() << " " << m_LocalToParent.v2().y() << " " << m_LocalToParent.v2().z() << " " << m_LocalToParent.v2().w() << std::endl
			<< m_LocalToParent.v3().x() << " " << m_LocalToParent.v3().y() << " " << m_LocalToParent.v3().z() << " " << m_LocalToParent.v3().w() << std::endl
			<< m_LocalToParent.v4().x() << " " << m_LocalToParent.v4().y() << " " << m_LocalToParent.v4().z() << " " << m_LocalToParent.v4().w() << std::endl;

		
		std::cout << "frame: concatenating, local.x =" << m_LocalToParent.position().x()
			<< ", parent.x=" << m_pParent->getLocalToWorld().position().x()
			<< ", res.x=" << m_LocalToWorld.position().x() << std::endl;*/
	}
	
	//std::cout << "****\n";
	
	return m_LocalToWorld;
}
