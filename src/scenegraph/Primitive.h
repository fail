/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_SCENEGRAPH_PRIMITIVE_H_
#define FAIL_SCENEGRAPH_PRIMITIVE_H_

#include "core/core.h"
#include "scenegraph/scenegraph_export.h"
#include "IndexBuffer.h"
#include <GL/gl.h>

namespace fail { namespace scenegraph
{
	struct FLSCENEGRAPH_EXPORT Primitive
	{
		enum e_Type
		{
			t_Triangles = GL_TRIANGLES,
   			t_Quads = GL_QUADS,
			t_TriangleFan = GL_TRIANGLE_FAN,
			t_TriangleStrip = GL_TRIANGLE_STRIP
			// TODO: other types
		};
		
		Primitive() {}
		Primitive( enum e_Type Type_, uint32_t Start_, uint32_t Count_ ) :
			m_Type( Type_ ),
			m_Start( Start_ ),
			m_Count( Count_ )
		{
		}
		
		e_Type&	type() { return m_Type; }
		const e_Type& type() const { return m_Type; }
		uint32_t&	start() { return m_Start; }
		const uint32_t& start() const { return m_Start; }
		uint32_t&	count() { return m_Count; }
		const uint32_t& count() const { return m_Count; }
		
		void render() const
		{
			glDrawElements( ( int )m_Type, m_Count,
				IndexBuffer::ms_GLType,
				IndexBuffer::ms_Stride * m_Start + IndexBuffer::ms_pIndexStart );
		}
		
		private:
			e_Type		m_Type;
			uint32_t	m_Start;
			uint32_t	m_Count;
	};
}}

#endif
