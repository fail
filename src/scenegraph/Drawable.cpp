/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "scenegraph/Drawable.h"
#include "scenegraph/RenderPass.h"

using namespace fail;
using namespace fail::scenegraph;

Drawable::Drawable( shared_ptr< Geometry > pGeometry, shared_ptr< Material > pMaterial, shared_ptr< Frame > pFrame ) :
	m_pGeometry( pGeometry ),
	m_pMaterial( pMaterial ),
	m_pFrame( pFrame )
{
}

Drawable::Drawable( const Serialization_tag& )
{
}

void Drawable::evaluate( const shared_ptr< RenderPass >& pPass )
{
	pPass->addDrawable( static_pointer_cast< Drawable >( shared_from_this() ) );
}
