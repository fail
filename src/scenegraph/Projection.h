/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_SCENEGRAPH_PROJECTION_H_
#define FAIL_SCENEGRAPH_PROJECTION_H_

#include "core/core.h"
#include "scenegraph/scenegraph_export.h"
#include "math/Rectu32.h"

namespace fail { namespace scenegraph
{
	class FLSCENEGRAPH_EXPORT Projection : public Serializable
	{
		public:
			Projection() :
				m_ZNear( 0.1f ),
				m_ZFar( 50.f )
			{
			}
			
			Projection( const Serialization_tag& ) {}

			
			const float& getZNear() const { return m_ZNear; }
			void setZNear( const float& x ) { m_ZNear = x; }

			const float& getZFar() const { return m_ZFar; }
			void setZFar( const float& x ) { m_ZFar = x; }

			virtual void renderSetup( const math::Rectu32& viewrect, float FOV ) const = 0;
			
		protected:
			template< class C, typename T > friend struct fail::attribute_traits;
			float				m_ZNear;
			float				m_ZFar;
	};
}}

#endif
