/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_SCENEGRAPH_CHILDRENDERPASS_H_
#define FAIL_SCENEGRAPH_CHILDRENDERPASS_H_

#include "core/core.h"
#include "scenegraph/scenegraph_export.h"
#include "Renderable.h"

namespace fail { namespace scenegraph
{
	class FLSCENEGRAPH_EXPORT ChildRenderPass : public Renderable
	{
		public:
			ChildRenderPass( shared_ptr< RenderPass > pSubContext, shared_ptr< Renderable > pSubRenderable, bool bPostRender = false );
			ChildRenderPass( const Serialization_tag& );
			
 			virtual void evaluate( const shared_ptr< RenderPass >& pPass );
			
			const shared_ptr< RenderPass >& getpChildPass() const { return m_pChildPass; }
			void setpChildPass( const shared_ptr< RenderPass >& x ) { m_pChildPass = x; }
			
			const shared_ptr< Renderable >& getpSubRenderable() const { return m_pSubRenderable; }
			void setpSubRenderable( const shared_ptr< Renderable >& x ) { m_pSubRenderable = x; }

			const bool& getbPostRender() const { return m_bPostRender; }
			void setbPostRender( const bool& x )  { m_bPostRender = x; }
			
		private:
			template< class C, typename T > friend struct fail::attribute_traits;
			shared_ptr< RenderPass >	m_pChildPass;
			shared_ptr< Renderable >	m_pSubRenderable;
			bool						m_bPostRender;
	};
}}

#endif
