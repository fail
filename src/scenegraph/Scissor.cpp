/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "scenegraph/Scissor.h"

using namespace fail;
using namespace fail::scenegraph;

Scissor::Scissor( const math::Rectu32& rect_ ) :
	m_Rect( rect_ )
{
}

Scissor::Scissor( const math::Vector2u32& position_, const math::Vector2u32& size_ ) :
	m_Rect( position_, size_ )
{
}

Scissor::Scissor( uint32_t left_, uint32_t top_, uint32_t width_, uint32_t height_ ) :
	m_Rect( left_, top_, width_, height_ )
{
}

Scissor::Scissor( const Serialization_tag& )
{
}
