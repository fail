/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "scenegraph/RenderPass.h"

using namespace fail;
using namespace fail::scenegraph;

RenderPass::PassList RenderPass::ms_RenderList;

RenderPass::RenderPass( const shared_ptr< ViewPort >& pViewPort ) :
	m_pViewPort( pViewPort ),
	m_bClearColorBuffer( false ),
	m_bClearDepthBuffer( false ),
	m_bClearStencil( false ),
	m_StencilClearValue( 0 )
{
}

RenderPass::RenderPass( const shared_ptr< ViewPort >& pViewPort, const shared_ptr< Scissor >& pScissor ) :
	m_pViewPort( pViewPort ),
	m_pScissor( pScissor ),
	m_bClearColorBuffer( false ),
	m_bClearDepthBuffer( false ),
	m_bClearStencil( false ),
	m_StencilClearValue( 0 )
{
}

RenderPass::RenderPass( const Serialization_tag& )
{
}

void RenderPass::RenderAll( bool bPreserveGLState )
{
	if( bPreserveGLState )
	{
		glPushAttrib( GL_ALL_ATTRIB_BITS );
		glMatrixMode( GL_PROJECTION );
		glPushMatrix();
	}

	VertexBuffer::ClearBound();
	Frame::ClearBound();

	PassList::const_iterator it;
	for( it = ms_RenderList.begin(); it != ms_RenderList.end(); ++it )
		( *it )->render();

	if( bPreserveGLState )
	{
		glMatrixMode( GL_PROJECTION );
		glPopMatrix();
		glPopAttrib();
	}
}

void RenderPass::ClearAll()
{
	PassList::iterator it;
	for( it = ms_RenderList.begin(); it != ms_RenderList.end(); ++it )
		( *it )->clear();

	ms_RenderList.clear();
}

void RenderPass::render() const
{
	PassList::const_iterator it;
	for( it = m_PreRenderList.begin(); it != m_PreRenderList.end(); ++it )
		( *it )->render();

	GLint clearflags = 0;

	if( m_bClearColorBuffer )
	{
		clearflags |= GL_COLOR_BUFFER_BIT;
		glClearColor(
			m_ColorClearValue.x(),
			m_ColorClearValue.y(),
			m_ColorClearValue.z(),
			m_ColorClearValue.w() );
	}
	if( m_bClearDepthBuffer )
		clearflags |= GL_DEPTH_BUFFER_BIT;
	if( m_bClearStencil )
	{
		clearflags |= GL_STENCIL_BUFFER_BIT;
		glClearStencil( m_StencilClearValue );
	}
		
	if( clearflags )
		glClear( clearflags );

	if( m_RenderMap.empty() ) //&& m_TextMap.empty() )
		return;
	
	m_pViewPort->renderSetup();

	if( m_pScissor )
		m_pScissor->renderSetup( m_pViewPort );
	else
		Scissor::Clear();

	math::Matrix44f CameraMtx;
	m_pViewPort->getCameraMatrix( CameraMtx );
	
	GLfloat mcam[16];

	mcam[0] = CameraMtx.v1().x();
	mcam[1] = CameraMtx.v1().z();
	mcam[2] = -CameraMtx.v1().y();
	mcam[3] = CameraMtx.v1().w();

	mcam[4] = CameraMtx.v2().x();
	mcam[5] = CameraMtx.v2().z();
	mcam[6] = -CameraMtx.v2().y();
	mcam[7] = CameraMtx.v2().w();

	mcam[8] = CameraMtx.v3().x();
	mcam[9] = CameraMtx.v3().z();
	mcam[10] = -CameraMtx.v3().y();
	mcam[11] = CameraMtx.v3().w();

	mcam[12] = CameraMtx.v4().x();
	mcam[13] = CameraMtx.v4().z();
	mcam[14] = -CameraMtx.v4().y();
	mcam[15] = CameraMtx.v4().w();
	
	glMatrixMode( GL_MODELVIEW );
	
	// Just for testing purpose, a proper light system will of course need to
	// be created later.
	glEnable( GL_LIGHT0 );
	
	glLoadMatrixf( mcam );
	GLfloat pos[] = { 0.f, -10.f, 0.f, 0.f };
	glLightfv( GL_LIGHT0, GL_POSITION, pos );

	RenderMap::const_iterator mapit;
	for( mapit = m_RenderMap.begin(); mapit != m_RenderMap.end(); ++mapit )
	{
		mapit->first.pMaterial->renderSetup();
		
		// I has a bucket
		const Bucket& MahBucket = mapit->second;
		Bucket::const_iterator it;
		for( it = MahBucket.begin(); it != MahBucket.end(); ++it )
		{
			// Render the fucking thing
			if( ( *it )->getpFrame()->needSetup() )
			{
				glLoadMatrixf( mcam );
				//glLoadIdentity();
				( *it )->getpFrame()->renderSetup();
			}

		( *it )->getpGeometry()->render();
		}
	}
	
/*	TextMap::const_iterator textit;
	for( textit = m_TextMap.begin(); textit != m_TextMap.end(); ++textit )
	{
		textit->second->getpMaterial()->renderSetup();
		if( textit->second->getpFrame()->needSetup() )
		{
			glLoadMatrixf( mcam );
		//	glLoadIdentity();
			textit->second->getpFrame()->renderSetup();
		}
		
		if( textit->second->getClip() == 0.f )
			textit->second->getpFont()->drawText( textit->second->getTextString() );
		else
			textit->second->getpFont()->drawTextRightClip( textit->second->getTextString(), textit->second->getClip() );
	}*/

	for( it = m_PostRenderList.begin(); it != m_PostRenderList.end(); ++it )
		( *it )->render();
}

void RenderPass::clear()
{
	m_RenderMap.clear();
	//m_TextMap.clear();

	PassList::iterator it;
	for( it = m_PreRenderList.begin(); it != m_PreRenderList.end(); ++it )
		( *it )->clear();

	for( it = m_PostRenderList.begin(); it != m_PostRenderList.end(); ++it )
		( *it )->clear();

	m_PreRenderList.clear();
	m_PostRenderList.clear();
}
