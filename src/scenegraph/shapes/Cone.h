/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_SCENEGRAPH_SHAPES_CONE_H_
#define FAIL_SCENEGRAPH_SHAPES_CONE_H_

#include "core/core.h"
#include "scenegraph/shapes/scenegraph-shapes_export.h"
#include "scenegraph/Drawable.h"
#include "Shape.h"
#include <map>

// TODO: the geometry cache management is not multithread friendly.

namespace fail { namespace scenegraph { namespace shapes
{
	class FLSCENEGRAPH_SHAPES_EXPORT Cone : public Shape
	{
		public:
			Cone( const shared_ptr< Material >& pMaterial,
					const shared_ptr< Frame >&	pFrame,
					uint16_t Subdivisions = 32 );
			Cone( const Serialization_tag& );
			~Cone();

			virtual void postLoad();
			virtual void evaluate( const shared_ptr< RenderPass >& pPass );
			
			const uint16_t& getSubdivisions() const { return m_Subdivisions; }
			
		private:
			template< class C, typename T > friend struct fail::attribute_traits;

			shared_ptr< Drawable > m_pDrawable;
			uint16_t m_Subdivisions;

			static shared_ptr< Geometry > GetGeom( uint16_t Subdivisions );
			typedef map< uint16_t, shared_ptr< Geometry > > GeometriesCache;
			static GeometriesCache ms_Geometries;
	};
}}}

#endif
