/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_SCENEGRAPH_SHAPES_SHAPE_H_
#define FAIL_SCENEGRAPH_SHAPES_SHAPE_H_

#include "core/core.h"
#include "scenegraph/shapes/scenegraph-shapes_export.h"
#include "scenegraph/Renderable.h"
#include "scenegraph/Material.h"
#include "scenegraph/Frame.h"

namespace fail { namespace scenegraph { namespace shapes
{
	class FLSCENEGRAPH_SHAPES_EXPORT Shape : public Renderable
	{
		public:
			const shared_ptr< Material >& getpMaterial() const { return m_pMaterial; }
			void setpMaterial( const shared_ptr< Material >& x ) { m_pMaterial = x; }
			
			const shared_ptr< Frame >& getpFrame() const { return m_pFrame; }
			void setpFrame( const shared_ptr< Frame >& x ) { m_pFrame = x; }
			
		protected:
			Shape();
			Shape( const shared_ptr< Material >& pMaterial,
				   const shared_ptr< Frame >& pFrame );
			
			template< class C, typename T > friend struct fail::attribute_traits;
			shared_ptr< Material >	m_pMaterial;
			shared_ptr< Frame >	m_pFrame;
	};
}}}

#endif
