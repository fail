/*
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "scenegraph/shapes/Cube.h"
#include "scenegraph/RenderPass.h"
#include "scenegraph/IndexBuffer16.h"

using namespace fail;
using namespace fail::scenegraph;
using namespace fail::scenegraph::shapes;

shared_ptr< Geometry > Cube::ms_pGeom;

Cube::Cube( const shared_ptr< Material >& pMaterial,
		const shared_ptr< Frame >&	pFrame ) :
	Shape( pMaterial, pFrame )
{
	if( !ms_pGeom )
		CreateGeometry();
	
	m_pDrawable = shared_ptr< Drawable >( new Drawable( ms_pGeom, m_pMaterial, m_pFrame ) );
}

Cube::Cube( const Serialization_tag& )
{
}

void Cube::postLoad()
{				
	if( !ms_pGeom )
		CreateGeometry();
	
	m_pDrawable = shared_ptr< Drawable >( new scenegraph::Drawable( ms_pGeom, m_pMaterial, m_pFrame ) );
}

void Cube::CreateGeometry()
{
	shared_ptr< VertexFormat > pVF( new VertexFormat );
	pVF->addAttribute( VertexFormat::at_Vector3f, VertexFormat::a_Position );
	pVF->addAttribute( VertexFormat::at_Vector3f, VertexFormat::a_Normal );
	
	shared_ptr< VertexBuffer > pVB( new VertexBuffer( pVF, 24 ) );
	
	// Sides can't share vertices since we want each of them to have different normals.
	
	// Right side
	pVB->setVector3fAttribute( 0, 0, math::Vector3f( 0.5f, -0.5f, 0.5f ) );
	pVB->setVector3fAttribute( 1, 0, math::Vector3f( 0.5f, -0.5f, -0.5f ) );
	pVB->setVector3fAttribute( 2, 0, math::Vector3f( 0.5f, 0.5f, -0.5f ) );
	pVB->setVector3fAttribute( 3, 0, math::Vector3f( 0.5f, 0.5f, 0.5f ) );

	// Left side
	pVB->setVector3fAttribute( 4, 0, math::Vector3f( -0.5f, 0.5f, 0.5f ) );
	pVB->setVector3fAttribute( 5, 0, math::Vector3f( -0.5f, 0.5f, -0.5f ) );
	pVB->setVector3fAttribute( 6, 0, math::Vector3f( -0.5f, -0.5f, -0.5f ) );
	pVB->setVector3fAttribute( 7, 0, math::Vector3f( -0.5f, -0.5f, 0.5f ) );
	
	// Front side
	pVB->setVector3fAttribute( 8, 0, math::Vector3f( 0.5f, 0.5f, 0.5f ) );
	pVB->setVector3fAttribute( 9, 0, math::Vector3f( 0.5f, 0.5f, -0.5f ) );
	pVB->setVector3fAttribute( 10, 0, math::Vector3f( -0.5f, 0.5f, -0.5f ) );
	pVB->setVector3fAttribute( 11, 0, math::Vector3f( -0.5f, 0.5f, 0.5f ) );

	// Back side
	pVB->setVector3fAttribute( 12, 0, math::Vector3f( -0.5f, -0.5f, 0.5f ) );
	pVB->setVector3fAttribute( 13, 0, math::Vector3f( -0.5f, -0.5f, -0.5f ) );
	pVB->setVector3fAttribute( 14, 0, math::Vector3f( 0.5f, -0.5f, -0.5f ) );
	pVB->setVector3fAttribute( 15, 0, math::Vector3f( 0.5f, -0.5f, 0.5f ) );

	// Top side
	pVB->setVector3fAttribute( 16, 0, math::Vector3f( -0.5f, 0.5f, 0.5f ) );
	pVB->setVector3fAttribute( 17, 0, math::Vector3f( -0.5f, -0.5f, 0.5f ) );
	pVB->setVector3fAttribute( 18, 0, math::Vector3f( 0.5f, -0.5f, 0.5f ) );
	pVB->setVector3fAttribute( 19, 0, math::Vector3f( 0.5f, 0.5f, 0.5f ) );

	// Bottom side
	pVB->setVector3fAttribute( 20, 0, math::Vector3f( 0.5f, 0.5f, -0.5f ) );
	pVB->setVector3fAttribute( 21, 0, math::Vector3f( 0.5f, -0.5f, -0.5f ) );
	pVB->setVector3fAttribute( 22, 0, math::Vector3f( -0.5f, -0.5f, -0.5f ) );
	pVB->setVector3fAttribute( 23, 0, math::Vector3f( -0.5f, 0.5f, -0.5f ) );

	// Normals
	math::Vector3f normal;
	
	// Right side
	normal.x() = 1.f; normal.y() = 0.f; normal.z() = 0.f;
	pVB->setVector3fAttribute( 0, 1, normal );
	pVB->setVector3fAttribute( 1, 1, normal );
	pVB->setVector3fAttribute( 2, 1, normal );
	pVB->setVector3fAttribute( 3, 1, normal );

	// Left side
	normal.x() = -1.f; normal.y() = 0.f; normal.z() = 0.f;
	pVB->setVector3fAttribute( 4, 1, normal );
	pVB->setVector3fAttribute( 5, 1, normal );
	pVB->setVector3fAttribute( 6, 1, normal );
	pVB->setVector3fAttribute( 7, 1, normal );
	
	// Front side
	normal.x() = 0.f; normal.y() = 1.f; normal.z() = 0.f;
	pVB->setVector3fAttribute( 8, 1, normal );
	pVB->setVector3fAttribute( 9, 1, normal );
	pVB->setVector3fAttribute( 10, 1, normal );
	pVB->setVector3fAttribute( 11, 1, normal );
	
	// Back side
	normal.x() = 0.f; normal.y() = -1.f; normal.z() = 0.f;
	pVB->setVector3fAttribute( 12, 1, normal );
	pVB->setVector3fAttribute( 13, 1, normal );
	pVB->setVector3fAttribute( 14, 1, normal );
	pVB->setVector3fAttribute( 15, 1, normal );
	
	// Top side
	normal.x() = 0.f; normal.y() = 0.f; normal.z() = 1.f;
	pVB->setVector3fAttribute( 16, 1, normal );
	pVB->setVector3fAttribute( 17, 1, normal );
	pVB->setVector3fAttribute( 18, 1, normal );
	pVB->setVector3fAttribute( 19, 1, normal );

	// Bottom side
	normal.x() = 0.f; normal.y() = 0.f; normal.z() = -1.f;
	pVB->setVector3fAttribute( 20, 1, normal );
	pVB->setVector3fAttribute( 21, 1, normal );
	pVB->setVector3fAttribute( 22, 1, normal );
	pVB->setVector3fAttribute( 23, 1, normal );
	
	// TODO: This index is completely pointless. I need to allow a geometry not to have an index buffer,
	// and render them using non-indexed opengl functions.
	shared_ptr< IndexBuffer16 > pIB( new IndexBuffer16( 24 ) );
	for( int i = 0; i < 24; ++i )
		pIB->setIndex( i, i );

	ms_pGeom = shared_ptr< Geometry >( new Geometry( pVB, pIB ) );
	ms_pGeom->addPrimitive( Primitive::t_Quads, 0, 24 );
}

void Cube::evaluate( const shared_ptr< scenegraph::RenderPass >& pPass )
{
	m_pDrawable->setpMaterial( m_pMaterial );
	m_pDrawable->setpFrame( m_pFrame );
	pPass->addDrawable( m_pDrawable );
}
