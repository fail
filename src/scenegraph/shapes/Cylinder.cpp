/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "scenegraph/shapes/Cylinder.h"
#include "scenegraph/RenderPass.h"
#include "scenegraph/IndexBuffer16.h"
#include "math/failmath.h"

using namespace fail;
using namespace fail::scenegraph;
using namespace fail::scenegraph::shapes;
			
Cylinder::GeometriesCache Cylinder::ms_Geometries;

Cylinder::Cylinder( const shared_ptr< Material >& pMaterial,
		const shared_ptr< Frame >&	pFrame,
		uint16_t Subdivisions ) :
	Shape( pMaterial, pFrame ),
	m_Subdivisions( Subdivisions )
{
	m_pDrawable = shared_ptr< Drawable >( new Drawable( GetGeom( Subdivisions ), m_pMaterial, m_pFrame ) );
}

Cylinder::Cylinder( const Serialization_tag& )
{
}

Cylinder::~Cylinder()
{
	m_pDrawable.reset();
	
	GeometriesCache::iterator it = ms_Geometries.find( m_Subdivisions );
	if( it != ms_Geometries.end() && it->second.unique() )
		ms_Geometries.erase( it );
}

void Cylinder::postLoad()
{
	m_pDrawable = shared_ptr< Drawable >( new scenegraph::Drawable( GetGeom( m_Subdivisions ), m_pMaterial, m_pFrame ) );
}

shared_ptr< Geometry > Cylinder::GetGeom( uint16_t Subdivisions )
{
	GeometriesCache::iterator it = ms_Geometries.find( Subdivisions );
	if( it != ms_Geometries.end() )
		return it->second;
	
	shared_ptr< VertexFormat > pVF( new VertexFormat );
	pVF->addAttribute( VertexFormat::at_Vector3f, VertexFormat::a_Position );
	pVF->addAttribute( VertexFormat::at_Vector3f, VertexFormat::a_Normal );
	
	shared_ptr< VertexBuffer > pVB( new VertexBuffer( pVF,
													  Subdivisions * 2	// For the tristrip
													  + Subdivisions * 2 ) );	// For the trifans (caps)
	
	shared_ptr< IndexBuffer16 > pIB( new IndexBuffer16(
				( Subdivisions + 1 ) * 2
				+ Subdivisions * 2 ) );

	float anglestep = ( math::pi * 2.f ) / static_cast< float >( Subdivisions );

	int i;
	float angle;

 	for( i = 0, angle = 0; i < Subdivisions; ++i, angle += anglestep )
	{
		float c = std::cos( angle );
		float s = std::sin( angle );
		
		// Back vertex
		pVB->setVector3fAttribute( i * 2 + 1, 0, math::Vector3f( c, 0.f, s ) );
		pVB->setVector3fAttribute( i * 2 + 1, 1, math::Vector3f( c, 0.f, s ) );
		pIB->setIndex( i * 2, i * 2 + 1 );
		
		// Front vertex
		pVB->setVector3fAttribute( i * 2, 0, math::Vector3f( c, 1.f, s ) );
		pVB->setVector3fAttribute( i * 2, 1, math::Vector3f( c, 0.f, s ) );
		pIB->setIndex( i * 2 + 1, i * 2 );	
		
		// Back cap vertex
		pVB->setVector3fAttribute( i + Subdivisions * 2, 0, math::Vector3f( c, 0.f, s ) );
		pVB->setVector3fAttribute( i + Subdivisions * 2, 1, math::Vector3f( 0.f, -1.f, 0.f ) );
		pIB->setIndex( ( Subdivisions + 1 ) * 2 + i, i + Subdivisions * 2 );
		
		// Front cap vertex
		pVB->setVector3fAttribute( i + Subdivisions * 3, 0, math::Vector3f( c, 1.f, s ) );
		pVB->setVector3fAttribute( i + Subdivisions * 3, 1, math::Vector3f( 0.f, 1.f, 0.f ) );
		pIB->setIndex( ( Subdivisions + 1 ) * 2 + Subdivisions * 2 - i - 1, i + Subdivisions * 3 );
	}
	
	pIB->setIndex( i * 2, 1 );
	pIB->setIndex( i * 2 + 1, 0 );
	
	shared_ptr< scenegraph::Geometry > pGeom( new Geometry( pVB, pIB ) );
	pGeom->addPrimitive( Primitive::t_TriangleStrip, 0, ( Subdivisions + 1 ) * 2 );
	pGeom->addPrimitive( Primitive::t_TriangleFan, ( Subdivisions + 1 ) * 2, Subdivisions );
	pGeom->addPrimitive( Primitive::t_TriangleFan, ( Subdivisions + 1 ) * 2 + Subdivisions, Subdivisions );
	
	ms_Geometries[ Subdivisions ] = pGeom;
	return pGeom;
}

void Cylinder::evaluate( const shared_ptr< scenegraph::RenderPass >& pPass )
{
	m_pDrawable->setpMaterial( m_pMaterial );
	m_pDrawable->setpFrame( m_pFrame );
	pPass->addDrawable( m_pDrawable );
}
