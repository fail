/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_SCENEGRAPH_SHAPES_RECTANGLE_H_
#define FAIL_SCENEGRAPH_SHAPES_RECTANGLE_H_

#include "core/core.h"
#include "scenegraph/shapes/scenegraph-shapes_export.h"
#include "scenegraph/Drawable.h"
#include "Shape.h"

namespace fail { namespace scenegraph { namespace shapes
{
	class FLSCENEGRAPH_SHAPES_EXPORT Rectangle : public Shape
	{
		public:
			Rectangle( const shared_ptr< Material >& pMaterial,
					const shared_ptr< Frame >&	pFrame );
			Rectangle( const Serialization_tag& );

			virtual void postLoad();
			virtual void evaluate( const shared_ptr< RenderPass >& pPass );
			
		private:
			shared_ptr< Drawable > m_pDrawable;
			
			// We only need one geometry object, along with one set of buffers
			// for every rectangle.
			static void CreateGeometry();
			static shared_ptr< Geometry > ms_pGeom;
	};
}}}

#endif
