/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "scenegraph/shapes/Rectangle.h"
#include "scenegraph/RenderPass.h"
#include "scenegraph/IndexBuffer16.h"

using namespace fail;
using namespace fail::scenegraph;
using namespace fail::scenegraph::shapes;

shared_ptr< Geometry > Rectangle::ms_pGeom;

Rectangle::Rectangle( const shared_ptr< Material >& pMaterial,
		const shared_ptr< Frame >&	pFrame ) :
	Shape( pMaterial, pFrame )
{
	if( !ms_pGeom )
		CreateGeometry();
	
	m_pDrawable = shared_ptr< Drawable >( new Drawable( ms_pGeom, m_pMaterial, m_pFrame ) );
}

Rectangle::Rectangle( const Serialization_tag& )
{
}

void Rectangle::postLoad()
{				
	if( !ms_pGeom )
		CreateGeometry();
	
	m_pDrawable = shared_ptr< Drawable >( new scenegraph::Drawable( ms_pGeom, m_pMaterial, m_pFrame ) );
}

void Rectangle::CreateGeometry()
{
	shared_ptr< VertexFormat > pVF( new VertexFormat );
	pVF->addAttribute( VertexFormat::at_Vector3f, VertexFormat::a_Position );
		
	shared_ptr< VertexBuffer > pVB( new VertexBuffer( pVF, 4 ) );

	pVB->setVector3fAttribute( 0, 0, math::Vector3f( 0.f, 0.f, 0.f ) );
	pVB->setVector3fAttribute( 1, 0, math::Vector3f( 0.f, 0.f, 1.f ) );
	pVB->setVector3fAttribute( 2, 0, math::Vector3f( 1.f, 0.f, 1.f ) );
	pVB->setVector3fAttribute( 3, 0, math::Vector3f( 1.f, 0.f, 0.f ) );
	
	shared_ptr< IndexBuffer16 > pIB( new IndexBuffer16( 4 ) );
	pIB->setIndex( 0, 0 );
	pIB->setIndex( 1, 1 );
	pIB->setIndex( 2, 2 );
	pIB->setIndex( 3, 3 );

	ms_pGeom = shared_ptr< Geometry >( new Geometry( pVB, pIB ) );
	ms_pGeom->addPrimitive( Primitive::t_Quads, 0, 4 );
}

void Rectangle::evaluate( const shared_ptr< scenegraph::RenderPass >& pPass )
{
	m_pDrawable->setpMaterial( m_pMaterial );
	m_pDrawable->setpFrame( m_pFrame );
	pPass->addDrawable( m_pDrawable );
}
