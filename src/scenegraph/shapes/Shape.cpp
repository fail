/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "scenegraph/shapes/Shape.h"
#include "scenegraph/IndexBuffer16.h"

using namespace fail;
using namespace fail::scenegraph;
using namespace fail::scenegraph::shapes;

Shape::Shape()
{
}
			
Shape::Shape( const shared_ptr< Material >& pMaterial,
		const shared_ptr< Frame >& pFrame ) :
	m_pMaterial( pMaterial ),
	m_pFrame( pFrame )
{
}
