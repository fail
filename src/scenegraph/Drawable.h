/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_SCENEGRAPH_DRAWABLE_H_
#define FAIL_SCENEGRAPH_DRAWABLE_H_

#include "core/core.h"
#include "scenegraph/scenegraph_export.h"
#include "Renderable.h"
#include "Geometry.h"
#include "Material.h"
#include "Frame.h"

namespace fail { namespace scenegraph
{
	class FLSCENEGRAPH_EXPORT Drawable : public Renderable
	{
		public:
			Drawable( shared_ptr< Geometry > pGeometry, shared_ptr< Material > pMaterial, shared_ptr< Frame > pFrame );
			Drawable( const Serialization_tag& );
			
			const shared_ptr< Geometry >& getpGeometry() const { return m_pGeometry; }
			void setpGeometry( const shared_ptr< Geometry >& x ) { m_pGeometry = x; }

			const shared_ptr< Material >& getpMaterial() const { return m_pMaterial; }
			void setpMaterial( const shared_ptr< Material >& x ) { m_pMaterial = x; }
			
			const shared_ptr< Frame >& getpFrame() const { return m_pFrame; }
			void setpFrame( const shared_ptr< Frame >& x ) { m_pFrame = x; }
			
 			virtual void evaluate( const shared_ptr< RenderPass >& pPass );
			
		private:
			template< class C, typename T > friend struct fail::attribute_traits;
			shared_ptr< Geometry >	m_pGeometry;
			shared_ptr< Material >	m_pMaterial;
			
			// TODO: deal with ConstPointer in FBF and python bindings
			shared_ptr< Frame >	m_pFrame;
	};
}}

#endif
