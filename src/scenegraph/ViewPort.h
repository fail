/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_SCENEGRAPH_VIEWPORT_H_
#define FAIL_SCENEGRAPH_VIEWPORT_H_

#include "core/core.h"
#include "scenegraph/scenegraph_export.h"
#include "Camera.h"
#include "Projection.h"
#include "math/Rectu32.h"
#include <GL/gl.h>

namespace fail { namespace scenegraph
{
	class FLSCENEGRAPH_EXPORT ViewPort : public Serializable
	{
		public:
			ViewPort( const shared_ptr< Projection >& pProjection, const shared_ptr< Camera >& pCamera = shared_ptr< Camera >() );
			ViewPort( const Serialization_tag& );
			
			const shared_ptr< Camera >& getpCamera() const { return m_pCamera; }
			void setpCamera( const shared_ptr< Camera >& x ) { m_pCamera = x; }

			const shared_ptr< Projection >& getpProjection() const { return m_pProjection; }
			void setpProjection( const shared_ptr< Projection >& x ) { m_pProjection = x; }
						
			const math::Rectu32& getRect() const { return m_Rect; }
			math::Rectu32& getRect() { return m_Rect; }
			void setRect( const math::Rectu32& x ) { m_Rect = x; }
			
			const float& getZNear() const { return m_ZNear; }
			void setZNear( const float& x ) { m_ZNear = x; }

			const float& getZFar() const { return m_ZFar; }
			void setZFar( const float& x ) { m_ZFar = x; }
		
			void renderSetup() const
			{				
				glViewport( m_Rect.left(), m_Rect.top(), m_Rect.width(), m_Rect.height() );	
				m_pProjection->renderSetup( m_Rect, m_pCamera ? m_pCamera->getFOV() : 60.f );					
			}
			
			void getCameraMatrix( math::Matrix44f& Dest )
			{
				if( m_pCamera )
					m_pCamera->matrix( Dest );
				else
					Dest.identity();
			}
			
		private:
			template< class C, typename T > friend struct fail::attribute_traits;
			shared_ptr< Projection >	m_pProjection;
			shared_ptr< Camera >		m_pCamera;
			math::Rectu32				m_Rect;
			float						m_ZNear;
			float						m_ZFar;
	};
}}

#endif
