/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_SCENEGRAPH_CAMERA_H_
#define FAIL_SCENEGRAPH_CAMERA_H_

#include "core/core.h"
#include "scenegraph/scenegraph_export.h"
#include "Frame.h"
#include <GL/gl.h>

namespace fail { namespace scenegraph
{
	class FLSCENEGRAPH_EXPORT Camera : public Serializable
	{
		public:
			Camera( const shared_ptr< Frame >& pFrame );
			Camera( const Serialization_tag& );
			
			const shared_ptr< Frame >& getpFrame() const { return m_pFrame; }
			void setpFrame( const shared_ptr< Frame >& x ) { m_pFrame = x; }
			
			const float& getFOV() const { return m_FOV; }
			void setFOV( const float& x ) { m_FOV = x; }
			
			void matrix( math::Matrix44f& dest ) const
			{
				m_pFrame->getLocalToWorld().inverse( dest );
			}
			
		private:
			template< class C, typename T > friend struct fail::attribute_traits;
			shared_ptr< Frame >	m_pFrame;
			float				m_FOV;
	};
}}

#endif
