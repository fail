/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "scenegraph/Material.h"

using namespace fail;
using namespace fail::scenegraph;

Material::Material() :
	m_VertexColorChannel( c_None ),
	m_Shininess( 0.0f ),
	m_bLighting( true ),
	m_bDepthTest( true ),
	m_bSmoothShading( true ),
	m_bBackfaceCulling( true ),
	m_bStencil( false ),
	m_StencilValue( 0 ),
	m_StencilCondition( sc_Always ),
	m_StencilOpFail( so_Keep ),
	m_StencilOpDTFail( so_Keep ),
	m_StencilOpPass( so_Keep )
{
	m_Ambient.value() = math::Vector4f( 0.f, 0.f, 0.f, 1.f );
	m_Diffuse.value() = math::Vector4f( 0.f, 0.f, 0.f, 1.f );
}

Material::Material( const Serialization_tag& )
{
}

// TODO: take stencil into account when sorting
bool Material::operator==( const Material& b ) const
{
	return 	( m_VertexColorChannel == b.m_VertexColorChannel )
		&&	( m_Shininess == b.m_Shininess )
		&&	( m_Ambient == b.m_Ambient )
		&&	( m_Diffuse == b.m_Diffuse )
		&&	( m_Specular == b.m_Specular )
		&&	( m_Emission == b.m_Emission );
}

bool Material::operator<( const Material& b ) const
{
	if( m_Ambient < b.m_Ambient )
		return true;
	if( m_Ambient != b.m_Ambient )
		return false;
	
	if( m_Diffuse < b.m_Diffuse )
		return true;
	if( m_Diffuse != b.m_Diffuse )
		return false;
	
	if( m_Specular < b.m_Specular )
		return true;
	if( m_Specular != b.m_Specular )
		return false;
	
	if( m_Emission < b.m_Emission )
		return true;
	if( m_Emission != b.m_Emission )
		return false;
	
	if( m_VertexColorChannel < b.m_VertexColorChannel )
		return true;
	if( m_VertexColorChannel != b.m_VertexColorChannel )
		return false;
	
	if( m_Shininess < b.m_Shininess )
		return true;

	return false;
}

void Material::renderSetup() const
{
	glMaterialfv( GL_FRONT, GL_AMBIENT, &m_Ambient.value().x() );
	glMaterialfv( GL_FRONT, GL_DIFFUSE, &m_Diffuse.value().x() );
	glMaterialfv( GL_FRONT, GL_SPECULAR, &m_Specular.value().x() );
	glMaterialfv( GL_FRONT, GL_EMISSION, &m_Emission.value().x() );
	glMaterialf( GL_FRONT, GL_SHININESS, m_Shininess );
	
	glDisable( GL_TEXTURE_2D );
	
	if( m_bLighting )
		glEnable( GL_LIGHTING );
	else
		glDisable( GL_LIGHTING );
	
	if( m_bDepthTest )
		glEnable( GL_DEPTH_TEST );
	else
		glDisable( GL_DEPTH_TEST );

	if( m_bStencil )
	{
		glEnable( GL_STENCIL_TEST );
		
		// TODO add a property for the mask
		glStencilFunc( m_StencilCondition, m_StencilValue, ~0 );
		glStencilOp( m_StencilOpFail, m_StencilOpDTFail, m_StencilOpPass );
	}
	else
		glDisable( GL_STENCIL_TEST );
	
	if( m_bSmoothShading )
		glShadeModel( GL_SMOOTH );
	else
		glShadeModel( GL_FLAT );

	if( m_bBackfaceCulling )
		glEnable( GL_CULL_FACE );
	else
		glDisable( GL_CULL_FACE );	
	
	glCullFace( GL_BACK );
	
	if( m_VertexColorChannel == c_None )
		return;
	
	glColorMaterial( GL_FRONT, m_VertexColorChannel );
	glEnable( GL_COLOR_MATERIAL );
}
