/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_SCENEGRAPH_SCISSOR_H_
#define FAIL_SCENEGRAPH_SCISSOR_H_

#include "core/core.h"
#include "scenegraph/scenegraph_export.h"
#include "ViewPort.h"
#include "math/Rectu32.h"
#include <GL/gl.h>

namespace fail { namespace scenegraph
{
	class FLSCENEGRAPH_EXPORT Scissor : public Serializable
	{
		public:
			Scissor( const math::Rectu32& rect_ );
			Scissor( const math::Vector2u32& position_, const math::Vector2u32& size_ );
			Scissor( uint32_t left_, uint32_t top_, uint32_t width_, uint32_t height_ );
			Scissor( const Serialization_tag& );
			
			const math::Rectu32& getRect() const { return m_Rect; }
			math::Rectu32& getRect() { return m_Rect; }
			void setRect( const math::Rectu32& x ) { m_Rect = x; }
		
			void renderSetup( shared_ptr< const ViewPort > pViewPort ) const
			{
				glScissor( m_Rect.left(), pViewPort->getRect().height() - ( m_Rect.top() + m_Rect.height() ),
							m_Rect.width(), m_Rect.height() );
				glEnable( GL_SCISSOR_TEST );
			}

			static void Clear()
			{
				glDisable( GL_SCISSOR_TEST );
			}
		
		private:
			template< class C, typename T > friend struct fail::attribute_traits;
			math::Rectu32			m_Rect;
	};
}}

#endif
