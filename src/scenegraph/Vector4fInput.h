/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_SCENEGRAPH_VECTOR4FINPUT_H_
#define FAIL_SCENEGRAPH_VECTOR4FINPUT_H_

#include "core/core.h"
#include "scenegraph/scenegraph_export.h"
#include "math/Vector4f.h"

namespace fail { namespace scenegraph
{
	struct FLSCENEGRAPH_EXPORT Vector4fInput
	{	
		math::Vector4f&			value() { return m_Value; }
		const math::Vector4f&	value() const { return m_Value; }
		
		bool operator==( const Vector4fInput& b ) const
		{
			return m_Value == b.m_Value;
		}
		
		bool operator!=( const Vector4fInput& b ) const
		{
			return m_Value != b.m_Value;
		}
		
		bool operator<( const Vector4fInput& b ) const
		{
			return m_Value < b.m_Value;
		}

		private:
			math::Vector4f	m_Value;
	};
}}

#endif
