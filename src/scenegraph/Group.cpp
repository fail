/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "scenegraph/Group.h"
#include "scenegraph/RenderPass.h"

using namespace fail;
using namespace fail::scenegraph;

Group::Group()
{
}

Group::Group( const shared_ptr< Frame >& pFrame ) :
	m_pFrame( pFrame )
{
}
	
Group::Group( const Serialization_tag& )
{
}

void Group::evaluate( const shared_ptr< RenderPass >& pPass )
{
	std::list< shared_ptr< Renderable > >::const_iterator it;
	for( it = m_Children.begin(); it != m_Children.end(); ++it )
	{
		( *it )->evaluate( pPass );
	}
}
