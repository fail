/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_SCENEGRAPH_FRAME_H_
#define FAIL_SCENEGRAPH_FRAME_H_

#include "core/core.h"
#include "scenegraph/scenegraph_export.h"
#include "math/Matrix44f.h"
#include <list>
#include <GL/gl.h>

namespace fail { namespace scenegraph
{
	class Camera;
	
	class FLSCENEGRAPH_EXPORT Frame : public Persistable
	{
		public:
			Frame();
			Frame( const Serialization_tag& );
			virtual void postLoad();

			const shared_ptr< Frame >& getpParent() const { return m_pParent; }
			void setpParent( const shared_ptr< Frame >& pParent );
			
			const math::Matrix44f& getLocalToParent() const { return m_LocalToParent; }
			math::Matrix44f& getLocalToParent() { return m_LocalToParent; }
			void setLocalToParent( const math::Matrix44f& x )
			{
				m_LocalToParent = x;
				dirty();
			}
			
			// Need an inline front-end to this that only call non-inline
			// if the frame is dirty perhaps?
			const math::Matrix44f& getLocalToWorld() const;
			
			void dirty() const
			{
				if( !m_bDirty )
					markAsDirty();
			}
			
			bool needSetup() const
			{
				return ms_pBound.lock().get() != this;
			}
			
			void getOpenGLMatrix( float* m ) const
			{
				const math::Matrix44f& ltw = getLocalToWorld();
				
				m[0] = ltw.v1().x();
				m[1] = ltw.v1().y();
				m[2] = ltw.v1().z();
				m[3] = ltw.v1().w();
			
				m[4] = ltw.v2().x();
				m[5] = ltw.v2().y();
				m[6] = ltw.v2().z();
				m[7] = ltw.v2().w();
			
				m[8] = ltw.v3().x();
				m[9] = ltw.v3().y();
				m[10] = ltw.v3().z();
				m[11] = ltw.v3().w();
			
				m[12] = ltw.v4().x();
				m[13] = ltw.v4().y();
				m[14] = ltw.v4().z();
				m[15] = ltw.v4().w();
			}
			
			void renderSetup() const
			{
				if( !needSetup() )
					return;

				ms_pBound = static_pointer_cast< const Frame >( shared_from_this() );
			
				float m[16];
				getOpenGLMatrix( m );

				glMultMatrixf( m );
			}

			static void ClearBound()
			{
				ms_pBound.reset();
			}
			
			mutable Signal<> sig_Dirty;
			
		private:
			void detach()
			{
				if( m_pParent )
					m_pParent->m_Children.erase( m_itParentList );
			}
						
			
			void markAsDirty() const;
			
			// Persistent data
			template< class C, typename T > friend struct fail::attribute_traits;
			shared_ptr< Frame >		m_pParent;
			math::Matrix44f			m_LocalToParent;
			
			// Non-persistent data (to be fixed up in PostLoad())
			typedef std::list< weak_ptr< Frame > >	ChildrenList;
			ChildrenList			m_Children;
			ChildrenList::iterator	m_itParentList;
			mutable math::Matrix44f	m_LocalToWorld;
			mutable bool			m_bDirty;
			
			static weak_ptr< const Frame > ms_pBound;
	};
}}

#endif
