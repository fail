/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "scenegraph/IndexBuffer16.h"
#include <GL/gl.h>

using namespace fail;
using namespace fail::scenegraph;

IndexBuffer16::IndexBuffer16()
{
}

IndexBuffer16::IndexBuffer16( uint32_t Size_ ) :
	m_Size( Size_ ),
	m_Indices( Size_ )
{
}

IndexBuffer16::IndexBuffer16( const Serialization_tag& )
{
}

void IndexBuffer16::bind() const
{
	// TODO: this is ugly, hackish, and ineffective (too lazy to use VBO right now, I'll do it later)
	// but this is on the "let's wait to see how it works in
	// long peaks before trying to figure a proper design" list.
	
	ms_GLType = GL_UNSIGNED_SHORT;
	ms_pIndexStart = static_cast< const uint8_t* >(
			static_cast< const void* >( &m_Indices[ 0 ] ) );
	ms_Stride = 2;
}
