/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "scenegraph/ChildRenderPass.h"
#include "scenegraph/RenderPass.h"

using namespace fail;
using namespace fail::scenegraph;

ChildRenderPass::ChildRenderPass( shared_ptr< RenderPass > pChildPass, shared_ptr< Renderable > pSubRenderable, bool bPostRender ) :
	m_pChildPass( pChildPass ),
	m_pSubRenderable( pSubRenderable ),
	m_bPostRender( bPostRender )
{
}
	
ChildRenderPass::ChildRenderPass( const Serialization_tag& )
{
}

void ChildRenderPass::evaluate( const shared_ptr< RenderPass >& pPass )
{
	if( m_bPostRender )
		pPass->addPostRenderChildPass( m_pChildPass );
	else
		pPass->addPreRenderChildPass( m_pChildPass );

	m_pSubRenderable->evaluate( m_pChildPass );
}
