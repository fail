#!${CMAKE_INSTALL_PREFIX}/bin/fail-lua
--[[
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

--package.path = package.path .. '${LUA_PACKAGE_PATH}'

Settings = {}

require 'qtcore'
require 'qtgui'
require 'qtopengl'

qtapp = QApplication( 1 + select( '#', ... ), { arg[0], ... } )
qtapp.__gc = qtapp.delete

QDir.addSearchPath( QString( "fail-lua" ), QString( "${CMAKE_INSTALL_PREFIX}/${LUA_MODULES_LDIR}/fail" ) )

-- Load plugins.
require 'fail.editor.mainwindow'
require 'fail.editor.settings'
require 'fail.editor.about'

fail.editor.mainwindow.mainwindow:show()
qtapp.exec()
