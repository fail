#
#   Fail game engine
#   Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
# 
#   This file is part of Fail.
#
#   Fail is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License version 3
#   as published by the Free Software Foundation.
#
#   Fail is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import fail

# The material exporter is very rudimentary for now, much like the
# material system itself. When the exporter is up and running
# and can actually export meshes, then the current placeholder material
# system will have to be implemented. 
#
# TODO: detect "export as:" in the name to insert materials in
# the manifest.

class MaterialExporter:
	def __init__( self ):
		self.failmats = dict()

	def export( self, material ):
		if material in self.failmats:
			return self.failmats[ material ]

		failmaterial = fail.scenegraph.Material()

		# Export colors.
		diffuse = material.getRGBCol()
		specular = material.getSpecCol()
		ambient = material.getAmb()
		emit = material.getEmit()
		alpha = material.getAlpha()
		specamount = material.getSpec()
		
		# TODO: why didn't I create r, g, b, a aliases for x, y, z, w in Vector4f?
		# This is ugly.
		
		failmaterial.Shininess = specamount
		
		failmaterial.Diffuse.value.x = diffuse[0]
		failmaterial.Diffuse.value.y = diffuse[1]
		failmaterial.Diffuse.value.z = diffuse[2]
		failmaterial.Diffuse.value.w = alpha
		
		failmaterial.Specular.value.x = 0.0 #specular[0]
		failmaterial.Specular.value.y = 0.0 #specular[1]
		failmaterial.Specular.value.z = 0.0 #specular[2]
		failmaterial.Specular.value.w = 1.0

		failmaterial.Ambient.value.x = ambient
		failmaterial.Ambient.value.y = ambient
		failmaterial.Ambient.value.z = ambient

		self.failmats[ material ] = failmaterial
		return failmaterial
