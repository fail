#
#   Fail game engine
#   Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
# 
#   This file is part of Fail.
#
#   Fail is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License version 3
#   as published by the Free Software Foundation.
#
#   Fail is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import fail
import mesh
import material

class SceneExporter:
	def __init__( self, scene ):
		self.scene = scene
		self.frames = dict()
		
		# Holds which renderable, if any, is associated with a given blender object.
		# Currently, I'm going to create a renderable group per object hierarchy found in the scene,
		# (unless said hierarchy only contains one renderable in which caseno gorup will be created).
		# In the future, it will be possible to insert culling objects and other
		# things.
		self.renderables = dict()
		
		self.MaterialExporter = material.MaterialExporter()


	def getOrMakeFrame( self, object ):
		if object in self.frames:
			return self.frames[ object ]
			
		frame = fail.scenegraph.Frame()
		mtx = object.getMatrix( "localspace" )
		frame.LocalToParent = fail.math.Matrix44f(
			fail.math.Vector4f( mtx[0][0], mtx[0][1], mtx[0][2], mtx[0][3] ),
			fail.math.Vector4f( mtx[1][0], mtx[1][1], mtx[1][2], mtx[1][3] ),
			fail.math.Vector4f( mtx[2][0], mtx[2][1], mtx[2][2], mtx[2][3] ),
			fail.math.Vector4f( mtx[3][0], mtx[3][1], mtx[3][2], mtx[3][3] )
			)
		
		parent = object.getParent()
		if parent:
			frame.pParent = self.getOrMakeFrame( parent )
		
		self.frames[ object ] = frame
		return frame


	def getOrMakeGroup( self, object ):
		# If there is no renderable at all for this object yet,
		# we don't need to create a group.
		if object not in self.renderables:
			return None
			
		r = self.renderables[ object ]
		
		# If it's not already a group, make one and insert this into it.
		if not isinstance( r, fail.scenegraph.Group ):
			g = fail.scenegraph.Group()
			g.add( r )
			self.renderables[ object ] = g
			r = g
			
		return r


	def shouldGoInManifest( self, object ):
		if not object.getParent():
			return True
			
		return object.name.startswith( "export as: " )


	def stripName( self, name ):
		if not name.startswith( "export as: " ):
			return name

		return name[11:]


	def export( self ):			
		for object in self.scene.objects:
			# TODO: maybe implement a way to make the exporter ignore an object.
			# For now, we export everything that's in the scene,
			# Objects without a parent are automatically inserted in the
			# manifest (both "objectname" and "frame_objectname"). It's also
			# possible to force an object into the manifest by prefixing
			# its name by "export as:"
			
			renderable = None
			
			# Decide what to do with the object according to its type, currently
			# only convert meshes.
			objtype = object.getType()
					
			if objtype == "Mesh":
				MeshExporter = mesh.MeshExporter( self.MaterialExporter )
				renderable = MeshExporter.export( object, self.getOrMakeFrame( object ) )
			
			if renderable:
				parent = object.getParent()
				if parent:
					group = self.getOrMakeGroup( parent )
					
					print group
					if group:
						group.add( renderable )
						renderable = group

				self.renderables[ object ] = renderable

		# Build the manifest.
		manifest = fail.fail.Manifest()
		
		# For each object we generated a frame for, we check whether it should be inserted into the manifest.
		# Object without parents and those with a name starting in "export as: objectname" will have their frame inserted
		# as frame_objectname, and their renderable (if any) as renderable_objectname.
	
		for obj in self.frames:
			if self.shouldGoInManifest( obj ):
				name = self.stripName( obj.name )
				manifest.addObject( "frame_" + name, self.frames[ obj ] )
				
				if obj in self.renderables:
					manifest.addObject( "renderable_" + name, self.renderables[ obj ] )
					
					# Make sure we automatically export a renderable_Root
					# even if no object has been explicitely named Root in the scene.
					# This should make it easier to export stuff and view it with the
					# viewer without caring about all this naming business, although
					# it might be easier to make the viewer more clever about it instead,
					# or pop up a dialog asking what to display if it finds more than one
					# renderable in the manifest.
					if not manifest.getObject( "renderable_root" ):
						manifest.addObject( "renderable_root", self.renderables[ obj ] )

		return manifest
