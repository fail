#
#   Fail game engine
#   Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
# 
#   This file is part of Fail.
#
#   Fail is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License version 3
#   as published by the Free Software Foundation.
#
#   Fail is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import fail

class MaterialGroup:
	def __init__( self ):
		self.NumTris = 0;
		self.Material = None
		self.Faces = []

	def appendFace( self, face ):
		if len( face.verts ) == 3:
			self.NumTris += 1
		else:
			self.NumTris += 2
			
		self.Faces.append( face )

class MeshExporter:
	def __init__( self, matexp ):
		self.MaterialExporter = matexp
		self.MaterialGroups = dict()

	def getMaterialGroup( self, matid ):
		if self.MaterialGroups.has_key( matid ):
			return self.MaterialGroups[ matid ]
		self.MaterialGroups[ matid ] = MaterialGroup()
		return self.MaterialGroups[ matid ]
		
	def exportVertex( self, face, faceindex, VertexBuffer, index ):
		blvert = face.verts[faceindex]
	
		VertexBuffer.setVector3fAttribute( index, 0, fail.math.Vector3f(
			blvert.co[0], blvert.co[1], blvert.co[2] ) )
			
		normal = fail.math.Vector3f()
		
		if face.smooth:
			normal.x = blvert.no[0]
			normal.y = blvert.no[1]
			normal.z = blvert.no[2]
		else:
			normal.x = face.no[0]
			normal.y = face.no[1]
			normal.z = face.no[2]
			
		VertexBuffer.setVector3fAttribute( index, 1, normal )		


	def export( self, object, frame ):
		mesh = object.getData( False, True )

		# Sort all the faces into the material groups
		for face in mesh.faces:
			mg = self.getMaterialGroup( face.mat )
			mg.appendFace( face )
			
		renderable = None
		if len( self.MaterialGroups ) > 1:
			renderable = fail.scenegraph.Group()

		for matid in self.MaterialGroups:
			failmaterial =  self.MaterialExporter.export( mesh.materials[matid] )
			mg = self.MaterialGroups[matid]
			
			# TODO: UVs, vertex colors
			vf = fail.scenegraph.VertexFormat()
			vf.addAttribute( fail.scenegraph.VertexFormat.at_Vector3f, 
							 fail.scenegraph.VertexFormat.a_Position )
			vf.addAttribute( fail.scenegraph.VertexFormat.at_Vector3f,
							 fail.scenegraph.VertexFormat.a_Normal )	
			
			vb = fail.scenegraph.VertexBuffer( vf, mg.NumTris * 3)			
			ib = fail.scenegraph.IndexBuffer16( mg.NumTris * 3 )
			
			geom = fail.scenegraph.Geometry( vb, ib )
			geom.addPrimitive( fail.scenegraph.Primitive.t_Triangles, 0, mg.NumTris * 3 );
			
			drawable = fail.scenegraph.Drawable( geom, failmaterial, frame )
			if isinstance( renderable, fail.scenegraph.Group ):
				renderable.add( drawable )
			else:
				renderable = drawable

			vindex = 0
			iindex = 0

			# We do a very dumb export with duplicated vertices for each triangle.
			# Identical vertices will be merged and other optimizations (like tri-stripping)
			# will be done by some native c++ utility functions called from here after
			# we're done. This kind of mesh manipulation operations could be useful outside
			# of the python exporter so it makes sense to have it in some library elsewhere.
			for face in mg.Faces:
				self.exportVertex( face, 0, vb, vindex );
				self.exportVertex( face, 1, vb, vindex + 1 );
				self.exportVertex( face, 2, vb, vindex + 2 );
			
				if len( face.verts ) == 3:
					ib.setIndex( iindex, vindex )
					ib.setIndex( iindex + 1, vindex + 1 )
					ib.setIndex( iindex + 2, vindex + 2 )
					
					vindex += 3
					iindex += 3
				else:
					self.exportVertex( face, 3, vb, vindex + 3 );
					ib.setIndex( iindex + 0, vindex + 0 )
					ib.setIndex( iindex + 1, vindex + 1 )
					ib.setIndex( iindex + 2, vindex + 2 )
					
					ib.setIndex( iindex + 3, vindex + 2 )
					ib.setIndex( iindex + 4, vindex + 3 )
					ib.setIndex( iindex + 5, vindex + 0 )
					
					vindex += 4
					iindex += 6

		return renderable

