#!BPY

"""
Name: 'Fail engine scenegraph (.fbf)...'
Blender: 243
Group: 'Export'
Tooltip: 'Export to a fail engine scenegraph in FBF format.'
"""

#
#   Fail game engine
#   Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
# 
#   This file is part of Fail.
#
#   Fail is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License version 3
#   as published by the Free Software Foundation.
#
#   Fail is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import sys
sys.path.append( "${CMAKE_INSTALL_PREFIX}/${PYTHON_MODULES_DIR}" )

import Blender
import fail
import failblender
from failblender import *

exporter = failblender.scene.SceneExporter( Blender.Scene.GetCurrent() )
failscenegraph = exporter.export()

def save( filename ):
	stream = fail.vfs.VFS.GetInstance().openWrite( filename )
	fail.fbffile.Writer.Save( stream, failscenegraph )

Blender.Window.FileSelector( save, "Export fail scenegraph", "unnamed.fbf" )
