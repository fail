#
#   Fail game engine
#   Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
# 
#   This file is part of Fail.
#
#   Fail is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License version 3
#   as published by the Free Software Foundation.
#
#   Fail is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
find_package( Blender )

if( BLENDER_FOUND )
	
	configure_file(
		export_fail.py
		${CMAKE_CURRENT_BINARY_DIR}/export_fail.py
	)

	install( DIRECTORY failblender DESTINATION ${PYTHON_MODULES_DIR} )
	install( PROGRAMS ${CMAKE_CURRENT_BINARY_DIR}/export_fail.py DESTINATION ${BLENDER_SHARE_DIR}/scripts/blender )

endif( BLENDER_FOUND )
