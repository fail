--[[
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

require 'fail.utils'
require 'fail.math'
input = require 'fail.game.input'
sg = require 'fail.scenegraph'

Camera = fail.utils.class
{
 	function( self, needupdatefunc )
		self.frame = sg.Frame()
		self.camera = sg.Camera( self.frame )

		local mtx = fail.math.Matrix44f()

		self.frame.LocalToParent = mtx
		self.frame.LocalToParent.position.y = -8
		
		input.InputManager.GetInstance().Actions.Left:bindHandler( function( pressed ) self:MoveLeft( pressed ) end )
		input.InputManager.GetInstance().Actions.Right:bindHandler( function( pressed ) self:MoveRight( pressed ) end )
		input.InputManager.GetInstance().Actions.Forward:bindHandler( function( pressed ) self:MoveForward( pressed ) end )
		input.InputManager.GetInstance().Actions.Backward:bindHandler( function( pressed ) self:MoveBackward( pressed ) end )

		input.InputManager.GetInstance().Axes.CamHeading:bindHandler( function( value ) self:Heading( value ) end )
		input.InputManager.GetInstance().Axes.CamPitch:bindHandler( function( value ) self:Pitch( value ) end )

		self.strafing = 0
		self.forwardmove = 0
		self.speed = 10
		self.heading = 0
		self.pitch = 0
		self.bMouseLook = false
		self.NeedUpdate = needupdatefunc
	end
}

function Camera:update( deltatime )
	local position = self.frame.LocalToParent.position
	
	strafingvec = self.frame.LocalToParent.right
	forwardvec = self.frame.LocalToParent.forward

	position.x = position.x + ( strafingvec.x * self.strafing + forwardvec.x * self.forwardmove ) * self.speed * deltatime / 1000;
	position.y = position.y + ( strafingvec.y * self.strafing + forwardvec.y * self.forwardmove ) * self.speed * deltatime / 1000;
	position.z = position.z + ( strafingvec.z * self.strafing + forwardvec.z * self.forwardmove ) * self.speed * deltatime / 1000;

	self.frame:dirty()
	
	return self.strafing ~= 0 or self.forwardmove ~= 0
end

function Camera:setMouseLook( bMouseLook )
	self.bMouseLook = bMouseLook
end

function Camera:recalcMatrix()
	local mtxh = fail.math.Matrix44f()
	mtxh:rotation( self.heading, fail.math.Vector3f( 0, 0, 1 ) )

	local mtxp = fail.math.Matrix44f()
	mtxp:rotation( self.pitch, fail.math.Vector3f( 1, 0, 0 ) )
	mtxh:multiplyBy( mtxp )

	local position = self.frame.LocalToParent.position
	mtxh.position.x = position.x
	mtxh.position.y = position.y
	mtxh.position.z = position.z
	
	self.frame.LocalToParent = mtxh
		self.frame:dirty()
	self.NeedUpdate()
end

function Camera:Heading( value )
	if( not self.bMouseLook ) then return end
	self.heading = self.heading - ( value / 4 )
	self:recalcMatrix()
end

function Camera:Pitch( value )
	if( not self.bMouseLook ) then return end
	self.pitch = self.pitch - ( value / 4 )
	self:recalcMatrix()
end

function Camera:MoveLeft( pressed )
	if( pressed ) then
		self.strafing = -1
	else
		self.strafing = 0
	end
	
	self.NeedUpdate()
end

function Camera:MoveRight( pressed )
	if( pressed ) then
		self.strafing = 1
	else
		self.strafing = 0
	end
	
	self.NeedUpdate()
end

function Camera:MoveForward( pressed )
	if( pressed ) then
		self.forwardmove = 1
	else
		self.forwardmove = 0
	end
	
	self.NeedUpdate()
end

function Camera:MoveBackward( pressed )
	if( pressed ) then
		self.forwardmove = -1
	else
		self.forwardmove = 0
	end
	
	self.NeedUpdate()
end
