#!${CMAKE_INSTALL_PREFIX}/bin/fail-lua
--[[
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

--package.path = package.path .. '${LUA_PACKAGE_PATH}'

Settings = {}

require 'fail'
require 'fail.math'
sg = require 'fail.scenegraph'
require 'fail.scenegraph.shapes'
require 'fail.gamestorage'
require 'fail.io'
require 'fail.vfs'
require 'fail.fbffile'
input = require 'fail.game.input'
require 'fail.game.keyboard'
require 'fail.game.mouse'
require 'qtcore'
require 'qtgui'
require 'qtopengl'
require 'fail.viewer'

input.Action( "Left", "Move left" )
input.Action( "Right", "Move right" )
input.Action( "Forward", "Move forward" )
input.Action( "Backward", "Move backward" )
input.Action( "MouseLook", "Enable mouse look" )

input.Axis( "CamHeading", "Camera rotation around the vertical axis" )
input.Axis( "CamPitch", "Camera rotation around the horizontal axis" )


qtapp = QApplication( 1 + select( '#', ... ), { arg[0], ...} )
qtapp.__gc = qtapp.delete
	
input.InputManager.GetInstance():bindAction( "Left", "keyboard", "Left" )
input.InputManager.GetInstance():bindAction( "Right", "keyboard", "Right" )
input.InputManager.GetInstance():bindAction( "Forward", "keyboard", "Up" )
input.InputManager.GetInstance():bindAction( "Backward", "keyboard", "Down" )

input.InputManager.GetInstance():bindAction( "Left", "keyboard", "J" )
input.InputManager.GetInstance():bindAction( "Right", "keyboard", "L" )
input.InputManager.GetInstance():bindAction( "Forward", "keyboard", "I" )
input.InputManager.GetInstance():bindAction( "Backward", "keyboard", "K" )

-- We don't seem to get released events from those for some reasons.
-- So no strafing with the side mouse buttons for now like I do in WoW. QQ
--input.InputManager.GetInstance():bindAction( "Left", "mouse", "XButton1" )
--input.InputManager.GetInstance():bindAction( "Right", "mouse", "XButton2" )

input.InputManager.GetInstance():bindAxis( "CamHeading", "mouse", "MouseX" )
input.InputManager.GetInstance():bindAxis( "CamPitch", "mouse", "MouseY" )
input.InputManager.GetInstance():bindAction( "MouseLook", "mouse", "RightButton" )

lasttime = 0
time = QTime()
time:start()

-- The current game scene (which can be the main menu, a game level, a mini game or basically anything occupying the rendering and UI)
-- will just provide a QGraphicsScene that we'll set as the current scene of the top level QGraphicsView. The actual fail engine action
-- shall be rendered with an overloaded drawBackground function, the QGraphicsItems should be used only for UI and hud elements.
-- The QGraphicsView used will always use a QGLWidget as its viewport.

-- Setup the main window, a QGraphicsView displaying our QGraphicsScene.
gv = QGraphicsView()

gv:setViewportUpdateMode( "FullViewportUpdate" )
gv:setCacheMode( {"CacheNone"} )
gv:setViewport( QGLWidget() )

qtscene = QGraphicsScene()

-- Create camera
cam = fail.viewer.Camera( function() lasttime = time:elapsed() qtscene:update() end )

-- Create viewport
viewport = sg.ViewPort( sg.PerspectiveProjection(), cam.camera )
viewport.Rect = fail.math.Rectu32( 0, 0, 1024, 768 )

renderpass = sg.RenderPass( viewport )
renderpass.bClearDepthBuffer = true
renderpass.bClearColorBuffer = true

print( "loading " .. arg[1] )
--scene = fail.fbffile.Reader.Load( arg[1] )
scene = fail.vfs.VFS.GetInstance():loadObject( arg[1] )
print "ok"

renderable = scene:getObject( "renderable_root" )
--frame = scene:getObject( "frame_root" )

print( renderable )
--print( frame )

function mouselook( pressed )
	cam:setMouseLook( pressed )
end

input.InputManager.GetInstance().Actions.MouseLook:bindHandler( mouselook )


function render()
	local deltatime = time:elapsed() - lasttime
	lasttime = lasttime + deltatime
	
	if( cam:update( deltatime ) ) then
		QTimer.singleShot( 0, qtscene, "1update()" )
	end

	sg.RenderPass.ClearAll()
	renderpass:prependToRenderList()
	renderable:evaluate( renderpass )
	sg.RenderPass.RenderAll( true )
end


qtscene.drawBackground = render
gv:setScene( qtscene )

fail.game.keyboard.Provider( gv )
fail.game.mouse.Provider( gv )

gv:show()
gv:resize(1024, 768)

qtapp.exec()
