/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "io/FileInputStream.h"
#include <stdexcept>

using namespace fail;
using namespace fail::io;

FileInputStream::FileInputStream( const std::string& Name_ ) :
	m_pFile( PHYSFS_openRead( Name_.c_str() ) ),
	m_EndPos( 0 )
{
	if( !m_pFile )
	{
		std::stringstream msg;
		msg << "FileInputStream: can't open file '" << Name_ << "'.";
		throw std::runtime_error( msg.str().c_str() );
	}
}

FileInputStream::~FileInputStream()
{
	PHYSFS_close( m_pFile );
}

unsigned int FileInputStream::read( void* pBuffer_, unsigned int Size_ )
{
	uint64_t pos = PHYSFS_tell( m_pFile );
	
	if( m_EndPos && pos >= m_EndPos )
		return 0;
	
	if( m_EndPos && ( pos + Size_ ) > m_EndPos )
		Size_ = m_EndPos - pos;
	
	return PHYSFS_read( m_pFile, pBuffer_, 1, Size_ );
}

void FileInputStream::seek( uint32_t Pos_ )
{
	if( m_EndPos && Pos_ > m_EndPos )
		Pos_ = m_EndPos;

	PHYSFS_seek( m_pFile, Pos_ );
}

void FileInputStream::seek( uint64_t Pos_ )
{
	if( m_EndPos && Pos_ > m_EndPos )
		Pos_ = m_EndPos;
	
	PHYSFS_seek( m_pFile, Pos_ );
}

uint64_t FileInputStream::tell() const
{
	return PHYSFS_tell( m_pFile );
}

void FileInputStream::setReadLimit( uint64_t EndPos )
{
	m_EndPos = EndPos;
}
