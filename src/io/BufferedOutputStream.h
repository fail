/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IO_BUFFEREDOUTPUTSTREAM_H_
#define FAIL_IO_BUFFEREDOUTPUTSTREAM_H_

#include "core/core.h"
#include "OutputStream.h"
#include "SeekableOutputStream.h"
#include <cstring>

namespace fail { namespace io
{
	// TODO: The buffer size is a compile time constant: this should allows the compiler to fold 
	// most of the "is the buffer full yet?" tests and just generate the actually necessary calls
	// to flush in the scenario of the code writing a bunch of small things back to back.
	// Premature optimisation?
	// possibly. The generated serialization code from fbf will have to be checked to see if
	// it actually turns out like that.
	template< class EndianessPolicy = endianess::Native, int BufferSize = 4096,
	 		class StreamType = OutputStream >
	class BufferedOutputStream
	{
		public:
			typedef EndianessPolicy endianess_policy;
			
			BufferedOutputStream( shared_ptr< StreamType > pOutStream_ ) :
				m_pOutStream( pOutStream_ )
			{
				m_WriteIndex = 0;
				m_TotalWritten = 0;
			}
			
			~BufferedOutputStream()
			{
				flush();
			}

			const shared_ptr< StreamType >& getOutStream() const { return m_pOutStream; }
			void setOutStream( const shared_ptr< StreamType >& s ) { m_pOutStream = s; }
			
			unsigned int used() const
			{
				return m_WriteIndex;
			}
			
			unsigned int remaining() const
			{
				return BufferSize - used();
			}
			
			uint32_t totalWritten() const
			{
				return m_TotalWritten;
			}
			
			void flush()
			{
				if( used() )
				{
					m_pOutStream->write( m_Buffer, used() );
					m_WriteIndex = 0;
				}
			}

			void write( const void* pBuffer, unsigned int Size_ )
			{
				if( Size_ > remaining() )
					flush();
				
				if( Size_ > BufferSize )
					m_pOutStream->write( pBuffer, Size_ );
				else
				{
					// I am ashamed by the usage of memcpy. The idea here is that
					// the compiler might have a builtin implementation. This stuff
					// will likely end up being called a gazillion times for each bit
					// of data stored in serialized objects so it will directly affect
					// loading/saving performances.
					// FIXME: premature optimisation again?					
					std::memcpy( &m_Buffer[ m_WriteIndex ], pBuffer, Size_ );
					m_WriteIndex += Size_;
				}
				
				m_TotalWritten += Size_;
			}
			
			void writeEndianessHeader()
			{
				uint8_t eh = EndianessPolicy::type;
				writeU8( eh );
			}
			
			void writeU8( uint8_t Val_ )
			{
				write( &Val_, sizeof( Val_ ) );
			}
			
			void writeS8( int8_t Val_ )
			{
				write( &Val_, sizeof( Val_ ) );
			}
			
			void writeU16( uint16_t Val_ )
			{
				EndianessPolicy::FixEndianessU16( Val_ );
				write( &Val_, sizeof( Val_ ) );
			}
			
			void writeS16( int16_t Val_ )
			{
				EndianessPolicy::FixEndianessS16( Val_ );
				write( &Val_, sizeof( Val_ ) );
			}
								
			void writeU32( uint32_t Val_ )
			{
				EndianessPolicy::FixEndianessU32( Val_ );
				write( &Val_, sizeof( Val_ ) );
			}
			
			void writeS32( int32_t Val_ )
			{
				EndianessPolicy::FixEndianessS32( Val_ );
				write( &Val_, sizeof( Val_ ) );
			}
			
			void writeU64( uint64_t Val_ )
			{
				EndianessPolicy::FixEndianessU64( Val_ );
				write( &Val_, sizeof( Val_ ) );
			}
						
			void writeS64( int64_t Val_ )
			{
				EndianessPolicy::FixEndianessS64( Val_ );
				write( &Val_, sizeof( Val_ ) );
			}
			
			void writeFloat( float Val_ )
			{
				EndianessPolicy::FixEndianessFloat( Val_ );
				write( &Val_, sizeof( Val_ ) );			
			}
						
			void writeDouble( double Val_ )
			{
				EndianessPolicy::FixEndianessDouble( Val_ );
				write( &Val_, sizeof( Val_ ) );			
			}
			
			void writeString( const char* pStr_ )
			{
				int32_t len = strlen( pStr_ );
				writeVarLenInt( len );
				write( pStr_, len );
			}
			
			// Is this shit really useful? I'm wondering.
			void writeVarLenInt( int32_t Val_ )
			{
				uint8_t out = 0;

				if( Val_ < 0 )
				{
					Val_ = -Val_;
					out = 0x40;
				}
				
				out |= Val_ & 0x3f;
				Val_ >>= 6;
								
				while( Val_ )
				{
					out |= 0x80;
					writeU8( out );
					out = Val_ & 0x7f;
					Val_ >>= 7;
				}
				
				writeU8( out );
			}
			
		protected:
			shared_ptr< StreamType >	m_pOutStream;
			
			uint32_t	m_WriteIndex;
			uint32_t	m_TotalWritten;
			uint8_t 	m_Buffer[ BufferSize ];
	};
	
	template< class EndianessPolicy = endianess::Native, int BufferSize = 4096 >
	class BufferedSeekableOutputStream :
			public BufferedOutputStream< EndianessPolicy, BufferSize, SeekableOutputStream >
	{
		typedef BufferedOutputStream< EndianessPolicy, BufferSize, SeekableOutputStream >
				base;

		public:
			BufferedSeekableOutputStream( SeekableOutputStream* pOutStream_ ) :
				base( pOutStream_ )
			{
			}
			
			void seek( uint32_t Pos_ )
			{
				base::flush();
				base::m_pOutStream->seek( Pos_ );
			}
			
			void seek( uint64_t Pos_ )
			{
				base::flush();
				base::m_pOutStream->seek( Pos_ );
			}
			
			uint64_t tell() const
			{
				return base::m_pOutStream->tell() + base::m_WriteIndex;
			}
	};
}}

#endif
