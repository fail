/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "io/FileOutputStream.h"
#include <stdexcept>

using namespace fail;
using namespace fail::io;

FileOutputStream::FileOutputStream( const std::string& Name_ ) :
	m_pFile( PHYSFS_openWrite( Name_.c_str() ) )
{
	// TODO: define proper exception classes, but this is better
	// than nothing for now.
	//std::cout << "pfile: " << m_pFile << std::endl;
	
	if( !m_pFile )
	{
		std::stringstream msg;
		msg << "FileOutputStream: can't open file '" << Name_ << "'.";
		throw std::runtime_error( msg.str().c_str() );
	}
}

FileOutputStream::~FileOutputStream()
{
	PHYSFS_close( m_pFile );
}

unsigned int FileOutputStream::write( const void* pBuffer_, unsigned int Size_ )
{
	return PHYSFS_write( m_pFile, pBuffer_, 1, Size_ );
}

void FileOutputStream::seek( uint32_t Pos_ )
{
	PHYSFS_seek( m_pFile, Pos_ );
}

void FileOutputStream::seek( uint64_t Pos_ )
{
	PHYSFS_seek( m_pFile, Pos_ );
}

uint64_t FileOutputStream::tell() const
{
	return PHYSFS_tell( m_pFile );
}
