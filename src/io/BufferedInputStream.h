/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IO_BUFFEREDINPUTSTREAM_H_
#define FAIL_IO_BUFFEREDINPUTSTREAM_H_

#include "core/core.h"
#include "InputStream.h"
#include "SeekableInputStream.h"

namespace fail { namespace io
{
	// TODO: see comment in BufferedOutputBuffer regarding templates, compile-time buffer size and
	// related neat optimizations.
	template< class EndianessPolicy = endianess::Native, int BufferSize = 4096,
	 		class StreamType = InputStream >
	class BufferedInputStream
	{
		public:
			typedef EndianessPolicy endianess_policy;			
			
			BufferedInputStream( shared_ptr< StreamType > pInStream_ ) :
				m_pInStream( pInStream_ )
			{
				m_ReadIndex = 0;
				m_EndIndex = 0;
				
				m_TotalRead = 0;
			}

			const shared_ptr< StreamType >& getInStream() const { return m_pInStream; }
			void setInStream( const shared_ptr< StreamType >& s ) { m_pInStream = s; }
			
			unsigned int available() const
			{
				return m_EndIndex - m_ReadIndex;
			}
			
			unsigned int remaining() const
			{
				return BufferSize - m_EndIndex;
			}
			
			uint32_t totalRead() const
			{
				return m_TotalRead;
			}

			unsigned int read( void* pBuffer, uint32_t Size_ )
			{
				unsigned int amountread = 0;

				if( available() )
				{
					amountread = Size_ > available() ? available() : Size_;
					std::memcpy( pBuffer, &m_Buffer[ m_ReadIndex ], amountread );
					Size_ -= amountread;

					m_ReadIndex += amountread;
					if( m_ReadIndex == m_EndIndex )
						m_ReadIndex = m_EndIndex = 0;
				}

				if( Size_ )
				{
					uint8_t* pbuf = static_cast< uint8_t* >( pBuffer );
					amountread += m_pInStream->read( pbuf + amountread, Size_ );
				}

				if( !available() )
				{
					unsigned int read = m_pInStream->read( &m_Buffer[ m_EndIndex ], remaining() );
					m_EndIndex += read;
				}

				m_TotalRead += amountread;

				return amountread;
			}
			
			uint8_t readU8()
			{
				uint8_t tmp;
				read( &tmp, sizeof( tmp ) );
				return tmp;
			}
			
			uint16_t readU16()
			{
				uint16_t tmp;
				read( &tmp, sizeof( tmp ) );
				EndianessPolicy::FixEndianessU16( tmp );
				return tmp;
			}

			uint32_t readU32()
			{
				uint32_t tmp;
				read( &tmp, sizeof( tmp ) );
				EndianessPolicy::FixEndianessU32( tmp );
				return tmp;
			}

			uint64_t readU64()
			{
				uint64_t tmp;
				read( &tmp, sizeof( tmp ) );
				EndianessPolicy::FixEndianessU64( tmp );
				return tmp;
			}
			
			float readFloat()
			{
				float tmp;
				read( &tmp, sizeof( tmp ) );
				EndianessPolicy::FixEndianessFloat( tmp );
				return tmp;
			}
			
			float readDouble()
			{
				double tmp;
				read( &tmp, sizeof( tmp ) );
				EndianessPolicy::FixEndianessDouble( tmp );
				return tmp;
			}
			
			void readString( std::string& Str_ )
			{
				int32_t len = readVarLenInt();
				
				Str_ = "";
				Str_.resize( len );
				read( &Str_[0], len );
			}
			
			// Is this shit really useful? I'm wondering.
			int32_t readVarLenInt()
			{
				uint8_t shift = 6;
				uint8_t in;
				in = readU8();

				bool bNeg = !!( in & 0x40 );

				int32_t val = in & 0x3f;

				while( in & 0x80 )
				{
					in = readU8();
					uint32_t newpart = ( in & 0x7f ) << shift;
					val |= newpart;
					shift += 7;
				}

				if( bNeg )
					val = -val;

				return val;
			}
			
		protected:
			// This is protected because it basically trash data away. On a non-seekable stream it means
			// you just loose whatever was left in the buffer forever. This is mostly useful for BufferedSeekableInputStream anyway. 
			void discardbuffer()
			{
				m_ReadIndex = m_EndIndex = 0;
			}

			shared_ptr< StreamType >	m_pInStream;
			
			uint32_t	m_ReadIndex;
			uint32_t	m_EndIndex;
			uint32_t	m_TotalRead;
			uint8_t		m_Buffer[ BufferSize ];
	};
	
	template< class EndianessPolicy = endianess::Native, int BufferSize = 4096 >
	class BufferedSeekableInputStream :
			public BufferedInputStream< EndianessPolicy, BufferSize, SeekableInputStream >
	{
		typedef BufferedInputStream< EndianessPolicy, BufferSize, SeekableInputStream >
				base;
		public:
			BufferedSeekableInputStream( SeekableInputStream* pInStream_ ) :
				base( pInStream_ )
			{
			}
			
			void seek( uint32_t Pos_ )
			{
				base::discardbuffer();
				base::m_pInStream->seek( Pos_ );
			}
			
			void seek( uint64_t Pos_ )
			{
				base::discardbuffer();
				base::m_pInStream->seek( Pos_ );
			}
			
			uint64_t tell() const
			{
				return base::m_pInStream->tell() - base::available();
			}
	};
}}

#endif
