/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IO_SEEKABLEINPUTSTREAM_H_
#define FAIL_IO_SEEKABLEINPUTSTREAM_H_

#include "core/core.h"
#include "io/io_export.h"
#include "InputStream.h"
#include "Seekable.h"

namespace fail { namespace io
{
	class FLIO_EXPORT SeekableInputStream : public InputStream, public Seekable
	{
		public:
			// Make it so EndPos is considered the end of the stream, even
			// if it actually ends beyond that. Setting it to 0 resets it
			// to its normal behavior.
			// This is useful to prevent BufferInputStream to read beyond
			// the chunk we're currently interested in in scenarios like FBF
			// where from the client point of view, a different stream is provided
			// for each chunk.
			virtual void setReadLimit( uint64_t EndPos ) = 0;
	};
}}

#endif
