/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "io/MemoryOutputStream.h"
#include <cstring>

using namespace fail;
using namespace fail::io;

MemoryOutputStream::MemoryOutputStream() :
	m_Position( 0 )
{
}

unsigned int MemoryOutputStream::write( const void* pBuffer, unsigned int Size )
{
	if( m_Buffer.size() < ( m_Position + Size ) )
	{
		m_Buffer.reserve( m_Position + Size + 4096 );
		m_Buffer.resize( m_Position + Size );
	}
	
	std::memcpy( &m_Buffer[ m_Position ], pBuffer, Size );
	m_Position += Size;
	
	return Size;
}

void MemoryOutputStream::seek( uint32_t Pos )
{
	m_Position = Pos;
}

void MemoryOutputStream::seek( uint64_t Pos )
{
	m_Position = Pos;
}

uint64_t MemoryOutputStream::tell() const
{
	return m_Position;
}
