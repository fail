/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IO_MEMORYOUTPUTSTREAM_H_
#define FAIL_IO_MEMORYOUTPUTSTREAM_H_

#include "core/core.h"
#include "io/io_export.h"
#include "SeekableOutputStream.h"
#include <physfs.h>

namespace fail { namespace io
{
	class FLIO_EXPORT MemoryOutputStream : public SeekableOutputStream
	{
		public:		
			MemoryOutputStream();
			virtual unsigned int write( const void* pBuffer_, unsigned int Size_ );
			
			virtual void seek( uint32_t Pos_ );
			virtual void seek( uint64_t Pos_ );
			virtual uint64_t tell() const;
			
			const void* getBuffer() const
			{
				return &m_Buffer[0];
			}
			
			uint32_t getSize() const
			{
				return m_Buffer.size();
			}
			
		private:
			std::vector< uint8_t >	m_Buffer;
			uint64_t				m_Position;
	};
}}

#endif
