/*
    Fail game engine
    Copyright 2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "VFS.h"
#include "DirectFS.h"

using namespace fail;
using namespace fail::vfs;

shared_ptr< VFS > VFS::ms_pInstance;

VFS::VFS() :
	m_pDefaultFS( new DirectFS )
{
}

void VFS::registerLoader( string extension, shared_ptr< Loader_i > pLoader )
{
	m_Loaders.insert( make_pair( extension, pLoader ) );
}

void VFS::registerFileSystem( string name, shared_ptr< FileSystem_i > pFS )
{
	m_FileSystems.insert( make_pair( name, pFS ) );
}

shared_ptr< Serializable > VFS::loadObject( string path )
{
	string::size_type dotpos = path.rfind( "." );
	
	if( dotpos == string::npos )
		return shared_ptr< Serializable >();
	
	string extension( path, dotpos + 1, path.size() );
	shared_ptr< Loader_i > pLoader = getLoader( extension );
	
	if( !pLoader )
		return shared_ptr< Serializable >();
	
	shared_ptr< io::SeekableInputStream > pStream = openRead( path );
	
	if( !pStream )
		return shared_ptr< Serializable >(); 
	
	return pLoader->load( pStream );
}

shared_ptr< io::SeekableInputStream > VFS::openRead( string path )
{
	std::string fsname;
	std::string filename;
	
	splicePath( path, fsname, filename );
	
	shared_ptr< FileSystem_i > pFS = getFileSystem( fsname );
	if( !pFS )
		return shared_ptr< io::SeekableInputStream >();
	
	return pFS->openRead( filename );
}

shared_ptr< io::SeekableOutputStream > VFS::openWrite( string path )
{
	std::string fsname;
	std::string filename;
	
	splicePath( path, fsname, filename );
	
	shared_ptr< FileSystem_i > pFS = getFileSystem( fsname );
	if( !pFS )
		return shared_ptr< io::SeekableOutputStream >();
	
	return pFS->openWrite( filename );
}

void VFS::splicePath( const string& path, string& out_fsname, string& out_filename )
{
	string::size_type colonpos = path.find( ":" );
	if( colonpos == string::npos )
	{
		out_fsname = "";
		out_filename = path;
		return;
	}
	
	out_fsname.assign( path, 0, colonpos );
 	out_filename.assign( path, colonpos + 1, path.size() );
}

shared_ptr< Loader_i > VFS::getLoader( const string& extension ) const
{
	unordered_map< string, shared_ptr< Loader_i > >::const_iterator it;
	
	it = m_Loaders.find( extension );
	if( it == m_Loaders.end() )
		return shared_ptr< Loader_i >();
	
	return it->second;
}


shared_ptr< FileSystem_i > VFS::getFileSystem( const string& fsname ) const
{
	if( fsname.empty() )
		return m_pDefaultFS;

	unordered_map< string, shared_ptr< FileSystem_i > >::const_iterator it;
	
	it = m_FileSystems.find( fsname );
	if( it == m_FileSystems.end() )
		return shared_ptr< FileSystem_i >();
	
	return it->second;
}
