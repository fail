/*
    Fail game engine
    Copyright 2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_VFS_VFS_H_
#define FAIL_VFS_VFS_H_

#include "core/core.h"
#include "Loader_i.h"
#include "FileSystem_i.h"
#include "vfs/vfs_export.h"

namespace fail { namespace vfs
{
	class FLVFS_EXPORT VFS
	{
		public:
			static shared_ptr< VFS > GetInstance()
			{
				if( !ms_pInstance )
					ms_pInstance = shared_ptr< VFS >( new VFS );
				return ms_pInstance;
			}
		
			void registerLoader( string extension, shared_ptr< Loader_i > pLoader );
			void registerFileSystem( string name, shared_ptr< FileSystem_i > pFS );
			
			shared_ptr< Serializable > loadObject( string path );
			shared_ptr< io::SeekableInputStream > openRead( string path );
			shared_ptr< io::SeekableOutputStream > openWrite( string path );
		
		private:
			VFS();
			static void splicePath( const string& path, string& out_fsname, string& out_filename );
			shared_ptr< Loader_i > getLoader( const string& extension ) const;
			shared_ptr< FileSystem_i > getFileSystem( const string& fsname ) const;
			
			static shared_ptr< VFS > ms_pInstance;
			
			unordered_map< string, shared_ptr< Loader_i > >		m_Loaders;
			unordered_map< string, shared_ptr< FileSystem_i > >	m_FileSystems;
			shared_ptr< FileSystem_i >							m_pDefaultFS;
	};
}}

#endif
