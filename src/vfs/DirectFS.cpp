/*
    Fail game engine
    Copyright 2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "DirectFS.h"
#include "io/FileInputStream.h"
#include "io/FileOutputStream.h"

using namespace fail;
using namespace fail::vfs;

DirectFS::DirectFS( string BasePath ) :
	m_BasePath( BasePath )
{
}

shared_ptr< io::SeekableInputStream > DirectFS::openRead( string filename )
{
	string fullpath = m_BasePath + filename;
	return shared_ptr< io::SeekableInputStream >( new io::FileInputStream( fullpath ) );
}

shared_ptr< io::SeekableOutputStream > DirectFS::openWrite( string filename )
{
	string fullpath = m_BasePath + filename;
	return shared_ptr< io::SeekableOutputStream >( new io::FileOutputStream( fullpath ) );
}
