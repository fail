/*
    Fail game engine
    Copyright 2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_VFS_DIRECTFS_I_H_
#define FAIL_VFS_DIRECTFS_I_H_

#include "FileSystem_i.h"

namespace fail { namespace vfs
{
	class FLVFS_EXPORT DirectFS : public FileSystem_i
	{
		public:
			DirectFS( string BasePath = "" );
			
			virtual shared_ptr< io::SeekableInputStream > openRead( string filename );
			virtual shared_ptr< io::SeekableOutputStream > openWrite( string filename );
			
		private:
			string	m_BasePath;
	};
}}

#endif
