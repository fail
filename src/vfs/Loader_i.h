/*
    Fail game engine
    Copyright 2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_VFS_LOADER_I_H_
#define FAIL_VFS_LOADER_I_H_

#include "core/core.h"
#include "io/SeekableInputStream.h"
#include "vfs/vfs_export.h"

namespace fail { namespace vfs
{
	class FLVFS_EXPORT Loader_i
	{
		public:
			virtual shared_ptr< Serializable > load( shared_ptr< io::SeekableInputStream > pStream ) = 0;
	};
}}

#endif
