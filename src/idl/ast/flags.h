/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IDLAST_FLAGS_H_
#define FAIL_IDLAST_FLAGS_H_

namespace fail { namespace idlast
{
	typedef map< string, bool > Flags;

	class Flaggable
	{
		public:			
			const Flags& getFlags() const
			{
				return m_Flags;
			}
			
			void setFlags( const Flags& NewFlags )
			{
				m_Flags = NewFlags;
			}

			void setFlag( const char* pFlagName, bool bValue )
			{
				m_Flags[ pFlagName ] = bValue;
			}
			
			bool isFlagDefined( const char* pFlagName ) const
			{
				return m_Flags.find( pFlagName ) != m_Flags.end();
			}
			
			bool testFlag( const char* pFlagName, bool bDefault ) const
			{
				Flags::const_iterator it = m_Flags.find( pFlagName );
				if( it == m_Flags.end() )
					return bDefault;
				
				return it->second;
			}
			
		private:
			Flags m_Flags;
	};
}}

#endif
