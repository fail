/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IDLAST_ENUM_H_
#define FAIL_IDLAST_ENUM_H_

namespace fail { namespace idlast
{
	class Enum : public Named, public Flaggable, public Element
	{
		friend class Class;
		
		public:
 			Enum( std::string Name_, std::string FileName_, unsigned long Line_ ) :
				Named( Name_ ),
				Element( FileName_, Line_ )
			{
				//std::cout << "new method '" << Name_ << "'\n";
			}
			
			shared_ptr< const Class > getClass() const
			{
				return m_pClass.lock();
			}

			typedef std::set< std::string > ValueSet;
			bool addValue( std::string ValueName_ );
			
			const ValueSet& getValues() const
			{
				return m_Values;
			}

		private:
			weak_ptr< Class >		m_pClass;
			ValueSet				m_Values;
	};	
}}

#endif
