/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IDLAST_CLASSREF_H_
#define FAIL_IDLAST_CLASSREF_H_

namespace fail { namespace idlast
{
	class Namespace;
	class Class;
	
	typedef set< const Class* > DependencyList;
	
	class ClassRef : public Element, public shared_ptr< Class >
	{
		public:
			ClassRef( string FileName_, unsigned long Line_ ) :
				Element( FileName_, Line_ )
			{
			//	std::cout << "new classref " << this << std::endl;
			}
			
			void addComponent( string Component_ )
			{
			//	std::cout << "add component " << Component_ << " to classref " << this << std::endl;
				m_NameComponents.push_back( Component_ );
			}
			
			const ClassRef& operator=( shared_ptr< Class > pClass )
			{
				this->shared_ptr< Class >::operator=( pClass );
				return *this;
			}
			
			void resolve( shared_ptr< const Class > pClass_, DependencyList& DepList_, bool bStruct_ );
			
		private:
			vector< string >	m_NameComponents;
	};
}}

#endif
