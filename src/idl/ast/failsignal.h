/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IDLAST_FAILSIGNAL_H_
#define FAIL_IDLAST_FAILSIGNAL_H_

namespace fail { namespace idlast
{
	class Signal : public Named, public Flaggable, public Element
	{
		public:
 			Signal( std::string Name_, std::string FileName_, unsigned long Line_ ) :
				Named( Name_ ),
				Element( FileName_, Line_ )
			{
				//std::cout << "new method '" << Name_ << "'\n";
			}

			typedef std::vector< std::shared_ptr< Type > > paramlist_type;

			void addParamType( shared_ptr< Type > pType_ )
			{
				m_ParamTypes.push_back( pType_ );
			}
	
			const paramlist_type& getParamTypes() const
			{
				return m_ParamTypes;
			}
			
			void resolveRefs( shared_ptr< const Class > pClass_, DependencyList& DepList_ ) const;
			
		private:
			paramlist_type	m_ParamTypes;
	};	
}}

#endif
