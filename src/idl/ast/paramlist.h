/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IDLAST_PARAMLIST_H_
#define FAIL_IDLAST_PARAMLIST_H_

namespace fail { namespace idlast
{
	class ParamList : public Element,
					  public vector< pair< string, shared_ptr< const Type > > >
	{
		typedef vector< pair< string, shared_ptr< const Type > > > vector_type;
		
		public:
			ParamList( shared_ptr< const Type > pReturnType_, string FileName_, unsigned long Line_ ) :
				Element( FileName_, Line_ ),
				m_pReturnType( pReturnType_ ),
				m_bConst( false )
			{
			}
			
			void resolveRefs( shared_ptr< const Class > pClass_, DependencyList& DepList_ ) const;
			
			shared_ptr< const Type > getReturnType() const
			{
				return m_pReturnType;
			}
			
			void setConst()
			{
				m_bConst = true;
			}
			
			bool isConst() const
			{
				return m_bConst;
			}
			
		private:
			shared_ptr< const Type >	m_pReturnType;
			bool							m_bConst;
	};

	typedef multimap< int, shared_ptr< const ParamList > > Overloads;
}}

#endif
