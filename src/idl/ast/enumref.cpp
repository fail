/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ast.h"

using namespace fail;
using namespace fail::idlast;

void EnumRef::resolve( shared_ptr< const Class > pClass_ )
{
	// If the name is unqualified, just look it up in the current class
	if( m_NameComponents.size() == 1 )
	{
		*this = pClass_->getEnums().get( m_NameComponents[0] );
	}
	else
	{	
		// Look up for the first namespace up from the class' namespace.
		// Any following namespace name will be looked up only in the current namespace (and will
		// become the new current namespace)
		shared_ptr< const Namespace > pCurrentNamespace = pClass_->getNamespace();
		pClass_ = shared_ptr< const Class >();
		vector< string >::const_iterator it = m_NameComponents.begin();
		
		// Go through intermediate namespace names, if any		
		if( m_NameComponents.size() > 2 )
		{
			pCurrentNamespace = pCurrentNamespace->findNamespace( *it );
			++it;

			vector< string >::const_iterator endit =
				m_NameComponents.begin() + m_NameComponents.size() - 2;
			
			for( ; it != endit && pCurrentNamespace; ++it )
			{
				pCurrentNamespace = pCurrentNamespace->getNamespaces().get( *it );
			}
		}
		
		// The penultimate component of the reference (currently pointed to by it)
		// should be either a class or a struct name.
		// Look for it.
		if( pCurrentNamespace )
		{
			pClass_ = pCurrentNamespace->getClasses().get( *it );
			if( !pClass_ )
				// No luck with classes, try the structs
				pClass_ = pCurrentNamespace->getStructs().get( *it );
		}
	
		if( pClass_ )
			*this = pClass_->getEnums().get( m_NameComponents.back() );
	}
	
	if( *this == NULL )
	{
		stringstream msg;
		msg << "undefined enum '";
 		vector< string >::const_iterator it;
		for( it = m_NameComponents.begin(); it != m_NameComponents.end(); ++it )
		{
			if( it != m_NameComponents.begin() )
				msg << "::"; 
			msg << *it;
		}
		
		msg << "'.";
		
		throw ASTError( getFileName(), getLine(), msg.str() );
	}
}
