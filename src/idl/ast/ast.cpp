/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ast.h"

using namespace fail;
using namespace fail::idlast;

ASTError::ASTError( std::string Filename_, int LineNumber_, std::string Message_ ) :
	m_LineNumber( LineNumber_ ),
	m_Filename( Filename_ ),
	m_Message( Message_ )
{
	stringstream what;
	what << m_Filename << ':' << m_LineNumber << ": " << m_Message;
	m_What = what.str();
}
