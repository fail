/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IDLAST_DICTIONARY_H_
#define FAIL_IDLAST_DICTIONARY_H_

namespace fail { namespace idlast
{
	template< class C > class Dictionary : public map< string, shared_ptr< C > >
	{
		typedef map< string, shared_ptr< C > > map_type;

		public:
			bool add( shared_ptr< C > pObj_ )
			{
				pair< typename map_type::iterator, bool > res = insert( make_pair( pObj_->getName(), pObj_ ) );
				return res.second;
			}
			
			shared_ptr< C > get( string Name_ ) const
			{
				typename map_type::const_iterator it = this->find( Name_ );
				if( it == this->end() )
					return shared_ptr< C >();
				return it->second;
			}
	};
}}

#endif
