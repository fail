/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IDLAST_ATTRIBUTE_H_
#define FAIL_IDLAST_ATTRIBUTE_H_

#include <memory>
#include <list>
#include "named.h"
#include "type.h"
#include "flags.h"

namespace fail { namespace idlast
{
	class Attribute : public Named, public Flaggable, public Element
	{
		public:
			Attribute( string Name_, shared_ptr< Type > pType_, string FileName_, unsigned long Line_ ) :
				Named( Name_ ),
				Element( FileName_, Line_ ),
				m_pType( pType_ )
			{
				//std::cout << "new attribute '" << Name_ << "', type " << m_pType << std::endl;
			}

			void resolveRefs( shared_ptr< const Class > pClass_, DependencyList& DepList_ ) const
			{
				m_pType->resolveRefs( pClass_, DepList_ );
			}
			
			shared_ptr< const Type > getType() const
			{
				return m_pType;
			}

		private:
			shared_ptr< const Type >	m_pType;
	};
}}

#endif
