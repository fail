/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IDLAST_NAMESPACE_H_
#define FAIL_IDLAST_NAMESPACE_H_

namespace fail { namespace idlast
{
	typedef set< const Namespace* > NamespaceDependencySet;
	
	class Namespace : public Named, public Element, public enable_shared_from_this< Namespace >
	{
		public:
			Namespace( string Name_, string FileName_, unsigned long Line_ ) :
				Named( Name_ ),
				Element( FileName_, Line_ )
			{
			}
			
			shared_ptr< const Namespace > getParent() const
			{
				return m_pParent.lock();
			}
			
			shared_ptr< Namespace > getParent()
			{
				return m_pParent.lock();
			}

			shared_ptr< Namespace > addNamespace( shared_ptr< Namespace > pNamespace_ );
			
			bool addClass( shared_ptr< Class > pClass_ )
			{
				if( !m_Classes.add( pClass_ ) )
					return false;
				
				pClass_->m_pNamespace = shared_from_this();
				return true;
			}
			
			bool addStruct( shared_ptr< Class > pClass_ )
			{
				if( !m_Structs.add( pClass_ ) )
					return false;
				
				pClass_->m_pNamespace = shared_from_this();
				return true;
			}
			
			void addDependency( shared_ptr< const Namespace > pNamespace_ )
			{
				m_Dependencies.insert( pNamespace_.get() );
			}
						
			const Dictionary< Namespace >& getNamespaces() const
			{
				return m_Namespaces;
			}
			
			const Dictionary< Class >& getClasses() const
			{
				return m_Classes;
			}
			
			const Dictionary< Class >& getStructs() const
			{
				return m_Structs;
			}
			
			const NamespaceDependencySet& getDependencies() const
			{
				return m_Dependencies;
			}
			
			shared_ptr< Namespace > findNamespace( string Name_ ) const;
			
			typedef pair< shared_ptr< Class >, shared_ptr< const Namespace > > FindClassResult;
			FindClassResult findClass( string Class_, bool bStruct_ = false ) const;

			void resolveRefs() const;
				
		private:
			weak_ptr< Namespace >	m_pParent;
			Dictionary< Namespace >	m_Namespaces;
			Dictionary< Class >		m_Classes;
			Dictionary< Class >		m_Structs;
			NamespaceDependencySet	m_Dependencies;
	};
	
	class AnonNamespace : public Namespace
	{
		public:
			AnonNamespace() :
				Namespace( "", "", 0 )
			{
			}
	};
}}

#endif
