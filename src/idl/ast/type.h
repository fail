/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IDLAST_TYPE_H_
#define FAIL_IDLAST_TYPE_H_

#include <list>
#include "element.h"
#include "classref.h"
#include "enumref.h"

namespace fail { namespace idlast
{
	class Namespace;
	
	enum TypeID
	{
		e_bool,
		
		e_uint8_t,
		e_int8_t,
		e_uint16_t,
		e_int16_t,
		e_uint32_t,
		e_int32_t,
		e_uint64_t,
		e_int64_t,

		e_float,
		e_double,
		e_string,
  		e_DynamicBuffer,
  
		e_Pointer,
		e_WeakPointer,
		e_StructRef,
  
		e_vector,
		e_list,
		e_set,
		e_map,
		e_pair,
 
		e_signal,
		e_enum
	};
	
	class Type : public Element
	{
		friend class Namespace;

		public:
			Type( string FileName_, unsigned long Line_ ) :
				Element( FileName_, Line_ )
			{
			}
			
			virtual ~Type() {}

			virtual TypeID getID() const = 0;
			virtual void resolveRefs( shared_ptr< const Class > pClass_, DependencyList& DepList_ ) const {}
	};
	
	template< TypeID TypeID_ > class BasicType : public Type
	{
		public:
			BasicType( string FileName_, unsigned long Line_ ) :
				Type( FileName_, Line_ )
			{
				//cout << "new basictype " << TypeID_ << ' ' << this << endl;	
			}
			
			virtual TypeID getID() const
			{
				return TypeID_;
			}
	};
	
	typedef BasicType< e_bool > BoolType;
	
	typedef BasicType< e_uint8_t > U8Type;
	typedef BasicType< e_int8_t > S8Type;
	typedef BasicType< e_uint16_t > U16Type;
	typedef BasicType< e_int16_t > S16Type;
	typedef BasicType< e_uint32_t > U32Type;
	typedef BasicType< e_int32_t > S32Type;
	typedef BasicType< e_uint64_t > U64Type;
	typedef BasicType< e_int64_t > S64Type;

	typedef BasicType< e_float > FloatType;
	typedef BasicType< e_double > DoubleType;
	typedef BasicType< e_string > StringType;
	typedef BasicType< e_DynamicBuffer > DynamicBufferType;

	template< TypeID PointerTypeID_ > class BasicPointerType : public BasicType< PointerTypeID_ >
	{
		public:
			BasicPointerType( string FileName_, unsigned long Line_ ) :
				BasicType< PointerTypeID_ >( FileName_, Line_ ),
				m_bConst( false )
			{
			}

			void setClassRef( shared_ptr< ClassRef > pClassRef_ )
			{
				m_pClassRef = pClassRef_;
			}
			
			shared_ptr< ClassRef > getClassRef() const
			{
				return m_pClassRef;
			}

			void setConst( bool bConst ) { m_bConst = bConst; }
			bool getConst() const { return m_bConst; }
			
			virtual void resolveRefs( shared_ptr< const Class > pClass_, DependencyList& DepList_ ) const
			{
				m_pClassRef->resolve( pClass_, DepList_, false );
			}
			
		private:
			shared_ptr< ClassRef >	m_pClassRef;
			bool					m_bConst;
	};
	
	typedef BasicPointerType< e_Pointer > PointerType;
	typedef BasicPointerType< e_WeakPointer > WeakPointerType;
	
	class StructRefType : public BasicType< e_StructRef >
	{
		public:
			StructRefType( string FileName_, unsigned long Line_ ) :
				BasicType< e_StructRef >( FileName_, Line_ )
			{
			}

			void setClassRef( shared_ptr< ClassRef > pClassRef_ )
			{
				m_pClassRef = pClassRef_;
			}
			
			shared_ptr< ClassRef > getClassRef() const
			{
				return m_pClassRef;
			}
			
			virtual void resolveRefs( shared_ptr< const Class > pClass, DependencyList& DepList ) const
			{
				m_pClassRef->resolve( pClass, DepList, true );
			}
			
		private:
			shared_ptr< ClassRef > m_pClassRef;
	};
	
	class EnumType : public BasicType< e_enum >
	{
		public:
			EnumType( string FileName_, unsigned long Line_ ) :
				BasicType< e_enum >( FileName_, Line_ )
			{
			}

			void setEnumRef( shared_ptr< EnumRef > pEnumRef_ )
			{
				m_pEnumRef = pEnumRef_;
			}
			
			shared_ptr< EnumRef > getEnumRef() const
			{
				return m_pEnumRef;
			}
			
			virtual void resolveRefs( shared_ptr< const Class > pClass_, DependencyList& DepList_ ) const
			{
				m_pEnumRef->resolve( pClass_ );
			}
			
		private:
			shared_ptr< EnumRef > m_pEnumRef;
	};
	
	template< TypeID TypeID_ > class MonadicType : public BasicType< TypeID_ >
	{
		public:
			MonadicType( string FileName_, unsigned long Line_ ) :
				BasicType< TypeID_ >( FileName_, Line_ )
			{
			}
						
			void setSubType( shared_ptr< Type > pSubType_ )
			{
				m_pSubType = pSubType_;
			}
			
			shared_ptr< Type > getSubType() const
			{
				return m_pSubType;
			}
			
			virtual void resolveRefs( shared_ptr< const Class > pClass, DependencyList& DepList ) const
			{
				m_pSubType->resolveRefs( pClass, DepList );
			}
			
		private:
			shared_ptr< Type >	m_pSubType;
	};
	
	typedef MonadicType< e_vector > VectorType;
	typedef MonadicType< e_list > ListType;
	typedef MonadicType< e_set > SetType;
	
	template< TypeID TypeID_ > class DyadicType : public BasicType< TypeID_  >
	{
		public:
			DyadicType( string FileName_, unsigned long Line_ ) :
				BasicType< TypeID_ >( FileName_, Line_ )
			{
			}
			
			void setFirstType( shared_ptr< Type > pFirstType_ )
			{
				m_pFirstType = pFirstType_;
			}
			
			shared_ptr< Type > getFirstType() const
			{
				return m_pFirstType;
			}
			
			void setSecondType( shared_ptr< Type > pSecondType_ )
			{
				m_pSecondType = pSecondType_;
			}
			
			shared_ptr< Type > getSecondType() const
			{
				return m_pSecondType;
			}
			
			virtual void resolveRefs( shared_ptr< const Class > pClass_, DependencyList& DepList_ ) const
			{
				m_pFirstType->resolveRefs( pClass_, DepList_ );
				m_pSecondType->resolveRefs( pClass_, DepList_ );
			}
			
		private:
			shared_ptr< Type >	m_pFirstType;
			shared_ptr< Type >	m_pSecondType;
	};
	
	typedef DyadicType< e_map > MapType;
	typedef DyadicType< e_pair > PairType;
}}

#endif
