/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ast.h"

using namespace fail;
using namespace fail::idlast;

void Class::addSuperClass( shared_ptr< ClassRef > pSuperClassRef_ )
{
	m_SuperClasses.push_back( pSuperClassRef_ );
}

void Class::addAttribute( shared_ptr< Attribute > pAttribute_ )
{
	m_Attributes.add( pAttribute_ );
}

shared_ptr< Method > Class::getOrAddMethod( string Name_, const Flags& Flags_ )
{
	shared_ptr< Method > pMethod = m_Methods.get( Name_ );

	if( !pMethod )
	{
		pMethod = shared_ptr< Method >( new Method( Name_ ) );
		m_Methods.add( pMethod );
		pMethod->setFlags( Flags_ );
	}

	return pMethod;
}

shared_ptr< Method > Class::getOrAddStaticMethod( string Name_ )
{
	shared_ptr< Method > pMethod = m_StaticMethods.get( Name_ );

	if( !pMethod )
	{
		pMethod = shared_ptr< Method >( new Method( Name_ ) );
		m_StaticMethods.add( pMethod );
	}

	return pMethod;
}

void Class::addConstructor( shared_ptr< const ParamList > pParamList_, const Flags& Flags_ )
{
	m_Constructors.insert( make_pair( pParamList_->size(), pParamList_ ) );
	m_CtorFlags = Flags_;
}

void Class::addSignal( shared_ptr< Signal > pSignal_ )
{
	m_Signals.add( pSignal_ );
}

void Class::addEnum( shared_ptr< Enum > pEnum_ )
{
	m_Enums.add( pEnum_ );
	pEnum_->m_pClass = shared_from_this();
}

void Class::resolveRefs( bool bStruct_ )
{
	list< shared_ptr< ClassRef > >::const_iterator scit;
	for( scit = m_SuperClasses.begin(); scit != m_SuperClasses.end(); ++scit )
		( *scit )->resolve( shared_from_this(), m_SuperClassDependencies, bStruct_ );
	
	Dictionary< Attribute >::const_iterator atit;
	for( atit = m_Attributes.begin(); atit != m_Attributes.end(); ++atit )
		atit->second->resolveRefs( shared_from_this(), m_AttrDependencies );
	
	Dictionary< Method >::const_iterator mtdit;
	for( mtdit = m_Methods.begin(); mtdit != m_Methods.end(); ++mtdit )
		mtdit->second->resolveRefs( shared_from_this(), m_MethodDependencies );

	for( mtdit = m_StaticMethods.begin(); mtdit != m_StaticMethods.end(); ++mtdit )
		mtdit->second->resolveRefs( shared_from_this(), m_MethodDependencies );

	Overloads::const_iterator ctorit;
	for( ctorit = m_Constructors.begin(); ctorit != m_Constructors.end(); ++ctorit )
		ctorit->second->resolveRefs( shared_from_this(), m_MethodDependencies );
	
	Dictionary< Signal >::const_iterator sigit;
	for( sigit = m_Signals.begin(); sigit != m_Signals.end(); ++sigit )
		sigit->second->resolveRefs( shared_from_this(), m_SignalDependencies );
}

void Class::resolvePolymorphism()
{
	list< shared_ptr< ClassRef > >::iterator scit;
	
	// If any super class is polymorphic, it means we are polymorphic as well.
	for( scit = m_SuperClasses.begin(); scit != m_SuperClasses.end(); ++scit )
	{
		( **scit )->resolvePolymorphism();
		if( ( **scit )->testFlag( "Polymorphic", false ) )
			setFlag( "Polymorphic", true );
	}
		
	// If we are polymorphic and have at least one constructor declared in the IDL,
	// then scripts can extend us.
	if( !getConstructors().empty() && testFlag( "Polymorphic", false ) )
		setFlag( "ExtensibleByScript", true );
}

bool Class::isSuperClassOf( shared_ptr< const Class > pClass ) const
{
	if( pClass.get() == this )
		return false;

	stack< shared_ptr< const Class > > workstack;
	workstack.push( pClass );

	while( !workstack.empty() )
	{
		shared_ptr< const Class > pCurrent = workstack.top();
		workstack.pop();

		if( pCurrent.get() == this )
			return true;

		list< shared_ptr< ClassRef > >::const_iterator scit;
		for( scit = pCurrent->m_SuperClasses.begin(); scit != pCurrent->m_SuperClasses.end(); ++scit )
			workstack.push( **scit );
	}

	return false;
}
