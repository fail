/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IDLAST_METHOD_H_
#define FAIL_IDLAST_METHOD_H_

namespace fail { namespace idlast
{
	class Method : public Named, public Flaggable
	{
		public:
 			Method( string Name_ ) :
				Named( Name_ )
			{
				//std::cout << "new method '" << Name_ << "'\n";
			}

			void addOverload( shared_ptr< const ParamList > pParamList_ )
			{
				m_Overloads.insert( make_pair( pParamList_->size(), pParamList_ ) );	
			}
			
			const Overloads& getOverloads() const
			{
				return m_Overloads;
			}
			
			void resolveRefs( shared_ptr< const Class > pClass_, DependencyList& DepList_ ) const;
			
		private:
			Overloads				m_Overloads;
	};
}}

#endif
