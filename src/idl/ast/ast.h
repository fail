/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IDLAST_AST_H_
#define FAIL_IDLAST_AST_H_

#include <string>
#include <vector>
#include <map>
#include <set>
#include <stack>
#include <memory>
#include <iostream>
#include <sstream>

namespace fail { namespace idlast
{
	using namespace std;
}}

#include "element.h"
#include "flags.h"
#include "named.h"
#include "dictionary.h"
#include "classref.h"
#include "type.h"
#include "paramlist.h"
#include "attribute.h"
#include "method.h"
#include "failsignal.h"
#include "class.h"
#include "enum.h"
#include "namespace.h"

namespace fail { namespace idlast
{
	typedef class AnonNamespace AST;
	
	class ASTError : public std::exception
	{
		public:
			ASTError( string Filename_, int LineNumber_, string Message_ );
			virtual const char* what() const throw()
			{
				return m_What.c_str();
			}
			
			virtual ~ASTError() throw()
			{
			}
			
		private:
			int			m_LineNumber;
			string		m_Filename;
			string		m_Message;
			string		m_What;
	};
}}

#endif
