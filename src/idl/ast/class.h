/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IDLAST_CLASS_H_
#define FAIL_IDLAST_CLASS_H_

namespace fail { namespace idlast
{
	class Namespace;
	
	class Class : public Named, public Flaggable, public Element, public enable_shared_from_this< Class >
	{
		friend class Namespace;

		public:
			Class( string Name_, string FileName_, unsigned long Line_ ) :
				Named( Name_ ),
				Element( FileName_, Line_ )
			{
			}
			
			void addSuperClass( shared_ptr< ClassRef > pSuperClassRef_ );
			const list< shared_ptr< ClassRef > >& getSuperClasses() const
			{
				return m_SuperClasses;
			}
			
			void addAttribute( shared_ptr< Attribute > pAttribute_ );
			const Dictionary< Attribute >& getAttributes() const
			{
				return m_Attributes;
			}
			
			shared_ptr< Method > getOrAddMethod( string Name_, const Flags& Flags_ );
			shared_ptr< Method > getOrAddStaticMethod( string Name_ );
			void addConstructor( shared_ptr< const ParamList > pParamList_, const Flags& Flags_ );
			const Overloads& getConstructors() const
			{
				return m_Constructors;
			}
			const Flags& getCtorFlags() const
			{
				return m_CtorFlags;
			}
			
			const Dictionary< Method >& getMethods() const
			{
				return m_Methods;
			}
				
			const Dictionary< Method >& getStaticMethods() const
			{
				return m_StaticMethods;
			}
			
			void addSignal( shared_ptr< Signal > pSignal_ );
			const Dictionary< Signal >& getSignals() const
			{
				return m_Signals;
			}
			
			void addEnum( shared_ptr< Enum > pEnum_ );
			const Dictionary< Enum >& getEnums() const
			{
				return m_Enums;
			}	
			
			void resolveRefs( bool bStruct_ );
			void resolvePolymorphism();
			
			shared_ptr< Namespace > getNamespace() const
			{
				return m_pNamespace.lock();
			}
			
			const DependencyList& getSuperClassDependencies() const
			{
				return m_SuperClassDependencies;
			}
						
			const DependencyList& getAttrDependencies() const
			{
				return m_AttrDependencies;
			}

			const DependencyList& getMethodDependencies() const
			{
				return m_MethodDependencies;
			}

			const DependencyList& getSignalDependencies() const
			{
				return m_SignalDependencies;
			}

			bool isSuperClassOf( shared_ptr< const Class > pClass ) const;

		private:
			weak_ptr< Namespace >				m_pNamespace;
			list< shared_ptr< ClassRef > >		m_SuperClasses;
			
			Dictionary< Attribute >				m_Attributes;
			Dictionary< Method >				m_Methods;
			Dictionary< Method >				m_StaticMethods;
			
			Flags								m_CtorFlags;
			Overloads							m_Constructors;
			
			Dictionary< Signal >				m_Signals;
			
			Dictionary< Enum >					m_Enums;
			
			DependencyList						m_SuperClassDependencies;
			DependencyList						m_AttrDependencies;
			DependencyList						m_MethodDependencies;
			DependencyList						m_SignalDependencies;
	};
}}

#endif
