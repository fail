/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IDLAST_ELEMENT_H_
#define FAIL_IDLAST_ELEMENT_H_

namespace fail { namespace idlast
{
	class Element
	{
		public:
			Element( std::string FileName_, unsigned long Line_ ) :
				m_FileName( FileName_ ),
				m_Line( Line_ )
			{
			}
			
			const std::string& getFileName() const
			{
				return m_FileName;
			}
			
			unsigned long getLine() const
			{
				return m_Line;
			}
			
		private:
			std::string		m_FileName;
			unsigned long	m_Line;
	};
}}

#endif
