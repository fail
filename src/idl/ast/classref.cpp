/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ast.h"

using namespace fail;
using namespace fail::idlast;

void ClassRef::resolve( shared_ptr< const Class > pClass_, DependencyList& DepList_, bool bStruct_ )
{
	/*{
		std::cout << "resolving classref ";
		std::vector< std::string >::const_iterator it;
		for( it = m_NameComponents.begin(); it != m_NameComponents.end(); ++it )
		{
			std::cout << "::" << *it;
		}
		
		std::cout << std::endl;
	}*/

	
	shared_ptr< Namespace > pNamespace = pClass_->getNamespace();
	shared_ptr< const Namespace > pCurrentNamespace = pNamespace;

	// If the name is unqualified, just look it up in the namespace hierarchy
	// starting with the current namespace
	if( m_NameComponents.size() == 1 )
	{
		Namespace::FindClassResult res = pNamespace->findClass( m_NameComponents[0], bStruct_ );
		*this = res.first;
		pCurrentNamespace = res.second;
	}
	else
	{	
		// Look for the first namespace up in the namespace hierarchy.
		// Any following namespace name will be looked up only in the current namespace (and will
		// become the new current namespace)
		pCurrentNamespace = pNamespace->findNamespace( m_NameComponents[0] );
	
		// Go through intermediate namespace names, if any
		std::vector< std::string >::const_iterator endit =
			m_NameComponents.begin() + m_NameComponents.size() - 1;
		
		std::vector< std::string >::const_iterator it;
		for( it = m_NameComponents.begin() + 1; it != endit && pCurrentNamespace; ++it )
			pCurrentNamespace = pCurrentNamespace->getNamespaces().get( *it );
		
		if( pCurrentNamespace )
		{
			Namespace::FindClassResult res = pCurrentNamespace->findClass( m_NameComponents.back(), bStruct_ );
			*this = res.first;
			pCurrentNamespace = res.second;
		}
	}
	
	if( *this == NULL )
	{
		stringstream msg;
		msg << "undefined class '";
		vector< string >::const_iterator it;
		for( it = m_NameComponents.begin(); it != m_NameComponents.end(); ++it )
		{
			if( it != m_NameComponents.begin() )
				msg << "::"; 
			msg << *it;
		}
		
		msg << "'.";
		
		throw ASTError( getFileName(), getLine(), msg.str() );
	}
	
	DepList_.insert( get() );
	
	if( pNamespace != pCurrentNamespace )
	{
	//	std::cout << "adding '" << pCurrentNamespace->getName() << "' as a dependency of '" << pNamespace->getName() << "'\n";
		pNamespace->addDependency( pCurrentNamespace );
	}
}
