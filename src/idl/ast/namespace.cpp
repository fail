/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ast.h"

using namespace fail;
using namespace fail::idlast;

shared_ptr< Namespace > Namespace::addNamespace( shared_ptr< Namespace > pNamespace_ )
{
	shared_ptr< Namespace > pNS = m_Namespaces.get( pNamespace_->getName() );
	if( pNS )
		return pNS;

	if( !m_Namespaces.add( pNamespace_ ) )
		return shared_ptr< Namespace >();
	
	pNamespace_->m_pParent = shared_from_this();
	return pNamespace_;
}

shared_ptr< Namespace > Namespace::findNamespace( std::string Name_ ) const
{
	shared_ptr< Namespace > pNS = m_Namespaces.get( Name_ );
	if( pNS )
		return pNS;

	shared_ptr< Namespace > pParent( m_pParent );
	if( !pParent )
		return shared_ptr< Namespace >();

	return pParent->findNamespace( Name_ );
}

Namespace::FindClassResult Namespace::findClass( std::string Name_, bool bStruct_ ) const
{
	shared_ptr< Class > pClass;
	
	//std::cout << "findclass: looking for " << Name_ << " in " << getName() << "\n";
	 
	if( bStruct_ )
		pClass = m_Structs.get( Name_ );
	else
		pClass = m_Classes.get( Name_ );
	
	if( pClass )
		return make_pair( pClass, shared_from_this() );
	
	shared_ptr< Namespace > pParent( m_pParent );
	if( !pParent )
		return FindClassResult();

	return pParent->findClass( Name_, bStruct_ );
}

void Namespace::resolveRefs() const
{
	stack< shared_ptr< const Namespace > > WorkStack;

	WorkStack.push( shared_from_this() );
	while( !WorkStack.empty() )
	{
		shared_ptr< const Namespace > pCurrentNamespace = WorkStack.top();
		WorkStack.pop();

		// Push child namespaces on the stack
		const Dictionary< Namespace >& Namespaces = pCurrentNamespace->getNamespaces();
		Dictionary< Namespace >::const_iterator nsit;
		for( nsit = Namespaces.begin(); nsit != Namespaces.end(); ++nsit )
			WorkStack.push( nsit->second );
		
		// Process classes
		const Dictionary< Class >& Classes = pCurrentNamespace->getClasses();
		Dictionary< Class >::const_iterator clit;
		for( clit = Classes.begin(); clit != Classes.end(); ++clit )
			clit->second->resolveRefs( false );

		// Process structs
		const Dictionary< Class >& Structs = pCurrentNamespace->getStructs();
		Dictionary< Class >::const_iterator stit;
		for( stit = Structs.begin(); stit != Structs.end(); ++stit )
			stit->second->resolveRefs( true );
	}
	
	// Iterate through classes again to resolve the polymorphism flag
	WorkStack.push( shared_from_this() );
	while( !WorkStack.empty() )
	{
		shared_ptr< const Namespace > pCurrentNamespace = WorkStack.top();
		WorkStack.pop();
		
		// Push child namespaces on the stack
		const Dictionary< Namespace >& Namespaces = pCurrentNamespace->getNamespaces();
		Dictionary< Namespace >::const_iterator nsit;
		for( nsit = Namespaces.begin(); nsit != Namespaces.end(); ++nsit )
			WorkStack.push( nsit->second );
		
		// Process classes
		const Dictionary< Class >& Classes = pCurrentNamespace->getClasses();
		Dictionary< Class >::const_iterator clit;
		for( clit = Classes.begin(); clit != Classes.end(); ++clit )
			clit->second->resolvePolymorphism();
	}
}
