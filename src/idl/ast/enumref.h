/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IDLAST_ENUMREF_H_
#define FAIL_IDLAST_ENUMREF_H_

namespace fail { namespace idlast
{
	class Enum;
	
	class EnumRef : public Element, public shared_ptr< const Enum >
	{	
		public:
			EnumRef( string FileName_, unsigned long Line_ ) :
				Element( FileName_, Line_ )
			{
			//	std::cout << "new classref " << this << std::endl;
			}
			
			void addComponent( string Component_ )
			{
			//	std::cout << "add component " << Component_ << " to classref " << this << std::endl;
				m_NameComponents.push_back( Component_ );
			}
			
			const EnumRef& operator=( shared_ptr< const Enum > pEnum_ )
			{
				this->shared_ptr< const Enum >::operator=( pEnum_ );
				return *this;
			}
			
			void resolve( shared_ptr< const Class > pClass_ );
			
		private:
			vector< string >	m_NameComponents;
	};
}}

#endif
