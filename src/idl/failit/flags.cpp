/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "failit.h"

using namespace fail::failit;

void fail::failit::OutputFlags( ostream& Out_, const idlast::Flags& Flags_, std::string Type_ )
{
	if( Flags_.empty() )
		return;
	
	Out_ << "\tnamespace flags\n"
			"\t{\n";
	
	idlast::Flags::const_iterator it;
	for( it = Flags_.begin(); it != Flags_.end(); ++it )
	{
		Out_ << "\t\ttemplate<> struct " << it->first << "< "
				<< Type_ << " > : public Flag< "
				<< ( it->second ? "true" : "false" ) << " > {};\n";
	}

	Out_ << "\t}\n\n";
}
