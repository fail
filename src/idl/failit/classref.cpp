/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <ostream>
#include "failit.h"

using namespace fail::failit;

void fail::failit::PrintClassRef( ostream& Out_, shared_ptr< const idlast::Class > pClass_ )
{
	// Build an array of namespace names, from the root namespace to the one we're interested in.
	vector< string > NSNames;
	shared_ptr< const idlast::Namespace > pCurrentNS = pClass_->getNamespace();
	while( pCurrentNS->getParent() )
	{
		NSNames.push_back( pCurrentNS->getName() );
		pCurrentNS = pCurrentNS->getParent();
	}
	
	vector< string >::const_reverse_iterator it;
	for( it = NSNames.rbegin(); it != NSNames.rend(); ++it )
	{
		Out_ << *it << "::";
	}
	
	Out_ << pClass_->getName();
}
