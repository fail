/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "failit.h"

using namespace fail::failit;

void fail::failit::GenerateSignalTraits( ostream& Out_,
							shared_ptr< const idlast::Class > pClass_,
							shared_ptr< const idlast::Signal > pSig_ )
{
	string FullClassName( pClass_->getName() );
	shared_ptr< const idlast::Namespace > pCurrentNS = pClass_->getNamespace();
	while( pCurrentNS->getParent() )
	{
		FullClassName = pCurrentNS->getName() + "::" + FullClassName;
		pCurrentNS = pCurrentNS->getParent();
	}
	
	Out_ << '\n';
	stringstream tagstring;
	tagstring << "class_traits< " << FullClassName << " >::signal_tags::" << pSig_->getName();
	OutputFlags( Out_, pSig_->getFlags(), tagstring.str() );
	
	const idlast::Signal::paramlist_type& params = pSig_->getParamTypes();
	
	Out_ << "\ttemplate< class C > struct signal_traits< C,\n"
			"\t\t" << tagstring.str() << " >\n"
			"\t{\n"
			"\t\ttypedef fail::Signal<";

	idlast::Signal::paramlist_type::const_iterator it;
	for( it = params.begin(); it != params.end(); ++it )
	{
		if( it != params.begin() )
			Out_ << ',';
		Out_ << ' ';
		PrintType( Out_, *it );
	}
	
	if( !params.empty() )
		Out_ << ' ';
	
	Out_ <<	"> type;\n"
			"\t\tstatic const char* Name() { return \"" << pSig_->getName() << "\"; }\n"
			"\t\tstatic type C::* MemberPointer() { return &C::sig_" << pSig_->getName() <<  "; }\n"
			"\n"
			"\t\ttemplate< class CC > struct CallWrapper\n"
			"\t\t{\n"
			"\t\t\tstatic bool Call( CC& CallContext_ )\n"
			"\t\t\t{\n"
			"\t\t\t\tC& Obj = CallContext_.getObject();\n"
			"\t\t\t\tCallContext_.numExpectedParams( " << params.size() << " );\n";
	
	int paramid = 1; 
	for( it = params.begin(); it != params.end(); ++it )
	{
		Out_ << "\n\t\t\t\t";
		PrintType( Out_, *it );
		Out_ << " param" << paramid << ";\n"
				"\t\t\t\tif( !CallContext_.template getParam( param" << paramid << " ) ) return false;\n";
		
		++paramid;
	}
	
	if( !params.empty() )
		Out_ << '\n';
	
	Out_ <<	"\t\t\t\tObj.sig_" << pSig_->getName() << '(';
	
	paramid = 1;
	for( it = params.begin(); it != params.end(); ++it )
	{
		if( paramid > 1 )
			Out_ << ',';
		Out_ << " param" << paramid;
		
		++paramid;
	}
	
	if( !params.empty() )
		Out_ << ' ';
	
	Out_ <<	");\n"
			"\t\t\t\treturn true;\n"
			"\t\t\t}\n"
			"\t\t};\n\n"
			
			"\t\ttemplate< class CC > struct SlotWrapper\n"
			"\t\t{\n"
			//"\t\t\tSlotWrapper( const CC& CallContext_ ) : m_CallContext( CallContext_ ) {}\n\n"
			"\t\t\tvoid Call(";
	
	paramid = 1; 
	for( it = params.begin(); it != params.end(); ++it )
	{
		if( paramid > 1 )
			Out_ << ',';
		Out_ << ' ';
		PrintType( Out_, *it );
		Out_ << " param" << paramid << '_';
		
		++paramid;
	}
	
	if( !params.empty() )
		Out_ << ' ';
	
	Out_ << ")\n"
			"\t\t\t{\n"
			"\t\t\t\tm_CallContext.prepare();\n";
	
	paramid = 1; 
	for( it = params.begin(); it != params.end(); ++it )
	{
		Out_ << "\t\t\t\tm_CallContext.template addParam( param" << paramid << "_ );\n";
		++paramid;
	}
	
	Out_ << "\t\t\t\tm_CallContext.call();\n"
			"\t\t\t}\n\n"
			"\t\t\tCC m_CallContext;\n"
			"\t\t};\n"
			"\t};\n";
}
