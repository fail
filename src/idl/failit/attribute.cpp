/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <ostream>
#include <sstream>
#include "failit.h"

using namespace fail::failit;

void fail::failit::GenerateAttrTraits( ostream& Out_,
							shared_ptr< const idlast::Class > pClass_,
							shared_ptr< const idlast::Attribute > pAttr_,
							bool bStruct_ )
{
	string FullClassName( pClass_->getName() );
	shared_ptr< const idlast::Namespace > pCurrentNS = pClass_->getNamespace();
	while( pCurrentNS->getParent() )
	{
		FullClassName = pCurrentNS->getName() + "::" + FullClassName;
		pCurrentNS = pCurrentNS->getParent();
	}

	Out_ << '\n';
	stringstream tagstring;
	tagstring << "class_traits< " << FullClassName << " >::attribute_tags::" << pAttr_->getName();
	OutputFlags( Out_, pAttr_->getFlags(), tagstring.str() );
	
	stringstream typestring;
	PrintType( typestring, pAttr_->getType() );
	
	crc32 digest;
	digest.write( pAttr_->getName().c_str(), pAttr_->getName().size() );
	digest.write( ' ' );
	digest.write( typestring.str().c_str(), typestring.str().size() );
	
	Out_ << "\ttemplate< class C > struct attribute_traits< C,\n"
			"\t\t" << tagstring.str() << " >\n"
			"\t{\n"
			"\t\ttypedef " << typestring.str() <<
			" type;\n";
	
	bool bGenerateGetSet = pAttr_->testFlag( "Scriptable", true );
	bool bMutable = pAttr_->testFlag( "Mutable", true );
	bool bReadOnly = pAttr_->testFlag( "ReadOnly", false );	
	
	if( bStruct_ )
	{
		Out_ << "\t\ttypedef type& ( C::* accessor_type )();\n"
				"\t\ttypedef const type& ( C::* getter_type )() const;\n\n";
	}
	else if( bGenerateGetSet )
	{
		Out_ <<	"\t\ttypedef const type& ( C::* getter_type )() const;\n";
		
		if( !bReadOnly )
		{
			if( bMutable )
				Out_ <<	"\t\ttypedef type& ( C::* getter_mutable_type )();\n";
			Out_ <<	"\t\ttypedef void ( C::* setter_type )( const type& );\n\n";
		}
	}

	
	Out_ <<	"\t\tstatic uint32_t Digest() { return 0x" << std::hex << digest.result() << "; }\n" 
			"\t\tstatic const char* Name() { return \"" << pAttr_->getName() << "\"; }\n\n";

	if( !bStruct_ )
	{
		Out_ <<	"\t\tstatic type C::* MemberPointer() { return &C::m_" << pAttr_->getName() << "; }\n";
	
		if( bGenerateGetSet )
		{
			Out_ << "\t\tstatic getter_type Getter() { return &C::get" << pAttr_->getName() << "; }\n";
		
			if( !bReadOnly )
			{
				if( bMutable )
					Out_ <<	"\t\tstatic getter_mutable_type GetterMutable() { return &C::get" << pAttr_->getName() << "; }\n";
				Out_ <<	"\t\tstatic setter_type Setter() { return &C::set" << pAttr_->getName() << "; }\n";
			}
		}
	}
	else
	{
		Out_ << "\t\tstatic accessor_type Accessor() { return &C::" << pAttr_->getName() << "; }\n"
				"\t\tstatic getter_type Getter() { return &C::" << pAttr_->getName() << "; }\n";
	}

	Out_ <<	"\t};\n";
}
