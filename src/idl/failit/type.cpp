/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "failit.h"

using namespace fail::failit;

namespace
{
	template< class PtrType > void PrintPointerClassRef( ostream& Out_, shared_ptr< const fail::idlast::Type > pType_ )
	{
		const PtrType* pActualType = dynamic_cast< const PtrType* >( pType_.get() );
		if( pActualType->getConst() )
			Out_ << "const ";
		PrintClassRef( Out_, *pActualType->getClassRef() );
	}
}

void fail::failit::PrintType( ostream& Out_, shared_ptr< const idlast::Type > pType_, bool bRef )
{
	switch( pType_->getID() )
	{
		case idlast::e_bool:
			Out_ << "bool";
			break;

		case idlast::e_uint8_t:
			Out_ << "uint8_t";
			break;
		case idlast::e_int8_t:
			Out_ << "int8_t";
			break;
			
		case idlast::e_uint16_t:
			Out_ << "uint16_t";
			break;
		case idlast::e_int16_t:
			Out_ << "int16_t";
			break;
				
		case idlast::e_uint32_t:
			Out_ << "uint32_t";
			break;
		case idlast::e_int32_t:
			Out_ << "int32_t";
			break;
			
		case idlast::e_uint64_t:
			Out_ << "uint64_t";
			break;
		case idlast::e_int64_t:
			Out_ << "int64_t";
			break;
			
		case idlast::e_float:
			Out_ << "float";
			break;

		case idlast::e_double:
			Out_ << "double";
			break;

		case idlast::e_string:
			if( bRef )
				Out_ << "const ";
 			Out_ << "string";
			if( bRef )
				Out_ << '&';
			break;

		case idlast::e_DynamicBuffer:
			if( bRef )
				Out_ << "const ";
			Out_ << "DynamicBuffer";
			if( bRef )
				Out_ << '&';
			break;
			
		case idlast::e_Pointer:
			if( bRef )
				Out_ << "const ";
			Out_ << "shared_ptr< ";
			PrintPointerClassRef< idlast::PointerType >( Out_, pType_ );
			Out_ << " >";
			if( bRef )
				Out_ << '&';
			break;

		case idlast::e_WeakPointer:
			if( bRef )
				Out_ << "const ";
			Out_ << "weak_ptr< ";
			PrintPointerClassRef< idlast::WeakPointerType >( Out_, pType_ );
			Out_ << " >";
			if( bRef )
				Out_ << '&';
			break;

		case idlast::e_StructRef:
			{
				if( bRef )
					Out_ << "const ";
				
				const idlast::StructRefType* pActualType = 
						dynamic_cast< const idlast::StructRefType* >( pType_.get() );
				PrintClassRef( Out_, *pActualType->getClassRef() );
				
				if( bRef )
					Out_ << '&';
			}
			break;
			
		case idlast::e_enum:
			{			
				const idlast::EnumType* pActualType = 
						dynamic_cast< const idlast::EnumType* >( pType_.get() );
				shared_ptr< const idlast::Enum > pEnum = *pActualType->getEnumRef();
				PrintClassRef( Out_, pEnum->getClass() );
				Out_ << "::" << pEnum->getName();
			}
			break;
			
		case idlast::e_signal:
			{
				Out_ << "Signal<";
			
				const idlast::Signal* pActualType = 
						dynamic_cast< const idlast::Signal* >( pType_.get() );
				
				idlast::Signal::paramlist_type ParamTypes = pActualType->getParamTypes();
				
				if( !ParamTypes.empty() )
				{
					idlast::Signal::paramlist_type::const_iterator it = ParamTypes.begin(), itnext;
					
					while( it != ParamTypes.end() )
					{
						itnext = it;
						++itnext;

						Out_ << ' ';
						PrintType( Out_, *it );
						
						if( itnext != ParamTypes.end() )
							Out_ << ',';
						
						it = itnext;
					}
					
					Out_ << ' ';
				}
			
				Out_ << '>';
			}
			break;
			
		case idlast::e_vector:
			{
				if( bRef )
					Out_ << "const ";
				
				const idlast::VectorType* pActualType = 
						dynamic_cast< const idlast::VectorType* >( pType_.get() );
				
				Out_ << "vector< ";
				PrintType( Out_, pActualType->getSubType() );
				Out_ << " >";
				
				if( bRef )
					Out_ << '&';
			}
			break;
		
		case idlast::e_list:
			{
				if( bRef )
					Out_ << "const ";
				
				const idlast::ListType* pActualType = 
						dynamic_cast< const idlast::ListType* >( pType_.get() );
				
				Out_ << "list< ";
				PrintType( Out_, pActualType->getSubType() );
				Out_ << " >";
				
				if( bRef )
					Out_ << '&';
			}
			break;
			
		case idlast::e_set:
			{
				if( bRef )
					Out_ << "const ";
				
				const idlast::SetType* pActualType = 
						dynamic_cast< const idlast::SetType* >( pType_.get() );
				
				Out_ << "set< ";
				PrintType( Out_, pActualType->getSubType() );
				Out_ << " >";
				
				if( bRef )
					Out_ << '&';
			}
			break;
			
		case idlast::e_map:
			{
				if( bRef )
					Out_ << "const ";
				
				const idlast::MapType* pActualType = 
						dynamic_cast< const idlast::MapType* >( pType_.get() );
				
				Out_ << "map< ";
				PrintType( Out_, pActualType->getFirstType() );
				Out_ << ", ";
				PrintType( Out_, pActualType->getSecondType() );
				Out_ << " >";
				
				if( bRef )
					Out_ << '&';
			}
			break;
			
		case idlast::e_pair:
			{
				if( bRef )
					Out_ << "const ";
				
				const idlast::PairType* pActualType = 
						dynamic_cast< const idlast::PairType* >( pType_.get() );
				
				Out_ << "pair< ";
				PrintType( Out_, pActualType->getFirstType() );
				Out_ << ", ";
				PrintType( Out_, pActualType->getSecondType() );
				Out_ << " >";
				
				if( bRef )
					Out_ << '&';
			}
			break;
			
		default:
			Out_ << "/* TODO: missing type printing */";
			break;
	}
}
