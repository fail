/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "failit.h"

using namespace fail::failit;

void fail::failit::GenerateClassTraitsFile( shared_ptr< const idlast::Class > pClass_, bool bStruct_ )
{	
	string FullNamespaceName;
	string FullClassNameUnderscored;

	shared_ptr< const idlast::Namespace > pCurrentNS = pClass_->getNamespace();
	while( pCurrentNS->getParent() )
	{
		FullNamespaceName = pCurrentNS->getName() + "::" + FullNamespaceName;
		FullClassNameUnderscored = pCurrentNS->getName() + "_" + FullClassNameUnderscored;
		pCurrentNS = pCurrentNS->getParent();
	}
	
	string FullClassName = FullNamespaceName + pClass_->getName();
	FullClassNameUnderscored = FullClassNameUnderscored + pClass_->getName();
	
	string name( pClass_->getName() );
	
	stringstream filename;
	filename << "traits_" << name << ".h";

	ofstream file( filename.str().c_str() );
	
	crc32 digest;
	digest.write( FullClassName.c_str(), FullClassName.size() );
	
	file << "//\n"
		"// Automatically generated by FailIT (Fail IDL Translator).\n"
		"//\n"
		"#ifndef TRAITS_" << FullClassNameUnderscored << "_H_\n"
		"#define TRAITS_" << FullClassNameUnderscored << "_H_\n\n"
		"#include \"core/core.h\"\n"
		"#include \"" << name << ".h\"\n\n"
		"namespace fail\n"
		"{\n";
	
	OutputFlags( file, pClass_->getFlags(), FullClassName );
	
	file << "\ttemplate<> struct class_traits< " << FullClassName << " >\n"
		"\t{\n"
		"\t\tstatic uint32_t Digest() { return 0x" << std::hex << digest.result() << "; }\n"
		"\t\tstatic const char* Name() { return \"" << name << "\"; }\n"
		"\t\tstatic const char* FullName() { return \"" << FullClassName << "\"; }\n"
		"\t\ttypedef " << FullNamespaceName << "fail_tag module_tag;\n\n"

	// Generate VisitSuperClasses
		"\t\ttemplate< class V > static void VisitSuperClasses( V& Visitor_ )\n"
		"\t\t{\n";
	
	const list< shared_ptr< idlast::ClassRef > >& SuperClasses( pClass_->getSuperClasses() );
	list< shared_ptr< idlast::ClassRef > >::const_iterator scit;
	for( scit = SuperClasses.begin(); scit != SuperClasses.end(); ++scit )
	{
		file << "\t\t\tVisitor_.template declSuperClass< ";
		PrintClassRef( file, **scit );
		file << " >();\n";
	}
	
	file << "\t\t}\n";
	
	// Generate attribute tags
	const idlast::Dictionary< idlast::Attribute >& Attributes( pClass_->getAttributes() );
	idlast::Dictionary< idlast::Attribute >::const_iterator atit;
			
	if( !Attributes.empty() )
	{
		file << "\n\t\tstruct attribute_tags\n"
				"\t\t{\n";

		for( atit = Attributes.begin(); atit != Attributes.end(); ++atit )
		{
			shared_ptr< const idlast::Attribute > pAttr = atit->second;
			file << "\t\t\tstruct " << pAttr->getName() << " {};\n"; 
		}
		
		file << "\t\t};\n";
	}
	
	// Generate VisitAttributes
	file << "\n\t\ttemplate< class V > static void VisitAttributes( V& Visitor_ )\n"
		"\t\t{\n";
	
	for( atit = Attributes.begin(); atit != Attributes.end(); ++atit )
	{
		shared_ptr< idlast::Attribute > pAttr = atit->second;
		file << "\t\t\tVisitor_.template declAttribute< attribute_tags::"
				<< pAttr->getName() << " >();\n";
	}

	file << "\t\t}\n";
	
	// Generate method tags
	const idlast::Dictionary< idlast::Method >& Methods( pClass_->getMethods() );
	idlast::Dictionary< idlast::Method >::const_iterator mtdit;
	
	if( !Methods.empty() )
	{
		file << "\n\t\tstruct method_tags\n"
				"\t\t{\n";
	
		for( mtdit = Methods.begin(); mtdit != Methods.end(); ++mtdit )
		{
			shared_ptr< idlast::Method > pMtd = mtdit->second;
			file << "\t\t\tstruct " << pMtd->getName() << " {};\n"; 
		}
		
		file << "\t\t};\n";
	}
	
	// Generate VisitMethods
	file << "\n\t\ttemplate< class V > static void VisitMethods( V& Visitor_ )\n"
		"\t\t{\n";
	
	for( mtdit = Methods.begin(); mtdit != Methods.end(); ++mtdit )
	{
		shared_ptr< idlast::Method > pMtd = mtdit->second;	
		file << "\t\t\tVisitor_.template declMethod< method_tags::"
				<< pMtd->getName() << " >();\n";
	}
	
	file << "\t\t}\n";	
	
	// Generate ctor tag
	bool bAbstract = pClass_->testFlag( "Abstract", false );
	bool bExtensibleByScript = pClass_->testFlag( "ExtensibleByScript", false );

	const idlast::Overloads& Constructors = pClass_->getConstructors();
	if( ( !bAbstract || bExtensibleByScript ) && !Constructors.empty() )
		file << "\n\t\tstruct ctor_tag {};\n";
	
	// Generate static method tags
	const idlast::Dictionary< idlast::Method >& StaticMethods( pClass_->getStaticMethods() );
	
	if( !StaticMethods.empty() )
	{
		file << "\n\t\tstruct static_method_tags\n"
				"\t\t{\n";
	
		for( mtdit = StaticMethods.begin(); mtdit != StaticMethods.end(); ++mtdit )
		{
			shared_ptr< idlast::Method > pMtd = mtdit->second;
			file << "\t\t\tstruct " << pMtd->getName() << " {};\n"; 
		}
		
		file << "\t\t};\n";
	}
	
	// Generate VisitStaticMethods
	file << "\n\t\ttemplate< class V > static void VisitStaticMethods( V& Visitor_ )\n"
		"\t\t{\n";
	
	// The constructors is considered to be a static method with overloads.
	// Calling declConstructor() on the visitor serves to indicate that
	// we have at least one constructor and that method_traits for it is available.
	if( !bAbstract && !Constructors.empty() )
		file << "\t\t\tVisitor_.declConstructor();\n";
	
	for( mtdit = StaticMethods.begin(); mtdit != StaticMethods.end(); ++mtdit )
	{
		shared_ptr< idlast::Method > pMtd = mtdit->second;	
		file << "\t\t\tVisitor_.template declStaticMethod< static_method_tags::"
				<< pMtd->getName() << " >();\n";
	}
	
	file << "\t\t}\n";
	
	// Generate signal tags
	const idlast::Dictionary< idlast::Signal >& Signals( pClass_->getSignals() );
	idlast::Dictionary< idlast::Signal >::const_iterator sigit;
	
	if( !Signals.empty() )
	{
		file << "\n\t\tstruct signal_tags\n"
				"\t\t{\n";
	
		for( sigit = Signals.begin(); sigit != Signals.end(); ++sigit )
		{
			shared_ptr< idlast::Signal > pSig = sigit->second;
			file << "\t\t\tstruct " << pSig->getName() << " {};\n"; 
		}
		
		file << "\t\t};\n";
	}
	
	// Generate VisitSignals
	file << "\n\t\ttemplate< class V > static void VisitSignals( V& Visitor_ )\n"
			"\t\t{\n";
	
	for( sigit = Signals.begin(); sigit != Signals.end(); ++sigit )
	{
		shared_ptr< idlast::Signal > pSig = sigit->second;	
		file << "\t\t\tVisitor_.template declSignal< signal_tags::"
				<< pSig->getName() << " >();\n";
	}
	
	file << "\t\t}\n";
	
	// Generate enum tags
	const idlast::Dictionary< idlast::Enum >& Enums( pClass_->getEnums() );
	idlast::Dictionary< idlast::Enum >::const_iterator enit;
	
	if( !Enums.empty() )
	{
		file << "\n\t\tstruct enum_tags\n"
				"\t\t{\n";
	
		for( enit = Enums.begin(); enit != Enums.end(); ++enit )
		{
			shared_ptr< idlast::Enum > pEnum = enit->second;
			file << "\t\t\tstruct " << pEnum->getName() << " {};\n"; 
		}
		
		file << "\t\t};\n";
	}
	
	// Generate VisitEnums
	file << "\n\t\ttemplate< class V > static void VisitEnums( V& Visitor_ )\n"
			"\t\t{\n";
	
	for( enit = Enums.begin(); enit != Enums.end(); ++enit )
	{
		shared_ptr< idlast::Enum > pEnum = enit->second;	
		file << "\t\t\tVisitor_.template declEnum< enum_tags::"
				<< pEnum->getName() << " >();\n";
	}

	file << "\t\t}\n"
			 "\t};\n";
			
	// Generate sub-classing wrapper, if the class is extensible by scripts
	if( bExtensibleByScript )
	{
		file << '\n';
		GenerateSubClassWrapper( file, pClass_, FullClassName );
	}
	
	// Generate attributes traits
	for( atit = Attributes.begin(); atit != Attributes.end(); ++atit )
	{
		shared_ptr< idlast::Attribute > pAttr = atit->second;
		GenerateAttrTraits( file, pClass_, pAttr, bStruct_ );
	}
	
	// Generate constructor traits
	if( ( !bAbstract || bExtensibleByScript ) && !Constructors.empty() )
		GenerateCtorTraits( file, pClass_, bStruct_ );
	
	// Generate methods traits
	for( mtdit = Methods.begin(); mtdit != Methods.end(); ++mtdit )
	{
		shared_ptr< idlast::Method > pMtd = mtdit->second;
		GenerateMethodTraits( file, pClass_, pMtd, false );
	}
	
	// Generate static methods traits
	for( mtdit = StaticMethods.begin(); mtdit != StaticMethods.end(); ++mtdit )
	{
		shared_ptr< idlast::Method > pMtd = mtdit->second;
		GenerateMethodTraits( file, pClass_, pMtd, true );
	}
	
	// Generate signal traits
	for( sigit = Signals.begin(); sigit != Signals.end(); ++sigit )
	{
		shared_ptr< idlast::Signal > pSig = sigit->second;
		GenerateSignalTraits( file, pClass_, pSig );
	}
	
	// Generate enum traits
	for( enit = Enums.begin(); enit != Enums.end(); ++enit )
	{
		shared_ptr< idlast::Enum > pEnum = enit->second;
		GenerateEnumTraits( file, pEnum );
	}
	
	file <<	"}"
			"\n\n"
			"#endif\n";
}
