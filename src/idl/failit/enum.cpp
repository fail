/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "failit.h"

using namespace fail::failit;

void fail::failit::GenerateEnumTraits( std::ostream& Out_,
						 shared_ptr< const idlast::Enum > pEnum_ )
{
	shared_ptr< const idlast::Class > pClass = pEnum_->getClass();
	
	string FullClassName( pClass->getName() );
	shared_ptr< const idlast::Namespace > pCurrentNS = pClass->getNamespace();
	while( pCurrentNS->getParent() )
	{
		FullClassName = pCurrentNS->getName() + "::" + FullClassName;
		pCurrentNS = pCurrentNS->getParent();
	}
	
	Out_ << '\n';
	stringstream tagstring;
	tagstring << "class_traits< " << FullClassName << " >::enum_tags::" << pEnum_->getName();
	OutputFlags( Out_, pEnum_->getFlags(), tagstring.str() );
	
	string FullName = FullClassName + "::" + pEnum_->getName();
	
	crc32 digest;
	digest.write( FullName.c_str(), FullName.size() );
	
	Out_ << "\ttemplate< class C > struct enum_traits< C,\n"
			"\t\t" << tagstring.str() << " >\n"
			"\t{\n"
			"\t\ttypedef " << FullName << " type;\n"
			"\t\tstatic uint32_t Digest() { return 0x" << digest.result() << "; }\n"
			"\t\tstatic const char* Name() { return \"" << pEnum_->getName() << "\"; }\n\n"
			"\t\ttemplate< class V > static void VisitValues( V& Visitor_ )\n"
			"\t\t{\n";
	
	const idlast::Enum::ValueSet& Values = pEnum_->getValues();
	idlast::Enum::ValueSet::const_iterator it;
	for( it = Values.begin(); it != Values.end(); ++it )
		Out_ << "\t\t\tVisitor_.declEnumValue( \"" << *it << "\", " << FullClassName << "::" << *it << " );\n";

	Out_ <<	"\t\t}\n"
			"\t};\n";
}
