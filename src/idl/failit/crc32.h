/*
 * This is a c++ized version of the crc32 implementation from PostgreSQL.
 *
 * Portions Copyright (c) 2007, Antoine Chavasse <a.chavasse@gmail.com>
 * Portions Copyright (c) 1996-2006, PostgreSQL Global Development Group
 * Portions Copyright (c) 1994, Regents of the University of California
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without a written
 * agreement is hereby granted, provided that the above copyright notice
 * and this paragraph and the following two paragraphs appear in all copies.
 *
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY FOR
 * DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING
 * LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 * EVEN IF THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE,
 * SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
#ifndef FAIL_CORE_FAILIT_CRC32_H
#define FAIL_CORE_FAILIT_CRC32_H

#include <cstdint>

namespace fail { namespace failit
{
	extern const uint32_t pg_crc32_table[];

	class crc32
	{
		public:
			crc32() : crc( 0xffffffffUL )
			{
			}
			
			uint32_t result()
			{
				uint32_t res = crc ^ 0xffffffffUL;
				crc = 0xffffffffUL;
				return res;
			}
			
			void write( uint8_t c )
			{				
				int tab_index = ( ( int )( crc >> 24 ) ^ c ) & 0xFF;
				crc = pg_crc32_table[tab_index] ^ ( crc << 8 );
			}
			
			void write( const char* pData_, uint32_t Len_ )
			{
				write( reinterpret_cast< const uint8_t* >( pData_ ), Len_ );
			}
	
			void write( const uint8_t* pData_, uint32_t Len_ )
			{			
				while ( Len_-- )
					write( *pData_++ );
			}
			
		private:
			uint32_t crc;
	};
}}

#endif
