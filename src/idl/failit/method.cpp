/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "failit.h"

using namespace fail::failit;

namespace
{
	using namespace fail;
	void GenerateFunctionTraits( ostream& Out_,
								const idlast::Overloads& Overloads_,
								bool bStatic_,
								const char* pName_,
							    bool bStruct_ );
	
	int GenerateParamNumOverloadResolver( ostream& Out_,
								const idlast::Overloads& Overloads_, 
								std::string Indent_,
								int OvlId_ );
			
	int GenerateTypeOverloadResolver( ostream& Out_,
									const idlast::Overloads& Overloads_,
									std::string Indent_,
									int OvlId_ );
}

void fail::failit::GenerateMethodTraits( ostream& Out_,
							shared_ptr< const idlast::Class > pClass_,
							shared_ptr< const idlast::Method > pMtd_,
							bool bStatic_ )
{
	string FullClassName( pClass_->getName() );
	shared_ptr< const idlast::Namespace > pCurrentNS = pClass_->getNamespace();
	while( pCurrentNS->getParent() )
	{
		FullClassName = pCurrentNS->getName() + "::" + FullClassName;
		pCurrentNS = pCurrentNS->getParent();
	}
	
	Out_ << '\n';
	stringstream tagstring;
	tagstring << "class_traits< " << FullClassName << " >::";
			
	if( bStatic_ )
		tagstring << "static_";
			
	tagstring << "method_tags::" << pMtd_->getName();
	OutputFlags( Out_, pMtd_->getFlags(), tagstring.str() );
	
	Out_ << "\ttemplate< class C > struct method_traits< C,\n"
			"\t\t" << tagstring.str() << " >\n"
			"\t{\n"
			"\t\tstatic const char* Name() { return \"" << pMtd_->getName() << "\"; }\n";
			
	GenerateFunctionTraits( Out_, pMtd_->getOverloads(), bStatic_, pMtd_->getName().c_str(), false );
			
	Out_ <<	"\t};\n";
}

void fail::failit::GenerateCtorTraits( ostream& Out_,
									 shared_ptr< const idlast::Class > pClass_,
									 bool bStruct_ )
{
	string FullClassName( pClass_->getName() );
	shared_ptr< const idlast::Namespace > pCurrentNS = pClass_->getNamespace();
	while( pCurrentNS->getParent() )
	{
		FullClassName = pCurrentNS->getName() + "::" + FullClassName;
		pCurrentNS = pCurrentNS->getParent();
	}
	
	Out_ << '\n';
	stringstream tagstring;
	tagstring << "class_traits< " << FullClassName << " >::ctor_tag";
	OutputFlags( Out_, pClass_->getCtorFlags(), tagstring.str() );	
	
	Out_ << "\ttemplate< class C > struct method_traits< C,\n"
			"\t\t" << tagstring.str() << " >\n"
			"\t{\n"
			"\t\tstatic const char* Name() { return \"ctor_\"; }\n";;
			
	GenerateFunctionTraits( Out_, pClass_->getConstructors(), false, NULL, bStruct_ );
			
	Out_ <<	"\t};\n";
}

namespace
{

void GenerateFunctionTraits( ostream& Out_,
							const idlast::Overloads& Overloads_,
							bool bStatic_,
							const char* pName_,
						    bool bStruct_ )
{	
	idlast::Overloads::const_iterator ovit;
	int ovlcount = 0;
	int numoverloads = Overloads_.size();

	for( ovit = Overloads_.begin(); ovit != Overloads_.end(); ++ovit )
	{
		Out_ << "\n\t\ttemplate< class CC > struct ";
		
		if( numoverloads == 1 )
			Out_ << "CallWrapper";
		else
			Out_ << "Overload" << ovlcount;
		
		Out_ << "\n"
				"\t\t{\n"
				"\t\t\tstatic bool Call( CC& CallContext_ )\n"
				"\t\t\t{\n";
				
		if( !bStatic_ && pName_ )
			Out_ << "\t\t\t\tC& Obj = CallContext_.getObject();\n";
		Out_ << "\t\t\t\tCallContext_.numExpectedParams( " << ovit->first << " );\n";

		const idlast::ParamList& Params = *ovit->second;
		idlast::ParamList::const_iterator paramit;
		for( paramit = Params.begin(); paramit != Params.end(); ++paramit )
		{
			Out_ << "\n\t\t\t\t";
			PrintType( Out_, paramit->second );
			Out_ << ' ' << paramit->first << ";\n"
					"\t\t\t\tif( !CallContext_.template getParam( \"" << paramit->first
						<< "\", " << paramit->first << " ) ) return false;\n";
		}
		
		if( !Params.empty() )
			Out_ << '\n';
		
		Out_ << "\t\t\t\t";
		
		if( !pName_ )
			// No name: this is a constructor.
			Out_ << "CallContext_.template constructObject(";
		else
		{
			// Plain method call, with or without a return value...
			if( Params.getReturnType() )
			{
				Out_ << "CallContext_.template setReturnValue\n"
						"\t\t\t\t\t< ";
				PrintType( Out_, Params.getReturnType() );
				Out_ << " >\n"
						"\t\t\t\t\t( ";
			}
		
			// And either static or not.
			if( bStatic_ )
				Out_ << "C::";
			else
				Out_ << "Obj.";
			
			Out_ << pName_ << '(';
		}
		
		for( paramit = Params.begin(); paramit != Params.end(); ++paramit )
		{
			if( paramit != Params.begin() )
				Out_ << ',';
			Out_ << ' ' << paramit->first;
		}
				
		if( !Params.empty() )
			Out_ << ' ';

		Out_ << ')';
		
		if( pName_ && Params.getReturnType() )
			Out_ << " )";
		
		Out_ <<	";\n"
				"\t\t\t\treturn true;\n"
				"\t\t\t}\n"
				"\t\t};\n";
		++ovlcount;
	}
		
	if( numoverloads > 1 )
	{
		Out_ << "\n\t\ttemplate< class CC > struct CallWrapper : public";
		GenerateParamNumOverloadResolver( Out_, Overloads_, "\t\t\t", 0 );
		
		Out_ <<	"\n\t\t{};\n";
	}
}
	
int GenerateParamNumOverloadResolver( ostream& Out_,
							const idlast::Overloads& Overloads_, 
							string Indent_,
							int OvlId_ )
{
	int minparams = Overloads_.begin()->first;
	int maxparams = Overloads_.rbegin()->first;
	
	if( minparams == maxparams )
	{
		OvlId_ = GenerateTypeOverloadResolver( Out_, Overloads_, Indent_, OvlId_ );
		return OvlId_;
	}
	
	int split = ( minparams + maxparams ) / 2;
	
	idlast::Overloads submap1( Overloads_.begin(), Overloads_.upper_bound( split ) );
	idlast::Overloads submap2( Overloads_.upper_bound( split ), Overloads_.end() );
	
	Out_ << '\n' << Indent_ << "OvlCountRes< CC, " << split << ",";
	OvlId_ = GenerateParamNumOverloadResolver( Out_, submap1, Indent_ + '\t', OvlId_ );
	Out_ << ",";
	OvlId_ = GenerateParamNumOverloadResolver( Out_, submap2, Indent_ + '\t', OvlId_ );
	Out_ << '\n' << Indent_ << ">";
	
	return OvlId_;
}
		
int GenerateTypeOverloadResolver( ostream& Out_,
								const idlast::Overloads& Overloads_,
								string Indent_,
								int OvlId_ )
{
	if( Overloads_.size() == 1 )
	{
		Out_ << '\n' << Indent_ << "Overload" << OvlId_ << "< CC >";
		return OvlId_ + 1;
	}
	
	Out_ << '\n' << Indent_ << "OvlTypeRes< CC,\n"
			<< Indent_ << '\t' << "Overload" << OvlId_ << "< CC >,";
	idlast::Overloads::const_iterator it = Overloads_.begin();
	++it;
	idlast::Overloads submap( it, Overloads_.end() );
	OvlId_ = GenerateTypeOverloadResolver( Out_, submap, Indent_ + '\t', OvlId_ + 1 );
	Out_ << '\n' << Indent_ << ">";
	
	return OvlId_;
}

}
