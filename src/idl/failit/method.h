/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FAILIT_METHOD_H_
#define FAIL_FAILIT_METHOD_H_

namespace fail { namespace failit
{
	void GenerateMethodTraits( std::ostream& Out_,
							 shared_ptr< const idlast::Class > pClass_,
							 shared_ptr< const idlast::Method > pMtd_,
							 bool bStatic_ );
	
	void GenerateCtorTraits( std::ostream& Out_,
							 shared_ptr< const idlast::Class > pClass_,
						   	 bool bStruct_ );
}}

#endif
