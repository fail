/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "failit.h"

using namespace fail;
using namespace fail::failit;

int main( int argc, char** argv )
{
	try
	{	
		if( argc != 3 )
		{
			cout << "Usage: failit <idl file> <module name>\n";
			return 0;
		}

		shared_ptr< const idlast::Namespace > pAnonNamespace = idlparser::Parse( argv[1] );
			//	static_cast< const idlast::Namespace* >(  );
		
		// Look in the ast for the namespace for which we should generate the files.
		string NSName( argv[2] );
		string::size_type start = 0;
		string::size_type end = 0;
		
		shared_ptr< const idlast::Namespace > pNamespace = pAnonNamespace;

		do
		{
			end = NSName.find( "::", start );
			string name( NSName, start, end == std::string::npos ? end : end - start );

			pNamespace = pNamespace->getNamespaces().get( name );
			
			if( !pNamespace )
				throw std::runtime_error( "Namespace '" + NSName + "' not defined in '"
						+ argv[1] + "'." );
			
			start = end + 2;
		} while( end != string::npos );
		
		GenerateModuleFiles( pNamespace );

		// Generate class headers
		const idlast::Dictionary< idlast::Class >& Classes = pNamespace->getClasses();
		idlast::Dictionary< idlast::Class >::const_iterator clit;
		
		for( clit = Classes.begin(); clit != Classes.end(); ++clit )
		{
			//GenerateClassBaseFile( clit->second );
			GenerateClassTraitsFile( clit->second, false );
		}
		
		// Generate struct headers
		const idlast::Dictionary< idlast::Class >& Structs = pNamespace->getStructs();
		idlast::Dictionary< idlast::Class >::const_iterator stit;
		
		for( stit = Structs.begin(); stit != Structs.end(); ++stit )
			GenerateClassTraitsFile( stit->second, true );
	}
	catch( const std::exception& e )
	{
		cout << e.what() << endl;
		return -1;
	}
	
	return 0;
}
