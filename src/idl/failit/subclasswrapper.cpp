/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "failit.h"

using namespace fail;
using namespace fail::idlast;

namespace
{
	void GenerateMethodOverloadWrapper(
		ostream& Out, const string& FullClassName, shared_ptr< const Method > pMethod, shared_ptr< const ParamList > pOverload )
	{
		Out << "\n\t\tvirtual ";
		
		if( pOverload->getReturnType() )
			failit::PrintType( Out, pOverload->getReturnType() );
		else
			Out << "void";
			
		Out << " " << pMethod->getName() << "(";
		
		ParamList::const_iterator it;
		for( it = pOverload->begin(); it != pOverload->end(); ++it )
		{
			if( it != pOverload->begin() )
				Out << ',';
			Out << ' ';
			failit::PrintType( Out, it->second, true );
			Out << ' ' << it->first;
		}
		
		if( !pOverload->empty() )
			Out << ' ';
		
		Out <<	")";
		
		if( pOverload->isConst() )
			Out << " const";
		
		Out	<<	"\n"
				"\t\t{\n"
				"\t\t\ttypename subclass_mixin::fail_call_context cc;\n\n"
				"\t\t\tif( !fail_prepareCall( cc, \"" << pMethod->getName() << "\" ) )\n"
				"\t\t\t";
		
		if( pOverload->getReturnType() )
			Out << "\treturn ";
		else Out << "{\n"
					"\t\t\t\t";
		
		Out << FullClassName << "::" << pMethod->getName() << "(";
		for( it = pOverload->begin(); it != pOverload->end(); ++it )
		{
			if( it != pOverload->begin() )
				Out << ',';
			Out << ' ' << it->first;
		}
		
		if( !pOverload->empty() )
			Out << ' ';
		
		Out <<	");\n";
		
		if( !pOverload->getReturnType() )
			Out <<	"\t\t\t\treturn;\n"
					"\t\t\t}\n";
		
		Out << '\n';
		
		for( it = pOverload->begin(); it != pOverload->end(); ++it )
		{
			Out << "\t\t\tcc.template addParam( " << it->first << " );\n";
		}
		
		if( pOverload->getReturnType() )
		{
			Out << "\t\t\treturn cc.template call< ";
			failit::PrintType( Out, pOverload->getReturnType() );
			Out << " >();\n";
		}
		else
			Out << "\t\t\tcc.call();\n";	
		
		Out <<	"\t\t}\n";
	}
	
	void GenerateMethodWrapper( ostream& Out, const string& FullClassName, shared_ptr< const Method > pMethod )
	{
		const Overloads overloads = pMethod->getOverloads();
		Overloads::const_iterator it;
		for( it = overloads.begin(); it != overloads.end(); ++it )
			GenerateMethodOverloadWrapper( Out, FullClassName, pMethod, it->second );
	}
}

void fail::failit::GenerateSubClassWrapper( ostream& Out,
										shared_ptr< const Class > pClass,
		  								const std::string& FullClassName )
{
	Out <<	"\ttemplate< class subclass_mixin >\n"
			"\tstruct subclass_wrapper< " << FullClassName << ", subclass_mixin > :\n"
			"\t\tpublic subclass_mixin\n"
			"\t{\n"
			"\t\ttemplate< typename... args_types > subclass_wrapper( args_types&&... args ) :\n"
			"\t\t\tsubclass_mixin( forward< args_types >( args )... )\n"
			"\t\t{}\n";
	
	// Establish a list of all the classes into which to look for
	// virtual methods to wrap. That is, the class we got plus all
	// of its direct and indirect superclasses (without doubles, as
	// they can happen because of multiple inheritance)
	set< shared_ptr< const Class > > classes;

	{
		stack< shared_ptr< const Class > > clstack;
		clstack.push( pClass );
		
		while( !clstack.empty() )
		{
			shared_ptr< const Class > pCurrentClass = clstack.top();
			clstack.pop();
			
			// Push all direct superclasses of the current one
			const list< shared_ptr< ClassRef > >& sclist = pCurrentClass->getSuperClasses();
			list< shared_ptr< ClassRef > >::const_iterator scit;
			for( scit = sclist.begin(); scit != sclist.end(); ++scit )
				clstack.push( **scit );
			
			// Insert the current class into the set
			classes.insert( pCurrentClass );
		}
	}
	
	// create the list of methods to wrap (this is to ensure that the wrappers
	// will be generated in the same order at each execution of FAILIT, which wouldn't
	// happen if we rely on the order given by the order of the class pointers in
	// a std::set)
	map< string, shared_ptr< const Method > > methodsToWrap;
	
	set< shared_ptr< const Class > >::const_iterator clit;
	for( clit = classes.begin(); clit != classes.end(); ++clit )
	{
		const idlast::Dictionary< Method >& methods = ( *clit )->getMethods();
		idlast::Dictionary< Method >::const_iterator mtdit;

		for( mtdit = methods.begin(); mtdit != methods.end(); ++mtdit )
			if( mtdit->second->testFlag( "Virtual", false ) )
				methodsToWrap.insert( make_pair( mtdit->second->getName(), mtdit->second ) );
	}
	
	map< string, shared_ptr< const Method > >::const_iterator mtdit;
	for( mtdit = methodsToWrap.begin(); mtdit != methodsToWrap.end(); ++mtdit )
	{
		GenerateMethodWrapper( Out, FullClassName, mtdit->second );
	}
	
	Out <<	"\t};\n";
}
