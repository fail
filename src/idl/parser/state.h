/*
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IDL_PARSER_STATE_H
#define FAIL_IDL_PARSER_STATE_H

namespace fail { namespace idlparser
{
	struct State : pegtl::capture_map
	{
		State( shared_ptr< idlast::AST > pAST_ ) :
			pAST( pAST_ ),
			pCurrentNamespace( pAST_ ),
			pCurrentFlags( &ClassFlags ),
			bConstPointer( false ),
			bVirtualMethod( false )
		{
			// TODO: implement actions in the grammar to track this
			// using the directives inserted by the preprocessor
			// Also wrap eol into a rule that apply an action that increments the line number
			CurrentFilename = "filename_nyi";
			CurrentLine = 1;
		}
		
		std::string						CurrentFilename;
		int								CurrentLine;
		
		shared_ptr< idlast::AST >		pAST;
		shared_ptr< idlast::Namespace >	pCurrentNamespace;

		shared_ptr< idlast::Class >		pCurrentClass;
		shared_ptr< idlast::ClassRef >	pCurrentClassRef;
		
		shared_ptr< idlast::Enum >		pCurrentEnum;
		shared_ptr< idlast::EnumRef >	pCurrentEnumRef;
		
		idlast::Flags					ClassFlags;
		idlast::Flags					MembersFlags;
		idlast::Flags*					pCurrentFlags;
		
		shared_ptr< idlast::Type >				pCurrentType;
		stack< shared_ptr< idlast::Type > >		TypeStack;
		bool									bConstPointer;
		
		shared_ptr< idlast::ParamList >			pCurrentParamList;
		bool									bVirtualMethod;

		shared_ptr< idlast::Signal >			pCurrentSignal;
	};
}}

#endif
