/*
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "parser.h"
#include "grammar.h"
#include "state.h"

using namespace fail::idlparser;

shared_ptr< const fail::idlast::AST > fail::idlparser::Parse( const char* pFilename )
{
	std::shared_ptr< idlast::AST > pAST( new idlast::AST );

	//pegtl::basic_parse_string< Grammar >( "  namespace // ffff" );

	State s( pAST );
	pegtl::basic_parse_file< Grammar >( pFilename, s );
	
	//std::cout << "captured class name: " << captures[ cap_ClassName ] << std::endl;
	//std::cout << "parsing ok\n";
	pAST->resolveRefs();
	//std::cout << "resolve refs ok\n";
	return pAST;
}
