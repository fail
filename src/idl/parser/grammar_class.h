/*
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IDL_PARSER_GRAMMAR_CLASS_H
#define FAIL_IDL_PARSER_GRAMMAR_CLASS_H

#include "actions_class.h"

namespace fail { namespace idlparser
{
	struct InheritanceList;
	struct ClassContent;
	struct Signal;
	struct Attribute;
	struct Function;
	struct Method;
	struct StaticMethod;
	struct Ctor;
	struct ParamList;

	struct Class :
		ifmust<
			KWclass,
			seq<
				Spacing,
				ifapply<
					ifapply< identifier, capture< cap_ClassName > >,
					NewClass >,
				opt< InheritanceList >,
				enclose< LeftBrace, ClassContent, RightBrace >,
				SemiColon,
				apply< EndClass >
			>
		> {};
		
	struct Struct :
		ifmust<
			KWstruct,
			seq<
				Spacing,
				ifapply<
					ifapply< identifier, capture< cap_ClassName > >,
					NewStruct >,
				opt< InheritanceList >,
				enclose< LeftBrace, ClassContent, RightBrace >,
				SemiColon
			>
		> {};
		
	struct ClassRef :
		seq<
			apply< NewClassRef >,
			FailList< identifier, ColonColon, AddClassRefComponent >
		> {};
		
	struct InheritanceList :
		ifmust<
			Colon,
			FailList< ClassRef, Comma, AddSuperClass >
		> {};
	
	struct ClassContent :
		sor<
			pegtl::plus< sor< FlagList, Signal, Function, Attribute, Enum > >,
			Spacing
		> {};
		
	struct Attribute :
		seq<
			Type,
			ifapply< identifier, capture< cap_MemberName > >,
			SemiColon,
			apply< NewAttribute >
		> {};
	
	struct Signal :
		seq<
			Padded< KWSignal >, apply< NewSignal >,
			Less,
			opt< FailList< Type, Comma, AddSignalParam > >,
			Greater,
			ifapply< identifier, AddSignal >,
			SemiColon
		> {};
	
	struct Function :
		sor<
			ifapply< StaticMethod, NewStaticMethod >,
			ifapply< Ctor, NewCtor >,
			ifapply< Method, NewMethod >
		> {};
	
	struct Method :
		seq<
			opt< Padded< ifapply< KWvirtual, SetVirtual > > >,
			sor< Padded< KWvoid >, Type >,
			not_at< capture< cap_ClassName > >,
			ifapply< identifier, capture< cap_MemberName > >,
			ParamList,
			opt< ifapply< KWconst, SetMethodConst > >,
			SemiColon
		> {};
	
	struct StaticMethod :
		seq<
			Padded< KWstatic >,
			sor< Padded< KWvoid >, Type >,
			not_at< capture< cap_ClassName > >,
			ifapply< identifier, capture< cap_MemberName > >,
			ParamList,
			SemiColon
		> {};
		
	struct Ctor :
		seq<
			Padded< capture< cap_ClassName > >,
			ParamList,
			SemiColon
		> {};
		
	struct Param :
		seq<
			Type,
			ifapply< identifier, AddParam >
		> {};
		
	struct ParamList :
		seq<
			LeftParen,
			apply< NewParamList >,
			opt< FailList< Param, Comma > >,
			RightParen
		> {};
}}

#endif
