/*
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IDL_PARSER_GRAMMAR_TYPE_H
#define FAIL_IDL_PARSER_GRAMMAR_TYPE_H

#include "actions_type.h"

namespace fail { namespace idlparser
{
	struct NestedType;
	struct BasicType;
	struct EnumType;
	struct PointerType;
	struct STLType;
	struct ClassRef;
	
	struct Type : ifapply< NestedType, PopType > {};
	
	struct NestedType :
		seq<
			opt< Spacing >,
			sor<
				EnumType,
				BasicType,
				PointerType,
				STLType,
				ifapply< ClassRef, PushStructRefType > 
			>,
			opt< Spacing >
		> {};
			
	struct BasicType :
		sor<
			ifapply< KWuint8_t, PushBasicType< idlast::e_uint8_t > >,
			ifapply< KWint8_t, PushBasicType< idlast::e_int8_t > >,
			ifapply< KWuint16_t, PushBasicType< idlast::e_uint16_t > >,
			ifapply< KWint16_t, PushBasicType< idlast::e_int16_t > >,
			ifapply< KWuint32_t, PushBasicType< idlast::e_uint32_t > >,
			ifapply< KWint32_t, PushBasicType< idlast::e_int32_t > >,
			ifapply< KWuint64_t, PushBasicType< idlast::e_uint64_t > >,
			ifapply< KWint64_t, PushBasicType< idlast::e_int64_t > >,
			
			ifapply< KWbool, PushBasicType< idlast::e_bool > >,
			ifapply< KWfloat, PushBasicType< idlast::e_float > >,
			ifapply< KWdouble, PushBasicType< idlast::e_double > >,
			ifapply< KWstring, PushBasicType< idlast::e_string > >,

			ifapply< KWDynamicBuffer, PushBasicType< idlast::e_DynamicBuffer > >
		> {};

	struct PointerType :
		seq<
			KWshared_ptr,
			Less,
			opt< Padded< ifapply< KWconst, SetConst > > >,
			ClassRef,
			Greater,
			apply< PushPointerType< idlast::e_Pointer > >
		> {};
		
	struct VectorType : seq< KWvector, Less, NestedType, Greater > {};
	struct ListType : seq< KWlist, Less, NestedType, Greater > {};
	struct PairType : seq< KWpair, Less, NestedType, Comma, NestedType, Greater > {};
	struct MapType : seq< KWmap, Less, NestedType, Comma, NestedType, Greater > {};

	struct STLType :
		sor<
			ifapply< VectorType, PushMonadicType< idlast::e_vector > >,
			ifapply< ListType, PushMonadicType< idlast::e_list > >,
			ifapply< PairType, PushDyadicType< idlast::e_pair > >,
			ifapply< MapType, PushDyadicType< idlast::e_map > >
		> {};
		
	struct EnumType : seq< Padded< KWenum >, EnumRef, apply< PushEnumType > > {};
}}

#endif
