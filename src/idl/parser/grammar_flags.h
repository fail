/*
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IDL_PARSER_GRAMMAR_FLAGS_H
#define FAIL_IDL_PARSER_GRAMMAR_FLAGS_H

#include "actions_flags.h"

namespace fail { namespace idlparser
{
	struct FlagTrue :
		Padded<
			ifapply< identifier, SetFlagTrue >
		> {};

	struct FlagFalse :
		seq<
			Bang,
			Padded<
				ifapply< identifier, SetFlagFalse >
			> > {};
	
	struct Flag :
		sor<
			FlagFalse,
			FlagTrue
		> {};
	
	struct FlagList :
		ifmust<
			LeftBracket,
			seq<
				sor<
					ifapply< KWdefault, ResetAllFlags >,
					pegtl::plus< Flag >
				>,
				RightBracket
			>
		> {};
}}

#endif
