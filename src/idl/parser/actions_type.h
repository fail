/*
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IDL_PARSER_ACTIONS_TYPE_H
#define FAIL_IDL_PARSER_ACTIONS_TYPE_H

namespace fail { namespace idlparser
{
	struct PopType : action_base< PopType >
	{
		static void apply( const std::string& str, State& s )
		{
			s.pCurrentType = s.TypeStack.top();
			//std::cout << "pop type " << m_State.m_pCurrentType << std::endl;
						
			// Clear the stack completly, as some garbage may be lying around if the parser
			// previously backtracked after parsing a type.
			while( !s.TypeStack.empty() )
				s.TypeStack.pop();
		}
	};
	
	template< idlast::TypeID TypeID_ > struct PushBasicType :
		action_base< PushBasicType< TypeID_ > >
	{
		static void apply( const std::string& str, State& s )
		{
			shared_ptr< idlast::Type > pType( new idlast::BasicType< TypeID_ >
					( s.CurrentFilename.c_str(), s.CurrentLine ) );
			s.TypeStack.push( pType );
		}
	};
	
	template< idlast::TypeID TypeID_ > struct PushMonadicType :
		action_base< PushMonadicType< TypeID_ > >
	{
		static void apply( const std::string& str, State& s )
		{
			shared_ptr< idlast::Type > pSubType = s.TypeStack.top();
			s.TypeStack.pop();
			
			shared_ptr< idlast::MonadicType< TypeID_ > > pType( new idlast::MonadicType< TypeID_ >
					( s.CurrentFilename.c_str(), s.CurrentLine ) );

			pType->setSubType( pSubType );
					
			s.TypeStack.push( pType );
		}
	};
	
	template< idlast::TypeID TypeID_ > struct PushDyadicType :
		action_base< PushDyadicType< TypeID_ > >
	{
		static void apply( const std::string& str, State& s )
		{
			shared_ptr< idlast::Type > pSecondType = s.TypeStack.top();
			s.TypeStack.pop();
			shared_ptr< idlast::Type > pFirstType = s.TypeStack.top();
			s.TypeStack.pop();
			
			shared_ptr< idlast::DyadicType< TypeID_ > > pType( new idlast::DyadicType< TypeID_ >
					( s.CurrentFilename.c_str(), s.CurrentLine ) );

			pType->setFirstType( pFirstType );
			pType->setSecondType( pSecondType );
					
			s.TypeStack.push( pType );
		}
	};
	
	struct PushEnumType : action_base< PushEnumType >
	{
		static void apply( const std::string& str, State& s )
		{
			shared_ptr< idlast::EnumType > pType = shared_ptr< idlast::EnumType >( new idlast::EnumType
					( s.CurrentFilename.c_str(), s.CurrentLine ) );

			pType->setEnumRef( s.pCurrentEnumRef );
			s.pCurrentEnumRef.reset();

			s.TypeStack.push( pType );
		}
	};
	
	struct PushStructRefType : action_base< PushStructRefType >
	{
		static void apply( const std::string& str, State& s )
		{			
			shared_ptr< idlast::StructRefType > pType = shared_ptr< idlast::StructRefType >( new idlast::StructRefType
					( s.CurrentFilename.c_str(), s.CurrentLine ) );

			pType->setClassRef( s.pCurrentClassRef );
			s.pCurrentClassRef.reset();

			s.TypeStack.push( pType );
		}
	};
	
	struct SetConst : action_base< SetConst >
	{
		static void apply( const std::string& str, State& s )
		{			
			s.bConstPointer = true;
		}
	};
	
	template< idlast::TypeID TypeID_ > struct PushPointerType :
		action_base< PushPointerType< TypeID_ > >
	{
		static void apply( const std::string& str, State& s )
		{
			shared_ptr< idlast::BasicPointerType< TypeID_ > > pType = shared_ptr< idlast::BasicPointerType< TypeID_ > >( 
					new idlast::BasicPointerType< TypeID_ >( s.CurrentFilename.c_str(), s.CurrentLine ) );
		
			pType->setClassRef( s.pCurrentClassRef );
			pType->setConst( s.bConstPointer );
			s.pCurrentClassRef.reset();
			s.bConstPointer = false;

			s.TypeStack.push( pType );
		}
	};
}}

#endif
