/*
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IDL_PARSER_ACTIONS_ENUM_H
#define FAIL_IDL_PARSER_ACTIONS_ENUM_H

namespace fail { namespace idlparser
{
	struct NewEnum : action_base< NewEnum >
	{
		static void apply( const std::string& str, State& s )
		{
			s.pCurrentEnum = shared_ptr< idlast::Enum >(
				new idlast::Enum( str.c_str(), s.CurrentFilename.c_str(), s.CurrentLine ) );

			s.pCurrentEnum->setFlags( s.MembersFlags );
			s.pCurrentClass->addEnum( s.pCurrentEnum );
		}
	};

	struct AddEnumValue : action_base< AddEnumValue >
	{
		static void apply( const std::string& str, State& s )
		{
			s.pCurrentEnum->addValue( str.c_str() );
		}
	};
	
	struct NewEnumRef : action_base< NewEnumRef >
	{
		static void apply( const std::string& str, State& s )
		{
			s.pCurrentEnumRef = shared_ptr< idlast::EnumRef >(
				new idlast::EnumRef( s.CurrentFilename.c_str(), s.CurrentLine ) );
			
			//s.pCurrentEnumRef->addComponent( str.c_str() );
		}
	};
	
	struct AddEnumRefComponent : action_base< AddEnumRefComponent >
	{
		static void apply( const std::string& str, State& s )
		{
			s.pCurrentEnumRef->addComponent( str.c_str() );
		}
	};
}}

#endif
