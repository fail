/*
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IDL_PARSER_ACTIONS_NAMESPACE_H
#define FAIL_IDL_PARSER_ACTIONS_NAMESPACE_H

namespace fail { namespace idlparser
{
	struct NewNamespace : action_base< NewNamespace >
	{
		static void apply( const std::string& str, State& s )
		{
			shared_ptr< idlast::Namespace > pNewNamespace(
				new idlast::Namespace( str.c_str(), s.CurrentFilename.c_str(), s.CurrentLine ) );
	
			s.pCurrentNamespace = s.pCurrentNamespace->addNamespace( pNewNamespace );
			s.ClassFlags.clear();
		}
	};
	
	struct EndNamespace : action_base< EndNamespace >
	{
		static void apply( const std::string& str, State& s )
		{
			s.pCurrentNamespace = s.pCurrentNamespace->getParent();
		}
	};
}}

#endif
