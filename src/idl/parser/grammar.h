/*
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IDL_PARSER_GRAMMAR_H
#define FAIL_IDL_PARSER_GRAMMAR_H

#include "pegtl.hh"
#include "state.h"

namespace fail { namespace idlparser
{
	using namespace pegtl;
	
	enum CaptureIndices
	{
		cap_ClassName,
		cap_MemberName,
		capture_last
	};
}}

#include "grammar_lowlevel.h"
#include "grammar_tokens.h"
#include "grammar_flags.h"
#include "grammar_enum.h"
#include "grammar_type.h"
#include "grammar_class.h"
#include "grammar_namespace.h"

namespace fail { namespace idlparser
{
	// Top level rule
	struct Grammar :
		seq<
			opt< Spacing >,
			star< Namespace >,
			eof
		> {};
}}

#endif
