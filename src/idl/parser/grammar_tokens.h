/*
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IDL_PARSER_GRAMMAR_TOKENS_H
#define FAIL_IDL_PARSER_GRAMMAR_TOKENS_H

#include "pegtl.hh"

namespace fail { namespace idlparser
{
	// Punctuation
	template< int C > struct Token : seq< opt< Spacing >, one< C >, opt< Spacing > > {};
	struct LeftBrace : Token< '{' > {};
	struct RightBrace : Token< '}' > {};
	struct LeftBracket : Token< '[' > {};
	struct RightBracket : Token< ']' > {};
	struct LeftParen : Token< '(' > {};
	struct RightParen : Token< ')' > {};
	struct Less : Token< '<' > {};
	struct Greater : Token< '>' > {};
	struct Colon : Token< ':' > {};
	struct SemiColon : Token< ';' > {};
	struct Comma : Token< ',' > {};
	struct Bang : Token< '!' > {};
	struct ColonColon : seq< opt< Spacing >, pegtl::string< ':', ':' >, opt< Spacing > > {};
	
	// Keywords
	struct KWnamespace : pegtl::string< 'n', 'a', 'm', 'e', 's', 'p', 'a', 'c', 'e' > {};
	struct KWclass : pegtl::string< 'c', 'l', 'a', 's', 's' > {};
	struct KWstruct : pegtl::string< 's', 't', 'r', 'u', 'c', 't' > {};
	struct KWenum : pegtl::string< 'e', 'n', 'u', 'm' > {};
	struct KWdefault : pegtl::string< 'd', 'e', 'f', 'a', 'u', 'l', 't' > {};
	struct KWconst : pegtl::string< 'c', 'o', 'n', 's', 't' > {};
	struct KWvoid : pegtl::string< 'v', 'o', 'i', 'd' > {};
	struct KWstatic : pegtl::string< 's', 't', 'a', 't', 'i', 'c' > {};
	struct KWvirtual : pegtl::string< 'v', 'i', 'r', 't', 'u', 'a', 'l' > {};
	
	struct KWSignal : pegtl::string< 'S', 'i', 'g', 'n', 'a', 'l' > {};
	
	struct KWuint8_t : pegtl::string< 'u', 'i', 'n', 't', '8', '_', 't' > {};
	struct KWint8_t : pegtl::string< 'i', 'n', 't', '8', '_', 't' > {};
	struct KWuint16_t : pegtl::string< 'u', 'i', 'n', 't', '1', '6', '_', 't' > {};
	struct KWint16_t : pegtl::string< 'i', 'n', 't', '1', '6', '_', 't' > {};
	struct KWuint32_t : pegtl::string< 'u', 'i', 'n', 't', '3', '2', '_', 't' > {};
	struct KWint32_t : pegtl::string< 'i', 'n', 't', '3', '2', '_', 't' > {};
	struct KWuint64_t : pegtl::string< 'u', 'i', 'n', 't', '6', '4', '_', 't' > {};
	struct KWint64_t : pegtl::string< 'i', 'n', 't', '6', '4', '_', 't' > {};
	
	struct KWbool : pegtl::string< 'b', 'o', 'o', 'l' > {};
	struct KWfloat : pegtl::string< 'f', 'l', 'o', 'a', 't' > {};
	struct KWdouble : pegtl::string< 'd', 'o', 'u', 'b', 'l', 'e' > {};
	struct KWstring : pegtl::string< 's', 't', 'r', 'i', 'n', 'g' > {};
	struct KWDynamicBuffer : pegtl::string< 'D', 'y', 'n', 'a', 'm', 'i', 'c', 'B', 'u', 'f', 'f', 'e', 'r' > {};
	
	struct KWshared_ptr : pegtl::string< 's', 'h', 'a', 'r', 'e', 'd', '_', 'p', 't', 'r' > {};
	struct KWlist : pegtl::string< 'l', 'i', 's', 't' > {};	
	struct KWvector : pegtl::string< 'v', 'e', 'c', 't', 'o', 'r' > {};	
	struct KWpair : pegtl::string< 'p', 'a', 'i', 'r' > {};	
	struct KWmap : pegtl::string< 'm', 'a', 'p' > {};	
}}

#endif
