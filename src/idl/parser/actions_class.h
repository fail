/*
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IDL_PARSER_ACTIONS_CLASS_H
#define FAIL_IDL_PARSER_ACTIONS_CLASS_H

namespace fail { namespace idlparser
{
	struct NewClass : action_base< NewClass >
	{
		static void apply( const std::string& str, State& s )
		{
			s.pCurrentClass = shared_ptr< idlast::Class >(
				new idlast::Class( str.c_str(), s.CurrentFilename.c_str(), s.CurrentLine ) );
	
			s.pCurrentNamespace->addClass( s.pCurrentClass );

			s.pCurrentClass->setFlags( s.ClassFlags );
			s.MembersFlags.clear();
			s.pCurrentFlags = &s.MembersFlags;
		}
	};
	
	struct NewStruct : action_base< NewClass >
	{
		static void apply( const std::string& str, State& s )
		{
			s.pCurrentClass = shared_ptr< idlast::Class >(
				new idlast::Class( str.c_str(), s.CurrentFilename.c_str(), s.CurrentLine ) );
	
			s.pCurrentNamespace->addStruct( s.pCurrentClass );

			s.pCurrentClass->setFlags( s.ClassFlags );
			s.pCurrentClass->setFlag( "ValueType", true );
			s.MembersFlags.clear();
			s.pCurrentFlags = &s.MembersFlags;
		}
	};
	
	struct EndClass : action_base< EndClass >
	{
		static void apply( const std::string& str, State& s )
		{
			s.pCurrentFlags = &s.ClassFlags;
		}
	};
	
	struct NewClassRef : action_base< NewClassRef >
	{
		static void apply( const std::string& str, State& s )
		{
			s.pCurrentClassRef = shared_ptr< idlast::ClassRef >(
				new idlast::ClassRef( s.CurrentFilename.c_str(), s.CurrentLine ) );
		}
	};
	
	struct AddClassRefComponent : action_base< AddClassRefComponent >
	{
		static void apply( const std::string& str, State& s )
		{
			s.pCurrentClassRef->addComponent( str.c_str() );
		}
	};
	
	struct AddSuperClass : action_base< AddSuperClass >
	{
		static void apply( const std::string& str, State& s )
		{
			s.pCurrentClass->addSuperClass( s.pCurrentClassRef );
			s.pCurrentClassRef.reset();
		}
	};
	
	
	struct NewAttribute : action_base< NewAttribute >
	{
		static void apply( const std::string& str, State& s )
		{
			shared_ptr< idlast::Attribute > pAttr = shared_ptr< idlast::Attribute >( new idlast::Attribute( 
				s[ cap_MemberName ], s.pCurrentType,
				s.CurrentFilename.c_str(), s.CurrentLine ) );
			
			pAttr->setFlags( s.MembersFlags );
				
			s.pCurrentClass->addAttribute( pAttr );
			s.pCurrentType.reset();
		}
	};
	
	struct NewParamList : action_base< NewParamList >
	{
		static void apply( const std::string& str, State& s )
		{		
			// The last parsed type at this point is the method return type.
			s.pCurrentParamList = shared_ptr< idlast::ParamList >(
				new idlast::ParamList( s.pCurrentType, s.CurrentFilename.c_str(), s.CurrentLine ) );
			s.pCurrentType.reset();
		}
	};
	
	struct AddParam : action_base< AddParam >
	{
		static void apply( const std::string& str, State& s )
		{
			s.pCurrentParamList->push_back( std::make_pair( str, s.pCurrentType ) );
			s.pCurrentType.reset();
		}
	};
	
	struct SetVirtual : action_base< SetVirtual >
	{
		static void apply( const std::string& str, State& s )
		{
			s.bVirtualMethod = true;
			s.pCurrentClass->setFlag( "Polymorphic", true );
		}
	};
	
	struct SetMethodConst : action_base< SetMethodConst >
	{
		static void apply( const std::string& str, State& s )
		{
			s.pCurrentParamList->setConst();
		}
	};
	
	struct NewMethod : action_base< NewMethod >
	{
		static void apply( const std::string& str, State& s )
		{
			shared_ptr< idlast::Method > pMethod = s.pCurrentClass->getOrAddMethod
				( s[ cap_MemberName ], s.MembersFlags );
			pMethod->addOverload( s.pCurrentParamList );
			s.pCurrentParamList.reset();
			
			if( s.bVirtualMethod )
			{
				pMethod->setFlag( "Virtual", true );
				s.bVirtualMethod = false;
			}
		}
	};
	
	struct NewStaticMethod : action_base< NewStaticMethod >
	{
		static void apply( const std::string& str, State& s )
		{
			shared_ptr< idlast::Method > pMethod = s.pCurrentClass->getOrAddStaticMethod
				( s[ cap_MemberName ] );
			pMethod->addOverload( s.pCurrentParamList );
			s.pCurrentParamList.reset();
		}
	};
	
	struct NewCtor : action_base< NewCtor >
	{
		static void apply( const std::string& str, State& s )
		{
			s.pCurrentClass->addConstructor( s.pCurrentParamList, s.MembersFlags );
			s.pCurrentParamList.reset();
		}
	};
	
	struct NewSignal : action_base< NewSignal >
	{
		static void apply( const std::string& str, State& s )
		{
			s.pCurrentSignal = shared_ptr< idlast::Signal > ( new idlast::Signal(
				"", s.CurrentFilename.c_str(), s.CurrentLine ) );
			
			s.pCurrentSignal->setFlags( s.MembersFlags );
		}
	};
	
	struct AddSignalParam : action_base< AddSignalParam >
	{
		static void apply( const std::string& str, State& s )
		{
			s.pCurrentSignal->addParamType( s.pCurrentType );
			s.pCurrentType.reset();
		}
	};

	struct AddSignal : action_base< AddSignal >
	{
		static void apply( const std::string& str, State& s )
		{
			s.pCurrentSignal->setName( str );
			s.pCurrentClass->addSignal( s.pCurrentSignal );
			s.pCurrentSignal.reset();
		}
	};
}}

#endif
