/*
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IDL_PARSER_ACTIONS_FLAGS_H
#define FAIL_IDL_PARSER_ACTIONS_FLAGS_H

namespace fail { namespace idlparser
{
	struct ResetAllFlags : action_base< ResetAllFlags >
	{
		static void apply( const std::string& str, State& s )
		{
			s.pCurrentFlags->clear();
		}
	};
	
	struct SetFlagTrue : action_base< SetFlagTrue >
	{
		static void apply( const std::string& str, State& s )
		{
			( *s.pCurrentFlags )[ str ] = true;
		}
	};
	
	struct SetFlagFalse : action_base< SetFlagFalse >
	{
		static void apply( const std::string& str, State& s )
		{
			( *s.pCurrentFlags )[ str ] = false;
		}
	};
}}

#endif
