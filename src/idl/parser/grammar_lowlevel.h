/*
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_IDL_PARSER_GRAMMAR_LOWLEVEL_H
#define FAIL_IDL_PARSER_GRAMMAR_LOWLEVEL_H

namespace fail { namespace idlparser
{
	// Low level lexer rules
	struct DoubleSlash : pegtl::string< '/', '/' > {};
	struct CommentStart : pegtl::string< '/', '*' > {};
	struct CommentEnd : pegtl::string< '*', '/' > {};
	
	struct Comment :
		ifmust<
			DoubleSlash,
			until< eol >
		> {};
	
	struct CommentBlock :
		ifmust<
			CommentStart,
			until< CommentEnd >
		> {};
		
	struct PreprocessorDirective :
		ifmust<
			one< '#' >,
			until< eol >
		> {};

	// The rule that matches any whitespace, comment etc.
	struct Spacing : pegtl::plus< sor< space, Comment, CommentBlock, PreprocessorDirective > > {};
	
	// Utility rule to match something padded by optional spacing
	template< typename R > struct Padded : seq< opt< Spacing >, R, opt< Spacing > > {};
	
	// Utility rule to match a list of elements with separators. It's the same as pegtl::list except
	// it applies the action to the first element as well.
	 template< typename Rule, typename Glue, typename Action = nop >
	 struct FailList : 
		seq<
			ifapply< Rule, Action >,
			star<
				ifmust< Glue,
					ifapply< Rule, Action >
				>
			>
		> {};
}}

#endif
