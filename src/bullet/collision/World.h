/*
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_BULLET_COLLISION_WORLD_H_
#define FAIL_BULLET_COLLISION_WORLD_H_

#include "core/core.h"
#include "bullet/collision/bullet-collision_export.h"
#include <btBulletCollisionCommon.h>

namespace fail { namespace bullet { namespace collision
{
	class Object;
	class Broadphase;
	
	class FLBULLET_COLLISION_EXPORT World : public enable_shared_from_this< World >
	{
		public:
 			World( shared_ptr< Broadphase > pBroadphase );
			void addObject( shared_ptr< Object > pObject );
			void removeObject( shared_ptr< Object > pObject );
			void performDiscreteCollisionDetection();
			
		private:
			template< class C, typename T > friend struct fail::attribute_traits;
			friend class Object;
			
			void addToDirtyList( shared_ptr< Object > pObj );
			void updateObjectPositions();
			
			shared_ptr< Broadphase >			m_pBroadphase;
			::btDefaultCollisionConfiguration	m_CollisionConfiguration;
			::btCollisionDispatcher				m_Dispatcher;
			shared_ptr< ::btCollisionWorld >	m_pbtColWorld;
			std::list< shared_ptr< Object > >	m_DirtyObjects;
			
			//float m_Radius;
	};
}}}

#endif
