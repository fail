/*
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "AxisSweep3.h"
#include <btBulletCollisionCommon.h>

using namespace fail::bullet::collision;

AxisSweep3::AxisSweep3( math::Vector3f worldAABBMin, math::Vector3f worldAABBMax ) :
	Broadphase( new ::btAxisSweep3(
		::btVector3(
			worldAABBMin.x(),
			worldAABBMin.y(),
			worldAABBMin.z() ),
								   
		::btVector3(
			worldAABBMax.x(),
			worldAABBMax.y(),
			worldAABBMax.z() )
	) )
{
}
