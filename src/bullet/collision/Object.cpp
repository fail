/*
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "World.h"
#include "Object.h"
#include "Shape.h"
#include <btBulletCollisionCommon.h>

using namespace fail::bullet::collision;

Object::Object() :
	m_pbtColObj( new ::btCollisionObject ),
	m_bDirty( false )
{
	m_pbtColObj->setUserPointer( this );
}

void Object::setpFrame( const shared_ptr< scenegraph::Frame >& x )
{
	disconnect();
	m_pFrame = x;
	connect();
 	markDirty();
}

void Object::setpShape( const shared_ptr< Shape >& x )
{
	m_pShape = x;
	m_pbtColObj->setCollisionShape( m_pShape->getbtShape() );
}

void Object::connect()
{
	if( m_pFrame )
		m_ConnectionHandle = m_pFrame->sig_Dirty.connect( shared_from_this(), &Object::markDirty );
}

void Object::disconnect()
{
	if( m_pFrame )
		m_pFrame->sig_Dirty.disconnect( m_ConnectionHandle );
}

void Object::markDirty()
{
	if( m_bDirty )
		return;
	
	shared_ptr< World > pWorld( m_pWorld.lock() );
	if( !pWorld )
		return;

	pWorld->addToDirtyList( shared_from_this() );
	m_bDirty = true;
}

void Object::updatePosition()
{
	float m[16];
	m_pFrame->getOpenGLMatrix( m );
	m_pbtColObj->getWorldTransform().setFromOpenGLMatrix( m );
	m_bDirty = false;
}
