/*
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_BULLET_COLLISION_OBJECT_H_
#define FAIL_BULLET_COLLISION_OBJECT_H_

#include "core/core.h"
#include "scenegraph/Frame.h"
#include "bullet/collision/bullet-collision_export.h"
#include <btBulletCollisionCommon.h>

class btCollisionObject;

namespace fail { namespace bullet { namespace collision
{
	class World;
	class Shape;
	
	class FLBULLET_COLLISION_EXPORT Object : public enable_shared_from_this< Object >
	{
		public:
			Object();
			
			const shared_ptr< scenegraph::Frame >& getpFrame() const { return m_pFrame; }
			void setpFrame( const shared_ptr< scenegraph::Frame >& x );
			
			const shared_ptr< Shape >& getpShape() const { return m_pShape; }
			void setpShape( const shared_ptr< Shape >& x );
			
			Signal< shared_ptr< Object > > sig_Contact;
			
		private:
			template< class C, typename T > friend struct fail::attribute_traits;
			friend class World;
			
			void connect();
			void disconnect();
			void markDirty();
			void updatePosition();
			
			/*static bool ContactCallback(
				btManifoldPoint& cp,
				const btCollisionObject* colObj0,
				int partId0,
				int index0,
				const btCollisionObject* colObj1,
				int partId1,
				int index1 );*/

			shared_ptr< ::btCollisionObject >	m_pbtColObj;
			shared_ptr< scenegraph::Frame >		m_pFrame;
			shared_ptr< Shape >					m_pShape;
			Signal<>::slot_handle				m_ConnectionHandle;
			weak_ptr< World >					m_pWorld;
			bool								m_bDirty;
	};
}}}

#endif
