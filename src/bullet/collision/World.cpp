/*
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "World.h"
#include "Object.h"
#include "Broadphase.h"

using namespace fail::bullet::collision;

World::World( shared_ptr< Broadphase > pBroadphase ) :
	m_pBroadphase( pBroadphase ),
	m_Dispatcher( &m_CollisionConfiguration ),
	m_pbtColWorld( new ::btCollisionWorld( &m_Dispatcher, pBroadphase->m_pBroadphase.get(), &m_CollisionConfiguration ) )
{
}

void World::addObject( shared_ptr< Object > pObject )
{
	pObject->m_pWorld = shared_from_this();
	m_pbtColWorld->addCollisionObject( pObject->m_pbtColObj.get() );
	pObject->markDirty();
}

void World::removeObject( shared_ptr< Object > pObject )
{
	m_pbtColWorld->removeCollisionObject( pObject->m_pbtColObj.get() );
}

void World::performDiscreteCollisionDetection()
{
	updateObjectPositions();
	m_pbtColWorld->performDiscreteCollisionDetection();
	
	int numManifolds = m_Dispatcher.getNumManifolds();
        
	for( int i = 0; i < numManifolds; ++i )
	{
		btPersistentManifold* contactManifold = m_Dispatcher.getManifoldByIndexInternal( i );
		btCollisionObject* obA = static_cast< btCollisionObject* >( contactManifold->getBody0() );
		btCollisionObject* obB = static_cast< btCollisionObject* >( contactManifold->getBody1() );

		int numContacts = contactManifold->getNumContacts();
		for( int j = 0; j < numContacts; ++j )
		{
			btManifoldPoint& pt = contactManifold->getContactPoint(j);
			if( pt.getDistance() < 0.f )
			{
				Object* pObj0 = static_cast< Object* >( obA->getUserPointer() );
				Object* pObj1 = static_cast< Object* >( obB->getUserPointer() );
				
				pObj0->sig_Contact( pObj1->shared_from_this() );
				pObj1->sig_Contact( pObj0->shared_from_this() );

				// Break the contact iteration loop because we only want
				// to notify once objects that they collided with each other once
				// regardless of how many contact points they have.
				break;
			}
		}
	}
}

void World::addToDirtyList( shared_ptr< Object > pObj )
{
	m_DirtyObjects.push_back( pObj );
}

void World::updateObjectPositions()
{
	std::list< shared_ptr< Object > >::const_iterator it;
	
	for( it = m_DirtyObjects.begin(); it != m_DirtyObjects.end(); ++it )
		( *it )->updatePosition();
	
	m_DirtyObjects.clear();
}
