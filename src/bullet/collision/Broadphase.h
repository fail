/*
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_BULLET_COLLISION_BROADPHASE_H_
#define FAIL_BULLET_COLLISION_BROADPHASE_H_

#include "core/core.h"
#include "bullet/collision/bullet-collision_export.h"

class btBroadphaseInterface;

namespace fail { namespace bullet { namespace collision
{
	class FLBULLET_COLLISION_EXPORT Broadphase
	{
		protected:
			Broadphase( ::btBroadphaseInterface* pBroadphase );
			virtual ~Broadphase();

		private:
			template< class C, typename T > friend struct fail::attribute_traits;
			friend class World;

			shared_ptr< ::btBroadphaseInterface > m_pBroadphase;
	};
}}}

#endif
