#! /usr/bin/lua
--[[
    Fail game engine
    Copyright 2007-2008 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

require 'qtgui'
require 'qtopengl'
require 'fail.math'
require 'fail.scenegraph'
require 'fail.scenegraph.shapes'


sg = fail.scenegraph


function CreateTestScene()
		
	-- Create cube frame
	cubeframe = sg.Frame()
		
	-- Create vertex buffer
	vb = sg.VertexBuffer()
	vb:addAttribute( sg.VertexBuffer.at_Vector3f, sg.VertexBuffer.a_Position )
	vb:resize( 8 )
	
	vec3 = fail.math.Vector3f
	vb:setVector3fAttribute( 0, 0, vec3( -1, -1, -1 ) )
	vb:setVector3fAttribute( 1, 0, vec3( -1, -1, 1 ) )
	vb:setVector3fAttribute( 2, 0, vec3( -1, 1, -1 ) )
	vb:setVector3fAttribute( 3, 0, vec3( -1, 1, 1 ) )
	vb:setVector3fAttribute( 4, 0, vec3( 1, -1, -1 ) )
	vb:setVector3fAttribute( 5, 0, vec3( 1, -1, 1 ) )
	vb:setVector3fAttribute( 6, 0, vec3( 1, 1, -1 ) )
	vb:setVector3fAttribute( 7, 0, vec3( 1, 1, 1 ) )
		
	-- Create index buffer
	ib = sg.IndexBuffer16( 24 )
	ib:setIndex( 0, 0 )
	ib:setIndex( 1, 1 )
	ib:setIndex( 2, 3 )
	ib:setIndex( 3, 2 )
		
	ib:setIndex( 4, 4 )
	ib:setIndex( 5, 6 )
	ib:setIndex( 6, 7 )
	ib:setIndex( 7, 5 )

	ib:setIndex( 8, 0 )
	ib:setIndex( 9, 4 )
	ib:setIndex( 10, 5 )
	ib:setIndex( 11, 1 )
		
	ib:setIndex( 12, 2 )
	ib:setIndex( 13, 3 )
	ib:setIndex( 14, 7 )
	ib:setIndex( 15, 6 )

	ib:setIndex( 16, 0 )
	ib:setIndex( 17, 2 )
	ib:setIndex( 18, 6 )
	ib:setIndex( 19, 4 )

	ib:setIndex( 20, 1 )
	ib:setIndex( 21, 5 )
	ib:setIndex( 22, 7 )
	ib:setIndex( 23, 3 )
		
	-- Create geometries
	geom1 = sg.Geometry( vb, ib )
	geom1:addPrimitive( sg.Primitive.t_Quads, 0, 8 )
	geom2 = sg.Geometry( vb, ib )
	geom2:addPrimitive( sg.Primitive.t_Quads, 8, 8 )
	geom3 = sg.Geometry( vb, ib )
	geom3:addPrimitive( sg.Primitive.t_Quads, 16, 8 )
		
	-- Create materials
	mat1 = sg.Material()
	mat1.Emission.value = fail.math.Vector4f( 1, 0, 0, 1 )
	mat1.Specular.value = fail.math.Vector4f( 0, 0, 0, 1 )
	mat2 = sg.Material()
	mat2.Emission.value = fail.math.Vector4f( 0, 1, 0, 1 )
	mat2.Specular.value = fail.math.Vector4f( 0, 0, 0, 1 )
	mat3 = sg.Material()
	mat3.Emission.value = fail.math.Vector4f( 0, 0, 1, 1 )
	mat3.Specular.value = fail.math.Vector4f( 0, 0, 0, 1 )
	
	-- Create drawables
	drawable1 = sg.Drawable( geom1, mat1, cubeframe )
	drawable2 = sg.Drawable( geom2, mat2, cubeframe )
	drawable3 = sg.Drawable( geom3, mat3, cubeframe )
		
	-- Create group holding all this stuff together
	cubegroup = sg.Group()
	cubegroup:add( drawable1 )
	cubegroup:add( drawable2 )
	cubegroup:add( drawable3 )
	cubegroup.pFrame = cubeframe
		
	-- Save the cube into a file, for the lulz
	--f = fail.io.FileOutputStream( "lulz.fsg" )
	--fail.fbf.Writer.Save( f, cubegroup )

	mat1.Diffuse.value = fail.math.Vector4f( 1, 0, 0, 1 )
	mat1.Emission.value = fail.math.Vector4f( 0, 0, 0, 1 )
	mat1.Ambient.value = fail.math.Vector4f( 0.2, 0.2, 0.2, 1 )
		
	mat1.bBackfaceCulling = true

	shape = cubegroup --sg.shapes.Cylinder( mat1, cubeframe )
		
	--fail.fbffile.Writer.Save( "lulz.fsg", shape )
		
	-- objdb persistence test

	--fail.objdb.Controller.GetInstance():makePersistent( shape ) --:createBundle()

	--fail.objdb.Controller.GetInstance():makePersistent( cubeframe ) --:createBundle()
	--cubeframe:createBundle()
	--shape.pBundle.Name = "Failure"
	
	-- test: forcefully drop all references to the test scene to force
	-- it to be queued for writing in the object db
	--shape = nil
	--cubegroup = nil
	--cubeframe = nil
	--drawable1 = nil
	--drawable2 = nil
--	drawable3 = nil

	collectgarbage()

--	fail.objdb.Controller.GetInstance():processWriteQueue();

	
	--fail.objdb.Controller.GetInstance():writeBundle( shape.pBundle )
	--fail.objdb.Controller.GetInstance():writeBundle( cubeframe.pBundle )

	return shape --cubegroup
		
end

scene = CreateTestScene()


--
qtapp = QApplication( 1, {"lulz"}) --( 1 + select( '#', ... ), { arg[0], ...} )
qtapp.__gc = qtapp.delete


glwidget = QGLWidget()




-- Create camera
camframe = sg.Frame()
camera = sg.Camera( camframe )
camframe.LocalToParent.position.y = -7
camframe:dirty()
	
proj = sg.PerspectiveProjection()
	
-- Create viewports
viewport = sg.ViewPort( proj, camera )
viewport.Rect = fail.math.Rectu32( 0, 0, 1024, 768 )



-- Create render passes
scenepass = sg.RenderPass( viewport )

scenepass.bClearDepthBuffer = true
scenepass.bClearColorBuffer = true



time = QTime()
time:start()

spin = 0


gvsc = QGraphicsScene()


gv = QGraphicsView()
gv:setViewportUpdateMode("FullViewportUpdate")
gv:setCacheMode({"CacheNone"})

lasttime = time:elapsed()

gvsc.drawBackground =
function( self )
	--print "lulz"
--	local deltatime = 10 / 1000
 		--collectgarbage()
		sg.RenderPass.ClearAll()
		
		--guipass:clear()

		
		--manippass:clear()
		-----manippass:prependToRenderList()
		-----maniproot.pRenderable:evaluate( manippass )
		
		scenepass:clear()
		scenepass:prependToRenderList()
		
		local deltatime = time:elapsed() - lasttime
		lasttime = lasttime + deltatime
		
		--print(deltatime)
		
		spin = spin + 150 * deltatime / 1000
		mtx = fail.math.Matrix44f()
		mtx:rotation( spin, fail.math.Vector3f( 1, 0, 0.5 ) )
		scene.pFrame.LocalToParent = mtx
		scene.pFrame.LocalToParent.position.x = 1
		
		--scene.pFrame.LocalToParent.position.y = 0
		--scene.pFrame.LocalToParent.position.z = 0
		scene.pFrame:dirty()
		
		--mtx:rotation( spin, fail.math.Vector3f( 1, 0, 0 ) )
		--camframe.LocalToParent = mtx
	--	camframe.LocalToParent.position.x = 0
	--	camframe.LocalToParent.position.y = -7
	--	camframe.LocalToParent.position.z = 2
		--camframe:dirty()
		
		scene:evaluate( scenepass )
		
		--maniproot.pRenderable:evaluate( scenepass )
			--print "paintgl lolwut"
		sg.RenderPass.RenderAll( true )

		--app:needSwap()
--		print "popopopop"
	QTimer.singleShot( 10, self, "1update()" )
	--print "kjhkjhj"
	end




gv:setViewport( QGLWidget() )

testwidget = QCalendarWidget()
proxy = gvsc:addWidget( testwidget ) --, {"Window"} )
proxy:setCacheMode("DeviceCoordinateCache")
--proxy:rotate( 30 )

gv:setScene( gvsc )

gv:setRenderHints({"Antialiasing","SmoothPixmapTransform"})


gv:show()
gv:resize(1024, 768);
qtapp.exec()
