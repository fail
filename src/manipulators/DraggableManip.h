/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_MANIPULATORS_DRAGGABLEMANIP_H_
#define FAIL_MANIPULATORS_DRAGGABLEMANIP_H_

#include "core/core.h"
#include "ContainerManipulator.h"

namespace fail { namespace manipulators
{
	class FLMANIPULATORS_EXPORT DraggableManip : public Manipulator
	{
		public:
			enum e_State
			{
				s_Idle,
				s_Hilighted,
				s_Dragging
			};	
			
			DraggableManip( Pointer< ContainerManipulator > pParent_ );
			
			const e_State& getState() const { return m_State; }
			void setState( const e_State& x ) { m_State = x; }

			Signal< math::Vector2f, math::Vector2f > m_Dragging;

		private:
			e_State			m_State;
			math::Vector2f	m_Offset;
	};
}}

#endif
