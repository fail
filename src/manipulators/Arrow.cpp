/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "manipulators/Arrow.h"
#include "scenegraph/Group.h"
#include "scenegraph/shapes/Cone.h"
#include "scenegraph/shapes/Cylinder.h"

using namespace fail;
using namespace fail::manipulators;
using namespace fail::scenegraph;

Arrow::Arrow( Pointer< ContainerManipulator > pParent,
		Pointer< scenegraph::Material > pMaterial,
		float Length, float Radius ) :
	DraggableManip( pParent ),
	m_pMaterial( pMaterial ),
	m_Length( Length ),
	m_Radius( Radius )
{
}

Arrow::Arrow( Pointer< ContainerManipulator > pParent,
		Pointer< scenegraph::Material > pMaterial,
		float Length, float Radius,
		Pointer< scenegraph::Frame > pFrame ) :
	DraggableManip( pParent ),
	m_pMaterial( pMaterial ),
	m_Length( Length ),
	m_Radius( Radius )
{
	m_pFrame = pFrame;
}

void Arrow::buildSceneGraph()
{
	if( !m_pFrame )
		m_pFrame = new scenegraph::Frame();
	
	Pointer< Group > pGroup = new scenegraph::Group();
	m_pRenderable = pGroup.raw();
	
	Pointer< Frame > pTipFrame = new scenegraph::Frame();
	pTipFrame->setpParent( m_pFrame );
	Pointer< shapes::Cone > pTip = new shapes::Cone( m_pMaterial, pTipFrame, 16 );

	pTipFrame->getLocalToParent().right().x() = m_Radius;
	pTipFrame->getLocalToParent().up().z() = m_Radius;
	pTipFrame->getLocalToParent().forward().y() = m_Radius * 3.f;
	pTipFrame->getLocalToParent().position().y() = m_Length - ( m_Radius * 3.f );
	pGroup->add( pTip.raw() );
	
	Pointer< Frame > pShaftFrame = new scenegraph::Frame();
	pShaftFrame->setpParent( m_pFrame );
	Pointer< shapes::Cylinder > pShaft = new shapes::Cylinder( m_pMaterial, pShaftFrame, 8 );
	pShaftFrame->getLocalToParent().right().x() = m_Radius / 3.f;
	pShaftFrame->getLocalToParent().up().z() = m_Radius / 3.f;
	pShaftFrame->getLocalToParent().forward().y() = m_Length - ( m_Radius * 3.f );
	pGroup->add( pShaft.raw() );
}
