/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "manipulators/Manipulator.h"
#include "manipulators/ContainerManipulator.h"

using namespace fail;
using namespace fail::manipulators;

Manipulator::Manipulator() : m_pParent( 0 )
{
}

Manipulator::Manipulator( ContainerManipulator* pParent_ )
{
	m_pParent = pParent_;
	pParent_->addChild( this );
}
