/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_MANIPULATORS_ARROW_H_
#define FAIL_MANIPULATORS_ARROW_H_

#include "core/core.h"
#include "manipulators/manipulators_export.h"
#include "DraggableManip.h"
#include "scenegraph/Material.h"

namespace fail { namespace manipulators
{
	class FLMANIPULATORS_EXPORT Arrow : public DraggableManip
	{
		public:			
			Arrow( Pointer< ContainerManipulator > pParent,
				   Pointer< scenegraph::Material > pMaterial,
 				   float Length, float Radius );

			Arrow( Pointer< ContainerManipulator > pParent,
				   Pointer< scenegraph::Material > pMaterial,
 				   float Length, float Radius,
				   Pointer< scenegraph::Frame > pFrame );
			
			const Pointer< scenegraph::Material >& getpMaterial() const { return m_pMaterial; }
			void setpMaterial( const Pointer< scenegraph::Material >& x ) { m_pMaterial = x; }

			const float& getLength() const { return m_Length; }
			void setLength( const float& x ) { m_Length = x; }
			
			const float& getRadius() const { return m_Radius; }
			void setRadius( const float& x ) { m_Radius = x; }
			
			virtual void buildSceneGraph();

		private:
			Pointer< scenegraph::Material > m_pMaterial;
 			float m_Length;
 			float m_Radius;
	};
}}

#endif
