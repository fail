/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_MANIPULATORS_MANIPULATOR_H_
#define FAIL_MANIPULATORS_MANIPULATOR_H_

#include "core/core.h"
#include "manipulators/manipulators_export.h"
#include "gui/game/EventHandler.h"
#include "collision/PlaceableGeom.h"
#include "scenegraph/Frame.h"
#include "scenegraph/Renderable.h"

namespace fail { namespace manipulators
{
	class ContainerManipulator;
	
	class FLMANIPULATORS_EXPORT Manipulator : public gui::game::EventHandler
	{
		public:
			Manipulator();
			Manipulator( ContainerManipulator* pParent_ );
			virtual ~Manipulator() {}
			
			virtual void buildSceneGraph() {}
			
			const Pointer< scenegraph::Frame >& getpFrame() const { return m_pFrame; }
			const Pointer< scenegraph::Renderable >& getpRenderable() const { return m_pRenderable; }
			const Pointer< collision::PlaceableGeom >& getpGeom() const { return m_pGeom; }
			
		protected:
			Pointer< scenegraph::Frame > m_pFrame;
			Pointer< scenegraph::Renderable > m_pRenderable;
			Pointer< collision::PlaceableGeom > m_pGeom;
			
 			ContainerManipulator* m_pParent;
	};
}}

#endif
