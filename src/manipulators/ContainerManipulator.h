/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_MANIPULATORS_CONTAINERMANIPULATOR_H_
#define FAIL_MANIPULATORS_CONTAINERMANIPULATOR_H_

#include "core/core.h"
#include "manipulators/manipulators_export.h"
#include "Manipulator.h"
#include "scenegraph/Group.h"

namespace fail { namespace manipulators
{
	class FLMANIPULATORS_EXPORT ContainerManipulator : public Manipulator
	{
		public:
			ContainerManipulator() {}
			ContainerManipulator( Pointer< ContainerManipulator > pParent_ ) :
				Manipulator( pParent_ )
			{
			}
			
			void addChild( Pointer< Manipulator > pChild_ )
			{
				m_Children.push_back( pChild_ );
			}
			
			virtual void buildSceneGraph();
			
		private:
			typedef std::list< Pointer< Manipulator > > ManipList;
			ManipList m_Children;
	};
}}

#endif
