/*
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "core/core.h"

using namespace fail;

Manifest::Manifest()
{
}

Manifest::Manifest( const Serialization_tag& )
{
}

void Manifest::addObject( string Name, shared_ptr< Serializable > pObject )
{
	m_ObjectMap[ Name ] = pObject;
}

shared_ptr< Serializable > Manifest::getObject( string Name )
{
	map< string, shared_ptr< Serializable > >::const_iterator it;
	it = m_ObjectMap.find( Name );
	
	if( it != m_ObjectMap.end() )
		return ( *it ).second;

	return shared_ptr< Serializable >();
}
