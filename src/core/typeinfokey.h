/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_CORE_TYPEINFOKEY_H_
#define FAIL_CORE_TYPEINFOKEY_H_

#include <typeinfo>
#include <cstring>

namespace fail
{
	// This is an utility class that wraps a pointer to a std::type_info
	// and maps operator== and operator< to the pointed type_info operator==
	// and before method.
	//
	// This way it can be used as a key in a map.
	//
	// Note: i'm actualy comparing the name strings, because
	// going through the comparison operator and the ordering method
	// don't seem to work when comparing two instances of identical typeinfo
	// from two different shared libraries, even though going through the
	// operators and such of the typeinfo class is precisely supposed
	// to avoid that kind of problem.
	class FLCORE_EXPORT TypeInfoKey
	{
		public:
			TypeInfoKey( const std::type_info& TypeInfo_ ) :
				m_pTypeInfo( &TypeInfo_ )
			{
			}

			bool operator==( const TypeInfoKey& Other_ ) const
			{
				return *m_pTypeInfo == *Other_.m_pTypeInfo;
			}
			
			bool operator!=( const TypeInfoKey& Other_ ) const
			{
				return *m_pTypeInfo != *Other_.m_pTypeInfo;
			}
			
			bool operator<( const TypeInfoKey& Other_ ) const
			{
				return m_pTypeInfo->before( *Other_.m_pTypeInfo );
			}

		private:
			const std::type_info* m_pTypeInfo;
	};
}

#endif
