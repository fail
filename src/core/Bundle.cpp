/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "core/core.h"

using namespace fail;

Bundle::Bundle( Persistable* pRootObject_ ) :
	m_pRootObject( pRootObject_ )
{
	m_UUID = uuid::Generate();
}

Bundle::Bundle( Persistable* pRootObject_, const uuid& uuid_ ) :
	m_UUID( uuid_ ),
	m_pRootObject( pRootObject_ )
{
}
