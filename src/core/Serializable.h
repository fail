/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_SERIALIZABLE_H_
#define FAIL_SERIALIZABLE_H_

namespace fail
{
	struct Serialization_tag {};

	class FLCORE_EXPORT Serializable
	{	
		public:
			Serializable() {}
			virtual ~Serializable() {}

			// This is a special constructor called when instanciating an object from a stream.
			// The parameter does nothing.
			Serializable( const Serialization_tag& ) {}

			// This is called when the serialization process is complete. At that point, all relevant Pointers
			// to other objects have been set.
			virtual void postLoad() {}
	};
}

#endif
