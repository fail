#ifndef FAIL_UUID_H_
#define FAIL_UUID_H_

#include <uuid/uuid.h>

namespace fail
{
	// A more useful uuid type than the one from libuuid.
	// It uses two uint64 for representation (allowing fast
	// comparisons and copying), and provide the necessary
	// operator overloads to use them transparently.
	struct FLCORE_EXPORT uuid
	{
		uint64_t hi;
		uint64_t lo;
		
		operator bool() const
		{
			return hi || lo;
		}
		
		bool operator==( const uuid& other ) const
		{
			return hi == other.hi && lo == other.lo;
		}
		
		bool operator!=( const uuid& other ) const
		{
			return hi != other.hi || lo != other.lo;
		}
		
		// define operator < so it can be used as a key in a map or a set
		bool operator<( const uuid& other ) const
		{
			if( hi < other.hi )
				return true;
			
			if( hi == other.hi )
				return lo < other.lo;
			
			return false;
		}
		
		friend std::ostream& operator<<( std::ostream& out, const uuid& uuid_ )
		{
			union
			{
				uuid	fail_uuid;
				uuid_t	uuidlib_uuid;
			} uuid_union;
			uuid_union.fail_uuid = uuid_;

			char tmp[128];	// Hrmpf... C APIs.
			uuid_unparse( uuid_union.uuidlib_uuid, tmp );
			out << tmp;
			
			return out;
		}
		
		static inline uuid Generate()
		{
			union
			{
				uuid	fail_uuid;
				uuid_t	uuidlib_uuid;
			} result;
			
			uuid_generate( result.uuidlib_uuid );
			return result.fail_uuid;
		}
	};
}

#endif
