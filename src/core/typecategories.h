/*
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_TYPECATEGORIES_H_
#define FAIL_TYPECATEGORIES_H_

namespace fail
{
	enum e_TypeCategory
	{
		tc_Default,
		tc_ValueType,
		tc_Enum,
		tc_Signal
	};

	template< typename T, bool bValueType = flags::ValueType< typename remove_pointer< T >::type >::value,
						  bool bEnum = is_enum< T >::value,
						  bool bSignal = is_base_of< SignalBase, T >::value >
	struct TypeCategory
	{
		static const e_TypeCategory value = tc_Default;
	};

	template< typename T >
	struct TypeCategory< T, true, false, false >
	{
		static const e_TypeCategory value = tc_ValueType;
	};

	template< typename T >
	struct TypeCategory< T, false, true, false >
	{
		static const e_TypeCategory value = tc_Enum;
	};

	template< typename T >
	struct TypeCategory< T, false, false, true >
	{
		static const e_TypeCategory value = tc_Signal;
	};
}

#endif
