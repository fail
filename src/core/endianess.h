/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_CORE_ENDIANESS_H_
#define FAIL_CORE_ENDIANESS_H_

#include "core/core.h"
#include <iostream>

namespace fail { namespace endianess
{
	enum Endianess
	{
		e_Big,
		e_Little
	};
	
	struct Native
	{
#if defined( FAIL_BIG_ENDIAN )
		static const Endianess type = e_Big;
#elif defined( FAIL_LITTLE_ENDIAN )
		static const Endianess type = e_Little;
#endif

		// TODO: all types... I will do them as I need them out of pure laziness.
		static void FixEndianessU16( uint16_t& Val_ ) {}
		static void FixEndianessS16( int16_t& Val_ ) {}
		
		static void FixEndianessU32( uint32_t& Val_ ) {}
		static void FixEndianessS32( int32_t& Val_ ) {}
		
		static void FixEndianessU64( uint64_t& Val_ ) {}
		static void FixEndianessS64( int64_t& Val_ ) {}
		
		static void FixEndianessFloat( float& Val_ ) {}
		static void FixEndianessDouble( double& Val_ ) {}

	};
	
	struct Swapped
	{
#if defined( FAIL_BIG_ENDIAN )
		static const Endianess type = e_Little;
#elif defined( FAIL_LITTLE_ENDIAN )
		static const Endianess type = e_Big;
#endif
		
		static void FixEndianessU16( uint16_t& Val_ )
		{
 			Val_ = ( Val_ << 8 ) | ( Val_ >> 8 );
		}
		
		static void FixEndianessS16( int16_t& Val_ )
		{
			FixEndianessU16( reinterpret_cast< uint16_t& >( Val_ ) );
		}
		
		static void FixEndianessU32( uint32_t& Val_ )
		{
			uint16_t lo = Val_;
			uint16_t hi = Val_ >> 16;
			FixEndianessU16( lo );
			FixEndianessU16( hi );
			Val_ = ( static_cast< uint32_t >( lo ) << 16 ) | hi;
		}
		
		static void FixEndianessS32( int32_t& Val_ )
		{
			FixEndianessU32( reinterpret_cast< uint32_t& >( Val_ ) );
		}
		
		static void FixEndianessU64( uint64_t& Val_ )
		{
			uint32_t lo = Val_;
			uint32_t hi = Val_ >> 32;
			FixEndianessU32( lo );
			FixEndianessU32( hi );
			Val_ = ( static_cast< uint64_t >( lo ) << 32 ) | hi;
		}
		
		static void FixEndianessS64( int64_t& Val_ )
		{
			FixEndianessU64( reinterpret_cast< uint64_t& >( Val_ ) );
		}
		
		static void FixEndianessFloat( float& Val_ )
		{
			void* pVal = &Val_;
			uint32_t tmp = *static_cast< uint32_t* >( pVal );
			FixEndianessU32( tmp );
			pVal = &tmp;
			Val_ = *static_cast< float* >( pVal );
		}
		
		static void FixEndianessDouble( double& Val_ )
		{
			void* pVal = &Val_;
			uint64_t tmp = *static_cast< uint64_t* >( pVal );
			FixEndianessU64( tmp );
			pVal = &tmp;
			Val_ = *static_cast< double* >( pVal );
		}
	};
	
// If neither FAIL_BIG_ENDIAN or FAIL_LITTLE_ENDIAN are defined, things will likely fail to compile because
// of those missing typedefs. This is intentional.
#if defined( FAIL_BIG_ENDIAN )
	typedef Native Big;
	typedef Swapped Little;
#elif defined( FAIL_LITTLE_ENDIAN )
	typedef Native Little;
	typedef Swapped Big;
#endif

}}

#endif
