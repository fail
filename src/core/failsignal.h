/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FAILSIGNAL_H_
#define FAIL_FAILSIGNAL_H_

#include <list>
#include <iostream>

namespace fail
{
	// We make signal inherit from this so we can easily determine if a type is a signal using type traits.
	class SignalBase {};

	template< typename... Args > class FLCORE_EXPORT Signal : SignalBase
	{
		private:
			struct Slot
			{
				virtual ~Slot() {}
				virtual bool operator()( Args&&... args_ ) = 0;
			};
			
			struct FuncSlot : public Slot
			{
				typedef void ( * func_type )( Args&&... );
				FuncSlot( func_type pFunc_ ) : m_pFunc( pFunc_ ) {}
				
				virtual bool operator()( Args&&... args_ )
				{
					m_pFunc( forward< Args >( args_ )... );
					return true;
				}
				
				func_type m_pFunc;
			};
			
			template< class C > struct MethodSlot : public Slot
			{
				// TODO: If I define Args as r-value reference, then overload resolution on the connect method fails.
				// No idea if it's not supposed to work or if it might be a gcc bug.
				typedef void ( C::* method_type )( Args... );
				MethodSlot( shared_ptr< C > pObj_, method_type pMtd_ ) : m_pObj( pObj_ ), m_pMtd( pMtd_ ) {}
				
				virtual bool operator()( Args&&... args_ )
				{
					// Lock the weak pointer by copying it into a shared_ptr
					shared_ptr< C > pObj( m_pObj.lock() );
					if( !pObj )
						return false;	// The object is gone, we return false to tell the signal to remove that slot.

					( pObj.get()->*m_pMtd )( forward< Args >( args_ )... );
					return true;
				}
				
				weak_ptr< C >	m_pObj;
				method_type		m_pMtd;
			};

		public:	
			Signal() : m_bTriggering( false ) {}

			typedef std::list< Slot* > list_type;
			typedef typename list_type::iterator slot_handle;
			
			slot_handle connect( typename FuncSlot::func_type pFunc_ )
			{
				return m_Slots.insert( m_Slots.end(), new FuncSlot( pFunc_ ) );
			}
			
			template< class C > slot_handle connect( shared_ptr< C > pObj_,
					typename MethodSlot< C >::method_type pMethod_ )
			{
				return m_Slots.insert( m_Slots.end(), new MethodSlot< C >( pObj_, pMethod_ ) );	
			}
			
			void disconnect( slot_handle& Handle )
			{
				m_Slots.erase( Handle );
			}
			
			void operator()( Args&&... args_ )
			{
				if( m_bTriggering )
					return;
				
				m_bTriggering = true;
				
				typename list_type::iterator it = m_Slots.begin();
				
				while( it != m_Slots.end() )
				{
					if( !( **it )( forward< Args >( args_ )... ) )
					{
						typename list_type::iterator remove_it = it;
						++it;
						m_Slots.erase( remove_it );
					}
					else
						++it;
				}
				
				m_bTriggering = false;
			}

		private:
			list_type	m_Slots;
			
			// This is to avoid cyclic notification loops.
			// TODO: add a mutex - so if the same thread tries to fire this same signal
			// again while as a result of triggering it it will ignore it,
			// whereas when another thread wants to trigger it it will wait
			// (we don't want another thread to ignore the signal just because it
			// happens to try to fire it at the same time as another thread)
			bool		m_bTriggering;
	};
}

#endif
