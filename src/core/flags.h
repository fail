/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FLAGS_H_
#define FAIL_FLAGS_H_

namespace fail
{ 
	template< bool Val > struct Flag
	{
		static const bool value;
	};
	
	template< bool Val > const bool Flag< Val >::value = Val;

	namespace flags
	{
		template< typename T > struct ValueType : public Flag< false > {};
		template< typename T > struct Scriptable : public Flag< true > {};
		template< typename T > struct Storable : public Flag< true > {};
		template< typename T > struct ReadOnly : public Flag< false > {};
		template< typename T > struct Mutable : public Flag< true > {};
		template< typename T > struct Abstract : public Flag< false > {};
		template< typename T > struct Polymorphic : public Flag< false > {};
		template< typename T > struct Virtual : public Flag< false > {};
		template< typename T > struct ExtensibleByScript : public Flag< false > {};
	}
}

#endif
