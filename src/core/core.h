/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_CORE_H_
#define FAIL_CORE_H_

#include <string>
#include <memory>
#include <cstdint>
#include <type_traits>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <unordered_map>
#include <stack>
#include <sstream>

namespace fail
{
	using namespace std;
}

#include "core/config.h"
#include "core/core_export.h"
#include "endianess.h"
#include "failsignal.h"
#include "moduletraits.h"
#include "classtraits.h"
#include "attributetraits.h"
#include "methodtraits.h"
#include "signaltraits.h"
#include "enumtraits.h"
#include "flags.h"
#include "typecategories.h"
#include "Serializable.h"
#include "typeinfokey.h"
#include "dynamicbuffer.h"
#include "subclasswrapper.h"
#include "uuid.h"
#include "ObjectStorage_i.h"
#include "Bundle.h"
#include "Persistable.h"
#include "Manifest.h"

#endif
