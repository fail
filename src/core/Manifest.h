/*
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_MANIFEST_H_
#define FAIL_MANIFEST_H_

namespace fail
{
	// This class manages a dictionary of named pointers to serializable objects.
	// The idea is to use it as the primary object of a serialized bundle of objects
	// to function as a mini-directory of the objects of interest to the client code
	// within the file.
	//
	// For instance, when storing a complex 3d object as a scenegraph in a file,
	// a manifest can be used as a front-end for that file and provide links with
	// specific names to objects of interest to the game code such as the root frame,
	// the root renderable object, additional sub-frames to use to bind sub-objects such
	// as weapons, animation controller parameters to bind to game code variables, etc.
	class FLCORE_EXPORT Manifest : public Serializable
	{	
		public:
			Manifest();
			Manifest( const Serialization_tag& );
			
			void addObject( string Name, shared_ptr< Serializable > pObject );
			shared_ptr< Serializable > getObject( string Name );

		private:
			template< class C, typename T > friend struct fail::attribute_traits;
			
			map< string, shared_ptr< Serializable > > m_ObjectMap;
	};
}

#endif
