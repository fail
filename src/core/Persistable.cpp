/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "core/core.h"

using namespace fail;

// TODO: see header
/*
void Persistable::dispose( int32_t ReferenceCount ) const
{
	if( m_pStorage && ReferenceCount == 0 )
	{
		// If we have an associated object storage, add ourselves to its writeback queue.
		// The storage object should take ownership of the object through the normal
		// smart pointer mechanism, so we reset our pointer to the object storage
		// so that we get actually deleted when it releases its reference on us once
		// the write back is done.
		m_pStorage->addToWriteQueue( shared_from_this() );
		m_pStorage.reset();
	}
	else if( !isPersistent() || ReferenceCount < 0 )
		Serializable::dispose( ReferenceCount );
}*/
