/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_PERSISTABLE_H_
#define FAIL_PERSISTABLE_H_

namespace fail
{
	class FLCORE_EXPORT Persistable : public Serializable, public enable_shared_from_this< Persistable >
	{	
		public:
			Persistable( bool bDefaultDirty = false ) :
				m_Flags( bDefaultDirty ? f_DefaultDirty : f_None )
			{
			}
			
			Persistable( fail::Serialization_tag& )
			{
			}

			// TODO: see tl;dr comment below
		//	virtual void dispose( int32_t ReferenceCount ) const;
			
			// Because an object can be persistent doesn't means it always has to be, or currently is.
			bool isPersistent() const
			{
				return !!m_pBundle;
			}
			
			// If the object already have an associated bundle, do nothing.
			// Otherwise, create a new bundle and then go through all the persistable
			// (and not yet persistent themselves) objects this one points to
			// (directly and indirectly), and include them in the bundle.
			void createBundle()
			{
				if( m_pBundle )
					return;
				
				m_pBundle = shared_ptr< Bundle >( new Bundle( this ) );

				// When an object is persistent, we change its reference counting rules:
				// at 0 it means that the only reference left is the one from the bundle index,
				// which means the object should be moved to the write queue.
				// at -1 it means the object was written back and should be actually
				// deleted from memory.

				///// TODO: revisit this and reimplement it with shared_ptr. It's probably just going to involve magic with deleters.
				///// It might be necessary to force persistable classes to be built with static factory functions, so that the proper
				// deleter (that handle puttiong the bundle on the writeback queue as necessary) is used. A generic factory function can
				// probably be implemented in Persistable as a variadoc template that then calls the constructor.
				// With concepts and static_assert messages explaining that classes that derive from Persistable should have private constructors
				// and make Persistable< C > friend it should be coder friendly enough.
		////		m_ReferenceCount--;
			}

			void setBundle( shared_ptr< Bundle > pBundle )
			{
				// TODO: see tl;dr comment above.
//				if( !m_pBundle )
	//				m_ReferenceCount--;
				m_pBundle = pBundle;
			}

			void setStorage( shared_ptr< ObjectStorage_i > pStorage )
			{
				m_pStorage = pStorage;
			}
			
			bool isDirty() const
			{
				return !!( m_Flags & f_Dirty );
			}
			
			bool isRoot() const
			{
				return !!( m_Flags & f_Root );				
			}

			enum e_Flags
			{
				f_None = 0,
				
				// Whether the object should be considered dirty by default.
				// When the object is flushed to the db, its dirty flag is reset
				// to that value.
				f_DefaultDirty = 1,
	
				// Whether the object is currently dirty and needs to be written
				// back to the db when flushed from memory.
				f_Dirty = 2,
	
				// If this flag is set, it means the object is the root of its own bundle.
				f_Root = 4
			};
			
			const shared_ptr< Bundle >& getpBundle() const { return m_pBundle; }
			
			mutable shared_ptr< Bundle >			m_pBundle;
			mutable shared_ptr< ObjectStorage_i >	m_pStorage;
			mutable e_Flags							m_Flags;
	};
}

#endif
