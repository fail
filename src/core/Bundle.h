/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_BUNDLE_H_
#define FAIL_BUNDLE_H_

namespace fail
{
	class Persistable;
	
	// TODO: add a mutex in there, as write back will be done in a separate thread
	// and a user thread may want to fetch this bundle back from the db before it is flushed
	// out.
	// TODO: in which thread should bundle splitting occur when an object points to a non-root object
	// from another bundle, or from a non-yet-bundled object?
	// Doing this just before writing back (from the writing thread) seems a good idea since at that point
	// there shouldn't be anymore outstanding user of the bundle. However, what if some other, live bundle points
	// to a non-root object of the bundle to be flushed?
	// -> just write the object incorrectly, it will be fixed later on when we come accross this object
	// and split it out from its bundle. At that point, its originating bundle will have to be loaded
	// back, and then saved again (which will in effect replace its embedded copy of the newly bundled object
	// with a bundle uuid)
	class FLCORE_EXPORT Bundle
	{
		public:
			Bundle( Persistable* pRootObject_ );
			Bundle( Persistable* pRootObject_, const uuid& uuid_ );
			
			const std::string& getName() const { return m_Name; }
			void setName( const std::string& x ) { m_Name = x; }
			
			const uuid& getUUID() const { return m_UUID; }
			Persistable* getRootObject() const { return m_pRootObject; }
		
		private:
			uuid			m_UUID;
			std::string 	m_Name;
			
			// Set to true when the bundle is scheduled for
			// writing, back to false after writing
			bool			m_bDirty;
			
 			// Pointer to the root object of the bundle
			// A dynamic_cast will be needed when fetching an object from an already loaded
			// bundle, as a safety check that the givven uuid points to a suitable object type
			
			// This is not a smart pointer because the bundle is owned by the root object, not
			// the other way around. Ownership works like this: on dispose(), the object deletes itself
			// as usual if its not dirty, otherwise it appends itself to the write queue.
			// Once in the write queue, it becomes owned by the write-back thread which can move it out of the
			// queue, write it and delete it.
			// If however someone wants to load this object again while it's still in the write queue,
			// it can be "resurrected" to a refcount of 1 and removed from the write queue.
			//
			// The write queue, obviously, is to be protected by a mutex.
			Persistable*	m_pRootObject;
	};
}

#endif
