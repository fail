/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_DYNAMICBUFFER_H_
#define FAIL_DYNAMICBUFFER_H_

#include <vector>

namespace fail
{
	enum e_ColumnType
	{
		ct_uint8,
		ct_int8,
		ct_uint16,
		ct_int16,
		ct_uint32,
		ct_int32,
		ct_uint64,
		ct_int64,
		ct_float,
		ct_double
	};
	

	template< e_ColumnType enum_value_ > struct column_type_enum
	{
		static const e_ColumnType enum_value;
	};
	
	template< e_ColumnType enum_value_ > const e_ColumnType column_type_enum< enum_value_ >::enum_value = enum_value_;

	template< typename T > struct column_type_traits {};	
	template<> struct column_type_traits< uint8_t > : public column_type_enum< ct_uint8 > {};
	template<> struct column_type_traits< int8_t > : public column_type_enum< ct_int8 > {};
	template<> struct column_type_traits< uint16_t > : public column_type_enum< ct_uint16 > {};
	template<> struct column_type_traits< int16_t > : public column_type_enum< ct_int16 > {};
	template<> struct column_type_traits< uint32_t > : public column_type_enum< ct_uint32 > {};
	template<> struct column_type_traits< int32_t > : public column_type_enum< ct_int32 > {};
	template<> struct column_type_traits< uint64_t > : public column_type_enum< ct_uint64 > {};
	template<> struct column_type_traits< int64_t > : public column_type_enum< ct_int64 > {};
	template<> struct column_type_traits< float > : public column_type_enum< ct_float > {};
	template<> struct column_type_traits< double > : public column_type_enum< ct_double > {};

	
	// A buffer with a dynamically definable per-entry layout.
	// Each entry is called a row, each attribute of an entry is called a column.
	//
	// This is used when a buffer need to be able to be defined to have
	// any possible arbitrary layout at runtime.
	//
	// It is recognized as a builtin type in the IDL and FBF include
	// specific code to serialize this with proper endianess conversion of
	// the content of the buffer as required (which is really the whole
	// point of having this DynamicBuffer thing)
	//
	// It's used for things like vertex buffers.	
	class FLCORE_EXPORT DynamicBuffer
	{
		public:
			DynamicBuffer();

			// Layout setup.
			template< typename T > void addColumn()
			{
				// TODO: throw if trying to change the layout
				// after the buffer has been allocated
				m_Layout.push_back( std::make_pair(
					column_type_traits< T >::enum_value,
	 				m_Stride ) );
				m_Stride += sizeof( T );
			}
			
			void addColumn( e_ColumnType ColumnType );
			
			// Buffer allocation.
			void resize( int NumRows )
			{
				m_Buffer.resize( NumRows * m_Stride );
			}
			
			int size() const
			{
				if( !m_Stride )
					return 0;
				return m_Buffer.size() / m_Stride;
			}
			
			// Data access.
			template< typename T > void put( int RowIndex, int ColumnIndex, T Value_ )
			{
				// TODO: throw if the type doesn't match				
				void* pRawCell = &m_Buffer[ RowIndex * m_Stride + m_Layout[ ColumnIndex ].second ];
				T* pElement = static_cast< T* >( pRawCell );
				*pElement = Value_;
			}

			template< typename T > T get( int RowIndex, int ColumnIndex ) const
			{
				// TODO: throw if the type doesn't match
				const void* pRawCell = &m_Buffer[ RowIndex * m_Stride + m_Layout[ ColumnIndex ].second ];
				const T* pElement = static_cast< const T* >( pRawCell );
				return *pElement;
			}
			
			// Layout queries (needed by FBF to save it)
			int numColumns() const { return m_Layout.size(); }
			e_ColumnType getColumnType( int ColumnIndex ) const { return m_Layout[ ColumnIndex ].first; } 
			
			int getStride() const
			{
				return m_Stride;
			}
			
			const uint8_t* getBufferAddress() const
			{
				return &m_Buffer[0];
			}
			
			const uint8_t* getColumnAddress( int ColumnIndex ) const
			{
				return getBufferAddress() + m_Layout[ ColumnIndex ].second;
			}
			
		private:
			std::vector< std::pair< e_ColumnType, int > >	m_Layout;
			std::vector< uint8_t >	m_Buffer;
			int						m_Stride;
	};
}

#endif
