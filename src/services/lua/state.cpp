/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "services/lua/state.h"
#include "services/lua/implementation.h"
#include <cstdlib>

using namespace fail;

LuaState* LuaState::ms_pInstance = NULL;

void LuaState::Init( lua_State* pState )
{
	if( !ms_pInstance )
		ms_pInstance = new LuaState( pState );	
}

LuaState::LuaState( lua_State* pState ) :
	m_pState( pState )
{
	LuaImpl::StackGuard guard( m_pState );
	
	// Create a weak table and store it with a &type_tag< LuaState >::dummy
	// light userdata. This will serve as a global weak registry
	// (currently to be used only by the subclassing stuff to properly
	// deal with the mutual ownership of the c++ wrapper and the
	// corresponding lua object instance)
	
	// The weak table itself
	lua_newtable( m_pState );

	// The metatable for the weak table
	lua_newtable( m_pState );
	lua_pushstring( m_pState, "v" );
	lua_setfield( m_pState, -2, "__mode" );
	lua_setmetatable( m_pState, -2 );

	LuaImpl::Registry< LuaState >::RegisterMetatable( m_pState );
}

void LuaState::prependPackagePath( std::string Path )
{
	LuaImpl::StackGuard guard( m_pState );

	lua_getglobal( m_pState, "package" );
	lua_getfield( m_pState, -1, "path" );
	Path = Path + ';' + lua_tostring( m_pState, -1 );
	lua_pop( m_pState, 1 );
	lua_pushstring( m_pState, Path.c_str() );
	lua_setfield( m_pState, -2, "path" );
}
