/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_LUA_STRUCTBOXING_H
#define FAIL_LUA_STRUCTBOXING_H

namespace fail { namespace LuaImpl
{
	template< class S > struct StructRefWrapper
	{
		StructRefWrapper( const shared_ptr< void >& pOwner_, S* pStruct_ )	:
			pOwner( pOwner_ ),
			pStruct( pStruct_ )
		{
		}
		
		shared_ptr< void >	pOwner;
		S*					pStruct;
	};
	
	template< class S > struct StructValWrapper
	{
		StructValWrapper( shared_ptr< S >& pStruct_ ) :
			pStruct( pStruct_ )
		{
		}
		
		shared_ptr< S >	pStruct;
	};
	
	template< class C > struct StructMetamethods
	{
		static int FinalizeRef( lua_State* pLS )
		{
			void* pData = lua_touserdata( pLS, 1 );
			StructRefWrapper< C >* pWrapper =
				static_cast< StructRefWrapper< C >* >( pData );
			pWrapper->~StructRefWrapper< C >();
			return 0;
		}

		static int FinalizeVal( lua_State* pLS )
		{
			void* pData = lua_touserdata( pLS, 1 );
			StructValWrapper< C >* pWrapper =
				static_cast< StructValWrapper< C >* >( pData );
			pWrapper->~StructValWrapper< C >();
			return 0;
		}
		
		static int ToStringRef( lua_State* pLS )
		{
			void* pData = lua_touserdata( pLS, 1 );
			StructRefWrapper< C >* pWrapper =
				static_cast< StructRefWrapper< C >* >( pData );	
			std::stringstream sstr;
			sstr << "[struct " << class_traits< C >::FullName() << " " << pWrapper->pStruct
				<< " (by ref, owner object: " << pWrapper->pOwner << ")]";
			lua_pushstring( pLS, sstr.str().c_str() );
			return 1;
		}
		
		static int ToStringVal( lua_State* pLS )
		{
			void* pData = lua_touserdata( pLS, 1 );
			StructValWrapper< C >* pWrapper =
				static_cast< StructValWrapper< C >* >( pData );	
			std::stringstream sstr;
			sstr << "[struct " << class_traits< C >::FullName() << " " << pWrapper->pStruct
				<< " (by value)]";
			lua_pushstring( pLS, sstr.str().c_str() );
			return 1;
		}
	};
	
	template< class C, e_CheckingPolicy Checking >
	struct TypeConverter< C*, Checking, tc_ValueType >
	{
		TypeConverter( lua_State* pLS_ ) :
			m_pLS( pLS_ ),
			m_bByRef( true )
		{
		}
		
		bool CheckType( int Index_ )
		{
			if( lua_type( m_pLS, Index_ ) != LUA_TUSERDATA )
			{
				// TODO: better error reporting
				TypeCheckHelpers< Checking >::ThrowTypeMismatch();
				return false;
			}
			
			if( !lua_getmetatable( m_pLS, Index_ ) )
			{
				TypeCheckHelpers< Checking >::ThrowTypeMismatch();
				return false;
			}
			
			// Check if it's a struct wrapped by reference
			Registry< C* >::PushMetatable( m_pLS );
			if( lua_rawequal( m_pLS, -1, -2 ) )
			{
				lua_pop( m_pLS, 2 );
				return true;
			}
			
			lua_pop( m_pLS, 1 );
			
			// Check if it's a struct wrapped by value
			Registry< C& >::PushMetatable( m_pLS );
			bool bRes = lua_rawequal( m_pLS, -1, -2 );	
			lua_pop( m_pLS, 2 );
			if( bRes )
			{
				m_bByRef = false;
				return true;
			}
			
			TypeCheckHelpers< Checking >::ThrowTypeMismatch();
			return false;
		}
		
		bool ConvertFromLua( int Index_, C*& pDest_ )
		{
			// Workaround for a warning occuring because the compiler apparently
			// fails to detect that when using a typeconverter with cp_Throw,
			// any code path not setting Dest_ throws
			pDest_ = 0;
			
			if( !CheckType( Index_ ) )
				return false;

			if( m_bByRef )
			{
				StructRefWrapper< C >* pWrapper =
					static_cast< StructRefWrapper< C >* >( lua_touserdata( m_pLS, Index_ ) );
				pDest_ = pWrapper->pStruct;
			}
			else
			{
				StructValWrapper< C >* pWrapper =
					static_cast< StructValWrapper< C >* >( lua_touserdata( m_pLS, Index_ ) );
				pDest_ = pWrapper->pStruct.get();
			}
				
			//	pDest_ = static_cast< C* >( lua_touserdata( m_pLS, Index_ ) );
				
			return true;
		}

		lua_State*	m_pLS;
		bool		m_bByRef;
	};
	
	template< class C >
	struct TypeConverter< C*, cp_NoCheck, tc_ValueType >
	{
		TypeConverter( lua_State* pLS_ ) :
			m_pLS( pLS_ ),
			m_bByRef( true )
		{
		}
		
		bool CheckType( int Index_ )
		{
			return true;
		}
		
		bool ConvertFromLua( int Index_, C*& pDest_ )
		{
			lua_getmetatable( m_pLS, Index_ );

			// Check if it's a struct wrapped by reference
			Registry< C* >::PushMetatable( m_pLS );
			m_bByRef = lua_rawequal( m_pLS, -1, -2 );	
			lua_pop( m_pLS, 2 );
			
			if( m_bByRef )
			{
				m_pRefWrapper = static_cast< StructRefWrapper< C >* >( lua_touserdata( m_pLS, Index_ ) );
				pDest_ = m_pRefWrapper->pStruct;
			}
			else
			{
				m_pValWrapper = static_cast< StructValWrapper< C >* >( lua_touserdata( m_pLS, Index_ ) );
				pDest_ = m_pValWrapper->pStruct.get();
			}
				
				
	//			pDest_ = static_cast< C* >( lua_touserdata( m_pLS, Index_ ) );
				
			return true;
		}

		lua_State*	m_pLS;
		bool		m_bByRef;
		StructRefWrapper< C >* m_pRefWrapper;
		StructValWrapper< C >* m_pValWrapper;
	};
	
	template< class C, e_CheckingPolicy Checking >
	struct TypeConverter< C, Checking, tc_ValueType >
	{
		TypeConverter( lua_State* pLS_ ) :
			m_pLS( pLS_ ),
			m_bByRef( true )
		{
		}
		
		bool CheckType( int Index_ )
		{
			if( lua_type( m_pLS, Index_ ) != LUA_TUSERDATA )
			{
				// TODO: better error reporting
				TypeCheckHelpers< Checking >::ThrowTypeMismatch();
				return false;
			}
			
			if( !lua_getmetatable( m_pLS, Index_ ) )
			{
				TypeCheckHelpers< Checking >::ThrowTypeMismatch();
				return false;
			}

			// Check if it's a struct wrapped by reference
			Registry< C* >::PushMetatable( m_pLS );
			if( lua_rawequal( m_pLS, -1, -2 ) )
			{
				lua_pop( m_pLS, 2 );
				return true;
			}
			
			lua_pop( m_pLS, 1 );

			// Check if it's a struct wrapped by value
			Registry< C& >::PushMetatable( m_pLS );
			if( lua_rawequal( m_pLS, -1, -2 ) )
			{
				lua_pop( m_pLS, 2 );
				m_bByRef = false;
				return true;
			}

			TypeCheckHelpers< Checking >::ThrowTypeMismatch( lua_type( m_pLS, -2 ), lua_type( m_pLS, -1 ) );
			return false;
		}
		
		bool ConvertFromLua( int Index_, C& Dest_ )
		{
			if( !CheckType( Index_ ) )
				return false;
			if( m_bByRef )
			{
				StructRefWrapper< C >* pWrapper =
					static_cast< StructRefWrapper< C >* >( lua_touserdata( m_pLS, Index_ ) );
				Dest_ = *pWrapper->pStruct;
			}
			else
			{
				StructValWrapper< C >* pWrapper =
					static_cast< StructValWrapper< C >* >( lua_touserdata( m_pLS, Index_ ) );
				Dest_ = *pWrapper->pStruct;
			}
				
				
			//	Dest_ = *static_cast< C* >( lua_touserdata( m_pLS, Index_ ) );
				
			return true;
		}
		
		void ConvertToLua( const C& Obj_ ) const
		{
			shared_ptr< C > pStruct( new C( Obj_ ) );
			
			void* pData = lua_newuserdata( m_pLS, sizeof( StructValWrapper< C > ) );
			if( !pData )
				throw std::bad_alloc();
			
			new( pData ) StructValWrapper< C >( pStruct );

			Registry< C& >::PushMetatable( m_pLS );	
			lua_setmetatable( m_pLS, -2 );
		}
		
		void ConvertToLuaByRef( shared_ptr< void > pOwner_, C& Obj_ ) const
		{
			void* pData = lua_newuserdata( m_pLS, sizeof( StructRefWrapper< C > ) );
			if( !pData )
				throw std::bad_alloc();
			
			new( pData ) StructRefWrapper< C >( pOwner_, &Obj_ );

			Registry< C* >::PushMetatable( m_pLS );

			lua_setmetatable( m_pLS, -2 );
		}
		
		lua_State*	m_pLS;
		bool		m_bByRef;
	};
	
	template< class C >
	struct TypeConverter< C, cp_NoCheck, tc_ValueType >
	{
		TypeConverter( lua_State* pLS_ ) :
			m_pLS( pLS_ )
		{
		}
		
		bool CheckType( int Index_ )
		{
			return true;
		}
		
		bool ConvertFromLua( int Index_, C& Dest_ )
		{
			lua_getmetatable( m_pLS, Index_ );

			// Check if it's a struct wrapped by reference
			Registry< C* >::PushMetatable( m_pLS );
		
			bool bRes = lua_rawequal( m_pLS, -1, -2 );	
			lua_pop( m_pLS, 2 );
			
			if( bRes )
			{
				StructRefWrapper< C >* pWrapper =
					static_cast< StructRefWrapper< C >* >( lua_touserdata( m_pLS, Index_ ) );
				Dest_ = *pWrapper->pStruct;
			}
			else
				Dest_ = *static_cast< C* >( lua_touserdata( m_pLS, Index_ ) );
				
			return true;
		}

		lua_State*	m_pLS;
	};
}}

#endif
