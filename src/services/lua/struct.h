/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_LUA_STRUCT_H
#define FAIL_LUA_STRUCT_H

namespace fail { namespace LuaImpl
{
	template< class S, typename AttrTag,
			  e_TypeCategory cat = TypeCategory< typename attribute_traits< S, AttrTag >::type >::value,
			  bool bReadOnly = flags::ReadOnly< AttrTag >::value,
	 		  bool bMutable = flags::Mutable< AttrTag >::value >
	struct StructAttrGetter
	{
		static void GetAttr( lua_State* pLS )
		{
			S* pObj;
			TypeConverter< S*, cp_NoCheck > obj_tc( pLS );
			obj_tc.ConvertFromLua( 1, pObj );
			
			typedef attribute_traits< S, AttrTag > traits;
			TypeConverter< typename traits::type > value_tc( pLS );
			value_tc.ConvertToLua( ( pObj->*traits::Accessor() )() );
		}
	};
	
	template< class S, typename AttrTag >
	struct StructAttrGetter< S, AttrTag, tc_ValueType, false, true >
	{
		static void GetAttr( lua_State* pLS )
		{
			S* pObj;
			TypeConverter< S*, cp_NoCheck > obj_tc( pLS );
			obj_tc.ConvertFromLua( 1, pObj );
			
			typedef attribute_traits< S, AttrTag > traits;
			TypeConverter< typename traits::type > value_tc( pLS );
			
			// TODO: the semantics of whether a struct attribute (ie stored by value in another struct)
			// should be referenced by value or by ref might need to be made explicit. (ie the user decides
			// whether he, for instance, wants to make a copy of a vector contained in a matrix or wants
			// to reference it to modify it in place)
			if( obj_tc.m_bByRef )
				value_tc.ConvertToLuaByRef( obj_tc.m_pRefWrapper->pOwner, ( pObj->*traits::Accessor() )() );
			else
				value_tc.ConvertToLuaByRef( obj_tc.m_pValWrapper->pStruct, ( pObj->*traits::Accessor() )() );

				//value_tc.ConvertToLua( ( pObj->*traits::Accessor() )() );
		}
	};
	
	template< class S > struct StructStuff
	{
		struct Struct_CallContext : public CallContext< S, S* >
		{
			typedef CallContext< S, S* > superclass;

			Struct_CallContext( lua_State* pLS, bool bStatic = false, int SkipParams = 0 ) :
				superclass( pLS, bStatic, SkipParams )
			{
			}

			template< typename... Args > void constructObject( Args&&... args_ )
			{
				TypeConverter< S > tc( superclass::m_pLS );
				tc.ConvertToLua( S( args_... ) );
			}
		};

		template< typename AttrTag > class AttributeWrapper : public AttributeWrapper_i
		{
			public:
				typedef attribute_traits< S, AttrTag > traits;
				
				virtual void get( lua_State* pLS ) const
				{
					StructAttrGetter< S, AttrTag >::GetAttr( pLS );
				}
				
				virtual void set( lua_State* pLS ) const
				{
					try
					{
						S* pObj;
						TypeConverter< S*, cp_NoCheck > obj_tc( pLS );
						obj_tc.ConvertFromLua( 1, pObj );
					
						typename traits::type Value;
						TypeConverter< typename traits::type > value_tc( pLS );
						value_tc.ConvertFromLua( 3, Value );
						( pObj->*traits::Accessor() )() = Value;		
					}
					catch( const Error::TypeMismatch& error )
					{
						throw Error::BadSetAttribute( error, traits::Name() );
					}
				}
		};

		struct SetupVisitor
		{
			SetupVisitor( lua_State* pLS_ ) : pLS( pLS_ ) {}

			lua_State* pLS;

			void declConstructor()
			{
				if( !flags::Scriptable< typename class_traits< S >::ctor_tag >::value )
					return;
				
				// Create a metatable for the global table, and redefine the __call function
				// to call the constructor wrapper.
				lua_newtable( pLS );
				lua_pushstring( pLS, "__call" );
				lua_pushcfunction( pLS, ( &CtorWrapper< S, Struct_CallContext >::LuaCFunc ) );
				lua_settable( pLS, -3 );
			
				lua_setmetatable( pLS, -2 );
			}

			template< typename AttrTag > void declAttribute()
			{
				if( !flags::Scriptable< AttrTag >::value )
					return;
				
				AttributeTable< S >::add( attribute_traits< S, AttrTag >::Name(),
								new AttributeWrapper< AttrTag >() );
			}
			
			template< typename MtdTag > void declMethod()
			{
				if( !flags::Scriptable< MtdTag >::value )
					return;
				
				lua_pushcfunction( pLS, ( &MethodWrapper< S, MtdTag, Struct_CallContext >::LuaCFunc ) );
				lua_setfield( pLS, -2, method_traits< S, MtdTag >::Name() );
				
				lua_pushcfunction( pLS, ( &MethodWrapper< S, MtdTag, Struct_CallContext >::LuaCFunc ) );
				lua_setfield( pLS, -3, method_traits< S, MtdTag >::Name() );
			}
			
			template< typename MtdTag > void declStaticMethod()
			{
				if( !flags::Scriptable< MtdTag >::value )
					return;
				
				lua_pushcfunction( pLS, ( &StaticMethodWrapper< S, MtdTag, Struct_CallContext >::LuaCFunc ) );
				lua_setfield( pLS, -2, method_traits< S, MtdTag >::Name() );
			}
			
			template< typename SigTag > void declSignal()
			{
				if( !flags::Scriptable< SigTag >::value )
					return;
				
				typedef typename signal_traits< S, SigTag >::type sig_type;
				
				// Create the meta-table for this signal type/class combination.
				// This might be redundant in some cases but it's only done at
				// init time so I don't care too much.
				lua_newtable( pLS );

				lua_pushcfunction( pLS, ( &SignalRef< S, sig_type >::Finalize ) );
				lua_setfield( pLS, -2, "__gc" );
				
				lua_pushcfunction( pLS, &SignalRefIndex );
				lua_setfield( pLS, -2, "__index" );
				
				lua_pushcfunction( pLS, ( &SignalConnectWrapper< S, SigTag >::LuaCFunc ) );
				lua_setfield( pLS, -2, "connect" );

				Registry< SignalRef< S, sig_type >* >::RegisterMetatable( pLS );
				
				// Create the meta table for connection handles to this signal.
				lua_newtable( pLS );

				lua_pushcfunction( pLS, ( SignalConnectionHandle< S, sig_type >::Finalize ) );
				lua_setfield( pLS, -2, "__gc" );
				
				lua_pushcfunction( pLS, ( &SignalConnectionHandle< S, sig_type >::LuaCFunc ) );
				lua_setfield( pLS, -2, "disconnect" );

				Registry< SignalConnectionHandle< S, sig_type >* >::RegisterMetatable( pLS );


				AttributeTable< S >::add( signal_traits< S, SigTag >::Name(),
									new SignalAttrWrapper< S, SigTag >() );
			}
		
			template< typename EnumTag > void declEnum()
			{
				if( !flags::Scriptable< EnumTag >::value )
					return;
				
				// Setup the metatable for this enum type
				lua_newtable( pLS );
	
				lua_pushcfunction( pLS, ( &EnumComparator< S, EnumTag >::Compare ) );
				lua_setfield( pLS, -2, "__eq" );
	
				Registry< typename enum_traits< S, EnumTag >::type >::RegisterMetatable( pLS );
				
				EnumVisitor ev( pLS );
				enum_traits< S, EnumTag >::template VisitValues( ev ); 
			}

			template< class SC > void declSuperClass()
			{
				if( !flags::Scriptable< SC >::value )
					return;

				//std::cout << "\tsuperclass: " << class_traits< SC >::FullName() << std::endl;
				
				class_traits< SC >::template VisitSuperClasses( *this );

				class_traits< SC >::template VisitAttributes( *this );
				class_traits< SC >::template VisitMethods( *this );
				class_traits< SC >::template VisitSignals( *this );
			}
		};

		static void Setup()
		{
			if( !flags::Scriptable< S >::value )
				return;

			//std::cout << "Setting up lua bindings for struct " << class_traits< S >::FullName() << std::endl;

			lua_State* pLS = LuaState::GetInstance()->get();

			// Create the meta table for instances (by reference) of this class.
			// Stored in the registry, key is a lightuserdata containing a unique pointer.
			lua_newtable( pLS );
			lua_pushvalue( pLS, -1 );
			Registry< S* >::RegisterMetatable( pLS );
			
			lua_pushstring( pLS, "__gc" );
			lua_pushcfunction( pLS, &StructMetamethods< S >::FinalizeRef );
			lua_settable( pLS, -3 );
			
			lua_pushstring( pLS, "__tostring" );
			lua_pushcfunction( pLS, &StructMetamethods< S >::ToStringRef );
			lua_settable( pLS, -3 );
			
			// Create the meta table for instances (by value) of this class.
			// Stored in the registry, key is a lightuserdata containing a unique pointer.
			lua_newtable( pLS );
			lua_pushvalue( pLS, -1 );
			Registry< S& >::RegisterMetatable( pLS );
			
			lua_pushstring( pLS, "__gc" );
			lua_pushcfunction( pLS, &StructMetamethods< S >::FinalizeVal );
			lua_settable( pLS, -3 );
			
			lua_pushstring( pLS, "__tostring" );
			lua_pushcfunction( pLS, &StructMetamethods< S >::ToStringVal );
			lua_settable( pLS, -3 );
			
			SetupVisitor sv( pLS );

			// Visit the superclasses. This will fill the attributes from the superclasses,
			// and setup the wrappers for the methods of the superclasses.
			// This must be done first so that any overloaded method or attribute
			// will be overwritten.
			class_traits< S >::template VisitSuperClasses( sv );
			class_traits< S >::template VisitAttributes( sv );
			class_traits< S >::template VisitSignals( sv );

			// The __index member points to a C++ function that will first attempt a lua_rawget
			// for the asked key on the metatable. If successful, it's returned (so we can index
			// a member of the metatable, ie a method wrapper), otherwise we look for it in
			// the attribute map. If found, we push the value of the attribute as the result.
			lua_pushstring( pLS, "__index" );
			lua_pushcfunction( pLS, &AttributeTable< S >::Index );
			lua_settable( pLS, -3 );
			
			lua_pushstring( pLS, "__index" );
			lua_pushcfunction( pLS, &AttributeTable< S >::Index );
			lua_settable( pLS, -4 );
			
			// The __newindex member points to a C++ function that looks for the attribute
			// in the attribute map. If found, it sets the value of the attribute to what's
			// provided, provided that the type is suitable.
			lua_pushstring( pLS, "__newindex" );
			lua_pushcfunction( pLS, &AttributeTable< S >::NewIndex );
			lua_settable( pLS, -3 );

			lua_pushstring( pLS, "__newindex" );
			lua_pushcfunction( pLS, &AttributeTable< S >::NewIndex );
			lua_settable( pLS, -4 );
			
			
			// Setup the method wrappers and store them in the meta table.
			class_traits< S >::VisitMethods( sv );
			
			// Setup the global table containing the static functions for this class, leave a copy of
			// it on the stack.
			lua_newtable( pLS );
			lua_pushvalue( pLS, -1 );
			lua_setfield( pLS, -5, class_traits< S >::Name() );

			// Store a reference to the global table for the class in a special field as a way to query an object's type
			// at runtime from lua (using the fail.utils.typeof() lua function)
			lua_pushvalue( pLS, -1 );
			lua_setfield( pLS, -3, "_flType" );
			lua_pushvalue( pLS, -1 );
			lua_setfield( pLS, -4, "_flType" );
			
			// Create the enum values.
			class_traits< S >::template VisitEnums( sv );
			
			// Setup the static method wrappers and store them in the global table for this class.
			class_traits< S >::template VisitStaticMethods( sv );

			// Cleanup the tables from the stack.
			lua_pop( pLS, 3 );
		}
	};
}}

#endif
