/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_LUA_BASIC_TYPES_H
#define FAIL_LUA_BASIC_TYPES_H

namespace fail { namespace LuaImpl
{
	template< typename T, e_CheckingPolicy Checking = cp_Throw, e_TypeCategory cat = TypeCategory< T >::value > struct TypeConverter
	{
	};
	
	template< typename T, e_CheckingPolicy Checking > struct NumericTypeConverter
	{
		NumericTypeConverter( lua_State* pLS_ ) :
			m_pLS( pLS_ )
		{
		}
		
		bool CheckType( int Index_ )
		{
			return LuaImpl::TypeCheckHelpers< Checking >::CheckType( m_pLS, Index_, LUA_TNUMBER );
		}
		
		bool ConvertFromLua( int Index_, T& Dest_ )
		{
			if( !CheckType( Index_ ) )
				return false;
			Dest_ = static_cast< T >( lua_tonumber( m_pLS, Index_ ) );
			return true;
		}
		
		void ConvertToLua( const T& Value_ )
		{
			lua_pushnumber( m_pLS, static_cast< lua_Number >( Value_ ) );
		}
		
		lua_State*	m_pLS;
	};
	
	template< e_CheckingPolicy Checking >
	struct TypeConverter< int32_t, Checking, tc_Default > : NumericTypeConverter< int32_t, Checking >
	{
		TypeConverter( lua_State* pLS_ ) : NumericTypeConverter< int32_t, Checking >( pLS_ ) {}
	};

	template< e_CheckingPolicy Checking >
	struct TypeConverter< uint32_t, Checking, tc_Default > : NumericTypeConverter< uint32_t, Checking >
	{
		TypeConverter( lua_State* pLS_ ) : NumericTypeConverter< uint32_t, Checking >( pLS_ ) {}
	};
	
	template< e_CheckingPolicy Checking >
	struct TypeConverter< int16_t, Checking, tc_Default > : NumericTypeConverter< int16_t, Checking >
	{
		TypeConverter( lua_State* pLS_ ) : NumericTypeConverter< int16_t, Checking >( pLS_ ) {}
	};
	template< e_CheckingPolicy Checking >
	struct TypeConverter< uint16_t, Checking, tc_Default > : NumericTypeConverter< uint16_t, Checking >
	{
		TypeConverter( lua_State* pLS_ ) : NumericTypeConverter< uint16_t, Checking >( pLS_ ) {}
	};
	
	template< e_CheckingPolicy Checking >
	struct TypeConverter< int8_t, Checking, tc_Default > : NumericTypeConverter< int8_t, Checking >
	{
		TypeConverter( lua_State* pLS_ ) : NumericTypeConverter< int8_t, Checking >( pLS_ ) {}
	};
	template< e_CheckingPolicy Checking >
	struct TypeConverter< uint8_t, Checking, tc_Default > : NumericTypeConverter< uint8_t, Checking >
	{
		TypeConverter( lua_State* pLS_ ) : NumericTypeConverter< uint8_t, Checking >( pLS_ ) {}
	};
	
	template< e_CheckingPolicy Checking >
	struct TypeConverter< float, Checking, tc_Default > : NumericTypeConverter< float, Checking >
	{
		TypeConverter( lua_State* pLS_ ) : NumericTypeConverter< float, Checking >( pLS_ ) {}
	};

	// bool
	template< e_CheckingPolicy Checking >
	struct TypeConverter< bool, Checking, tc_Default >
	{
		TypeConverter( lua_State* pLS_ ) :
			m_pLS( pLS_ )
		{
		}
		
		bool CheckType( int Index_ )
		{
			return LuaImpl::TypeCheckHelpers< Checking >::CheckType( m_pLS, Index_, LUA_TBOOLEAN );
		}
		
		bool ConvertFromLua( int Index_, bool& Dest_ )
		{
			if( !CheckType( Index_ ) )
				return false;
			Dest_ = !!lua_toboolean( m_pLS, Index_ );
			return true;
		}
		
		void ConvertToLua( const bool& Value_ )
		{
			lua_pushboolean( m_pLS, Value_ );
		}
		
		lua_State*	m_pLS;
	};

	// string
	template< e_CheckingPolicy Checking >
	struct TypeConverter< std::string, Checking, tc_Default >
	{
		TypeConverter( lua_State* pLS_ ) :
			m_pLS( pLS_ )
		{
		}
		
		bool CheckType( int Index_ )
		{
			return LuaImpl::TypeCheckHelpers< Checking >::CheckType( m_pLS, Index_, LUA_TSTRING );
		}

		bool ConvertFromLua( int Index_, std::string& Dest_ )
		{
			if( !CheckType( Index_ ) )
				return false;
			Dest_ = lua_tostring( m_pLS, Index_ );
			return true;
		}
		
		void ConvertToLua( const std::string& Value_ )
		{
			lua_pushstring( m_pLS, Value_.c_str() );
		}
		
		lua_State*	m_pLS;
	};
	
	// Dummy converters for all container types for now
	
	// TODO: fix this
/*	template< typename C, typename T, e_CheckingPolicy Checking >
	struct TypeConverter< MonadicType< C >, T, Checking, tc_Default >
	{
		TypeConverter( lua_State* pLS_ ) {}
		bool CheckType( int Index_ ) { return false; }
		bool ConvertFromLua( int Index_, T& Dest_ ) { return false; }
		void ConvertToLua( const T& Value_ ) {}
	};

	template< typename C1, typename C2, typename T, e_CheckingPolicy Checking >
	struct TypeConverter< DyadicType< C1, C2 >, T, Checking, tc_Default >
	{
		TypeConverter( lua_State* pLS_ ) {}
		bool CheckType( int Index_ ) { return false; }
		void ConvertFromLua( int Index_, T& Dest_ ) { return false; }
		void ConvertToLua( const T& Value_ ) {}
	};*/

	template< e_CheckingPolicy Checking >
	struct TypeConverter< DynamicBuffer, Checking, tc_Default >
	{
		TypeConverter( lua_State* pLS_ ) {}
		bool CheckType( int Index_ ) { return false; }
		void ConvertFromLua( int Index_, DynamicBuffer& Dest_ ) { return false; }
		void ConvertToLua( const DynamicBuffer& Value_ ) {}
	};
	
	// TODO: all other types
}}

#endif
