/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_LUA_ERROR_H
#define FAIL_LUA_ERROR_H

#include <stdexcept>

namespace fail { namespace LuaImpl
{
	namespace Error
	{
		class FLSERVICES_LUA_EXPORT Base
		{
			public:
				virtual ~Base() {}
				virtual void ConvertToLua( lua_State* pLS ) const = 0;
		};
		
		class FLSERVICES_LUA_EXPORT NotEnoughParams : public Base
		{
			public:
				NotEnoughParams( int NumGot_, int NumExpected_ ) throw() :
					m_NumGot( NumGot_ ),
					m_NumExpected( NumExpected_ )
				{
				}
				
				virtual void ConvertToLua( lua_State* pLS ) const;

			private:
				int m_NumGot;
				int m_NumExpected;
		};
		
		class FLSERVICES_LUA_EXPORT TypeMismatch : public Base
		{
			public:
				TypeMismatch( int Got_ = LUA_TNONE, int Expected_ = LUA_TNONE ) throw() :
					m_Got( Got_ ),
					m_Expected( Expected_ )
				{
				}

				virtual void ConvertToLua( lua_State* pLS ) const;

			protected:
				int m_Got;
				int m_Expected;
		};
		
		class FLSERVICES_LUA_EXPORT BadParameter : public TypeMismatch
		{
			public:
				BadParameter( const TypeMismatch& TM_, int Index_, const char* pName_ ) throw() :
					TypeMismatch( TM_ ),
					m_Index( Index_ ),
					m_pName( pName_ )
				{
				}
					
				virtual void ConvertToLua( lua_State* pLS ) const;

			private:
				int			m_Index;
				const char* m_pName;
		};
		
		class FLSERVICES_LUA_EXPORT BadMethodCall : public Base
		{
			public:
				virtual void ConvertToLua( lua_State* pLS ) const;
		};
		
		class FLSERVICES_LUA_EXPORT NoOverloadFound : public Base
		{
			public:
				virtual void ConvertToLua( lua_State* pLS ) const;
		};
			
		class FLSERVICES_LUA_EXPORT NoSuchAttribute : public Base
		{
			public:
				NoSuchAttribute( const char* pName_ ) :
					m_Name( pName_ )
				{
				}
				
				virtual void ConvertToLua( lua_State* pLS ) const;

			private:
				std::string	m_Name;
		};
		
		class FLSERVICES_LUA_EXPORT BadSetAttribute : public TypeMismatch
		{
			public:
				BadSetAttribute( const TypeMismatch& TM_, const char* pName_ ) throw() :
					TypeMismatch( TM_ ),
					m_pName( pName_ )
				{
				}
					
				virtual void ConvertToLua( lua_State* pLS ) const;

			private:
				const char* m_pName;
		};
	}
}}

#endif
