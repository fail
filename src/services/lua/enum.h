/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_LUA_ENUM_H
#define FAIL_LUA_ENUM_H

namespace fail { namespace LuaImpl
{
	template< typename enum_type > struct EnumWrapper
	{
		EnumWrapper( enum_type Value_ ) :
			pTypeInfo( &typeid( enum_type ) ),
			Value( Value_ )
		{
		}
			
		const std::type_info*	pTypeInfo;
		enum_type				Value;
	};
	
	struct EnumVisitor
	{
		EnumVisitor( lua_State* pLS_ ) :
			m_pLS( pLS_ )
		{
		}
		
		// Store the enum value in the class meta table (which should be on top of the lua stack).
		// It is stored as a userdata object containing the type tag address  for the enum (for
		// quick and easy type checking), followed by the enum value.
		template< typename enum_type > void declEnumValue( const char* pName_, enum_type Value_ )
		{
			void* pWrapper = lua_newuserdata( m_pLS, sizeof( EnumWrapper< enum_type > ) );
			if( !pWrapper )
				throw std::bad_alloc();
			
			new( pWrapper ) EnumWrapper< enum_type >( Value_ );

			// Set the metatable
			Registry< enum_type >::PushMetatable( m_pLS );
			lua_setmetatable( m_pLS, -2 );
			
			lua_setfield( m_pLS, -2, pName_ );
		}
		
		lua_State*	m_pLS;
	};
	
	template< typename enum_type, e_CheckingPolicy Checking  >
	struct TypeConverter< enum_type, Checking, tc_Enum >
	{
		TypeConverter( lua_State* pLS_ ) :
			m_pLS( pLS_ )
		{
		}

		bool CheckType( int Index_ )
		{		
			if( lua_type( m_pLS, Index_ ) != LUA_TUSERDATA )
			{
				TypeCheckHelpers< Checking >::ThrowTypeMismatch();
				return false;
			}
	
			const EnumWrapper< enum_type >* pWrapper =
					static_cast< const EnumWrapper< enum_type >* >( lua_touserdata( m_pLS, Index_ ) );
			if( *pWrapper->pTypeInfo != typeid( enum_type ) )
			{
				TypeCheckHelpers< Checking >::ThrowTypeMismatch();
				return false;
			}
			
			return true;
		}

		bool ConvertFromLua( int Index_, enum_type& Dest_ )
		{
			// Workaround for a warning occuring because the compiler apparently
			// fails to detect that when using a typeconverter with cp_Throw,
			// any code path not setting Dest_ throws
			Dest_ = static_cast< enum_type >( 0 );
			
			// TODO: better error reporting, reporting userdata as such instead of the type
			// we encapsulate inside is pretty useless
			if( !CheckType( Index_ ) )
				return false;

			const EnumWrapper< enum_type >* pWrapper =
					static_cast< const EnumWrapper< enum_type >* >( lua_touserdata( m_pLS, Index_ ) );
			Dest_ = pWrapper->Value;
			return true;
		}
		
		void ConvertToLua( const enum_type Value_ )
		{
			void* pWrapper = lua_newuserdata( m_pLS, sizeof( EnumWrapper< enum_type > ) );
			if( !pWrapper )
				throw std::bad_alloc();
			
			new( pWrapper ) EnumWrapper< enum_type >( Value_ );
			
			// Set the metatable
			Registry< enum_type >::PushMetatable( m_pLS );
			lua_setmetatable( m_pLS, -2 );
		}
		
		lua_State* m_pLS;
	};
	
	template< typename enum_type  >
	struct TypeConverter< enum_type, cp_NoCheck, tc_Enum >
	{
		TypeConverter( lua_State* pLS_ ) :
			m_pLS( pLS_ )
		{
		}

		bool CheckType( int Index_ )
		{		
			return true;
		}

		bool ConvertFromLua( int Index_, enum_type& Dest_ )
		{
			const EnumWrapper< enum_type >* pWrapper =
					static_cast< const EnumWrapper< enum_type >* >( lua_touserdata( m_pLS, Index_ ) );
			Dest_ = pWrapper->Value;
			return true;
		}
		
		void ConvertToLua( const enum_type Value_ )
		{
			void* pWrapper = lua_newuserdata( m_pLS, sizeof( EnumWrapper< enum_type > ) );
			if( !pWrapper )
				throw std::bad_alloc();
			
			new( pWrapper ) EnumWrapper< enum_type >( Value_ );
			
			// Set the metatable
			Registry< enum_type >::PushMetatable( m_pLS );
			lua_setmetatable( m_pLS, -2 );
		}
		
		lua_State* m_pLS;
	};
	template< class C, typename EnumTag > struct EnumComparator
	{
		static int Compare( lua_State* pLS_ )
		{
			typedef typename enum_traits< C, EnumTag >::type enum_type;
			enum_type a = static_cast< enum_type >( 0 );
			enum_type b = static_cast< enum_type >( 0 );
			
			TypeConverter< enum_type, cp_NoCheck, tc_Enum > tc( pLS_ );
			tc.ConvertFromLua( 1, a );
			tc.ConvertFromLua( 2, b );
			
			lua_pushboolean( pLS_, a == b ? 1 : 0 );
			
			return 1;
		}
	};
}}

#endif
