/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_LUA_CLASS_H
#define FAIL_LUA_CLASS_H

#include <type_traits>

namespace fail { namespace LuaImpl
{
	template< class C, typename AttrTag,
			  e_TypeCategory cat = TypeCategory< typename attribute_traits< C, AttrTag >::type >::value,
			  bool bReadOnly = flags::ReadOnly< AttrTag >::value,
	 		  bool bMutable = flags::Mutable< AttrTag >::value >
	struct ClassAttrGetter
	{
		static void GetAttr( lua_State* pLS )
		{
			shared_ptr< C > pObj;
			TypeConverter< shared_ptr< C >, cp_NoCheck > obj_tc( pLS );
			obj_tc.ConvertFromLua( 1, pObj );

			typedef attribute_traits< C, AttrTag > traits;
			TypeConverter< typename traits::type > value_tc( pLS );
			value_tc.ConvertToLua( ( pObj.get()->*traits::Getter() )() );
		}
	};
	
	template< class C, typename AttrTag >
	struct ClassAttrGetter< C, AttrTag, tc_ValueType, false, true >
	{
		static void GetAttr( lua_State* pLS )
		{
			shared_ptr< C > pObj;
			TypeConverter< shared_ptr< C >, cp_NoCheck > obj_tc( pLS );
			obj_tc.ConvertFromLua( 1, pObj );

			typedef attribute_traits< C, AttrTag > traits;
			TypeConverter< typename traits::type > value_tc( pLS );
 			value_tc.ConvertToLuaByRef( pObj, ( pObj.get()->*traits::GetterMutable() )() );
		}
	};
	
	template< class C, typename AttrTag,
			  bool bReadOnly = flags::ReadOnly< AttrTag >::value >
	struct ClassAttrSetter
	{
		static void SetAttr( lua_State* pLS )
		{
			typedef attribute_traits< C, AttrTag > traits;
	
			try
			{
				shared_ptr< C > pObj;
				TypeConverter< shared_ptr< C >, cp_NoCheck > obj_tc( pLS );
				obj_tc.ConvertFromLua( 1, pObj );

				typename traits::type Value;
				TypeConverter< typename traits::type > value_tc( pLS );
				value_tc.ConvertFromLua( 3, Value );
				( pObj.get()->*traits::Setter() )( Value );
			}
			catch( const Error::TypeMismatch& error )
			{
				throw Error::BadSetAttribute( error, traits::Name() );
			}
		}
	};
	
	template< class C, typename AttrTag >
	struct ClassAttrSetter< C, AttrTag, true >
	{
		static void SetAttr( lua_State* pLS )
		{
			// TODO: define an exception for this error
			std::cout << "error: Trying to set a read-only attribute\n";
		}
	};
	
	template< class C > struct ClassStuff
	{		
		struct Class_CallContext : public CallContext< C, shared_ptr< C > >
		{
			typedef CallContext< C, shared_ptr< C > > superclass;

			Class_CallContext( lua_State* pLS, bool bStatic = false, int SkipParams = 0 ) :
				superclass( pLS, bStatic, SkipParams )
			{
			}

			template< typename... Args > void constructObject( Args&&... args_ )
			{
				shared_ptr< C > pObj = shared_ptr< C >( new C( forward< Args >( args_ )... ) );
				TypeConverter< shared_ptr< C > > tc( superclass::m_pLS );
				tc.ConvertToLua( pObj );
			}
		};

		template< typename AttrTag > class AttributeWrapper : public AttributeWrapper_i
		{
			public:
				virtual void get( lua_State* pLS ) const
				{
					ClassAttrGetter< C, AttrTag >::GetAttr( pLS );
				}
				
				virtual void set( lua_State* pLS ) const
				{
					ClassAttrSetter< C, AttrTag >::SetAttr( pLS );
				}
		};

		struct SetupVisitor
		{
			SetupVisitor( lua_State* pLS_ ) : pLS( pLS_ ) {}

			lua_State* pLS;

			void declConstructor()
			{
				if( !flags::Scriptable< typename class_traits< C >::ctor_tag >::value )
					return;
				
				// Create a metatable for the global table, and redefine the __call function
				// to call the constructor wrapper.
				lua_newtable( pLS );
				lua_pushstring( pLS, "__call" );
				lua_pushcfunction( pLS, ( &CtorWrapper< C, Class_CallContext >::LuaCFunc ) );
				lua_settable( pLS, -3 );
			
				lua_setmetatable( pLS, -2 );
			}

			// This stuff is there to conditionally instantiate the attribute wrapping
			// code.
			//
			// Just a plain if doesn't work because the attribute wrapping code
			// is instantiated before getting discarded by the optimizer, and we don't
			// want this code to be instantiated at all so we're not forced to
			// define accessors for non-scriptable attributes.
			template< typename AttrTag > struct DoNotWantAttribute
			{
				static void setup( SetupVisitor& sv ) {}
			};
			
			template< typename AttrTag > struct DoWantAttribute
			{
				static void setup( SetupVisitor& sv )
				{
					sv.setupAttribute< AttrTag >();
				}
			};
			
			template< typename AttrTag > void declAttribute()
			{
				typedef std::conditional<
						flags::Scriptable< AttrTag >::value,
						DoWantAttribute< AttrTag >,
	  					DoNotWantAttribute< AttrTag > > AttrSetupProxy;
				AttrSetupProxy::type::setup( *this );
			}
			
			template< typename AttrTag > void setupAttribute()
			{
				if( !flags::Scriptable< AttrTag >::value )
					return;
				
				AttributeTable< C >::add( attribute_traits< C, AttrTag >::Name(),
								new AttributeWrapper< AttrTag >() );
			}
			
			template< typename MtdTag > void declMethod()
			{
				if( !flags::Scriptable< MtdTag >::value )
					return;
				
				lua_pushcfunction( pLS, ( &MethodWrapper< C, MtdTag, Class_CallContext >::LuaCFunc ) );
				lua_setfield( pLS, -2, method_traits< C, MtdTag >::Name() );
			}
			
			template< typename MtdTag > void declStaticMethod()
			{
				if( !flags::Scriptable< MtdTag >::value )
					return;

				lua_pushcfunction( pLS, ( &StaticMethodWrapper< C, MtdTag, Class_CallContext >::LuaCFunc ) );
				lua_setfield( pLS, -2, method_traits< C, MtdTag >::Name() );
			}
			
			template< typename SigTag > void declSignal()
			{
				if( !flags::Scriptable< SigTag >::value )
					return;
				
				typedef typename signal_traits< C, SigTag >::type sig_type;
				
				// Create the meta-tables for this signal type/class combination.
				// This might be redundant in some cases but it's only done at
				// init time so I don't care too much.
				lua_newtable( pLS );

				lua_pushcfunction( pLS, ( &SignalRef< C, sig_type >::Finalize ) );
				lua_setfield( pLS, -2, "__gc" );
				
				lua_pushcfunction( pLS, &SignalRefIndex );
				lua_setfield( pLS, -2, "__index" );
				
				lua_pushcfunction( pLS, ( &SignalConnectWrapper< C, SigTag >::LuaCFunc ) );
				lua_setfield( pLS, -2, "connect" );
				
				Registry< SignalRef< C, sig_type >* >::RegisterMetatable( pLS );
				
				// Create the meta table for connection handles to this signal.
				lua_newtable( pLS );

				lua_pushcfunction( pLS, ( SignalConnectionHandle< C, sig_type >::Finalize ) );
				lua_setfield( pLS, -2, "__gc" );
				
				lua_pushcfunction( pLS, ( &SignalConnectionHandle< C, sig_type >::LuaCFunc ) );
				lua_setfield( pLS, -2, "disconnect" );

				Registry< SignalConnectionHandle< C, sig_type >* >::RegisterMetatable( pLS );

		
				AttributeTable< C >::add( signal_traits< C, SigTag >::Name(),
									new SignalAttrWrapper< C, SigTag >() );
			}
		
			template< typename EnumTag > void declEnum()
			{
				if( !flags::Scriptable< EnumTag >::value )
					return;
				
				// Setup the metatable for this enum type
				lua_newtable( pLS );
	
				lua_pushcfunction( pLS, ( &EnumComparator< C, EnumTag >::Compare ) );
				lua_setfield( pLS, -2, "__eq" );
	
				Registry< typename enum_traits< C, EnumTag >::type >::RegisterMetatable( pLS );
				
				EnumVisitor ev( pLS );
				enum_traits< C, EnumTag >::template VisitValues( ev ); 
			}

			template< class SC > void declSuperClass()
			{
				if( !flags::Scriptable< C >::value )
					return;

				//std::cout << "\tsuperclass: " << class_traits< SC >::FullName() << std::endl;

				class_traits< SC >::template VisitSuperClasses( *this );

			//	std::cout << "in typecast map @" << &Registry< C >::GetTypeCastMap() <<": inserting "
				//	<< class_traits< C >::FullName() << " to " << class_traits< SC >::FullName() << " cast function\n";

				Registry< C* >::GetTypeCastMap().insert( std::make_pair(
						TypeInfoKey( typeid( SC ) ),										   
						&type_related_data< C >::template PointerUpCastFunc< SC > ) );

				class_traits< SC >::template VisitAttributes( *this );
				class_traits< SC >::template VisitMethods( *this );
				class_traits< SC >::template VisitSignals( *this );
			}
		};

		static void Setup()
		{
			if( !flags::Scriptable< C >::value )
				return;

			//std::cout << "Setting up lua bindings for class " << class_traits< C >::FullName() << std::endl;

			lua_State* pLS = LuaState::GetInstance()->get();

			StackGuard sg( pLS );

			// Create the meta table for pointers to const instances of this class.
			// It will not contain the newindex metamethod (used to implement
			// attribute assignation), and only wrappers for const methods.
			lua_newtable( pLS );
			lua_pushvalue( pLS, -1 );
			Registry< const C* >::RegisterMetatable( pLS );
			
			lua_pushstring( pLS, "__gc" );
			lua_pushcfunction( pLS, &PointerMetamethods< C >::Finalize );
			lua_settable( pLS, -3 );

			lua_pushstring( pLS, "__tostring" );
			lua_pushcfunction( pLS, &PointerMetamethods< C >::ToStringConst );
			lua_settable( pLS, -3 );
			
			// Create the meta table for pointers to instances of this class.
			// Stored in the registry, key is a lightuserdata containing a unique pointer.
			lua_newtable( pLS );
			lua_pushvalue( pLS, -1 );
			Registry< C* >::RegisterMetatable( pLS );
			
			lua_pushstring( pLS, "__gc" );
			lua_pushcfunction( pLS, &PointerMetamethods< C >::Finalize );
			lua_settable( pLS, -3 );

			lua_pushstring( pLS, "__tostring" );
			lua_pushcfunction( pLS, &PointerMetamethods< C >::ToString );
			lua_settable( pLS, -3 );
			
			SetupVisitor sv( pLS );

			// Visit the superclasses. This will fill the attributes from the superclasses,
			// and setup the wrappers for the methods of the superclasses.
			// This must be done first so that any overloaded method or attribute
			// will be overwritten.
			class_traits< C >::template VisitSuperClasses( sv );
			class_traits< C >::template VisitAttributes( sv );
			class_traits< C >::template VisitSignals( sv );

			// The __index member points to a C++ function that will first attempt a lua_rawget
			// for the asked key on the metatable. If successful, it's returned (so we can index
			// a member of the metatable, ie a method wrapper), otherwise we look for it in
			// the attribute map. If found, we push the value of the attribute as the result.
			lua_pushstring( pLS, "__index" );
			lua_pushcfunction( pLS, &AttributeTable< C >::Index );
			lua_settable( pLS, -3 );
			
			lua_pushstring( pLS, "__index" );
			lua_pushcfunction( pLS, &AttributeTable< C >::Index );
			lua_settable( pLS, -4 );
			
			// The __newindex member points to a C++ function that looks for the attribute
			// in the attribute map. If found, it sets the value of the attribute to what's
			// provided, provided that the type is suitable.
			lua_pushstring( pLS, "__newindex" );
			lua_pushcfunction( pLS, &AttributeTable< C >::NewIndex );
			lua_settable( pLS, -3 );
			
			
			// Setup the method wrappers and store them in the meta table.
			class_traits< C >::VisitMethods( sv );
			
			// Setup the global table containing the static functions for this class as well as
			// a pointer to the inheritance ctor wrapper (if any). Leave a copy of
			// it on the stack.
			lua_newtable( pLS );
			lua_pushvalue( pLS, -1 );
			lua_setfield( pLS, -5, class_traits< C >::Name() );
			
			// Store the object metatable in a special field of the class table, so
			// that the la module that manage lua classes can find it to do inheritance of
			// native classes.
			lua_pushvalue( pLS, -2 );
			lua_setfield( pLS, -2, "_flMetatable" );

			// Store a reference to the global table for the class in a special field as a way to query an object's type
			// at runtime from lua (using the fail.utils.typeof() lua function)
			lua_pushvalue( pLS, -1 );
			lua_setfield( pLS, -3, "_flType" );
			lua_pushvalue( pLS, -1 );
			lua_setfield( pLS, -4, "_flType" );
			
			// Setup the inheritance ctor wrapper pointer, if needed.
			SubClassingStuff< C >::Setup( pLS );
			
			// Create the enum values.
			class_traits< C >::template VisitEnums( sv );
			
			// Setup the static method wrappers and store them in the global table for this class.
			class_traits< C >::template VisitStaticMethods( sv );

			// Cleanup the tables from the stack.
		//	lua_pop( pLS, 3 );			
		}
	};
}}

#endif
