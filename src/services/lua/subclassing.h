/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_LUA_SUBCLASSING_H
#define FAIL_LUA_SUBCLASSING_H

namespace fail { namespace LuaImpl
{
	int InheritanceCtor( lua_State* pLS );

	typedef void ( * InheritCtorWrapperPtr )( lua_State* pLS, int InstanceIndex );
	
	template< class C, bool bExtensibleByScript = flags::ExtensibleByScript< C >::value >
	struct SubClassingStuff
	{
		static void Setup( lua_State* pLS )
		{
		}
	};
	
	template< class C > struct SubClassingStuff< C, true >
	{
		struct Deleter;

		struct LuaSubclassMixin : public C
		{
			template< typename... args_types > LuaSubclassMixin( args_types&&... args ) :
				C( forward< args_types >( args )... )
			{}

			weak_ptr< LuaSubclassMixin > fail_this;

			shared_ptr< LuaSubclassMixin > fail_getSharedPtr()
			{
				shared_ptr< LuaSubclassMixin > pObj = fail_this.lock();
				if( !pObj )
				{
					// There were no outstanding shared_ptr, so we create a new one.
					// It means that the native code just started pointing to us,
					// so we make the reference to the lua part strong as we might
					// need it longer than whatever lua code currently does.
					pObj = shared_ptr< LuaSubclassMixin >( this, Deleter() );
					fail_this = pObj;

					lua_State* pLS = LuaState::GetInstance()->get();

					// Get the registry index devoted to this instance
					// Get the weak registry table
					Registry< LuaState >::PushMetatable( pLS );
					lua_pushlightuserdata( pLS, this );
					lua_gettable( pLS, -2 );

					// Stick this thing in the non-weak registry
					lua_pushlightuserdata( pLS, this );
					lua_settable( pLS, LUA_REGISTRYINDEX );
				}

				return pObj;
			}

			struct fail_call_context
			{
				fail_call_context() :
					pLS( LuaState::GetInstance()->get() ),
					m_Guard( pLS ),
					m_ParamCount( 1 )
				{
				}
				
				template< typename T > void addParam( const T& Param )
				{
					TypeConverter< T > tc( pLS );
					tc.ConvertToLua( Param );
					++m_ParamCount;
				}
				
				void call()
				{
					if( lua_pcall( pLS, m_ParamCount, 0, 0 ) )
						throw runtime_error( lua_tostring( pLS, -1 ) );
				}
				
				template< typename T > T call()
				{
					if( lua_pcall( pLS, m_ParamCount, 1, 0 ) )
						throw runtime_error( lua_tostring( pLS, -1 ) );
					
					// Caveat: this will clearly not work for things
					// that don't have default constructors. I'm not
					// sure if I can find an acceptable solution or
					// just decree, as a constraint, that methods overidable
					// by scripts can't return things (ie structs) that don't
					// have default ctors.
					//
					// The alternative would be to use the copy ctor directly
					// from structs wrapped inside lua objects.
					//
					// A nice thing with the current system is that it allows
					// for soft failure if the lua script return a wrong type
					// or nothing at all.
					T result = T();
					TypeConverter< T, cp_NoThrow > tc( pLS );
					tc.ConvertFromLua( lua_gettop( pLS ), result );
					return result;
				}				
				
				lua_State*	pLS;
				StackGuard	m_Guard;
				int			m_ParamCount;
			};
			
			bool fail_prepareCall( fail_call_context& cc, const char* pMtdName )
			{
				// TODO: when I start using multithreading, obtain
				// the lua state for the current thread instead of this!
				lua_State* pLS = LuaState::GetInstance()->get();

				StackGuard guard( pLS );

				lua_pushlightuserdata( pLS, this );
				lua_gettable( pLS, LUA_REGISTRYINDEX );
				if( !lua_istable( pLS, -1 ) )
					return false;
				
				lua_getfield( pLS, -1, pMtdName );
				
				if( lua_iscfunction( pLS, -1 ) )
					return false;
			
				if( !lua_isfunction( pLS, -1 ) )
					return false;
		
				// Push the object table back at the top of the stack,
				// so it'll be used as the first parameter
				lua_pushvalue( pLS, -2 );

				guard.set( 0 );
				return true;
			}
		};

		struct Deleter
		{
			void operator()( LuaSubclassMixin* pObj )
			{
				// This is called when all outstanding shared_ptr to the object
				// are gone. We make the reference to the lua part weak so that
				// lua's garbage collection will take care of finalizing the
				// pointer wrapper when lua don't need the object anymore, which
				// will, at last, actually delete the damned native part.
				lua_State* pLS = LuaState::GetInstance()->get();
				lua_pushlightuserdata( pLS, pObj );
				lua_pushnil( pLS );
				lua_settable( pLS, LUA_REGISTRYINDEX );
			}
		};
		
		typedef subclass_wrapper< C, LuaSubclassMixin > Wrapper;

		struct SubClassPointerWrapper : public BasePointerWrapper< C >
		{
			SubClassPointerWrapper( Wrapper* pWrapper_ )	:
				pWrapper( pWrapper_ )
			{
			}

			virtual ~SubClassPointerWrapper()
			{
				// This is the final outcome of all this malarkey. We get there
				// when neither the native code nor lua need the object anymore,
				// so we can at last delete it.
				delete pWrapper;
			}
			
			virtual shared_ptr< C > getObj()
			{
				return static_pointer_cast< C >( pWrapper->fail_getSharedPtr() );
			}

			private:
				Wrapper* pWrapper;
		};

		
		struct InheritCtor_CallContext : public CallContext< C, shared_ptr< C > >
		{
			typedef CallContext< C, shared_ptr< C > > superclass;

			InheritCtor_CallContext( lua_State* pLS ) :
				superclass( pLS, true, 1 ),
				pWrapper( 0 ),
				m_pLS( pLS )
			{
			}

			template< typename... Args > void constructObject( Args&&... args_ )
			{
				pWrapper = new Wrapper( forward< Args >( args_ )... );

				void* pPointerWrapper = lua_newuserdata( m_pLS, sizeof( SubClassPointerWrapper ) );
				if( !pPointerWrapper )
					throw std::bad_alloc();
				
				new( pWrapper ) SubClassPointerWrapper( pWrapper );

				Registry< C* >::PushMetatable( m_pLS );
				lua_setmetatable( m_pLS, -2 );
			}
			
			Wrapper*	pWrapper;
			lua_State*	m_pLS;
		};
		
		// This is the implementation of class.init(), that is used by lua subclasses to init the
		// instance of their superclasses. This one will init the native part of the object for
		// lua classes inheriting from native classes.
		static int LuaInitFunc( lua_State* pLS )
		{
			luaL_checktype( pLS, 1, LUA_TTABLE );
				
			try
			{
				InheritCtor_CallContext context( pLS );
	
				// Construct the c++ instance. It will be put on the lua stack
				// as a wrapped pointer to the class we want to inherit from (whereas it is
				// actually an instance of the subclassing wrapper)
				if( !method_traits< C, typename class_traits< C >::ctor_tag >::
					template CallWrapper< InheritCtor_CallContext >::Call( context ) )
					throw Error::NoOverloadFound();
				
				// Store the instance in the registry using the address of the SubclassInfo
				// embedded into the wrapper. 
				// We always store it in the weak registry table.
				// It will be copied to the normal, non-weak registry to hold it as
				// a strong reference when the object is passed to C++ code.
				
				// Get the weak registry table
				Registry< LuaState >::PushMetatable( pLS );
				
				// Push the key
				lua_pushlightuserdata( pLS, static_cast< LuaSubclassMixin* >( context.pWrapper ) );
				
				// Push the value
				lua_pushvalue( pLS, 1 );
				lua_settable( pLS, -3 );
				
				// Remove the weak table from the stack
				lua_pop( pLS, 1 );
			
				// We now have the wrapped object pointer on the stack.			
				// Store it into the instance.
				lua_pushstring( pLS, "_flNativeInstance" );
				lua_pushvalue( pLS, -2 );
				lua_rawset( pLS, 1 );
			}
			catch( const Error::Base& error )
			{
				error.ConvertToLua( pLS );
			}
			
			return 0;
		}
		
		static void Setup( lua_State* pLS )
		{
			lua_pushcfunction( pLS, &LuaInitFunc );
			lua_setfield( pLS, -2, "init" );
		}
	};
}}

#endif
