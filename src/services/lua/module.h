/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_LUA_MODULE_H
#define FAIL_LUA_MODULE_H

namespace fail { namespace LuaImpl
{	
	struct ModuleVisitor
	{
		ModuleVisitor( lua_State* pLS_ ) :
			pLS( pLS_ ),
			bAtGlobalScope( true )
		{
		}
		
		// Called for each nested namespace until the namespace we're interested in.
		// Create all intermediate namespace tables as needed.
		// Also build the module name in the form module.submodule.subsubmodule
		// to be able to set the package.loaded properly.
		void QualifiedNameComponent( const char* pName_ )
		{			
			if( bAtGlobalScope )
			{
				FullModuleName = pName_;
				lua_getglobal( pLS, pName_ );
			}
			else
			{
				FullModuleName += '.';
				FullModuleName += pName_;
				lua_getfield( pLS, -1, pName_ );
			}

			if( lua_isnil( pLS, -1 ) )
			{
				lua_pop( pLS, 1 );
				lua_newtable( pLS );
				lua_pushvalue( pLS, -1 );
			
				if( bAtGlobalScope )
				{
					lua_setglobal( pLS, pName_ );
				}
				else
				{
					lua_setfield( pLS, -3, pName_ );
					lua_remove( pLS, -3 );
				}
			}
			
			bAtGlobalScope = false;
		}
		
		template< class C > void Class()
		{
			ClassStuff< C >::Setup();
		}
		
		template< class C > void Struct()
		{
			StructStuff< C >::Setup();
		}
		
		lua_State* pLS;
		bool bAtGlobalScope;
		std::string FullModuleName;
	};
}}

#endif
