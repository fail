/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_LUA_SIGNALATTR_H
#define FAIL_LUA_SIGNALATTR_H

#include <iostream>

namespace fail { namespace LuaImpl
{
	template< class C, typename SigTag > class SignalAttrWrapper : public AttributeWrapper_i
	{
		public:
			typedef signal_traits< C, SigTag > traits;
			
			virtual void get( lua_State* pLS ) const
			{
				shared_ptr< C > pObj;
				TypeConverter< shared_ptr< C >, cp_NoCheck, tc_Default > obj_tc( pLS );
				obj_tc.ConvertFromLua( 1, pObj );
				
				// Create a signalref
				TypeConverter< typename traits::type, cp_Throw, tc_Signal > sig_tc( pLS );
				sig_tc.ConvertToLua( pObj, &( pObj.get()->*traits::MemberPointer() ) );
			}
			
			virtual void set( lua_State* pLS ) const
			{
				// TODO: report an error properly (can't be arsed to do it now)
				cout << "lol trying to set a signal\n";
			}
	};
}}

#endif
