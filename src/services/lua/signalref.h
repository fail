/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_LUA_SIGNALREF_H
#define FAIL_LUA_SIGNALREF_H

//#include <iostream>

namespace fail { namespace LuaImpl
{
	// This is a reference to a signal of an object. It holds both the address of the signal
	// and the object it belongs to, so it also counts as an object reference. This way the
	// signal is valid as long as we hold a reference to it.
	template< class C, class SignalType > struct SignalRef
	{
		SignalRef( const shared_ptr< const C >& pObj_, SignalType* pSig_ ) :
			m_pObj( pObj_ ),
			m_pSignal( pSig_ )
		{
		}
		
		static int Finalize( lua_State* pLS )
		{
			void* pData = lua_touserdata( pLS, 1 );
			SignalRef* pSigRef = static_cast< SignalRef* >( pData );
			pSigRef->~SignalRef();
			return 0;
		}

		shared_ptr< const C >	m_pObj;
		SignalType*				m_pSignal;
	};
	
	template< class signal_type, e_CheckingPolicy Checking >
	struct TypeConverter< signal_type, Checking, tc_Signal >
	{
		TypeConverter( lua_State* pLS_ ) :
			m_pLS( pLS_ )
		{
		}
		
		template< class C > bool CheckType( int Index_ )
		{
			if( lua_isnil( m_pLS, Index_ ) )
				return true;
			
			if( lua_type( m_pLS, Index_ ) != LUA_TUSERDATA )
			{
				// TODO: better error reporting for that case
				TypeCheckHelpers< Checking >::ThrowTypeMismatch();
				return false;
			}

			if( !lua_getmetatable( m_pLS, Index_ ) )
			{
				TypeCheckHelpers< Checking >::ThrowTypeMismatch();
				return false;
			}

			Registry< SignalRef< C, signal_type >* >::PushMetatable( m_pLS );
		
			bool bRes = lua_rawequal( m_pLS, -1, -2 );	
			lua_pop( m_pLS, 2 );
			if( bRes )
				return true;

			TypeCheckHelpers< Checking >::ThrowTypeMismatch();
			return false;
		}
	
		template< class C > bool ConvertFromLua( int Index_, signal_type*& Dest_, shared_ptr< const C >& pObj_ )
		{

			if( !CheckType< C >( Index_ ) )
				return false;
			const SignalRef< C, signal_type >* pSigRef = 
					static_cast< const SignalRef< C, signal_type >* >( lua_touserdata( m_pLS, Index_ ) );
			Dest_ = pSigRef->m_pSignal;
			pObj_ = pSigRef->m_pObj;
			return true;
		}

		template< class C > void ConvertToLua( const shared_ptr< C >& pObj_, signal_type* pSig_ )
		{

			void* pData = lua_newuserdata( m_pLS, sizeof( SignalRef< C, signal_type > ) );
			if( !pData )
				throw bad_alloc();
			
			new( pData ) SignalRef< C, signal_type >( pObj_, pSig_ );

			Registry< SignalRef< C, signal_type >* >::PushMetatable( m_pLS );	
			lua_setmetatable( m_pLS, -2 );
		}
		
		lua_State*	m_pLS;
	};	
	
	template< class signal_type >
	struct TypeConverter< signal_type, cp_NoCheck, tc_Signal >
	{
		TypeConverter( lua_State* pLS_ ) :
			m_pLS( pLS_ )
		{
		}
		
		template< class C > bool CheckType( int Index_ )
		{
			return true;
		}
	
		template< class C > bool ConvertFromLua( int Index_, signal_type*& Dest_ )
		{
			const SignalRef< C, signal_type >* pSigRef = 
					static_cast< const SignalRef< C, signal_type >* >( lua_touserdata( m_pLS, Index_ ) );
			Dest_ = pSigRef->m_pSignal;
			return true;
		}
		
		lua_State*	m_pLS;
	};
}}

#endif
