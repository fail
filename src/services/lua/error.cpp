/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <sstream>
#include "lua.h"
#include "lauxlib.h"
#include "services/lua/interface.h"
#include "services/lua/error.h"

void fail::LuaImpl::Error::NotEnoughParams::ConvertToLua( lua_State* pLS ) const
{
	std::stringstream msg;
	msg << "expected " << m_NumExpected << " params, got " << m_NumGot;
	luaL_error( pLS, msg.str().c_str() );
}

void fail::LuaImpl::Error::TypeMismatch::ConvertToLua( lua_State* pLS ) const
{
	std::stringstream msg;
	msg << "type mismatch: expected " << lua_typename( pLS, m_Expected )
			<< ", got " << lua_typename( pLS, m_Got );
	luaL_error( pLS, msg.str().c_str() );
}

void fail::LuaImpl::Error::BadParameter::ConvertToLua( lua_State* pLS ) const
{
	std::stringstream msg;
	msg << "type mismatch for parameter #" << m_Index << " (" << m_pName << "): expected "
			<< lua_typename( pLS, m_Expected ) << ", got " << lua_typename( pLS, m_Got );
	luaL_error( pLS, msg.str().c_str() );
}

void fail::LuaImpl::Error::BadMethodCall::ConvertToLua( lua_State* pLS ) const
{
	luaL_error( pLS, "bad method call (missing instance parameter, or instance parameter type mismatch).\n"
			"  Did you write object.method instead of object:method?" );
}

void fail::LuaImpl::Error::NoOverloadFound::ConvertToLua( lua_State* pLS ) const
{
	luaL_error( pLS, "no matching overload found for method call" );
}

void fail::LuaImpl::Error::NoSuchAttribute::ConvertToLua( lua_State* pLS ) const
{
	std::stringstream msg;
	msg << "trying to set unknown attribute '" << m_Name << '\'';
	luaL_error( pLS, msg.str().c_str() );
}

void fail::LuaImpl::Error::BadSetAttribute::ConvertToLua( lua_State* pLS ) const
{
	std::stringstream msg;
	msg << "type mismatch for attribute '" << m_pName << "': expected "
			<< lua_typename( pLS, m_Expected ) << ", got " << lua_typename( pLS, m_Got );
	luaL_error( pLS, msg.str().c_str() );
}
