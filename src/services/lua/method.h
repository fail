/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_LUA_METHOD_H
#define FAIL_LUA_METHOD_H

namespace fail { namespace LuaImpl
{
	template< class C, typename PointerT > struct CallContext
	{
		CallContext( lua_State* pLS, bool bStatic = false, int SkipParams = 0 ) :
			m_pLS( pLS ),
			m_paramindex( SkipParams + 1 ),
			m_bReturnValue( false ),
			m_bStatic( bStatic ),
			m_SkipParams( SkipParams )
		{
		}
		
		int numParams() const
		{
			int numparams = lua_gettop( m_pLS );
			
			if( !m_bStatic )
				--numparams;
			
			numparams -= m_SkipParams;
			
			return numparams;
		}
		
		void numExpectedParams( int Num_ ) const
		{
			// Check if we have the proper number of parameters	
			int numargs = numParams();		
			if( numargs != Num_ )
				throw Error::NotEnoughParams( numargs, Num_ );
		}
		
		C& getObject()
		{
			PointerT Ptr;
								
			try
			{
				TypeConverter< PointerT > tc( m_pLS );
				tc.ConvertFromLua( m_paramindex, Ptr );							
				++m_paramindex;
				return *Ptr;
			}
			catch( const Error::TypeMismatch& error )
			{
				throw Error::BadMethodCall();
			}
		}
		
		template< typename T > bool getParam( const char* pName_, T& Dest_ )
		{
			TypeConverter< T, cp_NoThrow > tc( m_pLS );
			
			if( !tc.ConvertFromLua( m_paramindex, Dest_ ) )
			{
				m_paramindex = m_SkipParams + 1;
				return false;
			}
			
			++m_paramindex;
			return true;
		}
		
		template< typename T > void setReturnValue( const T& Value_ )
		{
			TypeConverter< T > tc( m_pLS );
			tc.ConvertToLua( Value_ );
			m_bReturnValue = true;
		}
		
		lua_State*	m_pLS;
		int 		m_paramindex;
		bool		m_bReturnValue;
		bool		m_bStatic;
		int			m_SkipParams;
	};
	
	template< class C, typename MtdTag, class CC > struct StaticMethodWrapper
	{
		static int LuaCFunc( lua_State* pLS )
		{
			try
			{
				CC context( pLS, true );
				if( !method_traits< C, MtdTag >::template CallWrapper< CC >::Call( context ) )
					throw Error::NoOverloadFound();
				return context.m_bReturnValue ? 1 : 0;
			}
			catch( const Error::Base& error )
			{
				error.ConvertToLua( pLS );
				return 0;
			}
			catch( const std::exception& e )
			{
				return luaL_error( pLS, e.what() );
			}
		}
	};

	template< class C, class CC > struct CtorWrapper
	{
		static int LuaCFunc( lua_State* pLS )
		{
			try
			{
				CC context( pLS, true, 1 );
				if( !method_traits< C, typename class_traits< C >::ctor_tag >::
					template CallWrapper< CC >::Call( context ) )
					throw Error::NoOverloadFound();
				return 1;
			}
			catch( const Error::Base& error )
			{
				error.ConvertToLua( pLS );
				return 0;
			}
			catch( const std::exception& e )
			{
				return luaL_error( pLS, e.what() );
			}
		}
	};
	
	template< class C, typename MtdTag, class CC > struct MethodWrapper
	{
		static int LuaCFunc( lua_State* pLS )
		{
			try
			{
				CC context( pLS, false );
				if( !method_traits< C, MtdTag >::template CallWrapper< CC >::Call( context ) )
					throw Error::NoOverloadFound();
				return context.m_bReturnValue ? 1 : 0;
			}
			catch( Error::Base& error )
			{
				error.ConvertToLua( pLS );
				return 0;
			}
			catch( const std::exception& e )
			{
				return luaL_error( pLS, e.what() );
			}
		}	
	};
}}

#endif
