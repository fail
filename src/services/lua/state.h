/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_LUA_STATE_H
#define FAIL_LUA_STATE_H

#include "core/core.h"
#include "interface.h"
#include "lua.h"
#include "lauxlib.h"
#include <string>

namespace fail
{
	class FLSERVICES_LUA_EXPORT LuaState
	{
		public:
			static void Init( lua_State* pState );
			static LuaState* GetInstance()
			{
				return ms_pInstance;
			}
			
			operator lua_State*() const
			{
				return m_pState;
			}

			operator const lua_State*() const
			{
				return m_pState;
			}
			
			lua_State* get() const
			{
				return m_pState;
			}
			
			void prependPackagePath( std::string Path );
			
		private:
			static void* LuaAlloc( void* ud, void* ptr, size_t osize, size_t nsize );
			
			LuaState( lua_State* pState );
					
			lua_State*			m_pState;
			static LuaState*	ms_pInstance;
	};
	
	namespace LuaImpl
	{
		// Utility class: this is a guard class that lives on the stack
		// and pops the specified number of lua stack levels at destruction.
		// The level can be changed at any time.
		// If set at -1, it will reset the stack to the number of elements
		// it had when the guard was created.
		class StackGuard
		{
			public:
				StackGuard( lua_State* pLS ) :
					m_pLS( pLS ),
					m_Levels( -1 )
				{
					m_OrigTop = lua_gettop( pLS );
				}
				
				StackGuard( lua_State* pLS, int Levels ) :
					m_pLS( pLS ),
					m_Levels( Levels )
				{
					m_OrigTop = lua_gettop( pLS );
				}
							
				~StackGuard()
				{
					if( m_Levels < 0 )
						lua_settop( m_pLS, m_OrigTop );
					else
						lua_pop( m_pLS, m_Levels );
				}
			
				void set( int Levels )
				{
					m_Levels = Levels;
				}
				
			private:
				lua_State* m_pLS;
				int m_OrigTop;
				int m_Levels;
		};
	}
}

#endif
