/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>

#define lua_c

#include "lua/interface.h"
#include "lua/state.h"
#include "module_fail-luatest.h"
#include "testapp.h"
#include "lua/lua.h"
#include "lua/lauxlib.h"
#include "lua/lualib.h"
#include "lua/luaconf.h"

using namespace std;
using namespace luatestapp;

LuaTestApp::LuaTestApp() throw( std::runtime_error )
{
	fail::LuaSetup< fail::luatest::fail_tag >();
	
	luaL_openlibs( fail::LuaState::GetInstance()->get() );
}

LuaTestApp::~LuaTestApp()
{
}

void LuaTestApp::run()
{
	//cout << "I see." << endl;
	
	bool bQuit = false;
	fail::LuaState* pState = fail::LuaState::GetInstance();
	lua_State* pLS = pState->get();
			
	while( !bQuit )
	{
		char buffer[512];
		char* pCmdLine = buffer;

		if( lua_readline( pLS, pCmdLine, "> " ) )
		{
			lua_pushstring( pLS, pCmdLine );
			lua_saveline( pLS, -1 );
			lua_remove( pLS, -1 );
			
			if( !strcmp( pCmdLine, "q" ) )
				bQuit = true;
			else
			{
				if( !luaL_loadstring( pLS, pCmdLine ) )
				{
					switch( lua_pcall( pLS, 0, 0, 0 ) )
					{
						case LUA_ERRRUN:
						{
							const char* pErrMsg = lua_tostring( pLS, -1 );
							std::cerr << pErrMsg << std::endl;
							break;
						}
							
						case LUA_ERRMEM:
							std::cerr << "LUA_ERRMEM" << std::endl;
							break;

						case LUA_ERRERR:
							std::cerr << "LUA_ERRERR" << std::endl;
							break;
					}
				}
				else
					cerr << "Syntax error" << endl;
			}
	
			lua_freeline( pState->get(), pCmdLine );
		}

		lua_settop( pState->get(), 0 );
	}
}
