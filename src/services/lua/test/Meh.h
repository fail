/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_LUA_MEH_H_
#define FAIL_LUA_MEH_H_

#include <iostream>
#include "core/core.h"
//#include "core/pointer.h"
//#include "base_Meh.h"

namespace fail { namespace luatest
{
	class Meh : public RefCounted
	{
		public:
			Meh()
			{
				std::cout << "Meh constructor\n";
				m_OhNoes = 12345;
				m_blah = fuck;
			}
			
			~Meh()
			{
				std::cout << "Meh destructor\n";
			}
			
			static int32_t Blah( int32_t blah )
			{
				std::cout << "Ha! Ha!\nI'm printing\nTEST STUFF!!1!\n"
					<< blah << std::endl;
				
				return blah + 1;
			}
			
			int32_t Orly( int32_t meh, int32_t grunt )
			{
				std::cout << "orly, overload 1:\n" << meh << ' ' << grunt << std::endl;
				return meh + grunt;
			}
			
			int32_t Orly( std::string& meh, int32_t grunt )
			{
				std::cout << "orly, overload 2:\n" << meh << ' ' << grunt << std::endl;
				return 3;
			}
			
			int32_t Orly()
			{
				std::cout << "orly, overload 3\n";
				return 2;
			}
						
			static ConstPointer< Meh > Create()
			{
				return new Meh;
			}
			
			virtual int32_t WTF( int32_t eat, int32_t shit )
			{
				std::cout << "meh wtf\n";
				return eat + shit;
			}
			
			const int32_t& getOhNoes() const
			{
				std::cout << "I'm in ur getterz\nstealing ur valuz\n";
				return m_OhNoes;
			}
			
			void setOhNoes( const int32_t& x )
			{
				std::cout << "I'm in ur methodz\nsetting ur attributz\n";
				m_OhNoes = x;
			}
			
			void do_lulz( int32_t epic, int32_t fail );

			void do_EpicFail()
			{
				m_EpicFail();
			}
			
			fail::Signal< int32_t, int32_t > m_lulz;
			fail::Signal<> m_EpicFail;
			
			enum Fail
			{
				eat,
				shit,
				fuck
			};
			
			const Fail& getblah() const
			{
				return m_blah;
			}
			
			void setblah( const Fail& x )
			{
				m_blah = x;
			}
			
			static int32_t CallWTF( Pointer< Meh > pMeh, int32_t eat, int32_t shit )
			{
				return pMeh->WTF( eat, shit );
			}

			
			const GenericPointer& getlolcat() const { return m_lolcat; }
			void setlolcat( const GenericPointer& x ) { m_lolcat = x; }
			
		protected:
			int32_t	m_OhNoes;
			Fail	m_blah;
			GenericPointer	m_lolcat;
	};
}}

#endif
