/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace fail { namespace luatest
{
	class Meh
	{
		static int32_t CallWTF( Pointer< Meh > pMeh, int32_t eat, int32_t shit );
	
		Meh();
		//Meh( int32_t lulz );
		static int32_t Blah( int32_t blah );
		int32_t Orly( int32_t meh, int32_t grunt );		
		int32_t Orly( string meh2, int32_t grunt2 );
		int32_t Orly();
		
		virtual int32_t WTF( int32_t eat, int32_t shit );
		
		static ConstPointer< Meh > Create();
		
		int32_t OhNoes;
		Signal<int32_t, int32_t> lulz;
		Signal<> EpicFail;
		
		GenericPointer lolcat;
		
		enum Fail
		{
			eat,
			shit,
			fuck
		};
		
		enum luatest::Meh::Fail blah;
		
		void do_lulz( int32_t epic, int32_t fail );
		void do_EpicFail();
	};
	
	struct MustardMan
	{
		MustardMan();
		void Blah();
		static void Gronk();
		static MustardMan Create();
		
		int32_t	Yaaaa;
	};

	class CeilingCat : Meh
	{
		CeilingCat(); // string what, bool bNo );
		static Pointer< CeilingCat > Create();
		
		void Meow();
		
		int32_t NotAmused;
		
		MustardMan ZOMG;
	};
}}
