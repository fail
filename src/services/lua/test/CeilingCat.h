/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_LUA_CEILINGCAT_H_
#define FAIL_LUA_CEILINGCAT_H_

#include <iostream>
#include "core/core.h"
#include "Meh.h"
#include "MustardMan.h"

namespace fail { namespace luatest
{
	class CeilingCat : public Meh
	{
		public:
			CeilingCat()
			{
				std::cout << "CeilingCat is watching you instancing objects\n";
				m_OhNoes = 12345;
				m_NotAmused = 2;
			}
			
			~CeilingCat()
			{
				std::cout << "CeilingCat destructor\n";
			}
			
			static Pointer< CeilingCat > Create()
			{
				return new CeilingCat;
			}
			
			const int32_t& getOhNoes() const
			{
				std::cout << "ceiling cat is watching you access attributes\n";
				return m_OhNoes;
			}
			
			void setOhNoes( const int32_t& x )
			{
				std::cout << "ceiling cat is watching you setting attributes\n";
				m_OhNoes = x;
			}
			
			int32_t WTF( int32_t eat, int32_t shit )
			{
				std::cout << "ceilingcat wtf\n";
				return eat * shit;
			}
			
			void Meow()
			{
				std::cout << "Dude... Wait, what?\n";
			}
			
			const int32_t& getNotAmused() const
			{
				return m_NotAmused;
			}
			void setNotAmused( const int32_t& x )
			{
				m_NotAmused = x;
			}
			
			const MustardMan& getZOMG() const
			{
				return m_ZOMG;
			}
			MustardMan& getZOMG()
			{
				return m_ZOMG;
			}
			void setZOMG( const MustardMan& x )
			{
				m_ZOMG = x;
			}
			
		protected:
			int32_t m_NotAmused;
			MustardMan m_ZOMG;
	};
}}

#endif
