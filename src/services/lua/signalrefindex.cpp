/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "lua.h"
#include "services/lua/interface.h"
#include "services/lua/signalrefindex.h"
	
int fail::LuaImpl::SignalRefIndex( lua_State* pLS )
{
	lua_getmetatable( pLS, 1 );
	lua_pushvalue( pLS, 2 );
	lua_gettable( pLS, -2 );
	return 1;
}
