/*
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_LUA_REGISTRY_H
#define FAIL_LUA_REGISTRY_H

#include "core/core.h"
#include "interface.h"
#include "lua.h"
#include "lauxlib.h"
#include <string>

// TODO: crashes can occur if C++ code returns a pointer to a class whose lua module hasn't been
// loaded by the user (we try to lookup the type data from the dynamic type and it's not registered
// in the map).
// We should either automatically load dependent modules or throw some warning message (the later is probably
// easier to do and should be done at least as a short term solution)

namespace fail
{
	namespace LuaImpl
	{
		typedef shared_ptr< void > ( * PointerCastFunc_p )( shared_ptr< const void > );
		typedef std::map< TypeInfoKey, PointerCastFunc_p > TypeCastMap;
		
		struct type_data
		{
			type_data() : MetatableIndex( -1 ) {}
			
			int MetatableIndex;
			TypeCastMap TypeCasts;
		};

		template< typename T > struct FLSERVICES_LUA_EXPORT type_related_data
		{
			template< class to_type > static shared_ptr< void > PointerUpCastFunc( shared_ptr< const void > pPointer )
			{
				shared_ptr< T > pPtr = static_pointer_cast< T >( const_pointer_cast< void >( pPointer ) );
				return static_pointer_cast< to_type >( pPtr );
			}
			
			static type_data data;
		};

		
		template< typename T > type_data type_related_data< T >::data;


		extern FLSERVICES_LUA_EXPORT std::map< TypeInfoKey, type_data* > g_MetatableMap;
		extern FLSERVICES_LUA_EXPORT std::map< TypeInfoKey, type_data* > g_MetatableMapConst;

		template< typename T, bool bConst = std::is_const< T >::value >
		struct FLSERVICES_LUA_EXPORT MetatableMap
		{
			static std::map< TypeInfoKey, type_data* >& Get()
			{
				return g_MetatableMap;
			}
		};
		
		template< typename T >
		struct FLSERVICES_LUA_EXPORT MetatableMap< T, true >
		{
			static std::map< TypeInfoKey, type_data* >& Get()
			{
				return g_MetatableMapConst;
			}
		};
		
		// For non-polymorphic types, we simply store the lua registry index of the metatable in a static
		// variable and use this directly.
		template< typename T, bool bPolymorphic = flags::Polymorphic< typename std::remove_pointer< T >::type >::value >
		struct Registry
		{		
			static int RegisterMetatable( lua_State* pLS )
			{
				return type_related_data< T >::data.MetatableIndex = luaL_ref( pLS, LUA_REGISTRYINDEX );
			}

			static void PushMetatable( lua_State* pLS )
			{
				lua_rawgeti( pLS, LUA_REGISTRYINDEX, type_related_data< T >::data.MetatableIndex );
			}

			static void PushMetatable( lua_State* pLS, T Obj )
			{
				lua_rawgeti( pLS, LUA_REGISTRYINDEX, type_related_data< T >::data.MetatableIndex );
			}
			
			static TypeCastMap& GetTypeCastMap()
			{
				return type_related_data< T >::data.TypeCasts;
			}

			static TypeCastMap& GetTypeCastMap( T Obj )
			{
				return type_related_data< T >::data.TypeCasts;
			}
		};
			
		// For polymorphic classes, we maintain a mapping of type_infos to lua registry indices of the metatables,
		// and use typeid to obtain the type_info at runtime from the object pointer to find the appropriate metatable.
		template< typename T > struct Registry< T, true >
		{
			static int RegisterMetatable( lua_State* pLS )
			{
				type_related_data< T >::data.MetatableIndex = luaL_ref( pLS, LUA_REGISTRYINDEX );
				MetatableMap< T >::Get()[ TypeInfoKey( typeid( typename std::remove_pointer< T >::type ) ) ] =
					&type_related_data< T >::data;
					
			//	std::cout << "typecastmap for type " << typeid( typename std::remove_pointer< T >::type ).name()
			//		<< " @" << &type_related_data< T >::data.TypeCasts << std::endl;
					
				return type_related_data< T >::data.MetatableIndex;
			}
			
			static void PushMetatable( lua_State* pLS )
			{
			//	std::cout << "pushmt: " << typeid( T ).name() << std::endl;
				lua_rawgeti( pLS, LUA_REGISTRYINDEX, type_related_data< T >::data.MetatableIndex );
			}
			
			static void PushMetatable( lua_State* pLS, T Obj )
			{
			//	std::cout << "pushmt: " << typeid( T ).name() << ", dyn type: " << typeid( *Obj ).name() << std::endl;
				lua_rawgeti( pLS, LUA_REGISTRYINDEX, MetatableMap< T >::Get()[ TypeInfoKey( typeid( *Obj ) ) ]->MetatableIndex );
			}
			
			static TypeCastMap& GetTypeCastMap()
			{
			//	std::cout << "retrieving type cast map using static type " << typeid( T ).name() <<
			//		": " << &type_related_data< T >::data.TypeCasts << std::endl;

				return type_related_data< T >::data.TypeCasts;
			}
			
			static TypeCastMap& GetTypeCastMap( T Obj )
			{
			//	std::cout << "retrieving type cast map using dynamic type " << typeid( *Obj ).name() <<
			//		": " << &MetatableMap< T >::Get()[ TypeInfoKey( typeid( *Obj ) ) ]->TypeCasts << std::endl;

				return MetatableMap< T >::Get()[ TypeInfoKey( typeid( *Obj ) ) ]->TypeCasts;
			}

		};
	}
}

#endif
