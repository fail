/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_LUA_SIGNALCONNECT_H
#define FAIL_LUA_SIGNALCONNECT_H

//#include <iostream>

// TODO: be able to delete a signal connection (connect() should return a bundle of a signalref + slot handle
// wrapped in a userdata object with a disconnect method - or just a handle to the slot and
// then use object.FuckingSignal::disconnect(handle) - it would bring symmetry and harmony to the world
// at least.

namespace fail { namespace LuaImpl
{
	// This object contains a signal connection handle. It's returned to the lua script
	// when creating a signal connection, and manages the connection's lifetime. The connection is
	// severed when this is garbage collected, so the object creating the connection must hold a reference to it.
	// It also allows to explicitely destroy the connection by calling the disconnect method.
	template< class C, class SignalType > struct SignalConnectionHandle
	{
		SignalConnectionHandle( const shared_ptr< const C >& pObj_, SignalType* pSig_,
								const shared_ptr< const void >& pWrapper_,
								typename SignalType::slot_handle SlotHandle_ ) :
			m_pObj( pObj_ ),
			m_pSignal( pSig_ ),
			m_pWrapper( pWrapper_ ),
			m_SlotHandle( SlotHandle_ )
		{
		}
		
		static int Finalize( lua_State* pLS )
		{
			void* pData = lua_touserdata( pLS, 1 );
			SignalConnectionHandle* pSigConHandle = static_cast< SignalConnectionHandle* >( pData );
			pSigConHandle->~SignalConnectionHandle();
			return 0;
		}

		static int LuaCFunc( lua_State* pLS )
		{
			try
			{
				SignalConnectionHandle< C, SignalType >* pSigConHandle = 
					static_cast< SignalConnectionHandle< C, SignalType >* >( lua_touserdata( pLS, -1 ) );
				
				pSigConHandle->m_pSignal->disconnect( pSigConHandle->m_SlotHandle );
				pSigConHandle->m_pWrapper.reset();
			}
			catch( Error::Base& error )
			{
				error.ConvertToLua( pLS );
				return 0;
			}
			catch( const std::exception& e )
			{
				return luaL_error( pLS, e.what() );
			}

			return 0;
		}

		shared_ptr< const C >				m_pObj;
		SignalType*							m_pSignal;
		
		// We don't really care about the wrapper type, we just want to
		// hold a reference to keep it alive. Using an untyped pointer makes
		// things easier.
		shared_ptr< const void >			m_pWrapper;
		typename SignalType::slot_handle	m_SlotHandle;
	};
	
	struct SlotCallContext
	{
		SlotCallContext() :
			m_pLS( NULL ),
			m_ParamCount( 0 )
		{
		}
		
		void prepare()
		{
			m_ParamCount = 0;
			lua_pushlightuserdata( m_pLS, this );
			lua_gettable( m_pLS, LUA_REGISTRYINDEX );
		}
		
		template< typename T > void addParam( const T& Param_ )
		{
			TypeConverter< T > tc( m_pLS );
			tc.ConvertToLua( Param_ );
			++m_ParamCount;
		}
		
		void call()
		{
			if( lua_pcall( m_pLS, m_ParamCount, 0, 0 ) )
				throw runtime_error( lua_tostring( m_pLS, -1 ) );
		}
		
		lua_State*	m_pLS;
		int			m_ParamCount;
	};
	
	template< class C, typename SigTag > struct SignalConnectWrapper
	{
		static int LuaCFunc( lua_State* pLS )
		{
			try
			{
				typedef typename signal_traits< C, SigTag >::type signal_type;
				signal_type* pSignal = NULL;
				shared_ptr< const C > pObj;

				try
				{
					TypeConverter< signal_type, cp_Throw, tc_Signal > tc( pLS );
					tc.template ConvertFromLua< C >( 1, pSignal, pObj );
				}
				catch( const Error::TypeMismatch& error )
				{
					throw Error::BadMethodCall();
				}

				luaL_checktype( pLS, 2, LUA_TFUNCTION );
				
				typedef typename
						signal_traits< C, SigTag >::template SlotWrapper< SlotCallContext >
						wrapper_type;
				
				shared_ptr< wrapper_type > pWrapper( new wrapper_type );
				pWrapper->m_CallContext.m_pLS = pLS;

				// Copy the lua function in the lua registry so we can retrieve it
				// when the signal is triggered.
				lua_pushlightuserdata( pLS, &pWrapper->m_CallContext );
				lua_pushvalue( pLS, 2 );
				lua_settable( pLS, LUA_REGISTRYINDEX );

				typename signal_type::slot_handle sh = pSignal->connect( pWrapper, &wrapper_type::Call );
				
				void* pData = lua_newuserdata( pLS, sizeof( SignalConnectionHandle< C, signal_type > ) );
				if( !pData )
					throw bad_alloc();
			
				new( pData ) SignalConnectionHandle< C, signal_type >( pObj, pSignal, pWrapper, sh );
				
				Registry< SignalConnectionHandle< C, signal_type >* >::PushMetatable( pLS );
				lua_setmetatable( pLS, -2 );
			}
			catch( Error::Base& error )
			{
				error.ConvertToLua( pLS );
				return 0;
			}
			catch( const std::exception& e )
			{
				return luaL_error( pLS, e.what() );
			}

			return 1;
		}
	};
}}

#endif
