/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_LUA_POINTER_H
#define FAIL_LUA_POINTER_H

namespace fail { namespace LuaImpl
{
	// TODO: need a way to make sure it's a pointerwrapper, like a magic number or something
	// otherwise if the user pass another kind of user data (say, first argument of a method
	// is an enum and the user wrote object.method instead of object:method), it segfaults
	// because it assumes that even if the metatable is unknown, it's a wrapped pointer.
	template< typename C > struct BasePointerWrapper
	{
		BasePointerWrapper() :
			pTypeInfo( &typeid( C ) ),
			pTypeCastMap( &Registry< C >::GetTypeCastMap() )
		{
		}

		BasePointerWrapper( C* pObj ) :
			pTypeInfo( &typeid( C ) ),
			pTypeCastMap( &Registry< C* >::GetTypeCastMap( pObj ) )		
		{
		}

		virtual ~BasePointerWrapper() {};
		virtual shared_ptr< C > getObj() = 0;
		
		const std::type_info*	pTypeInfo;
		TypeCastMap*			pTypeCastMap;
	};
	
	template< typename C > struct PointerWrapper : public BasePointerWrapper< C >
	{
		PointerWrapper( const shared_ptr< C >& pObj_ )	:
 			BasePointerWrapper< C >( pObj_.get() ),
			pObj( pObj_ )
		{
		}
		
		virtual shared_ptr< C > getObj()
		{
			return pObj;
		}

		private:
			shared_ptr< C >			pObj;
	};

	template< typename C > struct PointerMetamethods
	{
		static int Finalize( lua_State* pLS )
		{
			PointerWrapper< C >* pWrapper = static_cast< PointerWrapper< C >* >( lua_touserdata( pLS, 1 ) );			
			pWrapper->~PointerWrapper< C >();
			return 0;
		}

		static int ToString( lua_State* pLS )
		{
			PointerWrapper< C >* pWrapper = static_cast< PointerWrapper< C >* >( lua_touserdata( pLS, 1 ) );			
			std::stringstream sstr;
			sstr << "[" << class_traits< C >::FullName() << " " << pWrapper->getObj() << "]";
			lua_pushstring( pLS, sstr.str().c_str() );
			return 1;
		}
		
		static int ToStringConst( lua_State* pLS )
		{
			PointerWrapper< C >* pWrapper = static_cast< PointerWrapper< C >* >( lua_touserdata( pLS, 1 ) );			
			std::stringstream sstr;
			sstr << "[const " << class_traits< C >::FullName() << " " << pWrapper->getObj() << "]";
			lua_pushstring( pLS, sstr.str().c_str() );
			return 1;
		}
	};

	template< typename C, e_CheckingPolicy Checking >
	struct TypeConverter< shared_ptr< C >, Checking, tc_Default >
	{
		typedef PointerWrapper< C > base_wrapper_type;
		typedef PointerWrapper< C > wrapper_type;

		TypeConverter( lua_State* pLS_ ) :
			m_pLS( pLS_ ),
			m_bNeedCast( false )
		{
		}
		
		bool CheckType( int Index_ )
		{
			if( lua_isnil( m_pLS, Index_ ) )
				return true;
			
			// If we got a table and it has an "_flNativeInstance" field,
			// pop it and continue to work on that.
			StackGuard guard( m_pLS );
			if( lua_istable( m_pLS, Index_ ) )
			{
				lua_getfield( m_pLS, Index_, "_flNativeInstance" );
				Index_ = -1;
			}
			
			if( !lua_isuserdata( m_pLS, Index_ ) )
			{
				// TODO: better error reporting for that case
				TypeCheckHelpers< Checking >::ThrowTypeMismatch();
				return false;
			}

			if( !lua_getmetatable( m_pLS, Index_ ) )
			{
				TypeCheckHelpers< Checking >::ThrowTypeMismatch();
				return false;
			}

			Registry< C* >::PushMetatable( m_pLS );
		
			bool bRes = lua_rawequal( m_pLS, -1, -2 );	
			lua_pop( m_pLS, 2 );
			if( bRes )
				return true;

			base_wrapper_type* pWrapper = static_cast< base_wrapper_type* >( lua_touserdata( m_pLS, Index_ ) );

		//	std::cout << "looking in typecast map @" << pWrapper->pTypeCastMap
			//	<< " for conversion from " << pWrapper->pTypeInfo->name() << " to "
				//<< class_traits< C >::FullName() << "\n";

			m_typecast_it = pWrapper->pTypeCastMap->find( typeid( C ) );

			if( m_typecast_it == pWrapper->pTypeCastMap->end() )
			{
				//std::cout << " -> FAIL\n";
				TypeCheckHelpers< Checking >::ThrowTypeMismatch();
				return false;
			}

			//std::cout << " -> WIN\n";
			m_bNeedCast = true;
			return true;
		}
		
		bool ConvertFromLua( int Index_, shared_ptr< C >& Dest_ )
		{
			if( lua_isnil( m_pLS, Index_ ) )
			{
				Dest_.reset();
				return true;
			}
			
			if( !CheckType( Index_ ) )
				return false;
			
			// If we got a table and it has an "_flNativeInstance" field,
			// pop it and continue to work on that.
			// TODO: this duplicates the work of CheckType above, but I
			// don't have an easy way to cache the result unless I make
			// a special version of checktype to be called from
			// here. Not bothering right now.
			StackGuard guard( m_pLS );

			int TableIndex = Index_;
			if( lua_istable( m_pLS, Index_ ) )
			{
				lua_getfield( m_pLS, Index_, "_flNativeInstance" );
				Index_ = -1;
			}

			base_wrapper_type* pWrapper = static_cast< base_wrapper_type* >( lua_touserdata( m_pLS, Index_ ) );

			if( !m_bNeedCast )
				Dest_ = pWrapper->getObj();
			else
				// We need to cast.
				Dest_ = static_pointer_cast< typename remove_cv< C >::type >( m_typecast_it->second( pWrapper->getObj() ) );

			return true;
		}
	
		void ConvertToLua( const shared_ptr< C >& Ptr_ ) const
		{
			if( !flags::Scriptable< C >::value )
				return;
			
			if( !Ptr_ )
				lua_pushnil( m_pLS );
			else
			{
				void* pWrapper = lua_newuserdata( m_pLS, sizeof( wrapper_type ) );
				if( !pWrapper )
					throw std::bad_alloc();
				
				new( pWrapper ) wrapper_type( Ptr_ );

				Registry< C* >::PushMetatable( m_pLS, Ptr_.get() );
		
				lua_setmetatable( m_pLS, -2 );
			}
		}
			
		lua_State*	m_pLS;
		TypeCastMap::const_iterator m_typecast_it;
		bool		m_bNeedCast;
	};
	
	template< typename C >
	struct TypeConverter< shared_ptr< C >, cp_NoCheck, tc_Default >
	{
		typedef BasePointerWrapper< C > wrapper_type;

		TypeConverter( lua_State* pLS_ ) :
			m_pLS( pLS_ )
		{
		}
		
		bool CheckType( int Index_ )
		{
			return true;
		}
		
		bool ConvertFromLua( int Index_, shared_ptr< C >& Dest_ )
		{
			if( lua_isnil( m_pLS, Index_ ) )
			{
				Dest_.reset();
				return true;
			}
			
			// If we got a table and it has an "_flNativeInstance" field,
			// pop it and continue to work on that.
			StackGuard guard( m_pLS );
			bool bSubclassWrapper = false;
			int TableIndex = Index_;
			if( lua_istable( m_pLS, Index_ ) )
			{
				lua_getfield( m_pLS, Index_, "_flNativeInstance" );
				Index_ = -1;
				bSubclassWrapper = true;
			}
			
			wrapper_type* pWrapper = static_cast< wrapper_type* >( lua_touserdata( m_pLS, Index_ ) );
			
			lua_getmetatable( m_pLS, Index_ ) ;
			Registry< C* >::PushMetatable( m_pLS );

			bool bRes = lua_rawequal( m_pLS, -1, -2 );	
			lua_pop( m_pLS, 2 );
			if( bRes )
				Dest_ = pWrapper->getObj();
			else
			{
				// We need to cast.
				
				//std::cout << "looking for cast to type " << typeid( C ).name() << " in cast map " << pWrapper->pTypeCastMap << std::endl;
				TypeCastMap::const_iterator it = pWrapper->pTypeCastMap->find( typeid( C ) );				
				Dest_ = static_pointer_cast< C >( it->second( pWrapper->getObj() ) );
			}
			
			return true;
		}
			
		lua_State*	m_pLS;
	};
}}

#endif
