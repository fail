/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_LUA_TYPECHECK_H
#define FAIL_LUA_TYPECHECK_H

namespace fail { namespace LuaImpl
{
	enum e_CheckingPolicy
	{
		cp_NoCheck,
  		cp_NoThrow,
		cp_Throw
	};

	template< e_CheckingPolicy CheckPolicy > struct TypeCheckHelpers
	{
		static void ThrowTypeMismatch( int Got_ = LUA_TNONE, int Expected_ = LUA_TNONE )
		{
		}
		
		static bool CheckType( lua_State* pLS, int Index_, int ExpectedType_ )
		{
			return true;
		}
	};
	
	template<> struct TypeCheckHelpers< cp_Throw >
	{
		static void ThrowTypeMismatch( int Got_ = LUA_TNONE, int Expected_ = LUA_TNONE )
		{
			throw Error::TypeMismatch( Got_, Expected_ );
		}
		
		static bool CheckType( lua_State* pLS, int Index_, int ExpectedType_ )
		{
			int type = lua_type( pLS, Index_ );
			if( type != ExpectedType_ )
				throw Error::TypeMismatch( type, ExpectedType_ );
			
			return true;
		}
	};
	
	template<> struct TypeCheckHelpers< cp_NoThrow >
	{
		static void ThrowTypeMismatch( int Got_ = LUA_TNONE, int Expected_ = LUA_TNONE )
		{
		}
		
		static bool CheckType( lua_State* pLS, int Index_, int ExpectedType_ )
		{
			int type = lua_type( pLS, Index_ );
			return type == ExpectedType_;
		}
	};
}}

#endif
