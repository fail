/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_LUA_ATTRIBUTE_H
#define FAIL_LUA_ATTRIBUTE_H

namespace fail { namespace LuaImpl
{
	class AttributeWrapper_i
	{
		public:
			virtual ~AttributeWrapper_i() {}
			
			// Push the value for the attribute of the object whose pointer wrapper
			// is found at index 1 of the stack.
			virtual void get( lua_State* pLS ) const = 0;
			
			// Set the attribute for the object found at index 1 to the value
			// found at index 3 of the stack.
			virtual void set( lua_State* pLS ) const = 0;
	};

	template< class C > struct AttributeTable
	{
		typedef map< string, shared_ptr< const AttributeWrapper_i > > map_type;
		static map_type	Attributes;
			
		static void add( const char* pName_, AttributeWrapper_i* pWrapper_ )
		{
			Attributes.insert( make_pair( pName_, pWrapper_ ) );
		}
		
		static shared_ptr< const AttributeWrapper_i > get( const char* pName_ )
		{
			map_type::const_iterator it = Attributes.find( pName_ );
			if( it == Attributes.end() )
				return shared_ptr< const AttributeWrapper_i >();
			
			return it->second;
		}

		static int Index( lua_State* pLS )
		{							
			shared_ptr< const AttributeWrapper_i > pAttrWrapper = get( luaL_checkstring( pLS, 2 ) );
			
			try
			{
				if( pAttrWrapper )
					pAttrWrapper->get( pLS );
				else
				{
					// Not found, just do a lua_get on the metatable. This will cover other cases:
					// method calls and later inheritance (by setting the parent class metatable
					// as our metatable's metatable)
					lua_getmetatable( pLS, 1 );
					lua_pushvalue( pLS, 2 );
					lua_gettable( pLS, -2 );
				}
			}
			catch( const std::exception& e )
			{
				luaL_error( pLS, e.what() );
			}
				
			return 1;
		}
		
		static int NewIndex( lua_State* pLS )
		{
			const char* pName = luaL_checkstring( pLS, 2 );
			shared_ptr< const AttributeWrapper_i > pAttrWrapper = get( pName );
			
			try
			{
				if( !pAttrWrapper )
					throw Error::NoSuchAttribute( pName );

				pAttrWrapper->set( pLS );
			}
			catch( const Error::Base& error )
			{
				error.ConvertToLua( pLS );
			}
			catch( const std::exception& e )
			{
				return luaL_error( pLS, e.what() );
			}
			
			return 0;
		}
	};

	template< class C > typename AttributeTable< C >::map_type AttributeTable< C >::Attributes;
}}

#endif
