/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_LUA_IMPLEMENTATION_H
#define FAIL_LUA_IMPLEMENTATION_H

#include "core/core.h"

#include <sstream>
#include <map>

#include "lua.h"
#include "state.h"
#include "error.h"

#include "registry.h"
//#include "typecastmap.h"
#include "typecheck.h"
#include "basic_types.h"
#include "pointer.h"
//#include "constpointer.h"
#include "enum.h"
#include "structboxing.h"

#include "attribute.h"
#include "method.h"
#include "signalref.h"
#include "signalrefindex.h"
#include "signalattr.h"
#include "signalconnect.h"

#include "subclassing.h"

#include "class.h"
#include "struct.h"
#include "module.h"

// TODO: missing stuff:
//	- accessing STL containers from lua
//	- Extending classes with virtual function in lua in such a way that they can be transparently
//	  invoked through their base abstract class from C++ (requires FAILIT to generate a generic
//	  template-based proxy class for each abstract class)
//	- Wrapping exception classes and allowing exceptions to be catched from lua scripts

namespace fail
{
	template< typename ModuleTag > void LuaSetup( bool bMarkPackageAsLoaded = true )
	{
		lua_State* pLS = LuaState::GetInstance()->get();
		LuaImpl::StackGuard guard( pLS );

		LuaImpl::ModuleVisitor v( pLS );
		module_traits< ModuleTag >::VisitQualifiedName( v );
		
		// Setup the package.loaded entry for the module, if
		// requested by the caller.
		// Asking it not to be set is useful if the module
		// includes a lua part: it will let it be loaded and
		// setup at the first require for that module.
		if( bMarkPackageAsLoaded )
		{
			lua_getglobal( pLS, "package" );
			lua_getfield( pLS, -1, "loaded" );
			lua_pushstring( pLS, v.FullModuleName.c_str() );
			lua_pushvalue( pLS, -4 );
			lua_settable( pLS, -3 );
			lua_pop( pLS, 2 );
		}
		
		module_traits< ModuleTag >::VisitClasses( v );
	}
}

#endif
