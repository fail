/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_OBJDB_CONTROLLER_H_
#define FAIL_OBJDB_CONTROLLER_H_

#include "interface.h"

namespace fail { namespace objdb
{
	class Database;
	
	// This singleton will be the main object that users of the database will interact with.
	// It manages overlays and is able to fetch objects among all currently defined overlays.
	//
	// It also manage an index of bundles currently loaded in memory, along with their uuids.
	//
	// It also manage a queue of bundles scheduled for storage in the database.
	// Persistent objects adds their bundles to this queue when they are dispose()ed by smart pointers.
	// Eventually, writing back objects will have to be done by a separate thread, but the first
	// iteration will not be for easier debugging.
	class FLSERVICES_OBJDB_EXPORT Controller : public ObjectStorage_i
	{
		friend class Database;
		
		public:
			static Pointer< Controller > GetInstance()
			{
				if( !ms_pInstance )
					ms_pInstance = new Controller;
				return ms_pInstance;
			}
			
			void makePersistent( Persistable* pPersistable );
			virtual void addToWriteQueue( const Persistable* pObj );
			
			void processWriteQueue();

			void writeBundle( Bundle* pBundle );
			GenericPointer readBundle( const uuid& uuid_ );
			GenericPointer readBundle( const std::string& Name );
			
		private:
			Controller();
			
			void setWriteDB( Database* pDB )
			{
				// TODO: shall the old write database be reopened as the top readonly layer, or
				// let the application code take care of it?
				
				// TODO: this should be protected by a mutex when I get around to multithread this stuff.
				m_pWriteDB = pDB;
			}
			
			void addNewReadOnlyOverlay( Database* pDB )
			{
				// TODO: this should be protected by a mutex when I get around to multithread this stuff.
				m_Overlays.push_front( pDB );
			}
			
			typedef std::list< Pointer< Database > > Overlays;
			
			static Pointer< Controller > ms_pInstance;
			
			// The list of overlays to fetch objects from
			Overlays 			m_Overlays;
			
			// The database where new objects, and changes to existing objects will be written.
			// Typically this should also be the first overlay. I can't think of anything useful
			// that could be done by not having this in the overlays.
			Pointer< Database >	m_pWriteDB;
			
			// The write queue
			typedef std::queue< Pointer< Bundle > >	BundleQueue;
			BundleQueue	m_WriteQueue;
			
			// The bundle index
			typedef std::map< uuid, GenericPointer > BundleIndex;
			BundleIndex	m_BundleIndex;
	};
}}

#endif
