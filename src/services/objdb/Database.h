/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_OBJDB_DATABASE_H_
#define FAIL_OBJDB_DATABASE_H_

#include "core/core.h"
#include "services/objdb/services-objdb_export.h"
#include <sqlite3.h>

namespace fail { namespace objdb
{
	// This represent one database overlay. The database, globally, will consist of one or more of those.
	// For instance, when playing the game there could be one with the base game data, one or more containing
	// mods that will override some objects from the base game (and supply new ones),
	// and the game save themselves will probably be implemented as database overlays that will contain
	// whatever changes were made to from the initial database (as well as new objects that may have been
	// created dynamically during the game)
	//
	// While database fetching will happen from several, overlaid databases, writing will occur only in
	// one database overlay that will accumulate the changes. To take the example of the game again,
	// this would be a temporary copy of the last loaded game save (or an empty database when creating a new game),
	// that will accumulate the changes done by the player. If the player request to save his game,
	// all dirty objects in memory will be flushed, the write database overlay closed, and then it will be copied
	// as a new game save file, then reopened to resume the game and continue storing changes.
	class FLSERVICES_OBJDB_EXPORT Database : public RefCounted
	{
		public:
			// Factory functions
			static Pointer< Database > OpenRead( const std::string& Filename );
			static Pointer< Database > OpenReadWrite( const std::string& Filename );
			static Pointer< Database > Create( const std::string& Filename );
			static Pointer< Database > OpenOrCreate( const std::string& Filename );

			void writeBundle( Bundle* pBundle );
			GenericPointer readBundle( const uuid& uuid_ );
			uuid lookupBundle( const std::string& Name );
			
		private:
			Database( sqlite3* pSQLiteDB, bool bCreateTables = false );
			~Database();
			void createTables();
			
			sqlite3*		m_pSQLiteDB;
			sqlite3_stmt*	m_pWriteBundleStmt;
			sqlite3_stmt*	m_pWriteBundleNameStmt;
			sqlite3_stmt*	m_pBeginTxn;
			sqlite3_stmt*	m_pCommitTxn;
			sqlite3_stmt*	m_pBundleInfoStmt;
			sqlite3_stmt*	m_pGetBundleFromNameStmt;
	};
}}

#endif
