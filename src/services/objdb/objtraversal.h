/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_OBJDB_OBJTRAVERSAL_H_
#define FAIL_OBJDB_OBJTRAVERSAL_H_

// This is some utility code that traverse a graph of objects by using the reflection templates
// to go through pointers and call a functor on each object.
// It ignores weak pointers, only hard references are considered.
// It is assumed that no loop occur in the object graph, which would be problematic for a number of other reasons anyway.
// Non storable objects/attributes are ignored.

namespace fail { namespace objdb
{
	// This functor processes one attribute. The default implementation does nothing,
	// and some specialization are implemented only for the kind of attributes that we're interested
	// in: storable pointers, and storable containers containing other containers or pointers.
	//
	// TODO: implement containers.
	template< class C, class Functor, typename AttrTag,
 			  bool bStorable = flags::Storable< AttrTag >::value,
 			  typename Category = attribute_traits< C, AttrTag >::category,
	  		  typename Type = attribute_traits< C, AttrTag >::type >
	struct ObjectTraversalAttrHandler
	{
		ObjectTraversalAttrHandler( const C* pSrcObj, Functor& func ) {}
		void operator()() const {}
	};
	
	template< class C, class Functor, class Target, typename AttrTag >
	struct ObjectTraversalAttrHandler< C, AttrTag, true, NormalType, Pointer< Target > >
	{
		ObjectTraversalAttrHandler( const C* pSrcObj, Functor& Func ) :
			m_pSrcObj( pSrcObj ),
			m_Func( func )
		{}

		void operator()() const
		{
			const Target* pTarget = m_pSrcObj->*attribute_traits< C, AttrTag >::MemberPointer();
			if( !pTarget )
				return;
			
			m_Func( pSrcObj, pTarget );
		}
		
		const C* m_pSrcObj;
		Functor& m_Func;
	};

	template< class C, class Functor > struct ObjectTraversal
	{
		struct Visitor
		{
			Visitor( const C* pObj, Functor& func ) : m_pObj( pObj ) {}
			
			template< class SC > void declSuperClass()
			{
				class_traits< SC >::template VisitSuperClasses( *this );
				class_traits< SC >::template VisitAttributes( *this );
			}
			
			template< typename AttrTag > void declAttribute()
			{
				ObjectTraversalAttrHandler< C, AttrTag > attrhandler( m_pObj, m_Func );
				attrhandler();
			}
			
			const C*	m_pObj;
			Functor&	m_Func;
		};
		
		static void Traverse( const C* pObj, Functor& func )
		{
			Visitor v( pObj, func );
			class_traits< C >::template VisitSuperClasses( v );
			class_traits< C >::template VisitAttributes( v );
		}
	};
}}

#endif
