/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_OBJDB_IMPLEMENTATION_H
#define FAIL_OBJDB_IMPLEMENTATION_H

#include "interface.h"
/*
#include "schema-digest/digest.h"
#include "write-context-fwd.h"
#include "read-context-fwd.h"
#include "basic_types.h"
#include "stl_types.h"
#include "dynamicbuffer.h"
#include "struct.h"
#include "registry.h"
#include "pointer.h"

#include "writer-header.h"
#include "writer-DGST.h"
#include "writer-OBJS.h"

//#include "registry.h"
#include "writer-save.h"

#include "reader-header.h"
#include "reader-DGST.h"
#include "reader-OBJS.h"

#include "write-context.h"
#include "read-context.h"

#include "class.h"
*/

namespace fail { namespace objdb
{
	/*namespace Impl
	{
		struct SetupVisitor
		{
			SetupVisitor()
			{
			}
			
			template< class C > void Class()
			{
				ClassStuff< C >::Setup();
			}
			
			template< class S > void Struct()
			{
			}
		};
	}*/

	template< typename ModuleTag > void Setup()
	{	
		//std::cout << "module " << module_traits< ModuleTag >::FullName() <<
		//		" schema's digest: 0x" << std::hex << SchemaDigest< ModuleTag >::digest << std::endl;

	//	abf::Impl::Digests.insert( SchemaDigest< ModuleTag >::digest );
		
	//	Impl::SetupVisitor v;
	//	module_traits< ModuleTag >::VisitClasses( v );
	}
}}

#endif
