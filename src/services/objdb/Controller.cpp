/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "services/objdb/interface.h"

using namespace fail;
using namespace fail::objdb;

Pointer< Controller >	Controller::ms_pInstance = NULL;
/*Controller::Overlays 	Controller::ms_Overlays;
Pointer< Database >		Controller::ms_pWriteDB = NULL;*/

Controller::Controller()
{
}

void Controller::makePersistent( Persistable* pPersistable )
{
	pPersistable->createBundle();
	pPersistable->setStorage( this );
	m_BundleIndex[ pPersistable->getpBundle()->getUUID() ] = pPersistable;
}

void Controller::addToWriteQueue( const Persistable* pObj )
{
	// TODO: add a mutex
	m_WriteQueue.push( pObj->getpBundle() );
}

void Controller::processWriteQueue()
{	
	// TODO: locking (or maybe find and use some lock free container implementation?)
	while( !m_WriteQueue.empty() )
	{
		Pointer< Bundle > pBundle = m_WriteQueue.front();
		writeBundle( pBundle );
		m_WriteQueue.pop();
		m_BundleIndex.erase( pBundle->getUUID() );
	}
}

void Controller::writeBundle( Bundle* pBundle )
{
	m_pWriteDB->writeBundle( pBundle );
}

GenericPointer Controller::readBundle( const uuid& uuid_ )
{
	BundleIndex::const_iterator it = m_BundleIndex.find( uuid_ );
	
	if( it != m_BundleIndex.end() )
		return it->second;
	
	// TODO: do this right (go through each overlay in turn until
	// we find one that knows the bundle we want to load)
	GenericPointer pObj = m_Overlays.front()->readBundle( uuid_ );
	m_BundleIndex.insert( std::make_pair( uuid_, pObj ) );

	return pObj;
}

GenericPointer Controller::readBundle( const std::string& Name )
{
	// TODO: do this right (go through each overlay in turn until
	// we find one that knows the bundle we want to load)
	uuid bundle_uuid = m_Overlays.front()->lookupBundle( Name );
	return readBundle( bundle_uuid );
}
