/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_OBJDB_BUNDLESPLIT_H_
#define FAIL_OBJDB_BUNDLESPLIT_H_

// This contains the code that, starting from an object of a known type, goes through every pointer in it
// and splits them out in separate bundles if necessary, or include them in the bundle of the object that
// points to them.
// It will of course fail horribly in case of circular references not using weak pointers.

namespace fail { namespace objdb
{
	// This functor processes one attribute that points from one object to another,
	// and systematically move the target object into the provided bundle.
	// It's used for the first step of splitting out an object into its own bundle:
	// We move all objects it refers to directly or indirectly into the new bundle,
	// then perform the second step on the originating bundle, where every object
	// still pointed from it but no longer in he same bundle are split into their
	// new own bundles. This way we ensure that all objects refered only from the
	// newly split out object will be moved into this object's bundle and that
	// we'll only create new bundles for objects that are shared by several other bundles.
	struct MoveToNewBundle
	{
		MoveToNewBundle() {}
		
		template< class Source, class Target > void operator( const C* pSource, const Target* pTarget )()
		{
			pTarget->m_pBundle = pSource->m_pBundle;
			pTarget->m_Flags = Persistable::f_Dirty;
		}
	};
	
	// This functor does the following:
	//   - If the target object is not persistent yet, it moves it into
	//     the source object's bundle.
	//   - If the target object is a non-root object that belongs to a different bundle,
	//	   create a new bundle and perform a (recursive) bundle split from that object.
	//
	// It's used as part of the bundle splitting algorithm.
	struct SplitSubBundles
	{
		SplitSubBundles() {}
		
		template< class Source, class Target > void operator( const C* pSource, const Target* pTarget )()
		{
			if( !pTarget->isPersistent() )
			{
				// If the target is not persistent yet, include it in the same bundle as the source
				// object.
				pTarget->m_pBundle = pSource->m_pBundle;
				pTarget->m_Flags = Persistable::f_Dirty;
			}
			else if( pTarget->m_pBundle != pSource->m_pBundle
					&& !pTarget->isRoot() )
			{
				// We're pointing to a non-root object that is part of another bundle,
				// so this object needs to be split out in its own bundle.
				// First however, we make sure that the original bundle of this object is loaded,
				// and immediately discard the reference to its root object, to force the original
				// bundle to be rewritten with a reference to the bundle of the newly split
				// object.
				
				////////////Pointer< Persistable > pOriginalOwner = Controller::Load( 

				// Do bundle splitting on the target
				BundleSplitter< Target >::SplitToOwnBundle( pTarget );
			}
		}
	};

	template< class C > struct BundleSplitter
	{
		struct Visitor
		{
			Visitor( const C* pObj ) : m_pObj( pObj ) {}
			
			template< class SC > void declSuperClass()
			{
				class_traits< SC >::template VisitSuperClasses( *this );
				class_traits< SC >::template VisitAttributes( *this );
			}
			
			template< typename AttrTag > void declAttribute()
			{
				SplitAttrHandler< C, AttrTag > attrhandler;
				attrhandler();
			}
			
			const C* m_pObj;
		};
		
		static void SplitSubBundles( const C* pObj )
		{
			// To be called on every object about to be written back to the database
			// to normalize all the bundling stuff properly.
			// TODO: it should probably take a bundle as input and use the type_info
			// to find out the proper type-dependant function to perform that.
		}
		
		static void SplitToOwnBundle( const C* pObj )
		{
			Persistable* pTarget = static_cast< Persistable* >( pObj );
			Pointer< Bundle > pOriginalBundle = pTarget->m_pBundle;
			
			pTarget->m_pBundle = new Bundle( pTarget );
			pTarget->m_Flags = Persistable::f_Root | Persistable::f_Dirty;
			
			MoveToNewBundle move_func;
			ObjectTraversal::Traverse< C, MoveToNewBundle >( pTarget, move_func );
			
			if( pOriginalBundle )
			{
				SplitSubBundles split_func;
				
				// TODO: we need to use the original's bundle type_info to find a pointer to the
				// type-specific code to do that, which will have to be registered into
				// some map somewhere. It'll also have to be used to deal with pointers to
				// persistable polymorphic classes when traversing objects.
				//ObjectTraversal::Traverse< C, SplitSubBundles >( pOriginalBundle->m_pRootObject, split_func );
			}
		}
	};
}}

#endif
