/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "services/objdb/interface.h"
#include "services/fbf/interface.h"
#include "io/MemoryOutputStream.h"
#include <physfs.h>

using namespace fail;
using namespace fail::objdb;

Pointer< Database > Database::OpenRead( const std::string& Filename )
{
	try
	{	 
		const char* pPath = PHYSFS_getRealDir( Filename.c_str() );

		std::string fullname( pPath );
		fullname += '/';
		fullname += Filename;
	
		sqlite3* pSQLiteDB;
		if( sqlite3_open_v2( fullname.c_str(), &pSQLiteDB, SQLITE_OPEN_READONLY, NULL ) != SQLITE_OK )
			return NULL;
		
		Pointer< Database > pDB = new Database( pSQLiteDB );
		
		// Since this is a read only database, it becomes the new top read only overlay.
		Controller* pController = Controller::GetInstance();
		pController->addNewReadOnlyOverlay( pDB );
		
		return pDB;
	}
	catch(...)
	{
		// TODO: this is some temporary, lazy and stupid error handling.
		// I'll take care of cleaning those things once I get the objdb up and
		// running.
		return NULL;
	}
}

Pointer< Database > Database::OpenReadWrite( const std::string& Filename )
{
	try
	{	 
		const char* pPath = PHYSFS_getRealDir( Filename.c_str() );

		std::string fullname( pPath );
		fullname += '/';
		fullname += Filename;
	
		sqlite3* pSQLiteDB;
		if( sqlite3_open_v2( fullname.c_str(), &pSQLiteDB, SQLITE_OPEN_READWRITE, NULL ) != SQLITE_OK )
			return NULL;


		Pointer< Database > pDB = new Database( pSQLiteDB );
		
		// Since this is a read/write database, it is meant to become the new write database.
		Controller* pController = Controller::GetInstance();
		pController->setWriteDB( pDB );
		
		return pDB;
	}
	catch(...)
	{
		// TODO: this is some temporary, lazy and stupid error handling.
		// I'll take care of cleaning those things once I get the objdb up and
		// running.
		return NULL;
	}
}

Pointer< Database > Database::Create( const std::string& Filename )
{
	try
	{
		const char* pPath = PHYSFS_getWriteDir( );

		std::string fullname( pPath );
		fullname += '/';
		fullname += Filename;
	
		sqlite3* pSQLiteDB;
		
		if( sqlite3_open_v2( fullname.c_str(), &pSQLiteDB, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL ) != SQLITE_OK )
			return NULL;
		
		Pointer< Database > pDB = new Database( pSQLiteDB, true );
		
		// Since this is a read/write database, it is meant to become the new write database.
		Controller* pController = Controller::GetInstance();
		pController->setWriteDB( pDB );
		
		return pDB;
	}
	catch(...)
	{
		// TODO: this is some temporary, lazy and stupid error handling.
		// I'll take care of cleaning those things once I get the objdb up and
		// running.
		return NULL;
	}
}

Pointer< Database > Database::OpenOrCreate( const std::string& Filename )
{
	Pointer< Database > pDB = OpenReadWrite( Filename );
	
	if( !pDB )
		pDB = Create( Filename );
	
	return pDB;
}


Database::Database( sqlite3* pSQLiteDB, bool bCreateTables ) :
	m_pSQLiteDB( pSQLiteDB )
{
	if( bCreateTables )
		createTables();
	
	// TODO: error handling
	sqlite3_prepare_v2( m_pSQLiteDB,

						"insert or replace into 'bundles'( "
						"'uuid', 'endianess', 'digests_len', 'objects_len', "
						"'digests', 'objects' ) "
						"values( ?1, ?2, ?3, ?4, ?5, ?6 );",

	  					-1, &m_pWriteBundleStmt, NULL );
	
	sqlite3_prepare_v2( m_pSQLiteDB,

						"insert or replace into 'directory'( 'name', 'uuid' ) "
						"values( ?2, ?1 );",

	  					-1, &m_pWriteBundleNameStmt, NULL );
	
	sqlite3_prepare_v2( m_pSQLiteDB,
						"begin transaction;",
	  					-1, &m_pBeginTxn, NULL );
	
	sqlite3_prepare_v2( m_pSQLiteDB,
						"commit transaction;",
	  					-1, &m_pCommitTxn, NULL );
	
	sqlite3_prepare_v2( m_pSQLiteDB,

						"select rowid, endianess, digests_len, objects_len "
						"from 'bundles' where uuid = ?1;",

	  					-1, &m_pBundleInfoStmt, NULL );
	
	sqlite3_prepare_v2( m_pSQLiteDB,
						"select uuid from 'directory' where name = ?1;",
	  					-1, &m_pGetBundleFromNameStmt, NULL );
}

Database::~Database()
{
	sqlite3_finalize( m_pGetBundleFromNameStmt );
	sqlite3_finalize( m_pBundleInfoStmt );
	sqlite3_finalize( m_pCommitTxn );
	sqlite3_finalize( m_pBeginTxn );
	sqlite3_finalize( m_pWriteBundleNameStmt );
	sqlite3_finalize( m_pWriteBundleStmt );

	sqlite3_close( m_pSQLiteDB );
}

void Database::createTables()
{
	sqlite3_exec( m_pSQLiteDB, 

		"pragma auto_vacuum = 1;"
		"pragma user_version = 1;"

		"create table 'bundles' ( 'uuid' blob primary key, 'endianess' integer, "
		"'digests_len' integer, 'objects_len' integer, "
		"'digests' blob, 'objects' blob );"

		"create table 'directory' ( 'name' test primary key, 'uuid' blob unique );",

		NULL, NULL, NULL );
}

// Bundle writing
namespace
{
	struct FBFWriterBackend : public fbf::WriterBackend_i
	{
		virtual void beginBundle( endianess::Endianess en_ )
		{
			en = en_;
		}
		
		virtual void endBundle() {}
		
		virtual io::OutputStream* beginDigests( uint32_t Size )
		{
			pDigests = new io::MemoryOutputStream;
			return pDigests;
		}
		
		virtual void endDigests() {}
		
		virtual io::OutputStream* beginObjects()
		{
			pObjects = new io::MemoryOutputStream;
			return pObjects;
		}
		
		virtual void endObjects() {}

		Pointer< io::MemoryOutputStream > pDigests;
		Pointer< io::MemoryOutputStream > pObjects;
		endianess::Endianess en;
	};
}

void Database::writeBundle( Bundle* pBundle )
{
	Pointer< FBFWriterBackend > pBackend = new FBFWriterBackend;
	fbf::Writer::Save( pBackend, pBundle->getRootObject() );
	
	sqlite3_step( m_pBeginTxn );
	sqlite3_reset( m_pBeginTxn );
;
	if( !pBundle->getName().empty() )
	{
		sqlite3_bind_blob( m_pWriteBundleNameStmt, 1, &pBundle->getUUID(), sizeof( uuid ), SQLITE_STATIC );
		sqlite3_bind_text( m_pWriteBundleNameStmt, 2, pBundle->getName().c_str(), pBundle->getName().size(), SQLITE_STATIC );
		sqlite3_step( m_pWriteBundleNameStmt );
		sqlite3_reset( m_pWriteBundleNameStmt );
	}
	
	sqlite3_bind_blob( m_pWriteBundleStmt, 1, &pBundle->getUUID(), sizeof( uuid ), SQLITE_STATIC );
	sqlite3_bind_int( m_pWriteBundleStmt, 2, pBackend->en );
	sqlite3_bind_int( m_pWriteBundleStmt, 3, pBackend->pDigests->getSize() );
	sqlite3_bind_int( m_pWriteBundleStmt, 4, pBackend->pObjects->getSize() );
	sqlite3_bind_blob( m_pWriteBundleStmt, 5, pBackend->pDigests->getBuffer(), pBackend->pDigests->getSize(), SQLITE_STATIC );
	sqlite3_bind_blob( m_pWriteBundleStmt, 6, pBackend->pObjects->getBuffer(), pBackend->pObjects->getSize(), SQLITE_STATIC );
	sqlite3_step( m_pWriteBundleStmt );
	sqlite3_reset( m_pWriteBundleStmt );

	sqlite3_step( m_pCommitTxn );
	sqlite3_reset( m_pCommitTxn );
}

// Bundle reading
namespace
{
	class SQLiteBlobInputStream : public io::SeekableInputStream
	{
		public:
			SQLiteBlobInputStream( sqlite3* pDB, const char* pTableName, const char* pColumnName, uint64_t rowid, uint64_t size ) :
				m_Size( size ),
				m_Pos( 0 ),
				m_EndPos( size )
			{
				sqlite3_blob_open( pDB, NULL, pTableName, pColumnName, rowid, 0, &m_pBlob );
			}
			
			virtual ~SQLiteBlobInputStream()
			{
				sqlite3_blob_close( m_pBlob );
			}
			
			virtual unsigned int read( void* pBuffer, unsigned int Size )
			{
				if( m_EndPos && m_Pos >= m_EndPos )
					return 0;

				if( m_EndPos && ( m_Pos + Size ) > m_EndPos )
					Size = m_EndPos - m_Pos;
				
				sqlite3_blob_read( m_pBlob, pBuffer, Size, m_Pos );
								
				m_Pos += Size;
				return Size;
			}

			virtual void seek( uint32_t Pos )
			{
				if( Pos > m_EndPos )
					m_Pos = m_EndPos;
				else
					m_Pos = Pos;
			}
			
			virtual void seek( uint64_t Pos )
			{
				if( Pos > m_EndPos )
					m_Pos = m_EndPos;
				else
					m_Pos = Pos;
			}
			
			virtual uint64_t tell() const
			{
				return m_Pos;
			}
			
			virtual void setReadLimit( uint64_t EndPos )
			{
				if( EndPos )
					m_EndPos = EndPos;
				else
					m_EndPos = m_Size - 1;
			}

		private:
			sqlite3_blob*	m_pBlob;
			uint32_t		m_Size;
			uint32_t		m_Pos;
			uint32_t		m_EndPos;
	};
	
	struct FBFReaderBackend : public fbf::ReaderBackend_i
	{
		FBFReaderBackend( sqlite3* pSQLiteDB, sqlite3_stmt* pBundleInfoStmt, const uuid& uuid_ ) :
			m_pSQLiteDB( pSQLiteDB ),
			m_pBundleInfoStmt( pBundleInfoStmt )
		{
			m_pController = Controller::GetInstance();
			
			sqlite3_bind_blob( m_pBundleInfoStmt, 1, &uuid_, sizeof( uuid ), SQLITE_STATIC );
			if( sqlite3_step( m_pBundleInfoStmt ) != SQLITE_ROW )
			{
				// TODO as always: proper error handling
				sqlite3_reset( m_pBundleInfoStmt );
				throw std::bad_alloc();
			}
			
			m_Rowid = sqlite3_column_int( m_pBundleInfoStmt, 0 );
			m_en = static_cast< endianess::Endianess >( sqlite3_column_int( m_pBundleInfoStmt, 1 ) );
			m_DigestsSize = sqlite3_column_int( m_pBundleInfoStmt, 2 );
			m_ObjectsSize = sqlite3_column_int( m_pBundleInfoStmt, 3 );

			sqlite3_reset( m_pBundleInfoStmt );
		}
		
		virtual endianess::Endianess beginBundle()
		{
			return m_en;
		}
		
		virtual void endBundle() {}
		
		virtual io::InputStream* beginDigests()
		{
 			m_pInputStream = new SQLiteBlobInputStream( m_pSQLiteDB, "bundles", "digests", m_Rowid, m_DigestsSize );
			return m_pInputStream;
		}
		
		virtual uint32_t getDigestsSize()
		{
			return m_DigestsSize;
		}
		
		virtual void endDigests() {}
		
		virtual io::InputStream* beginObjects()
		{
			m_pInputStream = new SQLiteBlobInputStream( m_pSQLiteDB, "bundles", "objects", m_Rowid, m_ObjectsSize );
			return m_pInputStream;
		}
		
		virtual void endObjects() {}

		virtual GenericPointer resolveUUID( const uuid UUID )
		{
			return m_pController->readBundle( UUID );
		}
		

		sqlite3*		m_pSQLiteDB;
		sqlite3_stmt*	m_pBundleInfoStmt;

		int	m_Rowid;
		endianess::Endianess m_en;
		uint32_t m_DigestsSize;
		uint32_t m_ObjectsSize;
		Pointer< SQLiteBlobInputStream > m_pInputStream;
		Controller*	m_pController;
	};
}

GenericPointer Database::readBundle( const uuid& uuid_ )
{
	Pointer< FBFReaderBackend > pBackend = new FBFReaderBackend( m_pSQLiteDB, m_pBundleInfoStmt, uuid_ );
	GenericPointer pRootObj = fbf::Reader::Load( pBackend );

	Persistable* pRootPersObj = pRootObj.dynCast< Persistable >();
	Pointer< Bundle > pBundle = new Bundle( pRootPersObj, uuid_ );
	pRootPersObj->setBundle( pBundle );

	return pRootObj;
}

uuid Database::lookupBundle( const std::string& Name )
{
	sqlite3_bind_text( m_pGetBundleFromNameStmt, 1, Name.c_str(), Name.size(), SQLITE_STATIC );
	if( sqlite3_step( m_pGetBundleFromNameStmt ) != SQLITE_ROW )
	{
		// TODO as always: proper error handling
		sqlite3_reset( m_pGetBundleFromNameStmt );
		return uuid();
	}
	
	uuid bundle_uuid;
	memcpy( &bundle_uuid, sqlite3_column_blob( m_pGetBundleFromNameStmt, 0 ), sizeof( uuid ) );
	sqlite3_reset( m_pGetBundleFromNameStmt );
	
	return bundle_uuid;
}
