/*
    Fail game engine
    Copyright 2007-2008 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBFFILE_WRITER_H
#define FAIL_FBFFILE_WRITER_H

#include "core/core.h"
#include "services/fbffile/services-fbffile_export.h"
#include "services/fbf/interface.h"
#include "io/SeekableOutputStream.h"

namespace fail { namespace fbffile
{
	class FLSERVICES_FBFFILE_EXPORT Writer : public fbf::WriterBackend_i
	{
		public:
			virtual void beginBundle( endianess::Endianess en );
			virtual void endBundle();
			
			virtual shared_ptr< io::OutputStream > beginDigests( uint32_t Size );
			virtual void endDigests();
			
			virtual shared_ptr< io::OutputStream > beginObjects();
			virtual void endObjects();
			
			// Native endianess saving functions
			template< class C > static void Save( const shared_ptr< io::SeekableOutputStream > pStream, const shared_ptr< C >& pObj )
			{
				shared_ptr< Writer > pFileWriter( new Writer( pStream ) );
				fbf::Writer::Save( pFileWriter, pObj );
			}
			
			static void Save( const shared_ptr< io::SeekableOutputStream > pStream, const shared_ptr< Serializable >& pObj )
			{
				shared_ptr< Writer > pFileWriter( new Writer( pStream ) );
				fbf::Writer::SaveSerializable( pFileWriter, pObj );
			}
			
			// Big endian saving functions
			template< class C > static void SaveBigEndian( const shared_ptr< io::SeekableOutputStream > pStream, const shared_ptr< C >& pObj )
			{
				shared_ptr< Writer > pFileWriter( new Writer( pStream ) );
				fbf::WriterBigEndian::Save( pFileWriter, pObj );
			}
			
			static void SaveBigEndian( const shared_ptr< io::SeekableOutputStream > pStream, const shared_ptr< Serializable >& pObj )
			{
				shared_ptr< Writer > pFileWriter( new Writer( pStream ) );
				fbf::WriterBigEndian::SaveSerializable( pFileWriter, pObj );
			}
			
			// Little endian saving functions
			template< class C > static void SaveLittleEndian( const shared_ptr< io::SeekableOutputStream > pStream, const shared_ptr< C >& pObj )
			{
				shared_ptr< Writer > pFileWriter( new Writer( pStream ) );
				fbf::WriterLittleEndian::Save( pFileWriter, pObj );
			}
			
			static void SaveLittleEndian( const shared_ptr< io::SeekableOutputStream > pStream, const shared_ptr< Serializable >& pObj )
			{
				shared_ptr< Writer > pFileWriter( new Writer( pStream ) );
				fbf::WriterLittleEndian::SaveSerializable( pFileWriter, pObj );
			}	
			
		private:
			Writer( shared_ptr< io::SeekableOutputStream > pStream );
			
			shared_ptr< io::SeekableOutputStream >	m_pStream;
			endianess::Endianess					m_Endianess;
			uint64_t								m_OBJSBegin;
	};
}}

#endif
