/*
    Fail game engine
    Copyright 2007-2008 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBFFILE_READER_H
#define FAIL_FBFFILE_READER_H

#include "core/core.h"
#include "services/fbffile/services-fbffile_export.h"
#include "services/fbf/interface.h"
#include "io/SeekableInputStream.h"

namespace fail { namespace fbffile
{
	class FLSERVICES_FBFFILE_EXPORT Reader : public fbf::ReaderBackend_i
	{
		public:
			virtual endianess::Endianess beginBundle();
			virtual void endBundle();
			
			virtual shared_ptr< io::InputStream > beginDigests();
			virtual uint32_t getDigestsSize();
			virtual void endDigests();
			
			virtual shared_ptr< io::InputStream > beginObjects();
			virtual void endObjects();
				
			virtual shared_ptr< Persistable > resolveUUID( const uuid UUID );
			
			static shared_ptr< Serializable > Load( const shared_ptr< io::SeekableInputStream >& pStream )
			{
				shared_ptr< Reader > pFileReader( new Reader( pStream ) );
				return fbf::Reader::Load( pFileReader );
			}

			template< class C > static shared_ptr< C > Load( const shared_ptr< io::SeekableInputStream >& pStream )
			{
				return shared_ptr< C >( Load( pStream ) );
			}
			
		private:
			Reader( shared_ptr< io::SeekableInputStream > pStream );
			
			shared_ptr< io::SeekableInputStream >	m_pStream;
			endianess::Endianess					m_Endianess;
			uint32_t								m_DigestSize;
	};
}}

#endif
