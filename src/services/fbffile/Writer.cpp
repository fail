/*
    Fail game engine
    Copyright 2007-2008 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "services/fbffile/Writer.h"

using namespace fail;
using namespace fail::fbffile;

namespace
{
	const uint16_t Version = 1;
}

Writer::Writer( shared_ptr< io::SeekableOutputStream > pStream ) :
	m_pStream( pStream ),
	m_OBJSBegin( 0 )
{
}

void Writer::beginBundle( endianess::Endianess en )
{
	m_Endianess = en;

	uint8_t header[] = { 'F', 'B', 'F', 'F',
						Version,	// Version
	  					0	// Endianess,
					 };
	header[5] = en;
	
	m_pStream->write( header, sizeof( header ) );
}

void Writer::endBundle()
{
}

shared_ptr< io::OutputStream > Writer::beginDigests( uint32_t Size )
{
	struct
	{
		uint8_t id[4];
		uint32_t size;
	} header =
	{
		{ 'D', 'G', 'S', 'T' },
  		0
	};
	
	if( m_Endianess == endianess::e_Big )
		endianess::Big::FixEndianessU32( Size );
	else
		endianess::Little::FixEndianessU32( Size );

	header.size = Size;
	m_pStream->write( &header, sizeof( header ) );
	
	return m_pStream;
}

void Writer::endDigests()
{
}

shared_ptr< io::OutputStream > Writer::beginObjects()
{
	struct
	{
		uint8_t id[4];
		uint32_t size;
	} header =
	{
		{ 'O', 'B', 'J', 'S' },
  		0
	};

	m_pStream->write( &header, sizeof( header ) );
	
	m_OBJSBegin = m_pStream->tell();
	return m_pStream;
}

void Writer::endObjects()
{
	uint32_t size = m_pStream->tell() - m_OBJSBegin;
	
	if( m_Endianess == endianess::e_Big )
		endianess::Big::FixEndianessU32( size );
	else
		endianess::Little::FixEndianessU32( size );
	
	m_pStream->seek( m_OBJSBegin - sizeof( uint32_t ) );
	m_pStream->write( &size, sizeof( size ) );
}
