/*
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBFFILE_VFSLOADER_H
#define FAIL_FBFFILE_VFSLOADER_H

#include "core/core.h"
#include "services/fbffile/services-fbffile_export.h"
#include "Reader.h"
#include "vfs/Loader_i.h"

namespace fail { namespace fbffile
{
	class FLSERVICES_FBFFILE_EXPORT VFSLoader : public vfs::Loader_i
	{
		public:
			virtual shared_ptr< Serializable > load( shared_ptr< io::SeekableInputStream > pStream );
			static void Register();
	};
}}

#endif
