/*
    Fail game engine
    Copyright 2007-2008 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "services/fbffile/Reader.h"

using namespace fail;
using namespace fail::fbffile;

namespace
{
	const uint16_t Version = 1;
}

Reader::Reader( shared_ptr< io::SeekableInputStream > pStream ) :
	m_pStream( pStream )
{
}

endianess::Endianess Reader::beginBundle()
{
 	uint8_t header[] = { 'F', 'B', 'F', 'F',
						Version,	// Version
	  					0			// Endianess,
					 };
					 
	char headerbuf[6];
	m_pStream->read( headerbuf, sizeof( headerbuf ) );
	
	if( std::memcmp( header, headerbuf, sizeof( header ) - 1 ) )
	{
		// TODO: throw an error instead of setting ourself up for
		// an epic failure trying to load the file no matter what.
		std::cout << "Bad FBFF header\n";
	}
	
	m_Endianess = static_cast< endianess::Endianess >( headerbuf[5] );
	return m_Endianess;
}

void Reader::endBundle()
{
}

shared_ptr< io::InputStream > Reader::beginDigests()
{
	struct
	{
		uint8_t id[4];
		uint32_t size;
	} header;
	
	const char dgst[] = { 'D', 'G', 'S', 'T' };
	
	m_pStream->read( &header, sizeof( header ) );
	
	while( memcmp( dgst, header.id, sizeof( dgst ) ) )
	{
		if( m_Endianess == endianess::e_Big )
			endianess::Big::FixEndianessU32( header.size );
		else
			endianess::Little::FixEndianessU32( header.size );
		
		m_pStream->seek( m_pStream->tell() + header.size );
		
		if( m_pStream->read( &header, sizeof( header ) ) != sizeof( header ) )
		{
			// TODO: throw
			std::cout << "FBFF reader: DGST chunk not found\n";
		}
	}
	
	if( m_Endianess == endianess::e_Big )
		endianess::Big::FixEndianessU32( header.size );
	else
		endianess::Little::FixEndianessU32( header.size );
	
	m_DigestSize = header.size;
	
	m_pStream->setReadLimit( m_pStream->tell() + header.size );
	return m_pStream;
}

uint32_t Reader::getDigestsSize()
{
	return m_DigestSize;
}

void Reader::endDigests()
{
	m_pStream->setReadLimit( 0 );
}

shared_ptr< io::InputStream > Reader::beginObjects()
{
	struct
	{
		uint8_t id[4];
		uint32_t size;
	} header;
	
	const char dgst[] = { 'O', 'B', 'J', 'S' };
	
	m_pStream->read( &header, sizeof( header ) );
	
	while( memcmp( dgst, header.id, sizeof( dgst ) ) )
	{
		if( m_Endianess == endianess::e_Big )
			endianess::Big::FixEndianessU32( header.size );
		else
			endianess::Little::FixEndianessU32( header.size );

		m_pStream->seek( m_pStream->tell() + header.size );
		
		if( m_pStream->read( &header, sizeof( header ) ) != sizeof( header ) )
		{
			// TODO: throw
			std::cout << "FBFF reader: OBJS chunk not found\n";
		}
	}
	
	m_pStream->setReadLimit( m_pStream->tell() + header.size );	
	return m_pStream;
}

void Reader::endObjects()
{
	m_pStream->setReadLimit( 0 );
}

shared_ptr< Persistable > Reader::resolveUUID( const uuid UUID )
{
	return shared_ptr< Persistable >();
}
