/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_PYTHON_MODULE_H
#define FAIL_PYTHON_MODULE_H

#include "core/core.h"
#include <string>

namespace fail { namespace PythonImpl
{
	struct ClassesVisitor
	{
		ClassesVisitor( PyObject* pModule_ ) : m_pModule( pModule_ ) {}

		template< class C > void Class()
		{
			if( flags::Scriptable< C >::value )
				ClassStuff< C >::Setup( m_pModule );
		}

		template< class S > void Struct()
		{
			if( flags::Scriptable< S >::value )
				StructStuff< S >::Setup( m_pModule );
		}

		PyObject* m_pModule;
	};

	// Yes, this is cutting all kind of corners and making all kind of assumptions about the modules we bind.
	//
	// This is because I just need to be able to bind a limited, specific amount of things
	// to python, and it's just for the purpose of making the blender exporter.
	//
	// This is not intended to be a proper, universal binding service like the lua one that I can use with
	// anything and that doesn't make any assumption, because I don't care about python and really want to
	// have to deal with it as little as possible.
	template< typename ModuleTag > void SetupModule( PyObject* pFailModule_ )
	{
		// Incantations to create a python sub-module, involving specifying
		// the name of the parent module again for no good reason, AND to
		// have a pointer to said module as well, not to mention telling
		// Python the name of the module to be created twice.
		// That thing is a software bureaucracy.
		std::string modname( "fail." );
		modname += module_traits< ModuleTag >::Name();

		PyObject* pModule = Py_InitModule( modname.c_str(), NULL );

		PyObject* pDict = PyModule_GetDict( pFailModule_ );
		PyDict_SetItemString( pDict, module_traits< ModuleTag >::Name(), pModule );

		// Visit the classes. It will do whatever necessary setup classes and structs need,
		// and add their type definitions to the module.
		ClassesVisitor cv( pModule );
		module_traits< ModuleTag >::VisitClasses( cv );
	}
}}

#endif
