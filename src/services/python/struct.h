/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_PYTHON_STRUCT_H
#define FAIL_PYTHON_STRUCT_H

#include "core/core.h"
#include <string>
#include <vector>

namespace fail { namespace PythonImpl
{
	template< class S, typename AttrTag,
			  e_TypeCategory cat = TypeCategory< typename attribute_traits< S, AttrTag >::type >::value,
			  bool bReadOnly = flags::ReadOnly< AttrTag >::value,
	 		  bool bUnMutable = flags::Mutable< AttrTag >::value >
	struct StructAttrGetter
	{
		static PyObject* GetAttr( PyObject* pPyObj, void* )
		{
			typedef TypeConverter< S* > obj_tc;
			S* pStruct;
			obj_tc::ConvertFromPython_nocheck( pPyObj, pStruct );
			
			typedef attribute_traits< S, AttrTag > traits;
			typedef TypeConverter< typename traits::type > tc;
	
			return tc::ConvertToPython( ( pStruct->*traits::Accessor() )() );
		}
	};
	
	template< class S, typename AttrTag >
	struct StructAttrGetter< S, AttrTag, tc_ValueType, false, true >
	{
		static PyObject* GetAttr( PyObject* pPyObj, void* );
	};
	
	// Holds all necessary static data and utility sub classes related to a structure
	template< class S > struct StructStuff
	{
		static PyTypeObject					TypeValObject;
		static PyTypeObject					TypeRefObject;
		static std::vector< PyMethodDef >	Methods;
		static std::vector< PyGetSetDef >	Attributes;

		struct BaseWrapper : public PyObject
		{
			bool bByRef;
		};
		
		// This is the python representation of instances of the structure at hand.
		struct WrapperByVal : public BaseWrapper
		{
			S wrapped_struct;
		};
		
		struct WrapperByRef : public BaseWrapper
		{
			WrapperByRef( shared_ptr< void > pOwner_, S* pStruct_ ) :
				pOwner( pOwner_ ),
				p_wrapped_struct( pStruct_ )
			{
				BaseWrapper::bByRef = true;
			}
			
			//~WrapperByRef() { std::cout << "wrapperbyref dtor\n"; }

			shared_ptr< void >	pOwner;
			S* p_wrapped_struct;
		};

		// The callcontext to be used by all struct method wrappers. The wrappers themselves
		// are actually mostly the same for structs and classes, only bits of the callcontext
		// need to be tweaked for either case.
		struct Struct_CallContext : public CallContext< S, S* >
		{
			Struct_CallContext( PyObject* pPyObj_, PyObject* pArgs_ ) :
				CallContext< S, S* >( pPyObj_, pArgs_ )
			{
			}

			template< typename... Args > void constructObject( Args&&... args_ )
			{
				WrapperByVal* pWrapper = static_cast< WrapperByVal* >( CallContext< S, S* >::m_pPyObj );
				
				pWrapper->bByRef = false;

				// Placement new of the wrapped structure.
				new( &pWrapper->wrapped_struct ) S( args_... );
			}
		};

		// The python finalizer for that type. Calls the destructor of the wrapped struct.
		static void DeleteFunc( PyObject* pPyObj )
		{
			BaseWrapper* pBaseWrapper = static_cast< BaseWrapper* >( pPyObj );
			
			if( pBaseWrapper->bByRef )
			{
				WrapperByRef* pWrapper = static_cast< WrapperByRef* >( pPyObj );
				pWrapper->~WrapperByRef();				
			}
			else
			{
				WrapperByVal* pWrapper = static_cast< WrapperByVal* >( pPyObj );

				// Call the struct's destructor.
				pWrapper->wrapped_struct.~S();
			}

			pPyObj->ob_type->tp_free( pPyObj );
		}

		template< typename AttrTag > static int SetAttr( PyObject* pPyObj, PyObject *pValue, void* )
		{
			try
			{
				typedef TypeConverter< S* > obj_tc;
				S* pStruct;
				obj_tc::ConvertFromPython_nocheck( pPyObj, pStruct );
				
				typedef attribute_traits< S, AttrTag > traits;
				typedef TypeConverter< typename traits::type > tc;
	
				typename traits::type Value;
				tc::ConvertFromPython( pValue, Value );

				( pStruct->*traits::Accessor() )() = Value;

				return 0;
			}
			catch( const Error::Base& error )
			{
				error.ConvertToPython();
				return -1;
			}
		}

		// Visitor callback used to count things to reserve buffers.
		struct CountVisitor
		{
			CountVisitor() : MethodCount( 0 ), AttributeCount( 0 ) {}

			template< typename MtdTag > void declMethod()
			{
				++MethodCount;
			}

			template< typename MtdTag > void declStaticMethod()
			{
				++MethodCount;
			}

			template< typename AttrTag > void declAttribute()
			{
				++AttributeCount;
			}

			int MethodCount;
			int AttributeCount;
		};

		// Visitor callback used to walk through all the stuff the struct has to offer, and setup the
		// necessary wrappers.
		struct SetupVisitor
		{
			void declConstructor()
			{
				TypeValObject.tp_new = &CtorWrapper< S, Struct_CallContext >::PyFunc;
			}

			template< typename MtdTag > void declMethod()
			{
				PyMethodDef def;
				def.ml_name = method_traits< S, MtdTag >::Name();
				def.ml_meth = &MethodWrapper< S, MtdTag, Struct_CallContext >::PyFunc;
				def.ml_flags = METH_VARARGS;
				def.ml_doc = NULL;
				Methods.push_back( def );
			}

			template< typename MtdTag > void declStaticMethod()
			{
				PyMethodDef def;
				def.ml_name = method_traits< S, MtdTag >::Name();
				def.ml_meth = &MethodWrapper< S, MtdTag, Struct_CallContext >::PyFunc;
				def.ml_flags = METH_VARARGS | METH_STATIC;
				def.ml_doc = NULL;
				Methods.push_back( def );
			}

			template< typename AttrTag > void declAttribute()
			{
				PyGetSetDef def;

				// "How do I use const" -- probably said at one point or another by some drooling python developer
				def.name = const_cast< char* >( attribute_traits< S, AttrTag >::Name() );
				def.get = StructAttrGetter< S, AttrTag >::GetAttr;
				def.set = SetAttr< AttrTag >;
				def.doc = NULL;
				def.closure = NULL;
				Attributes.push_back( def );
			}

			template< class SC > void declSuperClass()
			{
				class_traits< SC >::VisitSuperClasses( *this );
				class_traits< SC >::VisitMethods( *this );
				class_traits< SC >::VisitAttributes( *this );
			}
			
			// Simplistic enum implementation: enum values are jyst converted to/from integers;
			// so there's no type enforcement like in the lua bindings.
			template< typename EnumTag > void declEnum()
			{
				if( !flags::Scriptable< EnumTag >::value )
					return;
				
				enum_traits< S, EnumTag >::VisitValues( *this );
			}
			
			template< typename enum_type > void declEnumValue( const char* pName_, enum_type Value_ )
			{
				PyObject* pEnumVal = PyLong_FromLong( Value_ );
				PyDict_SetItemString( TypeValObject.tp_dict, pName_, pEnumVal );
				Py_DECREF( pEnumVal );
			}
		};

		static void Setup( PyObject* pModule_ )
		{
			// It's really appalling to have to initialize this fucking structure manually like that.
			// Did they ever hear of opaqueness? Who let those guys design their own programming language?
			TypeValObject.ob_type = &PyType_Type;
			TypeRefObject.ob_type = &PyType_Type;
			TypeValObject.tp_basicsize = sizeof( WrapperByVal );
			TypeRefObject.tp_basicsize = sizeof( WrapperByRef );

			// The name's supposed to use dots as separators but I'm lazy
			// and if Python didn't suck that much it would prepend the
			// module name automatically as needed.
			TypeValObject.tp_name = class_traits< S >::FullName();
			TypeValObject.tp_flags = Py_TPFLAGS_DEFAULT;
			TypeRefObject.tp_name = class_traits< S >::Name();
			TypeRefObject.tp_flags = Py_TPFLAGS_DEFAULT;

			TypeValObject.tp_alloc = PyType_GenericAlloc;
			TypeRefObject.tp_alloc = PyType_GenericAlloc;

			// The C standard state that all identifiers beginning with an underscore are reserved for internal
			// use by the compiler and the standard libraries. But the Python devs are so awesome that
			// they are above rules and clean coding practices.
			TypeValObject.tp_free = _PyObject_Del;
			TypeRefObject.tp_free = _PyObject_Del;

			// So, dealloc is not the opposite of alloc as one might believe at first glance. It's the opposite of new.
			// Programming languages designed by people with a poor grasp of vocabulary are always the best ones.
			TypeValObject.tp_dealloc = DeleteFunc;
			TypeRefObject.tp_dealloc = DeleteFunc;

			Py_INCREF( &TypeValObject );
			Py_INCREF( &TypeRefObject );

			// Count the methods and attributes, then reserve the required amount of entries in the respective vectors.
			CountVisitor cv;
			class_traits< S >::VisitMethods( cv );
			class_traits< S >::VisitAttributes( cv );
			Methods.reserve( cv.MethodCount + 1 );
			Attributes.reserve( cv.AttributeCount + 1 );

			SetupVisitor sv;

			// Setup the methods and attributes from the superclasses.
			class_traits< S >::VisitSuperClasses( sv );

			// Setup the constructor and static methods.
			class_traits< S >::VisitStaticMethods( sv );

			// Setup the non-static methods.
			class_traits< S >::VisitMethods( sv );

			PyMethodDef terminator = { 0, 0, 0, 0 };
			Methods.push_back( terminator );
			TypeValObject.tp_methods = &Methods[0];
			TypeRefObject.tp_methods = &Methods[0];

			// Setup attributes.
			class_traits< S >::VisitAttributes( sv );
			PyGetSetDef attr_terminator = { 0, 0, 0, 0, 0 };
			Attributes.push_back( attr_terminator );
			TypeValObject.tp_getset = &Attributes[0];			
			TypeRefObject.tp_getset = &Attributes[0];			

			PyType_Ready( &TypeValObject );
			PyType_Ready( &TypeRefObject );
			
			// Setup enums.
			class_traits< S >::VisitEnums( sv );

			// Ah, the joy of shitty C apis and their careless, braindead reliance on
			// disgracious casts.
			// I could have used a (very pretty) reinterpret_cast to directly cast
			// the typeobject into a PyObject, except that it produces a warning about
			// breaking strict aliasing rules.
			// Since I don't want to disable that useful warning for the sake of python's
			// stupidity, I found this ugly workaround.
			// It was going to be ugly either way anyway because python's developers
			// are tasteless morons.
			PyModule_AddObject( pModule_, class_traits< S >::Name(), static_cast< PyObject* >( static_cast< void* >( &TypeValObject ) ) );
		}
	};

	template< class S, typename AttrTag >
	PyObject* StructAttrGetter< S, AttrTag, tc_ValueType, false, true >::GetAttr( PyObject* pPyObj, void* )
	{		
		typedef attribute_traits< S, AttrTag > traits;
		typedef TypeConverter< typename traits::type > tc;
		
		typename StructStuff< S >::BaseWrapper* pBaseWrapper =
			static_cast< typename StructStuff< S >::BaseWrapper* >( pPyObj );
		
		if( pBaseWrapper->bByRef )
		{
			typename StructStuff< S >::WrapperByRef* pWrapper =
				static_cast< typename StructStuff< S >::WrapperByRef* >( pPyObj );
			return tc::ConvertToPythonByRef( pWrapper->pOwner,
				( pWrapper->p_wrapped_struct->*traits::Accessor() )() );
		}
		else
		{
			typename StructStuff< S >::WrapperByVal* pWrapper =
				static_cast< typename StructStuff< S >::WrapperByVal* >( pPyObj );
			return tc::ConvertToPython( ( pWrapper->wrapped_struct.*traits::Accessor() )() );
		}
	}
	
	// Because of their very convenient PyObject_HEAD_INIT macro designed to be used
	// as part of an initialisation list, I can't just clear the whole thing and get
	// it initialized properly after, forcing me to supply this ridiculous initializer
	// list full of zeros.
	template< class S > PyTypeObject StructStuff< S >::TypeValObject =
		{ PyObject_HEAD_INIT(NULL)
		  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		  
	template< class S > PyTypeObject StructStuff< S >::TypeRefObject =
		{ PyObject_HEAD_INIT(NULL)
		  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

	template< class S > std::vector< PyMethodDef > StructStuff< S >::Methods( 0 );
	template< class S > std::vector< PyGetSetDef > StructStuff< S >::Attributes( 0 );
}}

#endif
