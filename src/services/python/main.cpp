/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
	"Python enables programs to be written compactly and readably. Programs written in Python are typically
	much shorter than equivalent C, C++, or Java programs, for several reasons:

	- the high-level data types allow you to express complex operations in a single statement; 
	- statement grouping is done by indentation instead of beginning and ending brackets; 
	- no variable or argument declarations are necessary."

			-- Python tutorial, setting the stage by demonstrating that Python's creators are retarded,
			   have no idea what C++ is, and have really clever, thoughtful ideas about how to
			   make code more concise: not by improving expressiveness, but by removing brackets.
			   I'm going to one up them and create a language where blank lines and comments
			   are forbidden, so you can write even shorter programs.
*/

// This must be included first, because according to the Python documentation,
// its developers apparently never graced by such thoughts as "there has to be a better way"
// obnoxiously decided they could fuck with preprocessor definitions that affect system headers
// as they please.
// Good thing that not every library around does that.
// Yet another drop in the vast ocean of Python's shittiness.
#include <Python.h>

#include "core/module.h"
#include "io/module.h"
#include "vfs/module.h"
#include "services/fbf/interface.h"
#include "services/fbffile/module.h"
#include "services/fbffile/VFSLoader.h"
//#include "services/objdb/module.h"
#include "math/module.h"
#include "scenegraph/module.h"
#include "scenegraph/shapes/module.h"
#include "scenegraph/utils/module.h"
#include "bullet/collision/module.h"
#include "services/fbf/implementation.h"

#include "error.h"
#include "basic_types.h"
#include "method.h"
#include "struct.h"
#include "structboxing.h"
#include "typecastmap.h"
#include "class.h"
#include "pointer.h"
#include "module.h"

#include <stdexcept>

// TODO: missing stuff that will probably be needed
// 	(there is more missing stuff like signals, but it won't be needed by the blender exporter):
//	- accessing STL containers from python
//  - enums

struct GlobalInitCleanup
{
	GlobalInitCleanup()
	{
		if( !PHYSFS_init( "fail" ) )
			throw std::runtime_error( "PhysFS init failed" );
		
		PHYSFS_setWriteDir( "/" );
		PHYSFS_addToSearchPath( ".", 0 );
	}
	
	~GlobalInitCleanup()
	{
		PHYSFS_deinit();
	}
};

static GlobalInitCleanup lulz;

PyMODINIT_FUNC FAIL_EXPORT initfail()
{
	// setup FBF serialization
	fail::fbf::Setup< fail::fail_tag >();
	fail::fbf::Setup< fail::math::fail_tag >();
	fail::fbf::Setup< fail::scenegraph::fail_tag >();
	fail::fbf::Setup< fail::scenegraph::shapes::fail_tag >();
	//fail::fbf::Setup< fail::bullet::collision::fail_tag >();
	
    PyObject* pFailModule = Py_InitModule( "fail", NULL );

 	fail::PythonImpl::SetupModule< fail::fail_tag >( pFailModule );
	fail::PythonImpl::SetupModule< fail::io::fail_tag >( pFailModule );
	fail::PythonImpl::SetupModule< fail::vfs::fail_tag >( pFailModule );
	fail::PythonImpl::SetupModule< fail::fbffile::fail_tag >( pFailModule );
	//fail::PythonImpl::SetupModule< fail::objdb::fail_tag >( pFailModule );
	
 	fail::PythonImpl::SetupModule< fail::math::fail_tag >( pFailModule );
	fail::PythonImpl::SetupModule< fail::scenegraph::fail_tag >( pFailModule );
	fail::PythonImpl::SetupModule< fail::scenegraph::shapes::fail_tag >( pFailModule );
	fail::PythonImpl::SetupModule< fail::scenegraph::utils::fail_tag >( pFailModule );
	fail::PythonImpl::SetupModule< fail::bullet::collision::fail_tag >( pFailModule );

	fail::fbffile::VFSLoader::Register();
}
