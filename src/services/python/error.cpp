/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <Python.h>
#include <sstream>
#include "error.h"

void fail::PythonImpl::Error::NotEnoughParams::ConvertToPython() const
{
	std::stringstream msg;
	msg << "expected " << m_NumExpected << " params, got " << m_NumGot;
	PyErr_SetString( PyExc_RuntimeError, msg.str().c_str() );
}

void fail::PythonImpl::Error::TypeMismatch::ConvertToPython() const
{
	PyErr_SetString( PyExc_TypeError, "type mismatch" );
}

void fail::PythonImpl::Error::NoOverloadFound::ConvertToPython() const
{
	PyErr_SetString( PyExc_RuntimeError, "no matching overload found for method call" );
}
