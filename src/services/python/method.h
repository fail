/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_PYTHON_METHOD_H
#define FAIL_PYTHON_METHOD_H

namespace fail { namespace PythonImpl
{
	template< class C, typename PointerT > struct CallContext
	{
		CallContext( PyObject* pPyObj_, PyObject* pArgs_ ) :
			m_pPyObj( pPyObj_ ),
			m_pArgs( pArgs_ ),
			m_paramindex( 0 ),
			m_pResult( 0 )
		{
		}
		
		~CallContext()
		{
			if( m_pResult )
			{
				// Fucking macros and their hidden synctactic nature. Is it a bunch of statements? A sub-scope?
				// Some random fragment of code only valid in a very specific context? Apparently here it is just
				// a naked group of statements so I need to add braces that serve no apparent purpose but
				// do change the meaning of the code.
				//
				// If only the C standard provided a way to define functions that are expanded at their call site
				// that could be used instead of those shitty macros...
				// Wait, who am I kidding? Python's developers discovered "const" in 2007.
				Py_DECREF( m_pResult );
			}
		}
		
		int numParams() const
		{
			return PyTuple_GET_SIZE( m_pArgs );
		}
		
		void numExpectedParams( int Num_ ) const
		{
			// Check if we have the proper number of parameters	
			int numargs = numParams();		
			if( numargs != Num_ )
				throw Error::NotEnoughParams( numargs, Num_ );
		}
		
		C& getObject()
		{
			PointerT Ptr;							
			typedef TypeConverter< PointerT > tc;
			tc::ConvertFromPython( m_pPyObj, Ptr );
			return *Ptr;
		}
		
		template< typename T > bool getParam( const char* pName_, T& Dest_ )
		{
			typedef TypeConverter< T > tc;
			PyObject* pParam = PyTuple_GET_ITEM( m_pArgs, m_paramindex );

			if( !tc::CheckType_nothrow( pParam ) )
			{
				m_paramindex = 0;
				return false;
			}
			
			tc::ConvertFromPython_nocheck( pParam, Dest_ );
			++m_paramindex;
			return true;
		}
		
		template< typename T > void setReturnValue( const T& Value_ )
		{
			typedef TypeConverter< T > tc;
			m_pResult = tc::ConvertToPython( Value_ );
		}

		// Cheapo ownership transfer
		// This way the call context destructor knows that it should do a decref
		// on the result object if it happens to be non-null.
		PyObject* getResult()
		{
			PyObject* pRes = m_pResult;
			m_pResult = NULL;
			return pRes;
		}		

		PyObject*	m_pPyObj;
		PyObject*	m_pArgs;
		int			m_paramindex;

		private:
			PyObject*	m_pResult;
	};

	template< class C, class CC > struct CtorWrapper
	{
		static PyObject* PyFunc( PyTypeObject* pSubType, PyObject* pArgs_, PyObject* )
		{
			PyObject* pPyObj = pSubType->tp_alloc( pSubType, 0 );	// Retarded C-style fake method call

			try
			{
				CC context( pPyObj, pArgs_ );
				if( !method_traits< C, typename class_traits< C >::ctor_tag >::
					template CallWrapper< CC >::Call( context ) )
					throw Error::NoOverloadFound();

				return pPyObj;
			}
			catch( const Error::Base& error )
			{
				error.ConvertToPython();
				Py_DECREF( pPyObj );
				return 0;
			}
			catch( const std::exception& e )
			{
				PyErr_SetString( PyExc_RuntimeError, e.what() );
				Py_DECREF( pPyObj );
				return 0;				
			}
		}
	};

 	template< class C, typename MtdTag, class CC > struct MethodWrapper
	{
		static PyObject* PyFunc( PyObject* pPyObj_, PyObject* pArgs_ )
		{
			try
			{
				CC context( pPyObj_, pArgs_ );
				if( !method_traits< C, MtdTag >::template CallWrapper< CC >::Call( context ) )
					throw Error::NoOverloadFound();

				PyObject* pRes = context.getResult();
				if( pRes )
					return pRes;

				Py_RETURN_NONE;
			}
			catch( const Error::Base& error )
			{
				error.ConvertToPython();
				return 0;
			}
			catch( const std::exception& e )
			{
				PyErr_SetString( PyExc_RuntimeError, e.what() );
				return 0;				
			}
		}
	};
}}

#endif
