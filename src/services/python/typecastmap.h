/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_PYTHON_TYPECASTMAP_H_
#define FAIL_PYTHON_TYPECASTMAP_H_

namespace fail { namespace PythonImpl
{
	typedef shared_ptr< void > ( * PointerCastFunc_p )( shared_ptr< const void > );
 	typedef std::map< const PyTypeObject*, PointerCastFunc_p > TypeCastMap;
	
	template< class C > struct TypeCasts
	{
		static TypeCastMap Map;

		template< class to_type > static shared_ptr< void > PointerCastFunc( shared_ptr< const void > pPointer )
		{
			shared_ptr< C > pPtr = static_pointer_cast< C >( const_pointer_cast< void >( pPointer ) );
			return static_pointer_cast< to_type >( pPtr );
		}
	};
	
	template< class C > TypeCastMap TypeCasts< C >::Map;
}}

#endif
