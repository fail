/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_PYTHON_ERROR_H
#define FAIL_PYTHON_ERROR_H

#include <stdexcept>

namespace fail { namespace PythonImpl
{
	namespace Error
	{
		class Base
		{
			public:
				virtual ~Base() {}
				virtual void ConvertToPython() const = 0;
		};
		
		class NotEnoughParams : public Base
		{
			public:
				NotEnoughParams( int NumGot_, int NumExpected_ ) throw() :
					m_NumGot( NumGot_ ),
					m_NumExpected( NumExpected_ )
				{
				}
				
				virtual void ConvertToPython() const;

			private:
				int m_NumGot;
				int m_NumExpected;
		};
		
		class TypeMismatch : public Base
		{
			public:
				TypeMismatch() throw()
				{
				}

				virtual void ConvertToPython() const;
		};
		
		class NoOverloadFound : public Base
		{
			public:
				virtual void ConvertToPython() const;
		};
	}
}}

#endif
