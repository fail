/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_PYTHON_POINTER_H
#define FAIL_PYTHON_POINTER_H

namespace fail { namespace PythonImpl
{	
	template< class C > struct TypeConverter< shared_ptr< C >, tc_Default >
	{				
		static bool CheckType_nothrow( PyObject* pPyObj_ )
		{
			if( pPyObj_ == Py_None )
				return true;

			if( PyObject_TypeCheck( pPyObj_, &ClassStuff< C >::TypeObject ) )
				return true;
			
			const PyTypeObject* pTypeObject = pPyObj_->ob_type;
			ClassWrapper< C >* pWrapper =
				static_cast< ClassWrapper< C >* >( pPyObj_ );

			// This should be kept around to be reused by convertfrompython.
			TypeCastMap::const_iterator it = pWrapper->pTypeCastMap->find( &ClassStuff< C >::TypeObject );
			return it != pWrapper->pTypeCastMap->end();
			
		//	if( PyObject_TypeCheck( pPyObj_, &ClassStuff< C >::TypeObject ) || pPyObj_ == Py_None;
		}
		
		static void ConvertFromPython_nocheck( PyObject* pPyObj_, shared_ptr< C >& Dest_ )
		{
			if( pPyObj_ == Py_None )
				Dest_.reset();
			else
			{
				ClassWrapper< C >* pWrapper =
					static_cast< ClassWrapper< C >* >( pPyObj_ );

				if( PyObject_TypeCheck( pPyObj_, &ClassStuff< C >::TypeObject ) )
					Dest_ = pWrapper->pObj;
				else
				{
					// We need to cast.
					TypeCastMap::const_iterator it = pWrapper->pTypeCastMap->find( &ClassStuff< C >::TypeObject );
					Dest_ = static_pointer_cast< C >( it->second( pWrapper->pObj ) );
				}
			}
		}
		
		static void ConvertFromPython( PyObject* pPyObj_, shared_ptr< C >& Dest_ )
		{
			if( !CheckType_nothrow( pPyObj_ ) )
				throw Error::TypeMismatch();
			ConvertFromPython_nocheck( pPyObj_, Dest_ );
		}
		
		static PyObject* ConvertToPython( const shared_ptr< C >& Ptr_ )
		{
			if( !flags::Scriptable< C >::value )
				Py_RETURN_NONE;
			
			if( !Ptr_ )
				Py_RETURN_NONE; 

			PyObject* pPyObj;

			pPyObj = PyType_GenericAlloc( &ClassStuff< C >::TypeObject, 0 );
			if( !pPyObj )
				Py_RETURN_NONE; //throw std::bad_alloc();

			try
			{
				new( pPyObj ) ClassWrapper< C >( Ptr_ );
				return pPyObj;
			}
			catch( const Error::Base& error )
			{
				error.ConvertToPython();
				Py_DECREF( pPyObj );
				return NULL;
			}
		}
	};
}}

#endif
