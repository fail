/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_PYTHON_BASIC_TYPES_H
#define FAIL_PYTHON_BASIC_TYPES_H

namespace fail { namespace PythonImpl
{	
	template< typename T, e_TypeCategory cat = TypeCategory< T >::value > struct TypeConverter
	{
	};

	// No overflow checking for now because I'm lazy.
	struct IntegerTypeConverter
	{
		static bool CheckType_nothrow( PyObject* pPyObj_ )
		{
			return PyInt_Check( pPyObj_ ) || PyLong_Check( pPyObj_ );
		}
	};

	template< typename T > struct SignedIntegerTypeConverter : public IntegerTypeConverter
	{
		static void ConvertFromPython_nocheck( PyObject* pPyObj_, T& Dest_ )
		{
			if( PyInt_Check( pPyObj_ ) )
				Dest_ = PyInt_AS_LONG( pPyObj_ );
			else
				Dest_ = PyLong_AsLongLong( pPyObj_ );
		}

		static void ConvertFromPython( PyObject* pPyObj_, T& Dest_ )
		{
			if( !CheckType_nothrow( pPyObj_ ) )
				throw Error::TypeMismatch();

			ConvertFromPython_nocheck( pPyObj_, Dest_ );
		}

		static PyObject* ConvertToPython( T Value_ )
		{
			return PyLong_FromLongLong( Value_ );
		}
	};

	template< typename T > struct UnsignedIntegerTypeConverter : public IntegerTypeConverter
	{
		static void ConvertFromPython_nocheck( PyObject* pPyObj_, T& Dest_ )
		{
			if( PyInt_Check( pPyObj_ ) )
				Dest_ = PyInt_AS_LONG( pPyObj_ );
			else
				Dest_ = PyLong_AsUnsignedLongLong( pPyObj_ );
		}

		static void ConvertFromPython( PyObject* pPyObj_, T& Dest_ )
		{
			if( !CheckType_nothrow( pPyObj_ ) )
				throw Error::TypeMismatch();

			ConvertFromPython_nocheck( pPyObj_, Dest_ );
		}

		static PyObject* ConvertToPython( T Value_ )
		{
			return PyLong_FromUnsignedLongLong( Value_ );
		}
	};

	template<> struct TypeConverter< uint8_t, tc_Default > : public UnsignedIntegerTypeConverter< uint8_t > {};
	template<> struct TypeConverter< int8_t, tc_Default > : public SignedIntegerTypeConverter< int8_t > {};

	template<> struct TypeConverter< uint16_t, tc_Default > : public UnsignedIntegerTypeConverter< uint16_t > {};
	template<> struct TypeConverter< int16_t, tc_Default > : public SignedIntegerTypeConverter< int16_t > {};

	template<> struct TypeConverter< uint32_t, tc_Default > : public UnsignedIntegerTypeConverter< uint32_t > {};
	template<> struct TypeConverter< int32_t, tc_Default > : public SignedIntegerTypeConverter< int32_t > {};

	template<> struct TypeConverter< uint64_t, tc_Default > : public UnsignedIntegerTypeConverter< uint64_t > {};
	template<> struct TypeConverter< int64_t, tc_Default > : public SignedIntegerTypeConverter< int64_t > {};
	
	template< typename enum_type > struct TypeConverter< enum_type, tc_Enum > :
		public SignedIntegerTypeConverter< int32_t >
	{
		typedef SignedIntegerTypeConverter< int32_t > base;
		
		static void ConvertFromPython_nocheck( PyObject* pPyObj_, enum_type& Dest_ )
		{
			int32_t val;
			base::ConvertFromPython_nocheck( pPyObj_, val );
			Dest_ = static_cast< enum_type >( val );
		}
		
		static void ConvertFromPython( PyObject* pPyObj_, enum_type& Dest_ )
		{
			int32_t val;
			base::ConvertFromPython( pPyObj_, val );
			Dest_ = static_cast< enum_type >( val );
		}
		
		static PyObject* ConvertToPython( enum_type Value_ )
		{
			return base::ConvertToPython( Value_ );
		}
	};

	// float
	template<> struct TypeConverter< float, tc_Default >
	{
		static bool CheckType_nothrow( PyObject* pPyObj_ )
		{
			return !!PyFloat_Check( pPyObj_ );
		}

		static void ConvertFromPython_nocheck( PyObject* pPyObj_, float& Dest_ )
		{
			Dest_ = PyFloat_AS_DOUBLE( pPyObj_ );
		}

		static void ConvertFromPython( PyObject* pPyObj_, float& Dest_ )
		{
			if( !CheckType_nothrow( pPyObj_ ) )
				throw Error::TypeMismatch();

			ConvertFromPython_nocheck( pPyObj_, Dest_ );
		}

		static PyObject* ConvertToPython( float Value_ )
		{
			return PyFloat_FromDouble( Value_ );
		}
	};

	// bool
	template<> struct TypeConverter< bool, tc_Default >
	{
		static bool CheckType_nothrow( PyObject* pPyObj_ )
		{
			return !!PyBool_Check( pPyObj_ );
		}

		static void ConvertFromPython_nocheck( PyObject* pPyObj_, bool& Dest_ )
		{
			// FOR FUCKS SAKE
			// Every single python developer is a fucking brainless eat-shitting moron
			// "warning: dereferencing type-punned pointer will break strict-aliasing rules"
			// because those shitty Py_True/Py_False macros do retarded casting, and there is
			// of course no way to test a boolean value without using these.
			//
			// And of course, it all comes from the incredibly stupid decision of python
			// to implement numeric values as objects, and share their instances. So instead of
			// carrying around a boolean value (1 byte), they lug around a pointer and need to
			// update reference counters for those numeric values. And they have to be dereferenced
			// anytime the actual numeric value is needed.
			//
			// So they make the common use cases slower for the sake of the rare use case
			// where you need to consider an integer as an object. They could have implemented
			// native types directly passed around by values, with automatic boxing/unboxing
			// into an object of the corresponding numeric class when needed, but I guess it's way
			// too clever for them.
			//
			// Anyway, if I want my stuff to build without warnings (which I of course do) I can either:
			//
			// - disable this warning (which I won't because it's useful to avoid
			//   accidentaly writing retarded code, but I guess that if the python
			//   fucktards did this, they wouldn't be able to figure out how to code
			//   anything at all)
			//
			// - use a pragma to disable this warning just here (non-portable, and it doesn't seem
			//   to work for that particular warning)
			//
			// - fuck their shitty macros and do it by hand with some workaround, and because
			//   I then don't use the python API properly anymore (funny how in the world of
			//   python, properly means "with a disgusting compilation warning"), things
			//   will probably break down somehow in the future if they decide to break
			//   binary compatilibty.
			//
			// The gene pool would be so much improved if everyone intentionally choosing
			// to use python for any purpose whatsoever, as well as anyone having anything
			// to do with its development would suddenly choke and die.
			//
			// Option 2 failed, so I'm going for option 3. If you're here because you
			// have a version of python above 2.5 and it fails to build/work, now you
			// know why.
			
			//Dest_ = ( pPyObj_ != Py_True );
			Dest_ = ( pPyObj_ == static_cast< PyObject* >( static_cast< void* >( &_Py_TrueStruct ) ) );
		}
												   
		static void ConvertFromPython( PyObject* pPyObj_, bool& Dest_ )
		{
			if( !CheckType_nothrow( pPyObj_ ) )
				throw Error::TypeMismatch();

			ConvertFromPython_nocheck( pPyObj_, Dest_ );
		}

		static PyObject* ConvertToPython( const bool& Value_ )
		{
			return PyBool_FromLong( Value_ );
		}
	};
	
	// string
	template<> struct TypeConverter< std::string, tc_Default >
	{
		static bool CheckType_nothrow( PyObject* pPyObj_ )
		{
			return !!PyString_Check( pPyObj_ );
		}

		static void ConvertFromPython_nocheck( PyObject* pPyObj_, std::string& Dest_ )
		{
			Dest_ = PyString_AsString( pPyObj_ );
		}

		static void ConvertFromPython( PyObject* pPyObj_, std::string& Dest_ )
		{
			if( !CheckType_nothrow( pPyObj_ ) )
				throw Error::TypeMismatch();

			ConvertFromPython_nocheck( pPyObj_, Dest_ );
		}

		static PyObject* ConvertToPython( const std::string& Value_ )
		{
			return PyString_FromString( Value_.c_str() );
		}
	};
	
	// Dummy type converters for all STL container types for now
	/*template< typename C, typename T > struct TypeConverter< MonadicType< C >, T >
	{
		static bool CheckType_nothrow( PyObject* pPyObj_ ) { return false; }
		static void ConvertFromPython_nocheck( PyObject* pPyObj_, T& Dest_ ) {}
		static void ConvertFromPython( PyObject* pPyObj_, T& Dest_ ) {}
		static PyObject* ConvertToPython( const T& Value_ ) { Py_RETURN_NONE; }
	};

	template< typename C1, typename C2, typename T > struct TypeConverter< DyadicType< C1, C2 >, T >
	{
		static bool CheckType_nothrow( PyObject* pPyObj_ ) { return false; }
		static void ConvertFromPython_nocheck( PyObject* pPyObj_, T& Dest_ ) {}
		static void ConvertFromPython( PyObject* pPyObj_, T& Dest_ ) {}
		static PyObject* ConvertToPython( const T& Value_ ) { Py_RETURN_NONE; }
	};*/
	
	template<> struct TypeConverter< DynamicBuffer, tc_Default >
	{
		static bool CheckType_nothrow( PyObject* pPyObj_ ) { return false; }
		static void ConvertFromPython_nocheck( PyObject* pPyObj_, DynamicBuffer& Dest_ ) {}
		static void ConvertFromPython( PyObject* pPyObj_, DynamicBuffer& Dest_ ) {}
		static PyObject* ConvertToPython( const DynamicBuffer& Value_ ) { Py_RETURN_NONE; }
	};

	// TODO: all other types
}}

#endif
