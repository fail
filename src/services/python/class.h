/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_PYTHON_CLASS_H
#define FAIL_PYTHON_CLASS_H

#include "core/core.h"
#include <string>
#include <vector>
#include <type_traits>

namespace fail { namespace PythonImpl
{
	template< bool DoWant, class C > struct RegisterIf
	{
		static void RegisterPointerConversion()
		{
			TypeConverter< shared_ptr< C > >::RegisterPointerConversion();
		}
	};
	
	template< class C > struct RegisterIf< false, C >
	{
		static void RegisterPointerConversion()
		{
		}
	};
	
	// This is the python representation of instances of the structure at hand. Just the
	// structure wrapped into a PyObject.
	template< class C > struct ClassWrapper : public PyObject	// I don't have to use their shitty little macro
										// that fakes inheritance in C.
	{
		ClassWrapper() :
			pTypeCastMap( &TypeCasts< C >::Map )
		{
		}
		
		ClassWrapper( const shared_ptr< C >& pObj_ ) :
			pObj( pObj_ ),
			pTypeCastMap( &TypeCasts< C >::Map )
		{
		}
	
		shared_ptr< C >	pObj;
		TypeCastMap*	pTypeCastMap;
	};
	
	template< class C, typename AttrTag,
			  e_TypeCategory cat = TypeCategory< typename attribute_traits< C, AttrTag >::type >::value,
  			  bool bReadOnly = flags::ReadOnly< AttrTag >::value,
	 		  bool bUnMutable = flags::Mutable< AttrTag >::value >
	struct ClassAttrGetter
	{
		static PyObject* GetAttr( PyObject* pPyObj, void* )
		{
			typedef TypeConverter< shared_ptr< C > > obj_tc;
			shared_ptr< C > pObj;
			obj_tc::ConvertFromPython_nocheck( pPyObj, pObj );
			
			typedef attribute_traits< C, AttrTag > traits;
			typedef TypeConverter< typename traits::type > tc;

			return tc::ConvertToPython( ( ( pObj.get() )->*traits::Getter() )() );
		}
	};
	
	template< class C, typename AttrTag >
	struct ClassAttrGetter< C, AttrTag, tc_ValueType, false, true >
	{
		static PyObject* GetAttr( PyObject* pPyObj, void* )
		{
			typedef TypeConverter< shared_ptr< C > > obj_tc;
			shared_ptr< C > pObj;
			obj_tc::ConvertFromPython_nocheck( pPyObj, pObj );
			
			typedef attribute_traits< C, AttrTag > traits;
			typedef TypeConverter< typename traits::type > tc;

			return tc::ConvertToPythonByRef( pObj, ( ( pObj.get() )->*traits::GetterMutable() )() );
		}
	};
	
	template< class C, typename AttrTag,
		bool bReadOnly = flags::ReadOnly< AttrTag >::value >
	struct ClassAttrSetter
	{
		static int SetAttr( PyObject* pPyObj, PyObject *pValue, void* )
		{
			try
			{
				typedef attribute_traits< C, AttrTag > traits;
				typedef TypeConverter< typename traits::type > tc;
	
				ClassWrapper< C >* pWrapper = static_cast< ClassWrapper< C >* >( pPyObj );
	
				typename traits::type Value;
				tc::ConvertFromPython( pValue, Value );

				( ( pWrapper->pObj.get() )->*traits::Setter() )( Value );

				return 0;
			}
			catch( const Error::Base& error )
			{
				error.ConvertToPython();
				return -1;
			}
		}
	};
	
	template< class C, typename AttrTag >
	struct ClassAttrSetter< C, AttrTag, true >
	{
		static int SetAttr( PyObject* pPyObj, PyObject *pValue, void* )
		{
			// TODO: define an exception for this error
			std::cout << "error: Trying to set a read-only attribute\n";
			return -1;
		}
	};
	
	// Holds all necessary static data and utility sub classes related to a class
	template< class C > struct ClassStuff
	{
		static PyTypeObject						TypeObject;
		static std::vector< PyMethodDef >		Methods;
		static std::vector< PyGetSetDef >		Attributes;

		// The callcontext to be used by all struct method wrappers. The wrappers themselves
		// are actually mostly the same for structs and classes, only bits of the callcontext
		// need to be tweaked for either case.
		struct Class_CallContext : public CallContext< C, shared_ptr< C > >
		{
			Class_CallContext( PyObject* pPyObj_, PyObject* pArgs_ ) :
				CallContext< C, shared_ptr< C > >( pPyObj_, pArgs_ )
			{
			}

			template< typename... Args > void constructObject( Args&&... args_ )
			{
				ClassWrapper< C >* pWrapper = static_cast< ClassWrapper< C >* >
						( CallContext< C, shared_ptr< C > >::m_pPyObj );
				new( pWrapper ) ClassWrapper< C >;
				pWrapper->pObj = shared_ptr< C >( new C( args_... ) );
			}
		};

		// The python finalizer for that type. Calls the destructor of the wrapped struct.
		static void DeleteFunc( PyObject* pPyObj )
		{
			ClassWrapper< C >* pWrapper = static_cast< ClassWrapper< C >* >( pPyObj );

			// Call the Pointer's destructor.
			pWrapper->pObj.~shared_ptr< C >();

			pPyObj->ob_type->tp_free( pPyObj );
		}
		
		static PyObject* GlobalGetAttr( PyObject* pPyObj, PyObject* pName )
		{
			const char* pAttrName = PyString_AS_STRING( pName );
			return PyObject_GenericGetAttr( pPyObj, pName );
		}

		// Visitor callback used to count things to reserve buffers.
		struct CountVisitor
		{
			CountVisitor() : MethodCount( 0 ), AttributeCount( 0 ) {}

			void declConstructor()
			{
			}

			template< typename MtdTag > void declMethod()
			{
				if( flags::Scriptable< MtdTag >::value )
					++MethodCount;
			}

			template< typename MtdTag > void declStaticMethod()
			{
				if( flags::Scriptable< MtdTag >::value )
					++MethodCount;
			}

			template< typename AttrTag > void declAttribute()
			{
				if( flags::Scriptable< AttrTag >::value )
					++AttributeCount;
			}

			template< class SC > void declSuperClass()
			{			
				if( flags::Scriptable< SC >::value )
				{
					class_traits< SC >::VisitMethods( *this );
					class_traits< SC >::VisitAttributes( *this );
				}
			}

			int MethodCount;
			int AttributeCount;
		};

		// Visitor callback used to walk through all the stuff the class has to offer, and setup the
		// necessary wrappers.
		struct SetupVisitor
		{
			void declConstructor()
			{
				if( flags::Scriptable< typename class_traits< C >::ctor_tag >::value )	
					TypeObject.tp_new = &CtorWrapper< C, Class_CallContext >::PyFunc;
			}

			template< typename MtdTag > void declMethod()
			{
				if( !flags::Scriptable< MtdTag >::value )
					return;
				
				PyMethodDef def;
				def.ml_name = method_traits< C, MtdTag >::Name();
				def.ml_meth = &MethodWrapper< C, MtdTag, Class_CallContext >::PyFunc;
				def.ml_flags = METH_VARARGS;
				def.ml_doc = NULL;
				Methods.push_back( def );
			}

			template< typename MtdTag > void declStaticMethod()
			{
				if( !flags::Scriptable< MtdTag >::value )
					return;

				PyMethodDef def;
				def.ml_name = method_traits< C, MtdTag >::Name();
				def.ml_meth = &MethodWrapper< C, MtdTag, Class_CallContext >::PyFunc;
				def.ml_flags = METH_VARARGS | METH_STATIC;
				def.ml_doc = NULL;
				Methods.push_back( def );
			}

			// This stuff is there to conditionally instantiate the attribute wrapping
			// code.
			//
			// Just a plain if doesn't work because the attribute wrapping code
			// is instantiated before getting discarded by the optimizer, and we don't
			// want this code to be instantiated at all so we're not forced to
			// define accessors for non-scriptable attributes.
			template< typename AttrTag > struct DoNotWantAttribute
			{
				static void setup( SetupVisitor& sv ) {}
			};
			
			template< typename AttrTag > struct DoWantAttribute
			{
				static void setup( SetupVisitor& sv )
				{
					sv.setupAttribute< AttrTag >();
				}
			};
			
			template< typename AttrTag > void declAttribute()
			{
				//call_member_if< flags::scriptable< AttrTag >::value >
				typedef std::conditional<
						flags::Scriptable< AttrTag >::value,
						DoWantAttribute< AttrTag >,
	  					DoNotWantAttribute< AttrTag > > AttrSetupProxy;
				AttrSetupProxy::type::setup( *this );
			}
			
			template< typename AttrTag > void setupAttribute()
			{
				PyGetSetDef def;

				// "How do I used const" -- probably said at one point or another by some drooling python developer
				def.name = const_cast< char* >( attribute_traits< C, AttrTag >::Name() );
				def.get = ClassAttrGetter< C, AttrTag >::GetAttr;
				def.set = ClassAttrSetter< C, AttrTag >::SetAttr;
				def.doc = NULL;
				def.closure = NULL;
				Attributes.push_back( def );
			}
			
			// Simplistic enum implementation: enum values are jyst converted to/from integers;
			// so there's no type enforcement like in the lua bindings.
			template< typename EnumTag > void declEnum()
			{
				if( !flags::Scriptable< EnumTag >::value )
					return;
				
				enum_traits< C, EnumTag >::VisitValues( *this );
			}
			
			template< typename enum_type > void declEnumValue( const char* pName_, enum_type Value_ )
			{
				PyObject* pEnumVal = PyLong_FromLong( Value_ );
				PyDict_SetItemString( TypeObject.tp_dict, pName_, pEnumVal );
				Py_DECREF( pEnumVal );
			}

			template< class SC > void declSuperClass()
			{
				if( !flags::Scriptable< SC >::value )
					return;

				class_traits< SC >::VisitSuperClasses( *this );
				
				//std::cout << "in typecast map @" << &TypeCasts< C >::Map <<": inserting "
					//<< class_traits< C >::FullName() << " to " << class_traits< SC >::FullName() << " cast function\n";
				
				TypeCasts< C >::Map.insert( std::make_pair(
					&ClassStuff< SC >::TypeObject,
					&TypeCasts< C >::template PointerCastFunc< SC > ) );
				
				class_traits< SC >::VisitMethods( *this );
				class_traits< SC >::VisitAttributes( *this );
				class_traits< SC >::VisitEnums( *this );
			}
		};

		static void Setup( PyObject* pModule_ )
		{
			// It's really appalling to have to initialize this fucking structure manually like that.
			// Did they ever hear of opaqueness? Who let those guys design their own programming language?
			TypeObject.ob_type = &PyType_Type;
			TypeObject.tp_basicsize = sizeof( ClassWrapper< C > );

			// The name's supposed to use dots as separators but I'm lazy
			// and if Python didn't suck that much it would prepend the
			// module name automatically as needed.
			TypeObject.tp_name = class_traits< C >::FullName();
			
			TypeObject.tp_flags = Py_TPFLAGS_DEFAULT;

			TypeObject.tp_alloc = PyType_GenericAlloc;

			// The C standard state that all identifiers beginning with an underscore are reserved for internal
			// use by the compiler and the standard libraries. But the Python devs are so awesome that
			// they are above rules and clean coding practices.
			TypeObject.tp_free = _PyObject_Del;

			// So, dealloc is not the opposite of alloc as one might believe at first glance. It's the opposite of new.
			// Programming languages designed by people with a poor grasp of vocabulary are always the best ones.
			TypeObject.tp_dealloc = DeleteFunc;
			
			TypeObject.tp_getattro = GlobalGetAttr;

			Py_INCREF( &TypeObject );
			
			// Count the methods and attributes, then reserve the required amount of entries in the respective vectors.
			CountVisitor cv;
			class_traits< C >::VisitSuperClasses( cv );
			class_traits< C >::VisitMethods( cv );
			class_traits< C >::VisitStaticMethods( cv );
			class_traits< C >::VisitAttributes( cv );

			Methods.reserve( cv.MethodCount + 1 );
			Attributes.reserve( cv.AttributeCount + 1 );

			SetupVisitor sv;

			// Setup the methods and attributes from the superclasses.
			class_traits< C >::VisitSuperClasses( sv );

			// Setup the constructor and static methods.
			class_traits< C >::VisitStaticMethods( sv );

			// Setup the non-static methods.
			class_traits< C >::VisitMethods( sv );

			PyMethodDef terminator = { 0, 0, 0, 0 };
			Methods.push_back( terminator );
			TypeObject.tp_methods = &Methods[0];

			// Setup attributes.
			class_traits< C >::VisitAttributes( sv );
			PyGetSetDef attr_terminator = { 0, 0, 0, 0, 0 };
			Attributes.push_back( attr_terminator );
			TypeObject.tp_getset = &Attributes[0];

			PyType_Ready( &TypeObject );
			
			// Setup enums.
			class_traits< C >::VisitEnums( sv );

			// Ah, the joy of shitty C apis and their careless, braindead reliance on
			// disgracious casts.
			// I could have used a (very pretty) reinterpret_cast to directly cast
			// the typeobject into a PyObject, except that it produces a warning about
			// breaking strict aliasing rules.
			// Since I don't want to disable that useful warning for the sake of python's
			// stupidity, I found this ugly workaround.
			// It was going to be ugly either way anyway because python's developers
			// are tasteless morons.
			PyModule_AddObject( pModule_, class_traits< C >::Name(), static_cast< PyObject* >( static_cast< void* >( &TypeObject ) ) );
		}
	};

	// Because of their very convenient PyObject_HEAD_INIT macro designed to be used
	// as part of an initialisation list, I can't just clear the whole thing and get
	// it initialized properly afterwards, forcing me to supply this ridiculous initializer
	// list full of zeros.
	template< class S > PyTypeObject ClassStuff< S >::TypeObject =
		{ PyObject_HEAD_INIT(NULL)
		  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

	template< class C > std::vector< PyMethodDef > ClassStuff< C >::Methods( 0 );
	template< class C > std::vector< PyGetSetDef > ClassStuff< C >::Attributes( 0 );
}}

#endif
