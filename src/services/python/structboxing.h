/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_PYTHON_STRUCTBOXING_H
#define FAIL_PYTHON_STRUCTBOXING_H

namespace fail { namespace PythonImpl
{
	template< class S > struct TypeConverter< S*, tc_ValueType >
	{
		static bool CheckType_nothrow( PyObject* pPyObj_ )
		{
			return PyObject_TypeCheck( pPyObj_, &StructStuff< S >::TypeValObject ) ||
				   PyObject_TypeCheck( pPyObj_, &StructStuff< S >::TypeRefObject );
		}

		static void ConvertFromPython_nocheck( PyObject* pPyObj_, S*& pDest_ )
		{
			typename StructStuff< S >::BaseWrapper* pBaseWrapper =
				static_cast< typename StructStuff< S >::BaseWrapper* >( pPyObj_ );
			
			if( pBaseWrapper->bByRef )
			{
				typename StructStuff< S >::WrapperByRef* pWrapper =
					static_cast< typename StructStuff< S >::WrapperByRef* >( pPyObj_ );
				pDest_ = pWrapper->p_wrapped_struct;
			}
			else
			{
				typename StructStuff< S >::WrapperByVal* pWrapper =
					static_cast< typename StructStuff< S >::WrapperByVal* >( pPyObj_ );
				pDest_ = &pWrapper->wrapped_struct;
			}
		}
		
		static void ConvertFromPython( PyObject* pPyObj_, S*& pDest_ )
		{
			if( !CheckType_nothrow( pPyObj_ ) )
				throw Error::TypeMismatch();
			ConvertFromPython_nocheck( pPyObj_, pDest_ );
		}
	};
	
	template< class S > struct TypeConverter< S, tc_ValueType >
	{		
		static bool CheckType_nothrow( PyObject* pPyObj_ )
		{
			return PyObject_TypeCheck( pPyObj_, &StructStuff< S >::TypeValObject ) ||
				   PyObject_TypeCheck( pPyObj_, &StructStuff< S >::TypeRefObject );
		}
		
		static void ConvertFromPython_nocheck( PyObject* pPyObj_, S& Dest_ )
		{
			typename StructStuff< S >::BaseWrapper* pBaseWrapper =
				static_cast< typename StructStuff< S >::BaseWrapper* >( pPyObj_ );
			
			if( pBaseWrapper->bByRef )
			{
				typename StructStuff< S >::WrapperByRef* pWrapper =
					static_cast< typename StructStuff< S >::WrapperByRef* >( pPyObj_ );
				Dest_ = *pWrapper->p_wrapped_struct;
			}
			else
			{
				typename StructStuff< S >::WrapperByVal* pWrapper =
					static_cast< typename StructStuff< S >::WrapperByVal* >( pPyObj_ );
				Dest_ = pWrapper->wrapped_struct;
			}
		}
		
		static void ConvertFromPython( PyObject* pPyObj_, S& Dest_ )
		{
			if( !CheckType_nothrow( pPyObj_ ) )
				throw Error::TypeMismatch();
			ConvertFromPython_nocheck( pPyObj_, Dest_ );
		}
		
		static PyObject* ConvertToPython( const S& Obj_ )
		{
			if( !flags::Scriptable< S >::value )
				Py_RETURN_NONE;
			
			PyObject* pPyObj;

			pPyObj = PyType_GenericAlloc( &StructStuff< S >::TypeValObject, 0 );
			if( !pPyObj )
				throw std::bad_alloc();

			try
			{
				typedef typename StructStuff< S >::WrapperByVal Wrapper;
				Wrapper* pWrapper = static_cast< Wrapper* >( pPyObj );
				pWrapper->bByRef = false;
				new( &pWrapper->wrapped_struct ) S( Obj_ );
				return pPyObj;
			}
			catch( const Error::Base& error )
			{
				error.ConvertToPython();
				Py_DECREF( pPyObj );
				return NULL;
			}
		}
		
		static PyObject* ConvertToPythonByRef( shared_ptr< void > pOwner_, S& Obj_ )
		{
			if( !flags::Scriptable< S >::value )
				Py_RETURN_NONE;
			
			PyObject* pPyObj;

			pPyObj = PyType_GenericAlloc( &StructStuff< S >::TypeRefObject, 0 );
			if( !pPyObj )
				throw std::bad_alloc();

			try
			{
				typedef typename StructStuff< S >::WrapperByRef Wrapper;
				Wrapper* pWrapper = static_cast< Wrapper* >( pPyObj );
				new( pWrapper ) Wrapper( pOwner_, &Obj_ );
				return pPyObj;
			}
			catch( const Error::Base& error )
			{
				error.ConvertToPython();
				Py_DECREF( pPyObj );
				return NULL;
			}
		}
	};
}}

#endif
