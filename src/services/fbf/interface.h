/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBF_INTERFACE_H
#define FAIL_FBF_INTERFACE_H

#include "core/core.h"
#include "services/fbf/services-fbf_export.h"
#include "ReaderBackend_i.h"
#include "WriterBackend_i.h"
#include "Writer.h"
#include "WriterBigEndian.h"
#include "WriterLittleEndian.h"
#include "Reader.h"

namespace fail { namespace fbf
{
	template< typename ModuleTag > void Setup();
}}

#endif
