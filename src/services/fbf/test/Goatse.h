/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBF_GOATSE_H_
#define FAIL_FBF_GOATSE_H_

#include "core/core.h"

namespace fail { namespace fbftest
{
	class Goatse : public Serializable
	{
		public:
			Goatse( int32_t lulz_ ) :
				m_Lulz( lulz_ )
			{
			}
	
			Goatse( const Serialization_tag& ) {}
			
			uint32_t getLulz() const { return m_Lulz; }
			void setLulz( const uint32_t& x ) { m_Lulz = x; }
		
		private:
			template< class C, typename T > friend struct fail::attribute_traits;
			int32_t	m_Lulz;
	};
}}

#endif
