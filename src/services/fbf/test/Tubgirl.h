/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBF_TUBGIRL_H_
#define FAIL_FBF_TUBGIRL_H_

#include "core/core.h"
#include "LongCat.h"
#include "Goatse.h"
#include <map>
#include <vector>
#include <set>

namespace fail { namespace fbftest
{
	class Tubgirl : public Serializable
	{
		public:
			Tubgirl() :
				m_blah( 0x1245678 )
			{
				m_STLTest.insert( "Fail" );		
				m_STLTest.insert( "Win" );		
				m_STLTest.insert( "Lulz" );
			}

			Tubgirl( const Serialization_tag& ) :
				m_blah( 0x1235678 )
			{}
			
			uint32_t getblah() const { return m_blah; }
			void setblah( const uint32_t& x ) { m_blah = x; }
			//const uint32_t& blah() const { return m_blah; }
			
			const LongCat& getLolCat() const { return m_LolCat; }
			void setLolCat( const LongCat& x ) { m_LolCat = x; }
			
			const Pointer< Goatse >& getpGoatse1() const { return m_pGoatse1; }
			void setpGoatse1( const Pointer< Goatse >& x ) { m_pGoatse1 = x; }
			
			const Pointer< Goatse >& getpGoatse2() const { return m_pGoatse2; }
			void setpGoatse2( const Pointer< Goatse >& x ) { m_pGoatse2 = x; }
			
			typedef std::set< std::string > stl_test_type;
						
			const stl_test_type& getSTLTest() const { return m_STLTest; }
			void setSTLTest( const stl_test_type& x ) { m_STLTest = x; }
			
			LongCat	m_LolCat;

		private:
			template< class C, typename T > friend struct fail::attribute_traits;
			uint32_t	m_blah;
			
			Pointer< Goatse > m_pGoatse1;
			Pointer< Goatse > m_pGoatse2;
			
			stl_test_type m_STLTest;
	};
}}

#endif
