/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace fail { namespace fbftest
{
	class Tubgirl
	{
		uint32_t blah;
		LongCat LolCat;
		Pointer< Goatse > pGoatse1;
		Pointer< Goatse > pGoatse2;

		set< string > STLTest;
	};

	class Goatse
	{		
		Goatse( int32_t lulz );
		int32_t	Lulz;
	};
	
	struct LongCat
	{
		uint32_t	Length;
	};
}}
