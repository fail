/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
#include <physfs.h>

#include "io/FileOutputStream.h"
#include "io/FileInputStream.h"
#include "fbf/interface.h"
#include "module_fail-fbftest.h"
#include "testapp.h"
#include "Goatse.h"

using namespace std;
using namespace fbftestapp;
using namespace fail;
using namespace fail::io;

FBFTestApp::FBFTestApp( int argc, char** argv ) throw( std::runtime_error )
{
	if( !PHYSFS_init( argv[0] ) )
		throw runtime_error( "PhysFS init failed" );
	
	PHYSFS_setWriteDir( "." );
	PHYSFS_addToSearchPath( ".", 0 );
	
	fail::fbf::Setup< fail::fbftest::fail_tag >();
}

FBFTestApp::~FBFTestApp()
{
	PHYSFS_deinit();
}

void FBFTestApp::run()
{
	{
		Pointer< SeekableOutputStream > pStream = new FileOutputStream( "blah.fbf" );
	//	fbf::Writer writer( new FileOutputStream( "blah.fbf" ) );

		Pointer< fail::fbftest::Tubgirl > pTG = new fail::fbftest::Tubgirl();
		pTG->setblah( 0xdeadbeef );
		pTG->m_LolCat.Length() = 1337;
		
		Pointer< fail::fbftest::Goatse > pG = new fail::fbftest::Goatse( 789456 );
		pTG->setpGoatse1( pG );
		
		//pG = new fail::fbftest::Goatse( 1234564 );
		pTG->setpGoatse2( pG );

	//	fbf::Writer::Save( pStream, pTG );

		GenericPointer pMeh = static_cast< fail::fbftest::Tubgirl* >( pTG );
		fbf::Writer::Save( pStream, pMeh );	
	}
	
	{
		Pointer< SeekableInputStream > pStream = new FileInputStream( "blah.fbf" );

		//GenericPointer pLulz = fbf::Reader::Load( pStream );		
		Pointer< fail::fbftest::Tubgirl > pTG =
				fbf::Reader::Load< fail::fbftest::Tubgirl >( pStream );

		if( pTG )
		{
			std::cout << hex << pTG->getblah() << std::endl;
			std::cout << dec << pTG->m_LolCat.Length() << std::endl;
			std::cout << "goatse1: " << pTG->getpGoatse1() << std::endl;
			std::cout << "goatse2: " << pTG->getpGoatse2() << std::endl;
		}
	}
}
