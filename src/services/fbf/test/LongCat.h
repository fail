/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBF_LONGCAT_H_
#define FAIL_FBF_LONGCAT_H_

#include "core/core.h"

namespace fail { namespace fbftest
{
	struct LongCat
	{
		public:
			LongCat() :
				m_Length( 456789 )
			{
			}

			uint32_t& Length() { return m_Length; }
			const uint32_t& Length() const { return m_Length; }

			uint32_t	m_Length;
	};
}}

#endif
