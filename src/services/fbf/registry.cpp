/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "services/fbf/implementation.h"
#include "services/fbf/registry.h"
#include "core/core.h"

using namespace fail;
using namespace fail::fbf::Impl;

std::set< uint64_t > fail::fbf::Impl::Digests;

template Registry< endianess::Big >::genericwriter_map_type Registry< endianess::Big >::m_GenericWriters;
template Registry< endianess::Little >::genericwriter_map_type Registry< endianess::Little >::m_GenericWriters;

template Registry< endianess::Big >::genericreader_map_type Registry< endianess::Big >::m_GenericReaders;
template Registry< endianess::Little >::genericreader_map_type Registry< endianess::Little >::m_GenericReaders;
