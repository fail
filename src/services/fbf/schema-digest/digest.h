/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBF_SCHEMADIGEST_DIGEST_H
#define FAIL_FBF_SCHEMADIGEST_DIGEST_H

#include "crc64.h"
#include "module.h"

namespace fail { namespace SchemaDigestImpl
{	
	template< typename ModuleTag > struct Digest
	{
		Digest()
		{
			SchemaDigestImpl::crc64 crc;

			crc.writeU32( module_traits< ModuleTag >::Digest() );
			ModuleVisitor mv( crc );
			module_traits< ModuleTag >::VisitClasses( mv );
			m_crc64 = crc.result();
		}
		
		operator uint64_t() const
		{
			return m_crc64;
		}
		
		uint64_t m_crc64;
	};
}}

namespace fail
{
	template< typename ModuleTag > struct SchemaDigest
	{
		static SchemaDigestImpl::Digest< ModuleTag > digest;
	};
	
	template< typename ModuleTag > SchemaDigestImpl::Digest< ModuleTag > SchemaDigest< ModuleTag >::digest;
}

#endif
