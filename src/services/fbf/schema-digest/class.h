/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBF_SCHEMADIGEST_CLASS_H
#define FAIL_FBF_SCHEMADIGEST_CLASS_H

namespace fail { namespace SchemaDigestImpl
{
	template< class C > struct ClassVisitor
	{
		ClassVisitor( SchemaDigestImpl::crc64& crc_ ) : crc( crc_ ) {}

		void CalcDigest()
		{
			crc.writeU32( class_traits< C >::Digest() );
		//	crc.writeU32( sizeof( C ) );
			class_traits< C >::template VisitSuperClasses( *this );
			class_traits< C >::template VisitAttributes( *this );
			class_traits< C >::template VisitEnums( *this );
		}
		
		template< class SC > void declSuperClass()
		{
			if( !flags::Storable< SC >::value )
				return;

			crc.writeU8( '(' );
			crc.writeU32( class_traits< SC >::Digest() );
			class_traits< SC >::template VisitSuperClasses( *this );
			class_traits< SC >::template VisitAttributes( *this );
			class_traits< SC >::template VisitEnums( *this );
			crc.writeU8( ')' );
		}
		
		template< typename AttrTag > void declAttribute()
		{
			if( flags::Storable< AttrTag >::value )
				crc.writeU32( attribute_traits< C, AttrTag >::Digest() );
			
			// Throw in the byte offset within the object instance, because
			// we need memory layout changes to be reflected by the digest.
		/*	const C* pObj = 0;
			const typename attribute_traits< C, AttrTag >::type* pAttr =
					&( pObj->*attribute_traits< C, AttrTag >::MemberPointer() );
			unsigned long offset = reinterpret_cast< unsigned long >( pAttr );

			crc.writeU32( offset );*/
			
			// Removed this because I will finally just do an attribute at a time
			// serialization. structs will be saved in one go, though.
		}
		
		template< typename EnumTag > void declEnum()
		{
			if( !flags::Storable< EnumTag >::value )
				return;
			
			crc.writeU8( '[' );
			crc.writeU32( enum_traits< C, EnumTag >::Digest() );
			// TODO: write values, otherwise enum changes won't be detected as breaking
			// OBJS chunk binary compatilibty
			crc.writeU8( ']' );
		}
			
		SchemaDigestImpl::crc64& crc;
	};
}}

#endif
