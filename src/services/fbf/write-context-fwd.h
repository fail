/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBF_WRITER_CONTEXT_FWD_H
#define FAIL_FBF_WRITER_CONTEXT_FWD_H

#include <map>

namespace fail { namespace fbf { namespace Impl
{
	template< class stream_type > class WriteContext
	{		
		public:
			typedef typename stream_type::endianess_policy endianess_policy;
			
			WriteContext( stream_type& Stream_ ) :
				Stream( Stream_ ),
				m_NextObjectID( 0 )
			{
			}

			template< class C > void writeObject( shared_ptr< C > pObj );
			
			int32_t getObjectID( shared_ptr< const Serializable > pObject )
			{
				map< shared_ptr< const Serializable >, int32_t >::const_iterator it =
					m_Objects.find( pObject );
				
				if( it == m_Objects.end() )
					return -1;
				
				return it->second;
			}
			
			void addObject( shared_ptr< const Serializable > pObject )
			{
				m_Objects.insert( make_pair( pObject, m_NextObjectID++ ) );
			}
			
			stream_type& Stream;
			
		private:
			map< shared_ptr< const Serializable >, int32_t >	m_Objects;
			int m_NextObjectID;
	};
}}}

#endif
