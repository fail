/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBF_READER_OBJS_H
#define FAIL_FBF_READER_OBJS_H

#include <vector>

namespace fail { namespace fbf
{
	template< class EndianessPolicy > shared_ptr< Serializable > BaseReader< EndianessPolicy >::
		ReadObjects( shared_ptr< ReaderBackend_i > pBackend )
	{
		shared_ptr< io::InputStream > pInStream = pBackend->beginObjects();
		buffered_stream_type stream( pInStream );
		
		Impl::ReadContext< buffered_stream_type > rct( stream, pBackend );
		shared_ptr< Serializable > pObj( rct.readObject() );
		rct.postLoad();
		
		pBackend->endObjects();
		return pObj;
	}
}}

#endif
