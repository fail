/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBF_REGISTRY_H
#define FAIL_FBF_REGISTRY_H

#include <cstdint>
#include <set>
#include <map>

// Global tables: factories, digests, etc.

namespace fail { namespace fbf { namespace Impl
{
	extern set< uint64_t > Digests FLSERVICES_FBF_EXPORT;
	
/*	template< class C > struct ForceInstantiation
	{
		typedef void ( *writer_save_func )( shared_ptr< WriterBackend_i > pBackend, shared_ptr< C >& );
		static writer_save_func pWriterSaveFunc;
	};
	
	template< class C > typename ForceInstantiation< C >::writer_save_func
		ForceInstantiation< C >::pWriterSaveFunc = NULL;*/
	
	template< class EndianessPolicy > struct Registry
	{
		typedef io::OutputStream outstream_type;
		typedef io::BufferedInputStream< EndianessPolicy > instream_type;
		typedef Impl::ReadContext< instream_type > readcontext_type;

		class ClassGenericWriter_i
		{
			public:
				typedef io::BufferedOutputStream< EndianessPolicy > buffered_stream_type;
				typedef WriteContext< buffered_stream_type > WCT;
				
				virtual ~ClassGenericWriter_i() {}
				virtual void save( shared_ptr< WriterBackend_i > pBackend, shared_ptr< Serializable > pObj ) const = 0;
				virtual void write( WCT& wct, shared_ptr< Serializable > pObj ) const = 0;
		};
		
		template< class C > class ClassGenericWriter : public ClassGenericWriter_i
		{
			public:
				virtual void save( shared_ptr< WriterBackend_i > pBackend, shared_ptr< Serializable > pObj ) const
				{
					shared_ptr< C > pTmp = static_pointer_cast< C >( pObj );
					BaseWriter< EndianessPolicy >::Save( pBackend, pTmp );
				}
				
				// Given a shared_ptr< Serializable >, insert the object in the stream.
				// This is a lower level operation intended to save sub-objects whose
				// type is unknown at compile time (objects pointed through polymorphic
				// bases)
				virtual void write( typename ClassGenericWriter_i::WCT& wct, shared_ptr< Serializable > pObj ) const
				{
					wct.Stream.writeU32( class_traits< C >::Digest() );
					shared_ptr< C > pTmp = static_pointer_cast< C >( pObj );
					wct.writeObject( pTmp );
				}
		};
		
		typedef map< TypeInfoKey, shared_ptr< ClassGenericWriter_i > > genericwriter_map_type;
		static genericwriter_map_type m_GenericWriters;
		
		class ClassGenericReader_i
		{
			public:
				virtual ~ClassGenericReader_i() {}
				virtual shared_ptr< Serializable > read( readcontext_type& rct ) const = 0;
		};
		
		template< class C > class ClassGenericReader : public ClassGenericReader_i
		{
			public:
				// The name is not loaded because this is not the symetrical operation of
				// save: it can be used to load any object in the OBJS chunk, after
				// its classID has been read.
				// On the other hand, Save is just for the root object and implies
				// writing the whole chunk, including the headers.
				virtual shared_ptr< Serializable > read( readcontext_type& rct ) const
				{
					return static_pointer_cast< Serializable >( rct.template readObject< C >() );
				}
		};
		
		typedef map< uint32_t, shared_ptr< ClassGenericReader_i > > genericreader_map_type;
		static genericreader_map_type m_GenericReaders;		
		
		template< class C > static void RegisterClass()
		{
			m_GenericWriters.insert( make_pair(
				TypeInfoKey( typeid( C ) ),
				new ClassGenericWriter< C > ) );
			
			m_GenericReaders.insert( make_pair(
				class_traits< C >::Digest(),
				new ClassGenericReader< C > ) );
			
			// The only way I found to force the instantiation of this
			// function (with external linkage) in the translation unit where
			// the FBF stuff is instanced is to store its address in a static
			// variable.
			// Even gcc's -fkeep-inline-functions didn't cut it for some reason.
			// This is a hackish way to go about it, but at least it's portable.
		/*	ForceInstantiation< C >::pWriterSaveFunc =
					static_cast< typename ForceInstantiation< C >::writer_save_func >
					( BaseWriter< EndianessPolicy >::Save );*/
		}
		
		static shared_ptr< ClassGenericWriter_i > GetGenericWriter( const type_info& TypeInfo )
		{
			typename genericwriter_map_type::const_iterator it = m_GenericWriters.find( TypeInfo );
			if( it == m_GenericWriters.end() )
				return shared_ptr< ClassGenericWriter_i >();
			
			return it->second;
		}
		
		static shared_ptr< ClassGenericReader_i > GetGenericReader( uint32_t ClassID )
		{
			typename genericreader_map_type::const_iterator it = m_GenericReaders.find( ClassID );
			if( it == m_GenericReaders.end() )
				return shared_ptr< ClassGenericReader_i >();
			
			return it->second;
		}
	};
	
	template< class EndianessPolicy > typename Registry< EndianessPolicy >::genericwriter_map_type
		Registry< EndianessPolicy >::m_GenericWriters FLSERVICES_FBF_EXPORT;
	
	template< class EndianessPolicy > typename Registry< EndianessPolicy >::genericreader_map_type
		Registry< EndianessPolicy >::m_GenericReaders FLSERVICES_FBF_EXPORT;
}}}

#endif
