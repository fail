/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBF_STL_TYPES_H
#define FAIL_FBF_STL_TYPES_H

#include <vector>
#include <list>
#include <set>
#include <map>

namespace fail { namespace fbf { namespace Impl
{
	template< class Owner, typename T >
	struct TypeMarshaler< Owner, vector< T >, tc_Default >
	{
		typedef vector< T > type;
		
		template< class WCT > static void write( shared_ptr< const Owner > pOwner, WCT& wct_, const type& val_ )
		{
			typedef TypeMarshaler< Owner, T > tm;

			wct_.Stream.writeVarLenInt( val_.size() );
			
			typename type::const_iterator it;
			for( it = val_.begin(); it != val_.end(); ++it )
				tm::write( pOwner, wct_, *it );
		}

		template< class RCT > static void read( RCT& rct_, type& val_ )
		{
			typedef TypeMarshaler< Owner, T > tm;

			int32_t count = rct_.Stream.readVarLenInt();
			val_.reserve( count );

			while( count > 0 )
			{
				T newentry;
				tm::read( rct_, newentry );
				val_.push_back( newentry );
				--count;
			}
		}
	};
	
	template< class Owner, typename T >
	struct TypeMarshaler< Owner, list< T >, tc_Default >
	{
		typedef list< T > type;
		
		template< class WCT > static void write( shared_ptr< const Owner > pOwner, WCT& wct_, const type& val_ )
		{
			typedef TypeMarshaler< Owner, T > tm;

			wct_.Stream.writeVarLenInt( val_.size() );
			
			typename type::const_iterator it;
			for( it = val_.begin(); it != val_.end(); ++it )
				tm::write( pOwner, wct_, *it );
		}

		template< class RCT > static void read( RCT& rct_, type& val_ )
		{
			typedef TypeMarshaler< Owner, T > tm;

			int32_t count = rct_.Stream.readVarLenInt();

			while( count > 0 )
			{
				T newentry;
				tm::read( rct_, newentry );
				val_.push_back( newentry );
				--count;
			}
		}
	};
	
	template< class Owner, typename T >
	struct TypeMarshaler< Owner, set< T >, tc_Default >
	{
		typedef set< T > type;
		
		template< class WCT > static void write( shared_ptr< const Owner > pOwner, WCT& wct_, const type& val_ )
		{
			typedef TypeMarshaler< Owner, T > tm;

			wct_.Stream.writeVarLenInt( val_.size() );
			
			typename type::const_iterator it;
			for( it = val_.begin(); it != val_.end(); ++it )
				tm::write( pOwner, wct_, *it );
		}

		template< class RCT > static void read( RCT& rct_, type& val_ )
		{
			typedef TypeMarshaler< Owner, T > tm;

			int32_t count = rct_.Stream.readVarLenInt();

			while( count > 0 )
			{
				T newentry;
				tm::read( rct_, newentry );
				val_.insert( newentry );
				--count;
			}
		}
	};
	
	template< class Owner, typename T1, typename T2 >
	struct TypeMarshaler< Owner, map< T1, T2 >, tc_Default >
	{
		typedef map< T1, T2 > type;
		
		template< class WCT > static void write( shared_ptr< const Owner > pOwner, WCT& wct_, const type& val_ )
		{
			typedef TypeMarshaler< Owner, T1 > tm1;
			typedef TypeMarshaler< Owner, T2 > tm2;

			wct_.Stream.writeVarLenInt( val_.size() );
			
			typename type::const_iterator it;
			for( it = val_.begin(); it != val_.end(); ++it )
			{
				tm1::write( pOwner, wct_, it->first );
				tm2::write( pOwner, wct_, it->second );
			}
		}

		template< class RCT > static void read( RCT& rct_, type& val_ )
		{
			typedef TypeMarshaler< Owner, T1 > tm1;
			typedef TypeMarshaler< Owner, T2 > tm2;

			int32_t count = rct_.Stream.readVarLenInt();

			while( count > 0 )
			{
				pair< T1, T2 > newentry;
				tm1::read( rct_, newentry.first );
				tm2::read( rct_, newentry.second );
				val_.insert( newentry );
				--count;
			}
		}
	};
	
	template< class Owner, typename T1, typename T2 >
	struct TypeMarshaler< Owner, pair< T1, T2 >, tc_Default >
	{
		typedef pair< T1, T2 > type;
		
		template< class WCT > static void write( shared_ptr< const Owner > pOwner, WCT& wct_, const type& val_ )
		{
			typedef TypeMarshaler< Owner, T1 > tm1;
			tm1::write( pOwner, wct_, val_.first );
			
			typedef TypeMarshaler< Owner, T2 > tm2;
			tm2::write( pOwner, wct_, val_.second );
		}

		template< class RCT > static void read( RCT& rct_, type& val_ )
		{
			typedef TypeMarshaler< Owner, T1 > tm1;
 			tm1::read( rct_, val_.first );
			
			typedef TypeMarshaler< Owner, T2 > tm2;
			tm2::read( rct_, val_.second );
		}
	};
}}}

#endif
