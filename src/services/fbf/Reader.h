/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBF_READER_H
#define FAIL_FBF_READER_H

#include "core/core.h"
#include "io/BufferedInputStream.h"

namespace fail { namespace fbf
{
	template< class EndianessPolicy > class BaseReader
	{
		friend class Reader;

		public:
			typedef io::InputStream stream_type;
			typedef io::BufferedInputStream< EndianessPolicy > buffered_stream_type;
			

		private:
			// Those functions should throw whatever's needed if an error happens.
			// TODO: actually do that
			static bool ReadDigests( shared_ptr< ReaderBackend_i > pBackend );

			static shared_ptr< Serializable > ReadObjects( shared_ptr< ReaderBackend_i > pBackend );
			
			// TODO: implement the meta-data stuff (to be done during a later iteration)
			//static template< class C > void WriteMETA( stream_type& Stream_ );
	};
	
	// The high level api is exposed by this class. It then uses either the big or little
	// endian low level api to do the actual work depending on the header.
	class FLSERVICES_FBF_EXPORT Reader
	{
		typedef io::SeekableInputStream stream_type;

		public:
			// High-level API

			static shared_ptr< Serializable > Load( shared_ptr< ReaderBackend_i > pBackend );

			template< class C > static shared_ptr< C > Load( shared_ptr< ReaderBackend_i > pBackend )
			{
				return dynamic_pointer_cast< C >( Load( pBackend ) );
			}
	};
}}

#endif
