/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBF_STRUCT_H
#define FAIL_FBF_STRUCT_H

namespace fail { namespace fbf { namespace Impl
{
	// TODO: if the endianess is native, save the struct all at once.
	// We'll need to have the byte offset within the structure of the various attributes,
	// and a way to distinguish actual attributes and aliases (like in math::Rect)
	template< class Owner, class S > struct TypeMarshaler< Owner, S, tc_ValueType >
	{
		template< class WCT > struct WriteStructVisitor
		{
			WriteStructVisitor( shared_ptr< const Owner > pOwner_, WCT& wct_, const S& Obj_ ) :
				pOwner( pOwner_ ),
				wct( wct_ ),
				Obj( Obj_ )
			{
			}
	
			template< class SC > void declSuperClass()
			{
				class_traits< SC >::template VisitSuperClasses( *this );
				class_traits< SC >::template VisitAttributes( *this );
			}
			
			template< typename AttrTag > void declAttribute()
			{
				if( flags::Storable< AttrTag >::value )
				{
					typedef attribute_traits< S, AttrTag > traits;
					typedef TypeMarshaler< Owner, typename traits::type > tm;
					tm::write( pOwner, wct, ( Obj.*traits::Getter() )() );
				}
			}

			shared_ptr< const Owner > pOwner;
			WCT&		wct;
			const S&	Obj;
		};
		
		template< class WCT > static void write( shared_ptr< const Owner > pOwner, WCT& wct_, const S& Val_ )
		{
			if( flags::Storable< S >::value )
			{			
				WriteStructVisitor< WCT > rov( pOwner, wct_, Val_ );
				class_traits< S >::template VisitSuperClasses( rov );
				class_traits< S >::template VisitAttributes( rov );
			}
		}

		template< class RCT > struct ReadStructVisitor
		{
			ReadStructVisitor( RCT& rct_, S& Obj_ ) :
				rct( rct_ ),
				Obj( Obj_ )
			{
			}
	
			template< class SC > void declSuperClass()
			{
				class_traits< SC >::template VisitSuperClasses( *this );
				class_traits< SC >::template VisitAttributes( *this );
			}
			
			template< typename AttrTag > void declAttribute()
			{
				if( flags::Storable< AttrTag >::value )
				{
					typedef attribute_traits< S, AttrTag > traits;
					typedef TypeMarshaler< Owner, typename traits::type > tm;
					tm::read( rct, ( Obj.*traits::Accessor() )() );
				}
			}
	
			RCT&	rct;
			S&		Obj;
		};
		
		template< class RCT > static void read( RCT& rct_, S& Val_ )
		{
			if( flags::Storable< S >::value )
			{
				ReadStructVisitor< RCT > rov( rct_, Val_ );
				class_traits< S >::template VisitSuperClasses( rov );
				class_traits< S >::template VisitAttributes( rov );
			} 
		}
	};
}}}

#endif
