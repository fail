/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBF_WRITER_SAVE_H
#define FAIL_FBF_WRITER_SAVE_H

namespace fail { namespace fbf
{
	template< class EndianessPolicy > template< class C > void BaseWriter< EndianessPolicy >::
		Save( shared_ptr< WriterBackend_i > pBackend, shared_ptr< C > pObj )
	{
		pBackend->beginBundle( EndianessPolicy::type );

		WriteDigests< C >( pBackend, pObj );
		WriteObjects< C >( pBackend, pObj );
		
		pBackend->endBundle();
	}
	
	template< class EndianessPolicy > void BaseWriter< EndianessPolicy >::
		SaveSerializable( shared_ptr< WriterBackend_i > pBackend, shared_ptr< Serializable > pObj )
	{
		// TODO: throw something if a writer for the pointed type don't exist
		shared_ptr< typename Impl::Registry< EndianessPolicy >::ClassGenericWriter_i > pWriter =
			Impl::Registry< EndianessPolicy >::GetGenericWriter( typeid( *pObj ) );

		if( pWriter )
			pWriter->save( pBackend, pObj );
	}
}}

#endif
