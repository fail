/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBF_WRITERBACKEND_I_H_
#define FAIL_FBF_WRITERBACKEND_I_H_

#include "io/OutputStream.h"

namespace fail { namespace fbf
{
	class WriterBackend_i
	{
		// TODO: add version of the FBF encoding
		// The FBF file implementation will probably also have its own version
		// in its header but it would be the container format version
		public:
			virtual void beginBundle( endianess::Endianess en ) = 0;
			virtual void endBundle() = 0;
			
			virtual shared_ptr< io::OutputStream > beginDigests( uint32_t Size ) = 0;
			virtual void endDigests() = 0;
			
			virtual shared_ptr< io::OutputStream > beginObjects() = 0;
			virtual void endObjects() = 0;		
	};
}}

#endif
