/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBF_DYNAMICBUFFER_H
#define FAIL_FBF_DYNAMICBUFFER_H

#include <vector>
#include <list>
#include <set>
#include <map>

namespace fail { namespace fbf { namespace Impl
{
	// TODO: make a specialization that just read/write the entire buffer in one go
	// when the stream is in native endianess.
	template< class Owner > struct TypeMarshaler< Owner, DynamicBuffer, tc_Default >
	{
		template< class WCT > static void WriteCell( shared_ptr< const Owner > pOwner, WCT& wct_, const DynamicBuffer& db_, int Row, int Column )
		{
			switch( db_.getColumnType( Column ) )
			{
				case ct_uint8:
					TypeMarshaler< Owner, uint8_t >::
						write( pOwner, wct_, db_.get< uint8_t >( Row, Column ) );
					break;
				case ct_int8:
					TypeMarshaler< Owner, int8_t >::
						write( pOwner, wct_, db_.get< int8_t >( Row, Column ) );
					break;
				case ct_uint16:
					TypeMarshaler< Owner, uint16_t >::
						write( pOwner, wct_, db_.get< uint16_t >( Row, Column ) );
					break;
				case ct_int16:
					TypeMarshaler< Owner, int16_t >::
						write( pOwner, wct_, db_.get< int16_t >( Row, Column ) );
					break;
				case ct_uint32:
					TypeMarshaler< Owner, uint32_t >::
						write( pOwner, wct_, db_.get< uint32_t >( Row, Column ) );
					break;
				case ct_int32:
					TypeMarshaler< Owner, int32_t >::
						write( pOwner, wct_, db_.get< int32_t >( Row, Column ) );
					break;
				case ct_uint64:
					TypeMarshaler< Owner, uint64_t >::
						write( pOwner, wct_, db_.get< uint64_t >( Row, Column ) );
					break;
				case ct_int64:
					TypeMarshaler< Owner, int64_t >::
						write( pOwner, wct_, db_.get< int64_t >( Row, Column ) );
					break;
				case ct_float:
					TypeMarshaler< Owner, float >::
						write( pOwner, wct_, db_.get< float >( Row, Column ) );
					break;
				case ct_double:
					TypeMarshaler< Owner, double >::
						write( pOwner, wct_, db_.get< double >( Row, Column ) );
					break;
			}
		}
		
		template< class WCT > static void write( shared_ptr< const Owner > pOwner, WCT& wct_, const DynamicBuffer& db_ )
		{
			int numcolumns = db_.numColumns();
			wct_.Stream.writeU8( numcolumns );
			
			int column;
			for( column = 0; column < numcolumns; ++column )
				wct_.Stream.writeU8( db_.getColumnType( column ) );

			int numrows = db_.size();
			wct_.Stream.writeVarLenInt( numrows );
			for( int row = 0; row < numrows; ++row )
				for( column = 0; column < numcolumns; ++column )
					WriteCell( pOwner, wct_, db_, row, column );
		}

		template< class RCT > static void ReadCell( RCT& rct_, DynamicBuffer& db_, int Row, int Column )
		{
			switch( db_.getColumnType( Column ) )
			{
				case ct_uint8:
					{
						uint8_t tmp;
						TypeMarshaler< Owner, uint8_t >::read( rct_, tmp );
						db_.put( Row, Column, tmp );
					}
					break;
				case ct_int8:
					{
						int8_t tmp;
						TypeMarshaler< Owner, int8_t >::read( rct_, tmp );
						db_.put( Row, Column, tmp );
					}
					break;
					
				case ct_uint16:
					{
						uint16_t tmp;
						TypeMarshaler< Owner, uint16_t >::read( rct_, tmp );
						db_.put( Row, Column, tmp );
					}
					break;
				case ct_int16:
					{
						int16_t tmp;
						TypeMarshaler< Owner, int16_t >::read( rct_, tmp );
						db_.put( Row, Column, tmp );
					}
					break;

				case ct_uint32:
					{
						uint32_t tmp;
						TypeMarshaler< Owner, uint32_t >::read( rct_, tmp );
						db_.put( Row, Column, tmp );
					}
					break;
				case ct_int32:
					{
						int32_t tmp;
						TypeMarshaler< Owner, int32_t >::read( rct_, tmp );
						db_.put( Row, Column, tmp );
					}
					break;

				case ct_uint64:
					{
						uint64_t tmp;
						TypeMarshaler< Owner, uint64_t >::read( rct_, tmp );
						db_.put( Row, Column, tmp );
					}
					break;
				case ct_int64:
					{
						int64_t tmp;
						TypeMarshaler< Owner, int64_t >::read( rct_, tmp );
						db_.put( Row, Column, tmp );
					}
					break;

				case ct_float:
					{
						float tmp;
						TypeMarshaler< Owner, float >::read( rct_, tmp );
						db_.put( Row, Column, tmp );
					}
					break;
				case ct_double:
					{
						double tmp;
						TypeMarshaler< Owner, double >::read( rct_, tmp );
						db_.put( Row, Column, tmp );
					}
					break;
			}
		}

		template< class RCT > static void read( RCT& rct_, DynamicBuffer& db_ )
		{
			int numcolumns = rct_.Stream.readU8();
			
			int column;
			for( column = 0; column < numcolumns; ++column )
				db_.addColumn( static_cast< e_ColumnType >( rct_.Stream.readU8() ) );
			
			int numrows = rct_.Stream.readVarLenInt();
			db_.resize( numrows );
			
			for( int row = 0; row < numrows; ++row )
				for( column = 0; column < numcolumns; ++column )
					ReadCell( rct_, db_, row, column );
		}
	};
}}}

#endif
