/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBF_CLASS_H
#define FAIL_FBF_CLASS_H

#include <iostream>

namespace fail { namespace fbf { namespace Impl
{
	template< bool DoWant, class C > struct RegisterIf
	{
		static void RegisterClass()
		{
			Registry< endianess::Big >::RegisterClass< C >();
			Registry< endianess::Little >::RegisterClass< C >();
		}
	};
	
	template< class C > struct RegisterIf< false, C >
	{
		static void RegisterClass()
		{
		}
	};
	
	template< class C > struct ClassStuff
	{	
		static void Setup()
		{
			RegisterIf< flags::Storable< C >::value && !flags::Abstract< C >::value, C >::RegisterClass();
	/*		if( flags::storable< C >::value )
			{
				Registry< endianess::Big >::RegisterClass< C >();
				Registry< endianess::Little >::RegisterClass< C >();
			}*/
		}
	};
}}}

#endif
