/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBF_IMPLEMENTATION_H
#define FAIL_FBF_IMPLEMENTATION_H

#include "interface.h"

#include "schema-digest/digest.h"
#include "write-context-fwd.h"
#include "read-context-fwd.h"
#include "basic_types.h"
#include "stl_types.h"
#include "dynamicbuffer.h"
#include "struct.h"
#include "registry.h"
#include "pointer.h"

#include "writer-digests.h"
#include "writer-objects.h"
#include "writer-save.h"

#include "reader-digests.h"
#include "reader-objects.h"

#include "write-context.h"
#include "read-context.h"

#include "class.h"

namespace fail { namespace fbf
{
	namespace Impl
	{
		struct SetupVisitor
		{
			SetupVisitor()
			{
			}
			
			template< class C > void Class()
			{
				ClassStuff< C >::Setup();
			}
			
			template< class S > void Struct()
			{
		/*		std::cout << "struct " << class_traits< S >::Name() << std::endl;
				StructInfo< S > si;
				si.setup();*/
			}
		};
	}

	template< typename ModuleTag > void Setup()
	{	
		//std::cout << "module " << module_traits< ModuleTag >::FullName() <<
		//		" schema's digest: 0x" << std::hex << SchemaDigest< ModuleTag >::digest << std::endl;

		fbf::Impl::Digests.insert( SchemaDigest< ModuleTag >::digest );
		
		Impl::SetupVisitor v;
		module_traits< ModuleTag >::VisitClasses( v );
	}
}}

#endif
