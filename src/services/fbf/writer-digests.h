/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBF_WRITER_DGST_H
#define FAIL_FBF_WRITER_DGST_H

#include <set>

namespace fail { namespace fbf
{
	namespace Impl
	{
		// TODO: this is supposed to scan all of the objects that we are going to save
		// and make sure that the digests for all the concerned modules are included.
		template< class C > static void AddObjDigest(
			shared_ptr< C > pObj,
			set< uint64_t >& Digests,
			set< const void* >& Objects )
		{
			if( !Objects.insert( static_cast< const void* >( pObj.get() ) ).second )
				return;
			
			uint64_t digest = SchemaDigest< typename class_traits< C >::module_tag >::digest;		
			Digests.insert( digest );
			
			// TODO: recurse
		}
	}
		
	template< class EndianessPolicy > template< class C > void BaseWriter< EndianessPolicy >::
		WriteDigests( shared_ptr< WriterBackend_i > pBackend, shared_ptr< C > pObj )
	{
		set< const void* > Objects;
		set< uint64_t >	Digests;

		Impl::AddObjDigest( pObj, Digests, Objects );
		
		shared_ptr< io::OutputStream > pOutStream = pBackend->beginDigests( Digests.size() * sizeof( uint64_t ) );
		
		{
			buffered_stream_type stream( pOutStream );
					
			set< uint64_t >::const_iterator it;
			for( it = Digests.begin(); it != Digests.end(); ++it )
				stream.writeU64( *it );
		}
		
		pBackend->endDigests();
	}
}}

#endif
