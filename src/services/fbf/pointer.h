/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBF_POINTER_H
#define FAIL_FBF_POINTER_H

#include <type_traits>

namespace fail { namespace fbf { namespace Impl
{
	template< class Owner, class C,
		bool bPersistable = std::is_base_of< Persistable, Owner >::value && std::is_base_of< Persistable, C >::value,
		bool bPolymorphic = flags::Polymorphic< C >::value >
		struct PtrTargetIO
	{
		template< class WCT > static void Write( shared_ptr< const Owner > pOwner, WCT& wct, shared_ptr< C > pObj )
		{
			static const int32_t NewObject = -1;
			wct.Stream.writeVarLenInt( NewObject );
			wct.writeObject( pObj );
		}
		
		template< class RCT > static shared_ptr< C > Read( RCT& rct )
		{
			return rct.template readObject< C >();
		}
	};
	
	template< class Owner, class C > struct PtrTargetIO< Owner, C, false, true >
	{
		template< class WCT > static void Write( shared_ptr< const Owner > pOwner, WCT& wct, shared_ptr< C > pObj )
		{
			typedef typename WCT::endianess_policy EP;
			shared_ptr< typename Impl::Registry< EP >::ClassGenericWriter_i > pWriter =
				Impl::Registry< EP >::GetGenericWriter( typeid( *pObj ) );

			static const int32_t NewObject = -1;
			static const int32_t NullPointer = -2;
			
			// pWriter may be null in a situation where a pointer to a storable polymorphic pointer
			// points to a class that isn't storable. In such a case, we save a null pointer.
			if( pWriter )
			{
				wct.Stream.writeVarLenInt( NewObject );
				pWriter->write( wct, static_pointer_cast< Serializable >( pObj ) );
			}
			else
				wct.Stream.writeVarLenInt( NullPointer );
		}
		
		template< class RCT > static shared_ptr< C > Read( RCT& rct )
		{
			return dynamic_pointer_cast< C >( rct.readObject() );
		}
	};
	
	template< class Owner, class C, bool bPolymorphic > struct PtrTargetIO< Owner, C, true, bPolymorphic >
	{
		template< class WCT > static void Write( shared_ptr< const Owner > pOwner, WCT& wct, shared_ptr< C > pObj )
		{
			// If pObj is part of a bundle, and pOwner don't belong to the same bundle
			// or to any objdb bundle, we write it as an external reference with its uuid.
			// Otherwise we save the object normally.
			// Subsequent references to this object will be treated like internal
			// references, though. This way when loading we'll have to resolve that
			// uuid only once even if the object is referenced multiple times by the
			// bundle we're saving.
			if( pObj->isPersistent() && ( !pOwner->isPersistent() || pOwner->m_pBundle != pObj->m_pBundle ) )
			{
				static const int32_t UUIDReference = -3;
				wct.Stream.writeVarLenInt( UUIDReference );
				wct.Stream.write( &pObj->m_pBundle->getUUID(), sizeof( uuid ) );
				wct.addObject(	pObj );
			}
			else
			{			
				typedef typename WCT::endianess_policy EP;
				shared_ptr< typename Impl::Registry< EP >::ClassGenericWriter_i > pWriter =
					Impl::Registry< EP >::GetGenericWriter( typeid( *pObj ) );
	
				static const int32_t NewObject = -1;
				static const int32_t NullPointer = -2;
				
				// pWriter may be null in a situation where a pointer to a storable polymorphic pointer
				// points to a class that isn't storable. In such a case, we save a null pointer.
				if( pWriter )
				{
					wct.Stream.writeVarLenInt( NewObject );
					pWriter->write( wct, static_pointer_cast< Serializable >( pObj ) );
				}
				else
					wct.Stream.writeVarLenInt( NullPointer );
			}
		}
		
		template< class RCT > static shared_ptr< C > Read( RCT& rct )
		{
			return dynamic_pointer_cast< C >( rct.readObject() );
		}
	};
	
	template< class Owner, class C > struct TypeMarshaler< Owner, shared_ptr< C >, tc_Default >
	{
		static const int32_t NewObject = -1;
		static const int32_t NullPointer = -2;
		static const int32_t UUIDReference = -3;
		
		template< class WCT > static void write( shared_ptr< const Owner > pOwner, WCT& wct, shared_ptr< C > pObj )
		{
			if( !flags::Storable< C >::value )
				return;

			if( !pObj )
			{
				wct.Stream.writeVarLenInt( NullPointer );
				return;
			}

			int32_t ObjectID = wct.getObjectID( pObj );
			
			if( ObjectID == NewObject )
				PtrTargetIO< Owner, C >::Write( pOwner, wct, pObj );
			else
				wct.Stream.writeVarLenInt( ObjectID );
		}

		template< class RCT > static void read( RCT& rct, shared_ptr< C >& pObj )
		{
			if( !flags::Storable< C >::value )
				return;
				
			int32_t ObjectID = rct.Stream.readVarLenInt();
			
			switch( ObjectID )
			{
				case NullPointer:
					pObj.reset();
					break;
					
				case NewObject:
					pObj = PtrTargetIO< Owner, C >::Read( rct );
					break;
					
				case UUIDReference:
					{
						uuid UUID;
						rct.Stream.read( &UUID, sizeof( UUID ) );
						pObj = dynamic_pointer_cast< C >( rct.resolveUUID( UUID ) );
						rct.addObject( pObj );
					}
					break;
					
				default:
					pObj = static_pointer_cast< C >( rct.getObject( ObjectID ) );
					break;
			}
		}
	};
}}}

#endif
