/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBF_READER_DGST_H
#define FAIL_FBF_READER_DGST_H

namespace fail { namespace fbf
{	
	template< class EndianessPolicy > bool BaseReader< EndianessPolicy >::
		ReadDigests( shared_ptr< ReaderBackend_i > pBackend )
	{
		shared_ptr< io::InputStream > pInStream = pBackend->beginDigests();
		buffered_stream_type stream( pInStream );

		uint32_t size = pBackend->getDigestsSize();
		bool bDigestCheckOk = true;

		while( size )
		{
			uint64_t digest = stream.readU64();
			bDigestCheckOk = bDigestCheckOk && Impl::Digests.find( digest ) != Impl::Digests.end();
			size -= sizeof( uint64_t );
		}

		pBackend->endDigests();
		return bDigestCheckOk;
	}
}}

#endif
