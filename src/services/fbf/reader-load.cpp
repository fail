/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "services/fbf/interface.h"
#include "services/fbf/implementation.h"

using namespace fail;
using namespace fail::fbf;

shared_ptr< Serializable > Reader::Load( shared_ptr< ReaderBackend_i > pBackend )	
{
	shared_ptr< Serializable > pObj;
	
	endianess::Endianess en = pBackend->beginBundle();

	if( en == endianess::e_Big )
	{	
		typedef endianess::Big EP;
		
		// TODO: if the digest check fails, use the slow loading path
		// using the metadatas
		BaseReader< EP >::ReadDigests( pBackend );
		pObj = BaseReader< EP >::ReadObjects( pBackend );
	}
	else
	{
		typedef endianess::Little EP;
		
		// TODO: if the digest check fails, use the slow loading path
		// using the metadatas
		BaseReader< EP >::ReadDigests( pBackend );
		pObj = BaseReader< EP >::ReadObjects( pBackend );
	}
	
	pBackend->endBundle();
	return pObj;
}
