/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBF_BASIC_TYPES_H
#define FAIL_FBF_BASIC_TYPES_H

namespace fail { namespace fbf { namespace Impl
{
	template< class Owner, typename T, e_TypeCategory cat = TypeCategory< T >::value > struct TypeMarshaler
	{
	};

	template< class Owner > struct TypeMarshaler< Owner, bool, tc_Default >
	{
		template< class WCT > static void write( shared_ptr< const Owner > pOwner, WCT& wct_, bool val_ )
		{
			wct_.Stream.writeU8( val_ );
		}

		template< class RCT > static void read( RCT& rct_, bool& val_ )
		{
			val_ = rct_.Stream.readU8();
		}
	};
	
	template< class Owner > struct TypeMarshaler< Owner, uint8_t, tc_Default  >
	{
		template< class WCT > static void write( shared_ptr< const Owner > pOwner, WCT& wct_, uint8_t val_ )
		{
			wct_.Stream.writeU8( val_ );
		}

		template< class RCT > static void read( RCT& rct_, uint8_t& val_ )
		{
			val_ = rct_.Stream.readU8();
		}
	};
	template< class Owner > struct TypeMarshaler< Owner, int8_t, tc_Default  >
	{
		template< class WCT > static void write( shared_ptr< const Owner > pOwner, WCT& wct_, int8_t val_ )
		{
			wct_.Stream.writeU8( val_ );
		}

		template< class RCT > static void read( RCT& rct_, int8_t& val_ )
		{
			val_ = rct_.Stream.readU8();
		}
	};
	
	template< class Owner > struct TypeMarshaler< Owner, uint16_t, tc_Default  >
	{
		template< class WCT > static void write( shared_ptr< const Owner > pOwner, WCT& wct_, uint16_t val_ )
		{
			wct_.Stream.writeU16( val_ );
		}

		template< class RCT > static void read( RCT& rct_, uint16_t& val_ )
		{
			val_ = rct_.Stream.readU16();
		}
	};
	template< class Owner > struct TypeMarshaler< Owner, int16_t, tc_Default  >
	{
		template< class WCT > static void write( shared_ptr< const Owner > pOwner, WCT& wct_, int16_t val_ )
		{
			wct_.Stream.writeU16( val_ );
		}

		template< class RCT > static void read( RCT& rct_, int16_t& val_ )
		{
			val_ = rct_.Stream.readU16();
		}
	};
	
	template< class Owner > struct TypeMarshaler< Owner, uint32_t, tc_Default  >
	{
		template< class WCT > static void write( shared_ptr< const Owner > pOwner, WCT& wct_, uint32_t val_ )
		{
			wct_.Stream.writeU32( val_ );
		}

		template< class RCT > static void read( RCT& rct_, uint32_t& val_ )
		{
			val_ = rct_.Stream.readU32();
		}
	};
	template< class Owner > struct TypeMarshaler< Owner, int32_t, tc_Default  >
	{
		template< class WCT > static void write( shared_ptr< const Owner > pOwner, WCT& wct_, int32_t val_ )
		{
			wct_.Stream.writeU32( val_ );
		}

		template< class RCT > static void read( RCT& rct_, int32_t& val_ )
		{
			val_ = rct_.Stream.readU32();
		}
	};
	
	template< class Owner > struct TypeMarshaler< Owner, uint64_t, tc_Default  >
	{
		template< class WCT > static void write( shared_ptr< const Owner > pOwner, WCT& wct_, uint64_t val_ )
		{
			wct_.Stream.writeU64( val_ );
		}

		template< class RCT > static void read( RCT& rct_, uint64_t& val_ )
		{
			val_ = rct_.Stream.readU64();
		}
	};
	template< class Owner > struct TypeMarshaler< Owner, int64_t, tc_Default  >
	{
		template< class WCT > static void write( shared_ptr< const Owner > pOwner, WCT& wct_, int64_t val_ )
		{
			wct_.Stream.writeU64( val_ );
		}

		template< class RCT > static void read( RCT& rct_, int64_t& val_ )
		{
			val_ = rct_.Stream.readU64();
		}
	};
	
	template< class Owner > struct TypeMarshaler< Owner, float, tc_Default  >
	{
		template< class WCT > static void write( shared_ptr< const Owner > pOwner, WCT& wct_, float val_ )
		{
			wct_.Stream.writeFloat( val_ );
		}

		template< class RCT > static void read( RCT& rct_, float& val_ )
		{
			val_ = rct_.Stream.readFloat();
		}
	};
	template< class Owner > struct TypeMarshaler< Owner, double, tc_Default  >
	{
		template< class WCT > static void write( shared_ptr< const Owner > pOwner, WCT& wct_, double val_ )
		{
			wct_.Stream.writeDouble( val_ );
		}

		template< class RCT > static void read( RCT& rct_, double& val_ )
		{
			val_ = rct_.Stream.readDouble();
		}
	};
	
	template< class Owner > struct TypeMarshaler< Owner, string, tc_Default  >
	{
		template< class WCT > static void write( shared_ptr< const Owner > pOwner, WCT& wct_, const string& val_ )
		{
			wct_.Stream.writeString( val_.c_str() );
		}

		template< class RCT > static void read( RCT& rct_, string& val_ )
		{
			rct_.Stream.readString( val_ );
		}
	};

	template< class Owner, typename enum_type > struct TypeMarshaler< Owner, enum_type, tc_Enum >
	{
		template< class WCT > static void write( shared_ptr< const Owner > pOwner, WCT& wct_, enum_type val_ )
		{
			wct_.Stream.writeVarLenInt( val_ );
		}

		template< class RCT > static void read( RCT& rct_, enum_type& val_ )
		{
			val_ = static_cast< enum_type >( rct_.Stream.readVarLenInt() );
		}
	};
	
	// TODO: all other types
}}}

#endif
