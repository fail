/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBF_WRITE_CONTEXT_H
#define FAIL_FBF_WRITE_CONTEXT_H

#include <map>

namespace fail { namespace fbf { namespace Impl
{
	template< class C, bool bStorable = flags::Storable< C >::value > struct VisitSuperClassIfStorable
	{
		template< class V > static void Visit( V& v )
		{
			class_traits< C >::template VisitSuperClasses( v );
			class_traits< C >::template VisitAttributes( v );
		}
	};
	
	template< class C > struct VisitSuperClassIfStorable< C, false >
	{
		template< class V > static void Visit( V& v )
		{

		}
	};
	
	template< typename AttrTag, bool bStorable = flags::Storable< AttrTag >::value > struct WriteAttrIfStorable
	{
		template< class C, class WCT > static void Write( shared_ptr< C > pObj, WCT& wct )
		{
			typedef attribute_traits< C, AttrTag > traits;
			typedef TypeMarshaler< C, typename traits::type > tm;
			//std::cout << "stream at " << wct.Stream.totalWritten() << " before "
			//	<< class_traits< typename remove_cv< C >::type >::FullName() << "::" << traits::Name() << "\n";
			tm::write( pObj, wct, pObj.get()->*( traits::MemberPointer() ) );
			//std::cout << "stream at " << wct.Stream.totalWritten() << " after "
			//	<< class_traits< typename remove_cv< C >::type >::FullName() << "::" << traits::Name() << "\n";
		}
	};
		
	template< typename AttrTag > struct WriteAttrIfStorable< AttrTag, false >
	{
		template< class C, class WCT > static void Write( shared_ptr< C > pObj, WCT& wct ) {}
	};
	
	template< class WCT, class C > struct WriteObjVisitor
	{
		WriteObjVisitor( WCT& wct_, shared_ptr< const C > pObj_ ) :
				wct( wct_ ),
				pObj( pObj_ )
		{
		}

		template< class SC > void declSuperClass()
		{
			VisitSuperClassIfStorable< SC >::Visit( *this );
		}
		
		template< typename AttrTag > void declAttribute()
		{
			WriteAttrIfStorable< AttrTag >::Write( pObj, wct );
		}

		WCT&					wct;
		shared_ptr< const C >	pObj;
	};
	
	template< class stream_type > template< class C > void WriteContext< stream_type >::
		writeObject( shared_ptr< C > pObj )
	{
		if( !flags::Storable< C >::value )
			return;
		
		addObject( static_pointer_cast< Serializable >( pObj ) );

		WriteObjVisitor< WriteContext< stream_type >, C > wov( *this, pObj );
		class_traits< C >::template VisitSuperClasses( wov );
		class_traits< C >::template VisitAttributes( wov );
	}
}}}

#endif
