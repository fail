/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBF_WRITER_OBJECTS_H
#define FAIL_FBF_WRITER_OBJECTS_H

#include <map>

namespace fail { namespace fbf
{
	template< class EndianessPolicy > template< class C > void BaseWriter< EndianessPolicy >::
		WriteObjects( shared_ptr< WriterBackend_i > pBackend, shared_ptr< C > pObj )
	{
		shared_ptr< io::OutputStream > pOutStream = pBackend->beginObjects();
		
		{
			buffered_stream_type stream( pOutStream );
			
			Impl::WriteContext< buffered_stream_type > wct( stream );
			wct.Stream.writeU32( class_traits< C >::Digest() );
			wct.writeObject( pObj );
		}
		
		pBackend->endObjects();
	}
}}

#endif
