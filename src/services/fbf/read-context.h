/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBF_READ_CONTEXT_H
#define FAIL_FBF_READ_CONTEXT_H

#include <vector>

namespace fail { namespace fbf { namespace Impl
{
	template< typename AttrTag, bool bStorable = flags::Storable< AttrTag >::value >
	struct ReadAttr
	{
		template< class RCT, class C > void operator()( RCT& rct, shared_ptr< C > pObj )
		{
		}
	};
	
	template< typename AttrTag >
	struct ReadAttr< AttrTag, true >
	{
		template< class RCT, class C > void operator()( RCT& rct, shared_ptr< C > pObj )
		{
			typedef attribute_traits< C, AttrTag > traits;
			typedef TypeMarshaler< C, typename traits::type > tm;
			//std::cout << "stream at " << rct.Stream.totalRead() << " before "
			//	<< class_traits< typename remove_cv< C >::type >::FullName() << "::" << traits::Name() << "\n";
			tm::read( rct, pObj.get()->*( traits::MemberPointer() ) );
			//std::cout << "stream at " << rct.Stream.totalRead() << " after "
			//	<< class_traits< typename remove_cv< C >::type >::FullName() << "::" << traits::Name() << "\n";
		}
	};
	
	template< class RCT, class C > struct ReadObjVisitor
	{
		ReadObjVisitor( RCT& rct_, shared_ptr< C > pObj_ ) :
				rct( rct_ ),
				pObj( pObj_ )
		{
		}

		template< class SC > void declSuperClass()
		{
			class_traits< SC >::template VisitSuperClasses( *this );
			class_traits< SC >::template VisitAttributes( *this );
		}
		
		template< typename AttrTag > void declAttribute()
		{
			ReadAttr< AttrTag > ra;
			ra( rct, pObj );
		}

		RCT&				rct;
		shared_ptr< C >		pObj;
	};

	template< class stream_type > shared_ptr< Serializable > ReadContext< stream_type >::
		readObject()
	{
		uint32_t ClassID = Stream.readU32();
		
		typedef typename stream_type::endianess_policy EP;
 		shared_ptr< typename Impl::Registry< EP >::ClassGenericReader_i > pReader =
		Impl::Registry< EP >::GetGenericReader( ClassID );
		
		// TODO: throw if the reader was not found
		if( !pReader )
		{
			std::cout << "epic fail\n";
			return shared_ptr< Serializable >();
		}

		return pReader->read( *this );
	}
			
	template< class stream_type > template< class C > shared_ptr< C > ReadContext< stream_type >::
		readObject()
	{
		if( !flags::Storable< C >::value )
			return shared_ptr< C >();
		Serialization_tag st;
		shared_ptr< C > pObj( new C( st ) );
		addObject( pObj );
		
		ReadObjVisitor< ReadContext< stream_type >, C > rov( *this, pObj );
		class_traits< C >::template VisitSuperClasses( rov );
		class_traits< C >::template VisitAttributes( rov ); 

		return pObj;
	}	
}}}

#endif
