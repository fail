/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBF_READ_CONTEXT_FWD_H
#define FAIL_FBF_READ_CONTEXT_FWD_H

#include <vector>

namespace fail { namespace fbf { namespace Impl
{
		template< class stream_type > class ReadContext
		{		
			public:
				ReadContext( stream_type& Stream_, shared_ptr< ReaderBackend_i > pBackend_ ) :
					Stream( Stream_ ),
					m_pBackend( pBackend_ )
				{
				}

				shared_ptr< Serializable > readObject();

				template< class C > shared_ptr< C > readObject();
				
				shared_ptr< Serializable > getObject( int32_t Index )
				{
					return m_Objects[Index];
				}
				
				void addObject( shared_ptr< Serializable > pObj )
				{
					m_Objects.push_back( pObj );
				}
				
				void postLoad() const
				{
					vector< shared_ptr< Serializable > >::const_iterator it;
					for( it = m_Objects.begin(); it != m_Objects.end(); ++it )
					{
						if( *it )
							( *it )->postLoad();
					}
				}
				
				shared_ptr< Serializable > resolveUUID( const uuid& UUID )
				{
					return m_pBackend->resolveUUID( UUID );
				}
				
				stream_type& Stream;
				
			private:
				vector< shared_ptr< Serializable > >	m_Objects;
				shared_ptr< ReaderBackend_i >			m_pBackend;
		};
}}}

#endif
