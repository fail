/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_FBF_WRITER_H
#define FAIL_FBF_WRITER_H

#include "core/core.h"
#include "io/BufferedOutputStream.h"

namespace fail { namespace fbf
{
	template< class EndianessPolicy > class FLSERVICES_FBF_EXPORT BaseWriter
	{
		public:
			typedef io::OutputStream stream_type;
			typedef io::BufferedOutputStream< EndianessPolicy > buffered_stream_type;
			
			template< class C > static void Save( shared_ptr< WriterBackend_i > pBackend, shared_ptr< C > pObj );
			static void SaveSerializable( shared_ptr< WriterBackend_i > pBackend, shared_ptr< Serializable > pObj );
			
		private:
			template< class C > static void WriteDigests( shared_ptr< WriterBackend_i > pBackend, shared_ptr< C > pObj );
			template< class C > static void WriteObjects( shared_ptr< WriterBackend_i > pBackend, shared_ptr< C > pObj );
			
			// TODO: implement the meta-data stuff (to be done during a later iteration)
			//static template< class C > void WriteMETA( stream_type& Stream_ );
	};

//	template<> template< class C > void BaseWriter< endianess::Big >::Save( shared_ptr< WriterBackend_i > pBackend, shared_ptr< C > pObj );
	//template<> template<> void BaseWriter< endianess::Little >::Save( shared_ptr< WriterBackend_i > pBackend, shared_ptr< Serializable > pObj );
	
	// This is a separate class and not a typedef, because as a typedef it would end up being the same
	// actual type as either WriterBigEndian or WriterLittleEndian. Since all of those are also
	// exposed in the IDL, we'd end up with two specializations of fail::class_traits for the
	// same type, which wouldn't quite work.
	class FLSERVICES_FBF_EXPORT Writer
	{
		typedef BaseWriter< endianess::Native > nativewriter;
		typedef io::SeekableOutputStream stream_type;
		
		public:
			template< class C > static void Save( shared_ptr< WriterBackend_i > pBackend, shared_ptr< C > pObj )
			{
				nativewriter::Save( pBackend, pObj );
			}
			
			static void SaveSerializable( shared_ptr< WriterBackend_i > pBackend, shared_ptr< Serializable > pObj )
			{
				nativewriter::SaveSerializable( pBackend, pObj );
			}
	};
}}

#endif
