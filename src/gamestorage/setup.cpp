/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "core/module.h"
#include "gamestorage/gamestorage.h"
#include "private.h"

#include "services/fbf/implementation.h"
//#include "services/objdb/implementation.h"

#include "services/fbffile/VFSLoader.h"

using namespace fail::game;

void fail::game::SetupStorage()
{
	fail::fbf::Setup< fail::fail_tag >();
 	//fail::objdb::Setup< fail::fail_tag >();
	
	SetupScenegraphStorage();
	SetupScenegraphShapesStorage();
	SetupMathStorage();
	//SetupCollisionStorage();

	fbffile::VFSLoader::Register();
}
