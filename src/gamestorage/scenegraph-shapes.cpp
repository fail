/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "private.h"

#include "core/module.h"
#include "math/module.h"
#include "scenegraph/module.h"
#include "scenegraph/shapes/module.h"

#include "services/fbf/implementation.h"
//#include "services/objdb/implementation.h"

void fail::game::SetupScenegraphShapesStorage()
{
	fail::fbf::Setup< fail::scenegraph::shapes::fail_tag >();
	//fail::objdb::Setup< fail::scenegraph::shapes::fail_tag >();
}
