--[[
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

module( "fail.uiutils", package.seeall )

require 'qtgui'
require 'qtuitools'
require 'fail.utils'

XMLWidget = fail.utils.class
{
	function( self, parent, filename )
		local loader = QUiLoader()
		local file = QFile( QString( filename ) )
		self.widget = loader:load( file )
	end
}
