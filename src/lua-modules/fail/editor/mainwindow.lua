--[[
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]
module( "fail.editor.mainwindow", package.seeall )

require 'qtcore'
require 'qtgui'

-- This module only a reference to the mainwindow object as a global.
-- It merely instanciates the qt main window.
-- It also provides a "menu registry" to allow independent plugins
-- to add stuff into the same menu, based on the menu's name.
-- The menu order is defined by the plugins loading order for now.

mainwindow = QMainWindow()
mainwindow:resize(1024, 768)

menus = {}

function GetMenu( name )
	local menu = menus[name]
	
	if( menu ) then
		return menu
	end

	menu = mainwindow:menuBar():addMenu( QString( name ) )
	return menu
end
