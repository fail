--[[
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]
module( "fail.editor.settings", package.seeall )

require 'qtcore'
require 'qtgui'
mw = require 'fail.editor.mainwindow'
require 'fail.uiutils'

SettingsDialog = fail.utils.class
{
	superclass = fail.uiutils.XMLWidget,
	
	function( self )
		fail.uiutils.XMLWidget.init( self, mw.mainwindow, "fail-lua:editor/settings.ui" )
		self.widget:show()
	end	
}

local toolsmenu = mw.GetMenu( "Tools" )
action = QAction( QString( "Settings..." ), mw.mainwindow )
toolsmenu:addAction( action )

mw.mainwindow:__addmethod( "settings", 'settings()',
	function( self )
		local settingsdialog = SettingsDialog() --fail.uiutils.XMLWidget( mw.mainwindow, "fail-lua:editor/settings.ui" )
	--	self.widget:show()
	end )
QObject.connect( action, '2triggered()', mw.mainwindow, '1settings()' )
