--[[
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]
module( "fail.editor.about", package.seeall )

require 'qtcore'
require 'qtgui'
mw = require 'fail.editor.mainwindow'

local helpmenu = mw.GetMenu( "Help" )
action = QAction( QString( "About" ), mw.mainwindow )
helpmenu:addAction( action )

mw.mainwindow:__addmethod( "about", 'about()',
	function( self )
		QMessageBox.information(
			mw.mainwindow,
			QString( "About facepalm" ),
			QString( "Facepalm - The fail engine's level editor" ),
			"Ok", "NoButton" )
	end )
QObject.connect( action, '2triggered()', mw.mainwindow, '1about()' )
