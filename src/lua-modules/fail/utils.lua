--[[
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]
module( "fail.utils", package.seeall )

function typeof( object )
	return object._flType
end
		
function class( classtable )

	-- Extract the information we need to setup the class from the
	-- template. 
	local superclass = classtable.superclass
	local ctor = classtable[1]

	classtable.superclass = nil
	classtable[1] = nil
	classtable._flSuperClasses = { superclass }

	-- Instance meta table
	local instancemeta = { _flNonNative = true, _flType = classtable }
		
	-- We can't just point the table on itself here,
	-- because then when we reach the native class up in
	-- the hierarchy, we no longer have the instance table
	-- that contains a field pointing to the C++ wrapper, but
	-- one of the classdef table.
	-- So we try to rawget up the hierarchy until we hit a native
	-- table, and then we just get the key from the wrapped C++
	-- object in _flNativeInstance.
	function instancemeta.__index( table, key )

		local v = rawget( table, key )
		if v ~= nil then return v end
		
		local t = getmetatable( table )
		while( rawget( t, "_flNonNative" ) ) do
			local v = rawget( t, key )
			if v ~= nil then return v end
			t = getmetatable( t )
		end

		if( table._flNativeInstance ) then
			return table._flNativeInstance[key]
		else
			return nil
		end
	end
	
	-- set up superclass' (if any) instance meta table to be the meta table
	-- of instancemeta.
	-- Also copy the __newindex meta method, so we can write to attributes
	-- inherited from native classes.
	if( superclass ) then
		instancemeta.__newindex = rawget( superclass._flMetatable, "__newindex" )
		setmetatable( instancemeta, superclass._flMetatable )
	end

	--	
 	-- The class table will be returned and stored globally as
	-- modulename.classname. It's built form the table passed as parameter,
	-- with some changes. Most of the things in it will be left untouched,
	-- which allows to define non-member function that are global for the class
	-- by setting them into the table before calling this function.
	--
	-- It contains:
	-- * a field called _flMetatable, which is the metatable used
	--   of instances of this class
	-- * a function called init, which allows subclasses to call the constructor.
	-- * a metatable that define a __call function that allows the
	--   creation of a new instance of this class by writing
	--   instance = someclass( params )
	--   Its metatable also defines a __newindex function that
	--   allow to add things to the instance metatable, so member functions
	--   can then be defined with "function MyClass:func() bla bla end".
	--
	-- Native classes also define all these things and thus can
	-- be subclassed (for those that allows it) and instanciated
	-- the same way.
	-- This allows to write drop in C++ replacements of existing lua classes
	-- if it turns out they need to be rewritten for performance reasons,
	-- provided the lua class(es) to be replaced inherit directly from a native
	-- class, as a lua class can inherit from a C++ class but not the other way around.
	classtable._flMetatable = instancemeta
	
	classtable.init = ctor
	
	-- Metatable for the class table.
	local classmeta = {}
			
	function classmeta.__newindex( table, key, value )
		rawset( instancemeta, key, value)
	end
			
	-- The wrapper ctor that will be called by the user to instantiate the class.
	-- It is not returned directly as a function, because we want the global class
	-- definition to be a table so we can get things like the correpsonding
	-- instance metatable from it for inheritance purpose, and the init function.
	-- Note that native classes also expose their instance metatables the same way.
 	function classmeta.__call( func, ... )
		local obj = {}

		setmetatable( obj, instancemeta )
		ctor( obj, ... )
		return obj
	end
					
	setmetatable( classtable, classmeta )		
	return classtable

end
