--[[
    Fail game engine
    Copyright 2007-2008 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

module( "fail.game.mouse", package.seeall )

require 'fail.game.input'
require 'qtgui'

Provider = fail.utils.class
{
	superclass = fail.game.input.EventProvider,
	
	function( self, widget )
		fail.game.input.EventProvider.init( self, "mouse" )

		-- Only provide two axes corresponding to mouse movement deltas for now.
		-- TODO: buttons, wheel (perhaps both as an axis and as an up and a down key), coordinates maybe

		widget.mouseMoveEvent = function( o, mme ) self:mouseMoveEvent( mme ) end
		widget.mousePressEvent = function( o, me ) self:mousePressEvent( me ) end
		widget.mouseReleaseEvent = function( o, me ) self:mouseReleaseEvent( me ) end
		
		self.prevx = 0
		self.prevy = 0
		self.bfirst = true
--		widget.keyPressEvent = function( o, ke ) self:keyEvent( ke, true ) end
	--	widget.keyReleaseEvent = function( o, ke ) self:keyEvent( ke, false ) end
	end,
}

function Provider:mousePressEvent( me )
	local eventsource = fail.game.input.EventSource( self, me:button() )
	local event = fail.game.input.KeyEvent( eventsource, true )
	fail.game.input.InputManager.GetInstance():feedKeyEvent( event )
end

function Provider:mouseReleaseEvent( me )
	local eventsource = fail.game.input.EventSource( self, me:button() )
	local event = fail.game.input.KeyEvent( eventsource, false )
	fail.game.input.InputManager.GetInstance():feedKeyEvent( event )
end

function Provider:mouseMoveEvent( mme )
	if( not self.bfirst ) then

		local dx = mme:globalX() - self.prevx
		local dy = mme:globalY() - self.prevy

		if( dx ) then
			local eventsource = fail.game.input.EventSource( self, "MouseX" )
			local event = fail.game.input.AxisEvent( eventsource, dx )
			fail.game.input.InputManager.GetInstance():feedAxisEvent( event )
		end
		
		if( dy ) then
			local eventsource = fail.game.input.EventSource( self, "MouseY" )
			local event = fail.game.input.AxisEvent( eventsource, dy )
			fail.game.input.InputManager.GetInstance():feedAxisEvent( event )
		end
	end

	self.prevx = mme:globalX()
	self.prevy = mme:globalY()
	self.bfirst = false
end

