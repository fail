--[[
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

Axis = fail.utils.class
{
	 function( self, Name, Description )
		self.Name = Name
		self.Description = Description

		self.CurrentValue = 0
		self.Handler = false
		InputManager.GetInstance():addAxis( self )
	end,
}

-- TODO: create a superclass for action/axes/etc. to hold those common attributes?
function Axis:getName()
	return self.Name
end

function Axis:getDescription()
	return self.Description
end

function Axis:bindHandler( Handler )
	self.Handler = Handler
end

function Axis:feed( Event )
	self.CurrentValue = Event:getValue()

	if( self.Handler ) then
		self.Handler( self.CurrentValue )
	end
end
