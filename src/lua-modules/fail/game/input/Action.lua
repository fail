--[[
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

Action = fail.utils.class
{
 	function( self, Name, Description )
		self.Name = Name
		self.Description = Description
		self.CurrentState = false
		self.Handler = false
		InputManager.GetInstance():addAction( self )
	end,
}

function Action:getName()
	return self.Name
end

function Action:getDescription()
	return self.Description
end

function Action:bindHandler( Handler )
	self.Handler = Handler
end

function Action:feed( Event )

	if( Event:isPressed() ) then
		self.CurrentState = true
	else
		self.CurrentState = false
	end
		
	if( self.Handler ) then
		self.Handler( self.CurrentState )
	end
end
