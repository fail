--[[
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

--[[
	This modules provides a system to map game actions to inputs.
	An Action is a possible action in the game, defined by it (example: move forward, strafe left, etc.)
	An EventProvider is a module providing input events (example: keyboard event provider, mouse event provider)
	And EventSource is a specific source of event provided by an event provider (example: z key, left mouse button)
	
	Binding an action is the action of linking together a game action with an eventsource.
	This require three pieces of informations: the name of the action to bind (ie, move forward), the name
	of the even provider (ie, keyboard), and the name of the event source, which is only meaningful in the context
	of the specified event provider (ie "shift + a")
	
	The InputManager is a singleton that provides a central point for all input related stuff. It will itself maintain
	a list of bindings in Settings.Bindings, ready to be serialized as part of the settings.
	
	We handle only all-or-nothing events with this at the moment (ie keyboard keys, joystick buttons, mouse buttons.)
	Analog input will be implemented when it is needed (ie later) (ie i'm lazy and cba with it right now)
--]]

module( "fail.game.input", package.seeall )
require 'fail.utils'

dofile '${CMAKE_INSTALL_PREFIX}/${LUA_MODULES_LDIR}/fail/game/input/InputManager.lua'
dofile '${CMAKE_INSTALL_PREFIX}/${LUA_MODULES_LDIR}/fail/game/input/EventSource.lua'
dofile '${CMAKE_INSTALL_PREFIX}/${LUA_MODULES_LDIR}/fail/game/input/EventProvider.lua'
dofile '${CMAKE_INSTALL_PREFIX}/${LUA_MODULES_LDIR}/fail/game/input/Event.lua'
dofile '${CMAKE_INSTALL_PREFIX}/${LUA_MODULES_LDIR}/fail/game/input/KeyEvent.lua'
dofile '${CMAKE_INSTALL_PREFIX}/${LUA_MODULES_LDIR}/fail/game/input/AxisEvent.lua'
dofile '${CMAKE_INSTALL_PREFIX}/${LUA_MODULES_LDIR}/fail/game/input/Axis.lua'
dofile '${CMAKE_INSTALL_PREFIX}/${LUA_MODULES_LDIR}/fail/game/input/Action.lua'
