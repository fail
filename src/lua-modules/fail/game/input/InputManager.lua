--[[
    Fail game engine
    Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

--[[
	This modules provides a system to map game actions to inputs.
	An Action is a possible action in the game, defined by it (example: move forward, strafe left, etc.)
	An EventProvider is a module providing input events (example: keyboard event provider, mouse event provider)
	And EventSource is a specific source of event provided by an event provider (example: z key, left mouse button)
	
	Binding an action is the action of linking together a game action with an eventsource.
	This require three pieces of informations: the name of the action to bind (ie, move forward), the name
	of the even provider (ie, keyboard), and the name of the event source, which is only meaningful in the context
	of the specified event provider (ie "shift + a")
	
	The InputManager is a singleton that provides a central point for all input related stuff. It will itself maintain
	a list of bindings in Settings.Bindings, ready to be serialized as part of the settings.
	
	We handle only all-or-nothing events with this at the moment (ie keyboard keys, joystick buttons, mouse buttons.)
	Analog input will be implemented when it is needed (ie later) (ie i'm lazy and cba with it right now)
--]]

InputManager = fail.utils.class
{
	function( self )		
		if( not Settings.InputManager ) then
			Settings.InputManager =
			{
				Actions = {},
				Axes = {}
			}
		end
		
		self.KeyEventCaptureHandler = false
		self.AxisEventCaptureHandler = false
		self.Actions = {}
		self.Axes = {}
	end,
	
	GetInstance = function()
		if( not Instance ) then
			Instance = InputManager()
		end
		
		return Instance
	end,
	
	Instance = false
}

function InputManager.GetActionTable( eventprovidername )
	local table = Settings.InputManager.Actions[eventprovidername]
	
	if( not table ) then
		table = {}
		Settings.InputManager.Actions[eventprovidername] = table
	end
	
	return table
end

function InputManager.GetAxisTable( eventprovidername )
	local table = Settings.InputManager.Axes[eventprovidername]
	
	if( not table ) then
		table = {}
		Settings.InputManager.Axes[eventprovidername] = table
	end
	
	return table
end

function InputManager:addAction( action )
	self.Actions[action:getName()] = action
end

function InputManager:addAxis( axis )
	self.Axes[axis:getName()] = axis
end

function InputManager:bindAction( actionname, eventprovidername, eventsourcename )
	local ActionTable = self.GetActionTable( eventprovidername )
	ActionTable[eventsourcename] = actionname;
end

function InputManager:bindAxis( axisname, eventprovidername, eventsourcename )
	local AxisTable = self.GetAxisTable( eventprovidername )
	AxisTable[eventsourcename] = axisname;
end


function InputManager:setKeyCaptureHandler( handler )
	self.KeyEventCaptureHandler = handler
end

function InputManager:setAxisCaptureHandler( handler )
	self.AxisEventCaptureHandler = handler
end


function InputManager:feedKeyEvent( event )
	local ActionTable = Settings.InputManager.Actions[event:getSource():getProvider():getName()]

	if( not ActionTable ) then
		return
	end
	
	
	local sourcename = event:getSource():getName();
	local actioname = ActionTable[sourcename]
	
	if( not actioname ) then
		return
	end
	
	local action = self.Actions[actioname]
	
	if( not action ) then
		return
	end
	
	if( self.KeyEventCaptureHandler ) then
		self.KeyEventCaptureHandler( event )
	else
		action:feed( event )
	end
end

function InputManager:feedAxisEvent( event )
	local AxisTable = Settings.InputManager.Axes[event:getSource():getProvider():getName()]

	if( not AxisTable ) then
		return
	end
	
	local sourcename = event:getSource():getName();
	local axisname = AxisTable[sourcename]
		
	if( not axisname ) then
		return
	end

	local axis = self.Axes[axisname]
	if( not axis ) then
		return
	end

	if( self.AxisEventCaptureHandler ) then
		self.AxisEventCaptureHandler( event )
	else
		axis:feed( event )
	end
end
