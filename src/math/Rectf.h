/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_MATH_RECTF_H_
#define FAIL_MATH_RECTF_H_

#include "Vector2f.h"
#include "core/core.h"

namespace fail { namespace math
{
	struct Rectf
	{
		Rectf() {}
		
		Rectf( const Vector2f& position_, const Vector2f& size_ ) :
			m_position( position_ ),
			m_size( size_ )
		{
		}
		
		Rectf( float left_, float top_, float width_, float height_ ) :
			m_position( left_, top_ ),
			m_size( width_, height_ )
		{
		}
		
		Vector2f& position() { return m_position; }
		Vector2f& size() { return m_size; }
		
		float& left() { return m_position.x(); }
		float& top() { return m_position.y(); }
		float& width() { return m_size.x(); }
		float& height() { return m_size.y(); }

		const Vector2f& position() const { return m_position; }
		const Vector2f& size() const { return m_size; }
		
		const float& left() const { return m_position.x(); }
		const float& top() const { return m_position.y(); }
		const float& width() const { return m_size.x(); }
		const float& height() const { return m_size.y(); }

		private:
			Vector2f m_position;
			Vector2f m_size;
	};
}}

#endif
