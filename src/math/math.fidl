/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace fail { namespace math
{
	struct Vector2f
	{
		Vector2f();
		Vector2f( float x, float y );
		float x;
		float y;
	};
	
	struct Vector2u32
	{
		Vector2u32();
		Vector2u32( uint32_t x, uint32_t y );
		uint32_t x;
		uint32_t y;
	};
	
	struct Vector3f
	{
		Vector3f();
		Vector3f( float x, float y, float z );
		Vector3f( Vector3f v );

		float length() const;
		void normalize();
		
		float x;
		float y;
		float z;
	};

	struct Vector4f
	{
		Vector4f();
		Vector4f( float x, float y, float z, float w );
		
		float x;
		float y;
		float z;
		float w;
	};

	struct Vector4u8
	{
		Vector4u8();
		Vector4u8( uint8_t r, uint8_t g, uint8_t b, uint8_t a );
		Vector4u8( uint8_t r, uint8_t g, uint8_t b );
		uint8_t r;
		uint8_t g;
		uint8_t b;
		uint8_t a;
	};

	struct Matrix44f
	{
		Matrix44f();
		Matrix44f( Vector4f v1, Vector4f v2, Vector4f v3, Vector4f v4 );
		
		void identity();
		void rotation( float Angle, Vector3f Dir );
		
		void multiplyBy( Matrix44f rhs );
		
		Vector4f v1;
		Vector4f v2;
		Vector4f v3;
		Vector4f v4;
		
		// Convenient aliases.
		//
		// Those names reflect the coordinate system
		// conventions used by the engine.
		// It's a right-hand coordinate system with
		// x pointing to the right, y pointing forward and z pointing up.
		//
		// This differ from the opengl eye space where x points right, y up and z backwards.
		// This difference is taken care of by RenderContext at the time where it converts
		// the camera matrix into an opengl matrix.
		//
		// This also differ from the blender conventions in which x points left, y backwards
		// and z up. This will be taken care of by the exporter by rotating vertices coordinates
		// 180 degrees around the z axis.
		[ !Storable ]
		Vector4f x_axis;
		Vector4f right;
		
		Vector4f y_axis;
		Vector4f forward;
		
		Vector4f z_axis;
		Vector4f up;

		Vector4f position;
	};

	struct Rectf
	{
		Rectf();
		Rectf( Vector2f position_, Vector2f size_ );
		Rectf( float left, float top, float width, float height );
		
		Vector2f position;
		Vector2f size;
		
		// Aliases for direct fine grained access to the above.
		[ !Storable ]
		float left;
		float top;
		float width;
		float height;
	};

	struct Rectu32
	{
		Rectu32();
		Rectu32( Vector2u32 position_, Vector2u32 size_ );
		Rectu32( uint32_t left, uint32_t top, uint32_t width, uint32_t height );
		
		Vector2u32 position;
		Vector2u32 size;
		
		// Aliases for direct fine grained access to the above.
		[ !Storable ]
		uint32_t left;
		uint32_t top;
		uint32_t width;
		uint32_t height;
	};
}}
