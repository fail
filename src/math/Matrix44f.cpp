/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "math/failmath.h"

using namespace fail;
using namespace fail::math;

#define M11	v1().x()
#define M21	v1().y()
#define M31	v1().z()
#define M41 v1().w()

#define M12	v2().x()
#define M22	v2().y()
#define M32	v2().z()
#define M42 v2().w()

#define M13	v3().x()
#define M23	v3().y()
#define M33	v3().z()
#define M43 v3().w()

#define M14	v4().x()
#define M24	v4().y()
#define M34	v4().z()
#define M44 v4().w()

Matrix44f fail::math::operator*( const Matrix44f& a, const Matrix44f& b )
{
	Matrix44f Res;
	
	Res.v1().x() =	a.v1().x() * b.v1().x()
				+	a.v2().x() * b.v1().y()
				+	a.v3().x() * b.v1().z()
				+	a.v4().x() * b.v1().w();
	
	Res.v1().y() =	a.v1().y() * b.v1().x()
				+	a.v2().y() * b.v1().y()
				+	a.v3().y() * b.v1().z()
				+	a.v4().y() * b.v1().w();

	Res.v1().z() =	a.v1().z() * b.v1().x()
				+	a.v2().z() * b.v1().y()
				+	a.v3().z() * b.v1().z()
				+	a.v4().z() * b.v1().w();
				
	Res.v1().w() =	a.v1().w() * b.v1().x()
				+	a.v2().w() * b.v1().y()
				+	a.v3().w() * b.v1().z()
				+	a.v4().w() * b.v1().w();
	

	
	Res.v2().x() =	a.v1().x() * b.v2().x()
				+	a.v2().x() * b.v2().y()
				+	a.v3().x() * b.v2().z()
				+	a.v4().x() * b.v2().w();
	
	Res.v2().y() =	a.v1().y() * b.v2().x()
				+	a.v2().y() * b.v2().y()
				+	a.v3().y() * b.v2().z()
				+	a.v4().y() * b.v2().w();

	Res.v2().z() =	a.v1().z() * b.v2().x()
				+	a.v2().z() * b.v2().y()
				+	a.v3().z() * b.v2().z()
				+	a.v4().z() * b.v2().w();
				
	Res.v2().w() =	a.v1().w() * b.v2().x()
				+	a.v2().w() * b.v2().y()
				+	a.v3().w() * b.v2().z()
				+	a.v4().w() * b.v2().w();
			

	
	Res.v3().x() =	a.v1().x() * b.v3().x()
				+	a.v2().x() * b.v3().y()
				+	a.v3().x() * b.v3().z()
				+	a.v4().x() * b.v3().w();
	
	Res.v3().y() =	a.v1().y() * b.v3().x()
				+	a.v2().y() * b.v3().y()
				+	a.v3().y() * b.v3().z()
				+	a.v4().y() * b.v3().w();

	Res.v3().z() =	a.v1().z() * b.v3().x()
				+	a.v2().z() * b.v3().y()
				+	a.v3().z() * b.v3().z()
				+	a.v4().z() * b.v3().w();
				
	Res.v3().w() =	a.v1().w() * b.v3().x()
				+	a.v2().w() * b.v3().y()
				+	a.v3().w() * b.v3().z()
				+	a.v4().w() * b.v3().w();
	
	
	
	Res.v4().x() =	a.v1().x() * b.v4().x()
				+	a.v2().x() * b.v4().y()
				+	a.v3().x() * b.v4().z()
				+	a.v4().x() * b.v4().w();
	
	Res.v4().y() =	a.v1().y() * b.v4().x()
				+	a.v2().y() * b.v4().y()
				+	a.v3().y() * b.v4().z()
				+	a.v4().y() * b.v4().w();

	Res.v4().z() =	a.v1().z() * b.v4().x()
				+	a.v2().z() * b.v4().y()
				+	a.v3().z() * b.v4().z()
				+	a.v4().z() * b.v4().w();
				
	Res.v4().w() =	a.v1().w() * b.v4().x()
				+	a.v2().w() * b.v4().y()
				+	a.v3().w() * b.v4().z()
				+	a.v4().w() * b.v4().w();

	return Res;
}

bool Matrix44f::inverse( Matrix44f& Dest_ ) const
{
	float Det;

	float D12, D13, D23, D24, D34, D41;
	D12 = ( M31 * M42 - M41 * M32 );
	D13 = ( M31 * M43 - M41 * M33 );
	D23 = ( M32 * M43 - M42 * M33 );
	D24 = ( M32 * M44 - M42 * M34 );
	D34 = ( M33 * M44 - M43 * M34 );
	D41 = ( M34 * M41 - M44 * M31 );

	Dest_.M11 =  ( M22 * D34 - M23 * D24 + M24 * D23 );
	Dest_.M21 = -( M21 * D34 + M23 * D41 + M24 * D13 );
	Dest_.M31 =  ( M21 * D24 + M22 * D41 + M24 * D12 );
	Dest_.M41 = -( M21 * D23 - M22 * D13 + M23 * D12 );

	Det = M11 * Dest_.M11 + M12 * Dest_.M21 + M13 * Dest_.M31;
		+ M14 * Dest_.M41;

	if( Det == 0.f )
		return false;

	Dest_.M12 = -( M12 * D34 - M13 * D24 + M14 * D23 );
	Dest_.M22 =  ( M11 * D34 + M13 * D41 + M14 * D13 );
	Dest_.M32 = -( M11 * D24 + M12 * D41 + M14 * D12 );
	Dest_.M42 =  ( M11 * D23 - M12 * D13 + M13 * D12 );

	D12 = M11 * M22 - M21 * M12;
	D13 = M11 * M23 - M21 * M13;
	D23 = M12 * M23 - M22 * M13;
	D24 = M12 * M24 - M22 * M14;
	D34 = M13 * M24 - M23 * M14;
	D41 = M14 * M21 - M24 * M11;

	Dest_.M13 =  ( M42 * D34 - M43 * D24 + M44 * D23 );
	Dest_.M23 = -( M41 * D34 + M43 * D41 + M44 * D13 );
	Dest_.M33 =  ( M41 * D24 + M42 * D41 + M44 * D12 );
	Dest_.M43 = -( M41 * D23 - M42 * D13 + M43 * D12 );

	Dest_.M14 = -( M32 * D34 - M33 * D24 + M34 * D23 );
	Dest_.M24 =  ( M31 * D34 + M33 * D41 + M34 * D13 );
	Dest_.M34 = -( M31 * D24 + M32 * D41 + M34 * D12 );
	Dest_.M44 =  ( M31 * D23 - M32 * D13 + M33 * D12 );

	Det = 1.f / Det;

	Dest_.v1() *= Det;
	Dest_.v2() *= Det;
	Dest_.v3() *= Det;
	Dest_.v4() *= Det;
	
	return true;
}

void Matrix44f::rotation( float Angle_, const Vector3f& Dir_ )
{
	Vector3f Dir = Dir_;

	float xx, yy, zz, xy, yz, zx, xs, ys, zs, one_c, s, c;

	Dir.normalize();

	s = sin( Angle_ * pi / 180.0f );
	c = cos( Angle_ * pi / 180.0f );

	xx = Dir.x() * Dir.x();
	yy = Dir.y() * Dir.y();
	zz = Dir.z() * Dir.z();
	xy = Dir.x() * Dir.y();
	yz = Dir.y() * Dir.z();
	zx = Dir.z() * Dir.x();
	xs = Dir.x() * s;
	ys = Dir.y() * s;
	zs = Dir.z() * s;
	one_c = 1.0f - c;

	identity();

	/* We already hold the identity-matrix so we can skip some statements */
	M11 = ( one_c * xx ) + c;
	M12 = ( one_c * xy ) - zs;
	M13 = ( one_c * zx ) + ys;

	M21 = ( one_c * xy ) + zs;
	M22 = ( one_c * yy ) + c;
	M23 = ( one_c * yz ) - xs;

    M31 = ( one_c * zx ) - ys;
	M32 = ( one_c * yz ) + xs;
	M33 = ( one_c * zz ) + c;
}
