/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_MATH_VECTOR4U8_H_
#define FAIL_MATH_VECTOR4U8_H_

#include "core/core.h"

namespace fail { namespace math
{
	struct Vector4u8
	{
		Vector4u8() :
			m_r( 0 ),
			m_g( 0 ),
			m_b( 0 ),
			m_a( 255 )
		{
		}
		
		Vector4u8( uint8_t r, uint8_t g, uint8_t b, uint8_t a = 255 ) :
			m_r( r ),
			m_g( g ),
			m_b( b ),
			m_a( a )
		{
		}
		
		uint8_t& r() { return m_r; }
		uint8_t& g() { return m_g; }
		uint8_t& b() { return m_b; }
		uint8_t& a() { return m_a; }
		const uint8_t& r() const { return m_r; }
		const uint8_t& g() const { return m_g; }
		const uint8_t& b() const { return m_b; }
		const uint8_t& a() const { return m_a; }
		
		Vector4u8 operator+( const Vector4u8& b ) const
		{
			return Vector4u8( m_r + b.m_r,
							  m_g + b.m_g,
							  m_b + b.m_b,
							  m_a + b.m_a );
		}
		
		Vector4u8 operator-( const Vector4u8& b ) const
		{
			return Vector4u8( m_r - b.m_r,
							  m_g - b.m_g,
							  m_b - b.m_b,
							  m_a - b.m_a );
		}
		
		const Vector4u8& operator+=( const Vector4u8& b )
		{
			m_r += b.m_r;
			m_g += b.m_g;
			m_b += b.m_b;
			m_a += b.m_a;
			return *this;
		}

		const Vector4u8& operator-=( const Vector4u8& b )
		{
			m_r -= b.m_r;
			m_g -= b.m_g;
			m_b -= b.m_b;
			m_a -= b.m_a;
			return *this;
		}
		
		bool operator==( const Vector4u8& b ) const
		{
			return m_r == b.m_r &&
				   m_g == b.m_g &&
				   m_b == b.m_b &&
				   m_a == b.m_a;
		}
		
		bool operator!=( const Vector4u8& b ) const
		{
			return m_r != b.m_r ||
				   m_g != b.m_g ||
				   m_b != b.m_b ||
				   m_a != b.m_a;
		}
		
		bool operator<( const Vector4u8& b_ ) const
		{
			uint32_t a = ( m_r << 24 ) | ( m_g << 16 ) | ( m_b << 8 ) | m_a;
			uint32_t b = ( b_.m_r << 24 ) | ( b_.m_g << 16 ) | ( b_.m_b << 8 ) | b_.m_a;
			return a < b;
		}

		private:
			uint8_t m_r;
			uint8_t m_g;
			uint8_t m_b;
			uint8_t m_a;
	};
}}

#endif
