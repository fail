/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_MATH_VECTOR2F_H_
#define FAIL_MATH_VECTOR2F_H_

#include "core/core.h"

namespace fail { namespace math
{
	struct Vector2f
	{
		Vector2f() :
			m_x( 0.f ),
			m_y( 0.f )
		{
		}
		
		Vector2f( float x_, float y_ ) :
			m_x( x_ ),
			m_y( y_ )
		{
		}
		
		float& x() { return m_x; }
		float& y() { return m_y; }
		const float& x() const { return m_x; }
		const float& y() const { return m_y; }
		
		Vector2f operator+( const Vector2f& b ) const
		{
			return Vector2f( m_x + b.m_x, m_y + b.m_y );
		}
		
		Vector2f operator-( const Vector2f& b ) const
		{
			return Vector2f( m_x - b.m_x, m_y - b.m_y );
		}
		
		const Vector2f& operator+=( const Vector2f& b )
		{
			m_x += b.m_x;
			m_y += b.m_y;
			return *this;
		}

		const Vector2f& operator-=( const Vector2f& b )
		{
			m_x -= b.m_x;
			m_y -= b.m_y;
			return *this;
		}
		
		Vector2f operator-() const
		{
			return Vector2f( -m_x, -m_y );
		}
		
		bool operator==( const Vector2f& b ) const
		{
			return m_x == b.m_x && m_y == b.m_y;
		}
				
		private:
			float m_x;
			float m_y;
	};
}}

#endif
