/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_MATH_VECTOR3F_H_
#define FAIL_MATH_VECTOR3F_H_

#include "core/core.h"
#include <cmath>

namespace fail { namespace math
{
	struct Vector3f
	{
		Vector3f() :
			m_x( 0.f ),
			m_y( 0.f ),
			m_z( 0.f )
		{
		}
		
		Vector3f( float x_, float y_, float z_ ) :
			m_x( x_ ),
			m_y( y_ ),
			m_z( z_ )
		{
		}
		
		float& x() { return m_x; }
		float& y() { return m_y; }
		float& z() { return m_z; }
		const float& x() const { return m_x; }
		const float& y() const { return m_y; }
		const float& z() const { return m_z; }
		
		Vector3f operator+( const Vector3f& b ) const
		{
			return Vector3f( m_x + b.m_x, m_y + b.m_y, m_z + b.m_z );
		}
		
		Vector3f operator-( const Vector3f& b ) const
		{
			return Vector3f( m_x - b.m_x, m_y - b.m_y, m_z + b.m_z );
		}
		
		const Vector3f& operator+=( const Vector3f& b )
		{
			m_x += b.m_x;
			m_y += b.m_y;
			m_z += b.m_z;
			return *this;
		}

		const Vector3f& operator-=( const Vector3f& b )
		{
			m_x -= b.m_x;
			m_y -= b.m_y;
			m_z -= b.m_z;
			return *this;
		}
		
		bool operator==( const Vector3f& b ) const
		{
			return m_x == b.m_x && m_y == b.m_y && m_z == b.m_z;
		}
		
		float length() const
		{
			return std::sqrt( m_x * m_x + m_y * m_y + m_z * m_z );
		}

		void normalize()
		{
			float Len = length();
			m_x /= Len;
			m_y /= Len;
			m_z /= Len;
		}
				
		private:
			float m_x;
			float m_y;
			float m_z;
	};
}}

#endif
