/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_MATH_RECTU32_H_
#define FAIL_MATH_RECTU32_H_

#include "Vector2u32.h"
#include "core/core.h"

namespace fail { namespace math
{
	struct Rectu32
	{
		Rectu32() {}
		
		Rectu32( const Vector2u32& position_, const Vector2u32& size_ ) :
			m_position( position_ ),
			m_size( size_ )
		{
		}
		
		Rectu32( uint32_t left_, uint32_t top_, uint32_t width_, uint32_t height_ ) :
			m_position( left_, top_ ),
			m_size( width_, height_ )
		{
		}
		
		Vector2u32& position() { return m_position; }
		Vector2u32& size() { return m_size; }
		
		uint32_t& left() { return m_position.x(); }
		uint32_t& top() { return m_position.y(); }
		uint32_t& width() { return m_size.x(); }
		uint32_t& height() { return m_size.y(); }

		const Vector2u32& position() const { return m_position; }
		const Vector2u32& size() const { return m_size; }
		
		const uint32_t& left() const { return m_position.x(); }
		const uint32_t& top() const { return m_position.y(); }
		const uint32_t& width() const { return m_size.x(); }
		const uint32_t& height() const { return m_size.y(); }

		private:
			Vector2u32 m_position;
			Vector2u32 m_size;
	};
}}

#endif
