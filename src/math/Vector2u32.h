/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_MATH_VECTOR2U32_H_
#define FAIL_MATH_VECTOR2U32_H_

#include "core/core.h"

namespace fail { namespace math
{
	struct Vector2u32
	{
		Vector2u32() :
			m_x( 0.f ),
			m_y( 0.f )
		{
		}
		
		Vector2u32( uint32_t x_, uint32_t y_ ) :
			m_x( x_ ),
			m_y( y_ )
		{
		}
		
		uint32_t& x() { return m_x; }
		uint32_t& y() { return m_y; }
		const uint32_t& x() const { return m_x; }
		const uint32_t& y() const { return m_y; }
		
		Vector2u32 operator+( const Vector2u32& b ) const
		{
			return Vector2u32( m_x + b.m_x, m_y + b.m_y );
		}
		
		Vector2u32 operator-( const Vector2u32& b ) const
		{
			return Vector2u32( m_x - b.m_x, m_y - b.m_y );
		}
		
		Vector2u32 operator-() const
		{
			return Vector2u32( -m_x, -m_y );
		}
		
		const Vector2u32& operator+=( const Vector2u32& b )
		{
			m_x += b.m_x;
			m_y += b.m_y;
			return *this;
		}

		const Vector2u32& operator-=( const Vector2u32& b )
		{
			m_x -= b.m_x;
			m_y -= b.m_y;
			return *this;
		}
		
		bool operator==( const Vector2u32& b ) const
		{
			return m_x == b.m_x && m_y == b.m_y;
		}
				
		private:
			uint32_t m_x;
			uint32_t m_y;
	};
}}

#endif
