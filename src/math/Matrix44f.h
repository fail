/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_MATH_MATRIX44F_H_
#define FAIL_MATH_MATRIX44F_H_

#include "core/core.h"
#include "math/math_export.h"
#include "Vector4f.h"
#include "Vector3f.h"

namespace fail { namespace math
{
	struct Matrix44f;
	Matrix44f operator*( const Matrix44f& a, const Matrix44f& b ) FLMATH_EXPORT;
	
	struct FLMATH_EXPORT Matrix44f
	{
		Matrix44f() :
			m_v1( 1.f, 0.f, 0.f, 0.f ),
			m_v2( 0.f, 1.f, 0.f, 0.f ),
			m_v3( 0.f, 0.f, 1.f, 0.f ),
			m_v4( 0.f, 0.f, 0.f, 1.f )
		{
		}

		Matrix44f( const Vector4f& v1, const Vector4f& v2, const Vector4f& v3, const Vector4f& v4 ) :
			m_v1( v1 ),
			m_v2( v2 ),
			m_v3( v3 ),
			m_v4( v4 )
		{
		}

		// Accessors
		Vector4f& v1() { return m_v1; }
		Vector4f& v2() { return m_v2; }
		Vector4f& v3() { return m_v3; }
		Vector4f& v4() { return m_v4; }
		const Vector4f& v1() const { return m_v1; }
		const Vector4f& v2() const { return m_v2; }
		const Vector4f& v3() const { return m_v3; }
		const Vector4f& v4() const { return m_v4; }
		
		// Aliases		
		Vector4f& x_axis() { return m_v1; }
		Vector4f& y_axis() { return m_v2; }
		Vector4f& z_axis() { return m_v3; }
		const Vector4f& x_axis() const { return m_v1; }
		const Vector4f& y_axis() const { return m_v2; }
		const Vector4f& z_axis() const { return m_v3; }
		
		Vector4f& right() { return m_v1; }
		Vector4f& forward() { return m_v2; }
		Vector4f& up() { return m_v3; }
		const Vector4f& right() const { return m_v1; }
		const Vector4f& forward() const { return m_v2; }
		const Vector4f& up() const { return m_v3; }
		
		Vector4f& position() { return m_v4; }
		const Vector4f& position() const { return m_v4; }
		
		// Operations
		
		// Identity
		void identity()
		{
			m_v1.x() = 1.f; m_v1.y() = 0.f; m_v1.z() = 0.f; m_v1.w() = 0.f;
			m_v2.x() = 0.f; m_v2.y() = 1.f; m_v2.z() = 0.f; m_v2.w() = 0.f;
			m_v3.x() = 0.f; m_v3.y() = 0.f; m_v3.z() = 1.f; m_v3.w() = 0.f;
			m_v4.x() = 0.f; m_v4.y() = 0.f; m_v4.z() = 0.f; m_v4.w() = 1.f;
		}
		
		
		// TODO: overloaded operators in the lua bindings.
		// In the meantime, I'll expose those operators as functions.
		void multiplyBy( const Matrix44f& rhs )
		{
			*this = *this * rhs;
		}
		
		// Inversion
		bool inverse( Matrix44f& Dest_ ) const;
		
		// Setup the matrix as a rotation of a given angle around a given direction
		void rotation( float Angle_, const Vector3f& Dir_ );
		
		private:
			Vector4f m_v1;
			Vector4f m_v2;
			Vector4f m_v3;
			Vector4f m_v4;
	};
	
	// Operations
	
	// Multiplication
	inline Matrix44f& operator*=( Matrix44f& a, const Matrix44f& b )
	{
		a = a * b;
		return a;
	}		
}}

#endif
