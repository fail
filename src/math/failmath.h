/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_MATH_MATH_H_
#define FAIL_MATH_MATH_H_

#include <cmath>
#include "core/core.h"
#include "Vector2f.h"
#include "Vector3f.h"
#include "Vector4f.h"
#include "Vector2u32.h"
#include "Vector4u8.h"
#include "Matrix44f.h"
#include "Rectf.h"
#include "Rectu32.h"

namespace fail { namespace math
{
	const double pi = 3.14159265358979323846;
}}

#endif
