/*
    Fail game engine
    Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Fail.

    Fail is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Fail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FAIL_MATH_VECTOR4F_H_
#define FAIL_MATH_VECTOR4F_H_

#include "core/core.h"

namespace fail { namespace math
{
	struct Vector4f
	{
		Vector4f() :
			m_x( 0.f ),
			m_y( 0.f ),
			m_z( 0.f ),
			m_w( 0.f )
		{
		}
		
		Vector4f( float x_, float y_, float z_, float w_ ) :
			m_x( x_ ),
			m_y( y_ ),
			m_z( z_ ),
			m_w( w_ )
		{
		}
		
		float& x() { return m_x; }
		float& y() { return m_y; }
		float& z() { return m_z; }
		float& w() { return m_w; }
		const float& x() const { return m_x; }
		const float& y() const { return m_y; }
		const float& z() const { return m_z; }
		const float& w() const { return m_w; }
		
		Vector4f operator+( const Vector4f& b ) const
		{
			return Vector4f( m_x + b.m_x, m_y + b.m_y, m_z + b.m_z, m_w + b.m_w );
		}
		
		Vector4f operator-( const Vector4f& b ) const
		{
			return Vector4f( m_x - b.m_x, m_y - b.m_y, m_z + b.m_z, m_w + b.m_w );
		}
		
		const Vector4f& operator+=( const Vector4f& b )
		{
			m_x += b.m_x;
			m_y += b.m_y;
			m_z += b.m_z;
			m_w += b.m_w;
			return *this;
		}

		const Vector4f& operator-=( const Vector4f& b )
		{
			m_x -= b.m_x;
			m_y -= b.m_y;
			m_z -= b.m_z;
			m_w -= b.m_w;
			return *this;
		}
		
		const Vector4f& operator*=( float f )
		{
			m_x *= f;
			m_y *= f;
			m_z *= f;
			m_w *= f;
			return *this;
		}
		
		bool operator==( const Vector4f& b ) const
		{
			// Placeholder, need to do the comparison right
			return m_x == b.m_x &&
				   m_y == b.m_y &&
				   m_z == b.m_z &&
				   m_w == b.m_w;		}
		
		bool operator!=( const Vector4f& b ) const
		{
			return m_x != b.m_x ||
				   m_y != b.m_y ||
				   m_z != b.m_z ||
				   m_w != b.m_w;
		}
		
		bool operator<( const Vector4f& b_ ) const
		{
			// TODO: You fail it (it is floating point comparisons)
			if( m_x < b_.m_x )
				return true;
			if( m_x != b_.m_x )
				return false;
			
			if( m_y < b_.m_y )
				return true;
			if( m_y != b_.m_y )
				return false;

			if( m_z < b_.m_z )
				return true;
			if( m_z != b_.m_z )
				return false;

			if( m_w < b_.m_w )
				return true;

			return false;
		}
		
		private:
			float m_x;
			float m_y;
			float m_z;
			float m_w;
	};
}}

#endif
