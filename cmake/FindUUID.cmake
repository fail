# Look for libuuid.
# Once done, it will set UUID_FOUND to true, UUID_LIBRARY and UUID_INCLUDE_DIR.

set( UUID_FOUND "NO" )

find_path( UUID_INCLUDE_DIR
	uuid.h

	/usr/include/uuid
	/usr/local/include/uuid
	/opt/local/include/uuid
	/sw/include/uuid
)

find_library( UUID_LIBRARY
	uuid
	
	/usr/lib
	/usr/local/lib
	/opt/local/lib
	/sw/lib
)

if( UUID_INCLUDE_DIR AND UUID_LIBRARY )
	set( UUID_FOUND "YES" )
endif( UUID_INCLUDE_DIR AND UUID_LIBRARY )

if( UUID_FOUND )
	if( NOT UUID_FIND_QUIETLY )
		message( STATUS "Found libuuid: ${UUID_LIBRARY}" )
	endif( NOT UUID_FIND_QUIETLY )
else( UUID_FOUND )
	if( UUID_FIND_REQUIRED )
		message( FATAL_ERROR "Could not find libuuid" )
	endif( UUID_FIND_REQUIRED )
endif( UUID_FOUND )
