#
#   Copyright 2007 Antoine Chavasse <a.chavasse@gmail.com>
# 
#   This file is part of Fail.
#
#   Fail is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License version 3
#   as published by the Free Software Foundation.
#
#   Fail is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# Look for ODE.
# Once done, it will set ODE_FOUND to true, ODE_LIBRARY and ODE_INCLUDE_DIR.

set( ODE_FOUND "NO" )

find_path( ODE_INCLUDE_DIR
	ode/ode.h

	/usr/include
	/usr/local/include
	/opt/local/include
	/sw/include
)

find_library( ODE_LIBRARY

	ode
	
	/usr/lib
	/usr/local/lib
	/opt/local/lib
	/sw/lib
)

if( ODE_INCLUDE_DIR AND ODE_LIBRARY )
	set( ODE_FOUND "YES" )
endif( ODE_INCLUDE_DIR AND ODE_LIBRARY )

if( ODE_FOUND )
	if( NOT ODE_FIND_QUIETLY )
		message( STATUS "Found ODE: ${ODE_LIBRARY}" )
	endif( NOT ODE_FIND_QUIETLY )
else( ODE_FOUND )
	if( ODE_FIND_REQUIRED )
		message( FATAL_ERROR "Could not find ODE" )
	endif( ODE_FIND_REQUIRED )
endif( ODE_FOUND )
