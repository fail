#
#   Fail game engine
#   Copyright 2007-2008 Antoine Chavasse <a.chavasse@gmail.com>
# 
#   This file is part of Fail.
#
#   Fail is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License version 3
#   as published by the Free Software Foundation.
#
#   Fail is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
macro( failAddModuleDirectories modulename )
	include_directories( ${PROJECT_BINARY_DIR}/src/${modulename} )
	include_directories( ${PROJECT_SOURCE_DIR}/src/${modulename} )
endmacro( failAddModuleDirectories )

macro( failBuildLibrary libpath )

	string( REPLACE "/" "-" libname ${libpath} )
	string( REPLACE "/" "_" libname_underscores ${libpath} )
	string( TOUPPER ${libname_underscores} libname_caps )

	configure_file(
		${PROJECT_SOURCE_DIR}/src/core/export.h.in
		${PROJECT_BINARY_DIR}/include/${libpath}/${libname}_export.h
	)

	#include_directories( ${PROJECT_BINARY_DIR}/src/${libpath} )
	#include_directories( ${PROJECT_SOURCE_DIR}/src/${libpath} )
	
	add_library( fail-${libname} SHARED ${ARGN} )
	install( TARGETS fail-${libname} DESTINATION ${LIBDIR} )

endmacro( failBuildLibrary )

macro( failBuildLuaModule libpath mod )
	set( modnamespace ${mod} )
	set( modheaderpath ${libpath} )

	string( REPLACE "/" "-" libname ${libpath} )
	string( REPLACE "::" "-" moddash ${mod} )
	string( REPLACE "::" "_" modunderscore ${mod} )
	string( REPLACE "::" "/" modslash ${mod} )

	string( REGEX MATCH "^(.*/)" modpath ${modslash} )
	string( REGEX MATCH "([^/]+)$" modname ${modslash} )

	#message( STATUS "modpath: ${modpath}, modname: ${modname}" )

	if( ${ARGC} LESS 3 )
		set( luaopenfile ${PROJECT_SOURCE_DIR}/src/services/lua/luaopen.cpp.in )
	else( ${ARGC} LESS 3 )
		set( luaopenfile ${ARGV2} )
	endif( ${ARGC} LESS 3 )

	configure_file(
		${luaopenfile}
		${PROJECT_BINARY_DIR}/lua-modules-binaries/${modslash}/luaopen.cpp
	)

	include_directories(
		${PROJECT_SOURCE_DIR}/libs/lua/src
	)

	add_library( luamodule_${moddash} SHARED ${PROJECT_BINARY_DIR}/lua-modules-binaries/${modslash}/luaopen.cpp )
	set_target_properties( luamodule_${moddash} PROPERTIES PREFIX "" OUTPUT_NAME ${modname} )
	add_dependencies( luamodule_${moddash} failit_${moddash} )

	target_link_libraries( luamodule_${moddash} fail-services-lua fail-${libname} )

	install( TARGETS luamodule_${moddash} DESTINATION ${LUA_MODULES_CDIR}/${modpath} )
endmacro( failBuildLuaModule )
