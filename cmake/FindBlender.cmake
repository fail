#
#   Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
# 
#   This file is part of Fail.
#
#   Fail is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License version 3
#   as published by the Free Software Foundation.
#
#   Fail is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# Look for the directory where Blender scripts are to be installed.
# Once done, it will set BLENDER_FOUND to true, and BULLET_SHARE_DIR.

set( BLENDER_FOUND "NO" )

find_path( BLENDER_SHARE_DIR
	VERSION

	/usr/share/blender
	/usr/local/share/blender
)

if( BLENDER_SHARE_DIR )
	set( BLENDER_FOUND "YES" )
endif( BLENDER_SHARE_DIR )

if( BLENDER_FOUND )
	if( NOT BLENDER_FIND_QUIETLY )
		message( STATUS "Found Blender: ${BLENDER_SHARE_DIR}" )
	endif( NOT BLENDER_FIND_QUIETLY )
else( BLENDER_FOUND )
	if( BLENDER_FIND_REQUIRED )
		message( FATAL_ERROR "Could not find Blender" )
	endif( BLENDER_FIND_REQUIRED )
endif( BLENDER_FOUND )
