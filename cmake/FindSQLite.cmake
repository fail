# Look for SQLite.
# Once done, it will set SQLITE_FOUND to true, SQLITE_LIBRARY and SQLITE_INCLUDE_DIR.

set( SQLITE_FOUND "NO" )

find_path( SQLITE_INCLUDE_DIR
	sqlite3.h

	/usr/include
	/usr/local/include
	/opt/local/include
	/sw/include
)

find_library( SQLITE_LIBRARY
	sqlite3
	
	/usr/lib
	/usr/local/lib
	/opt/local/lib
	/sw/lib
)

# TODO: check that version is >=3.5.0 (for the incremental blob IO features and open_v2)

if( SQLITE_INCLUDE_DIR AND SQLITE_LIBRARY )
	set( SQLITE_FOUND "YES" )
endif( SQLITE_INCLUDE_DIR AND SQLITE_LIBRARY )

if( SQLITE_FOUND )
	if( NOT SQLITE_FIND_QUIETLY )
		message( STATUS "Found SQLite: ${SQLITE_LIBRARY}" )
	endif( NOT SQLITE_FIND_QUIETLY )
else( SQLITE_FOUND )
	if( SQLITE_FIND_REQUIRED )
		message( FATAL_ERROR "Could not find SQLite" )
	endif( SQLITE_FIND_REQUIRED )
endif( SQLITE_FOUND )
