#
#   Copyright 2007-2009 Antoine Chavasse <a.chavasse@gmail.com>
# 
#   This file is part of Fail.
#
#   Fail is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License version 3
#   as published by the Free Software Foundation.
#
#   Fail is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# Look for Bullet.
# Once done, it will set BULLET_FOUND to true, BULLET_INCLUDE_DIR, and
# BULLET_COLLISION_LIBRARY, BULLET_DYNAMICS_LIBRARY, BULLET_SOFTBODY_LIBRARY
# and BULLET_LINEARMATH_LIBRARY.

set( BULLET_FOUND "NO" )

find_path( BULLET_INCLUDE_DIR
	btBulletCollisionCommon.h

	/usr/include
	/usr/local/include
	/opt/local/include
	/sw/include
)

find_library( BULLET_COLLISION_LIBRARY

	BulletCollision
	
	/usr/lib
	/usr/local/lib
	/opt/local/lib
	/sw/lib
)

find_library( BULLET_DYNAMICS_LIBRARY

	BulletDynamics
	
	/usr/lib
	/usr/local/lib
	/opt/local/lib
	/sw/lib
)

find_library( BULLET_SOFTBODY_LIBRARY

	BulletSoftBody
	
	/usr/lib
	/usr/local/lib
	/opt/local/lib
	/sw/lib
)

find_library( BULLET_LINEARMATH_LIBRARY

	LinearMath
	
	/usr/lib
	/usr/local/lib
	/opt/local/lib
	/sw/lib
)


if( BULLET_INCLUDE_DIR
	AND BULLET_COLLISION_LIBRARY
	AND BULLET_DYNAMICS_LIBRARY
	AND BULLET_SOFTBODY_LIBRARY
	AND BULLET_LINEARMATH_LIBRARY )
	set( BULLET_FOUND "YES" )
endif( BULLET_INCLUDE_DIR
	AND BULLET_COLLISION_LIBRARY
	AND BULLET_DYNAMICS_LIBRARY
	AND BULLET_SOFTBODY_LIBRARY
	AND BULLET_LINEARMATH_LIBRARY )

if( BULLET_FOUND )
	if( NOT BULLET_FIND_QUIETLY )
		message( STATUS "Found Bullet: ${BULLET_COLLISION_LIBRARY} ${BULLET_DYNAMICS_LIBRARY} ${BULLET_SOFTBODY_LIBRARY} ${BULLET_LINEARMATH_LIBRARY}" )
	endif( NOT BULLET_FIND_QUIETLY )
else( BULLET_FOUND )
	if( BULLET_FIND_REQUIRED )
		message( FATAL_ERROR "Could not find Bullet" )
	endif( BULLET_FIND_REQUIRED )
endif( BULLET_FOUND )
